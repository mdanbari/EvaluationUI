(function() {
	'use strict';

	angular
		.module('evaluationApp')
		.controller('HomeController', HomeController);

	HomeController.$inject = [ '$scope', 'Principal', 'LoginService', '$state', '$locale' ];

	function HomeController($scope, Principal, LoginService, $state, $locale) {
		var vm = this;

		vm.account = null;
		vm.isAuthenticated = null;
		vm.login = LoginService.open;
		vm.register = register;
		$scope.myInterval = 3000;
		$scope.noWrapSlides = true;
		$scope.active = 0;
		var currIndex = 0;
		var slides = $scope.slides = [];
			slides.push({
				image : "content/images/pic1.jpg",
				text :  'home.slider.generalEvaluator',
				id : currIndex++
			});
		slides.push({
			image : "content/images/pic2.jpg",
			text :  'home.slider.specialEvaluator',
			id : currIndex++
		});
		slides.push({
			image : "content/images/pic3.jpg",
			text :  'home.slider.expertEvaluator',
			id : currIndex++
		});
		slides.push({
			image : "content/images/pic4.jpg",
			text :  'home.slider.judge',
			id : currIndex++
		});
		
		$scope.$on('authenticationSuccess', function() {
			getAccount();
		});

		getAccount();

		function getAccount() {
			Principal.identity().then(function(account) {
				vm.account = account;
				vm.isAuthenticated = Principal.isAuthenticated;
			});
		}
		function register() {
			$state.go('register');
		}

		$scope.lang = $locale.id;
	}
})();