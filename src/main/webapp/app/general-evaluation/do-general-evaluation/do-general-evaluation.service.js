(function() {
    'use strict';
    angular
        .module('evaluationApp')
        .factory('DoGeneralEvaluation', DoGeneralEvaluation);

    DoGeneralEvaluation.$inject = ['$resource'];

    function DoGeneralEvaluation ($resource) {
        var resourceUrl =  'api/evaluations/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
