(function() {
	'use strict';

	angular.module('evaluationApp').controller('DoGeneralEvaluationController',
			DoGeneralEvaluationController);

	DoGeneralEvaluationController.$inject = [ '$scope', 'Principal',
			'LoginService', '$state', '$http', 'QuestionAnswer',
			'MaturityStage', 'Evaluation', 'Locale' , 'AlertService' ];

	function DoGeneralEvaluationController($scope, Principal, LoginService,
			$state, $http, QuestionAnswer, MaturityStage, Evaluation, Locale , AlertService) {
		var vm = this;
		$scope.locale = Locale;
		$scope.userEval = JSON.parse(localStorage.getItem("eval"));
		$scope.maturitystages = MaturityStage.query();
		$scope.formData = {};

		$scope.temporarySave = function() {
			$http.post("/api/temporarySaveEvaluation" , $scope.userEval)
			.then(onSaveSuccess, onSaveError);
		}

		$scope.answering = function() {
			if ($scope.checkEvaluationValidToFinish()) {
				
				vm.isSaving = true;
				

								$http.post("/api/temporarySaveEvaluation", $scope.userEval)
						.then(
								function() {
									$http.get(
											"/api/answeringEvaluation/"
													+ $scope.userEval.id).then(
											onSaveSuccess, onSaveError);
								}, onSaveError);
				
			} else {
				//window.moveTo(0,0);
				scroll(0,0);
				if ($scope.locale.lang == 'fa'){
					AlertService.error($scope.errorMessageFa);
				}
				else
					AlertService.error($scope.errorMessageEn);
			}
		}

		function onSaveSuccess(result) {
			$scope.$emit('evaluationApp:evaluationUpdate', result);
			$state.go('home');
			scroll(0,0);
			$scope.successMessageEN="The Evaluation Save successfully";
			$scope.successMessageFa="ارزیابی با موفقیت دخیره گشت";
			if($scope.locale.lang=='fa')
				{
					AlertService.success($scope.successMessageFa);
				}
			else
				AlertService.success($scope.successMessageEN);
		}

		function onSaveError() {
			vm.isSaving = false;
		}

		$scope.checkEvaluationValidToFinish = function() {
			var questionAnswers = $scope.userEval.questionAnswers;

			$scope.errorMessageFa = ' لطفا به سوال (های) ';
			$scope.errorMessageEn = 'Please answer question(s): ';
			
			var isValidToFinish = true;
			
			for (var i = 0; i < questionAnswers.length; i++) {
				if (!questionAnswers[i].answer
						|| questionAnswers[i].answer == null){
					isValidToFinish = false;
					$scope.errorMessageFa += (i+1) + " ,"
					$scope.errorMessageEn += (i+1) + " ,"
				}
			}
			
			$scope.errorMessageFa += " پاسخ دهید"
			
			return isValidToFinish;
		}
	}
})();
