(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('do-general-evaluation', {
            parent: 'general',
            url: '/dogeneral',
            data: {
                authorities: ['GENERAL_EVALUATOR']
            },
            views: {
                'content@': {
                    templateUrl: 'app/general-evaluation/do-general-evaluation/do-general-evaluation.html',
                    controller: 'DoGeneralEvaluationController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('do-general-evaluation');
                    $translatePartialLoader.addPart('electronicService');
                    $translatePartialLoader.addPart('evaluation');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
