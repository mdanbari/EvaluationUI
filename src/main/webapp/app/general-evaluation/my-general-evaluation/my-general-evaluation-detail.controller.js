(function() {
	'use strict';

	angular.module('evaluationApp').controller(
			'MyGeneralEvaluationDetailController',
			MyGeneralEvaluationDetailController);

	MyGeneralEvaluationDetailController.$inject = [ '$scope', '$state',
			'entity' , 'Locale'];

	function MyGeneralEvaluationDetailController($scope, $state, entity , Locale) {
		var vm = this;
		$scope.locale = Locale;
		$scope.userEval = entity;

	}
})();
