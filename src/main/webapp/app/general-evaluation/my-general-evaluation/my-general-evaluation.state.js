(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('my-general-evaluation', {
            parent: 'general',
            url: '/mygeneralevaluation',
            data: {
                authorities: ['GENERAL_EVALUATOR']
            },
            views: {
                'content@': {
                    templateUrl: 'app/general-evaluation/my-general-evaluation/my-general-evaluation.html',
                    controller: 'MyGeneralEvaluationController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('evaluation');
                    $translatePartialLoader.addPart('evaluationState');
                    $translatePartialLoader.addPart('evaluationState');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('my-general-evaluation-detail', {
            parent: 'my-general-evaluation',
            url: '/{id}/detail',
            data: {
                authorities: ['GENERAL_EVALUATOR']
            },
            views: {
                'content@': {
                	 templateUrl: 'app/general-evaluation/my-general-evaluation/my-general-evaluation-detail.html',
                     controller: 'MyGeneralEvaluationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('do-general-evaluation');
                    $translatePartialLoader.addPart('electronicService');
                    $translatePartialLoader.addPart('evaluation');
                    return $translate.refresh();
                }],
                
                entity: ['$stateParams', 'MyGeneralEvaluation', function($stateParams, MyGeneralEvaluation) {
                    return MyGeneralEvaluation.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'my-general-evaluation',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        });
        ;
    }
})();
