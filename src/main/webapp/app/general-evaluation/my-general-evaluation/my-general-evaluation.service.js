(function() {
    'use strict';
    angular
        .module('evaluationApp')
        .factory('MyGeneralEvaluation', MyGeneralEvaluation);

    MyGeneralEvaluation.$inject = ['$resource'];

    function MyGeneralEvaluation ($resource) {
        var resourceUrl =  'api/myevaluations/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
