(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('general', {
            abstract: true,
            parent: 'app'
        });
    }
})();
