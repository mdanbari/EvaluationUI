(function() {
	'use strict';

	angular.module('evaluationApp').controller('GeneralEvaluationController',
			GeneralEvaluationController);

	GeneralEvaluationController.$inject = [ '$scope', 'Principal',
			'LoginService', '$state', '$http' , 'Locale' ];

	function GeneralEvaluationController($scope, Principal, LoginService,
			$state, $http , Locale) {
		var vm = this;

		$scope.locale = Locale;
		$scope.treeNodes = [];
		$scope.elecServiceDepartment = [];

		$scope.electronic = [];
		$scope.electronicSelected = {};

		$scope.evaluation = {};

		$http.get("../api/getCurrentGeneralEvaluation").then(function(response) {
			if (response.data.id != null) {
				$scope.evaluation = response.data;
				$scope.start();
			}
		}, function(errResponse) {
			console.error('Error while deleting user');
		});

		$scope
				.$on(
						'selection-changed',
						function(e, node) {
							$http
									.get(
											"/api/findCountAllChildrenOfDepartment/"
													+ parseInt(node.id))
									.then(
											function(response) {
												if (response.data > 0) {
													$http
															.get(
																	"/api/findallchildrenofdepartment/"
																			+ parseInt(node.id))
															.then(
																	function(
																			response) {
																		if (response.data.length > 0) {
																			node.children = [];
																			node.children.push
																					.apply(
																							node.children,
																							response.data);
																		}
																	},
																	function(
																			errResponse) {
																		console
																				.error('Error while loading department');
																	});

												}
											},
											function(errResponse) {
												console
														.error('Error while loading department');
											});

							if (node.electronicServiceCount > 0) {
								$http
										.get(
												"/api/findAllElecServiceOfDepartment/"
														+ parseInt(node.id))
										.then(
												function(response) {
													$scope.elecServiceDepartment = response.data;
												},
												function(errResponse) {
													console
															.error('Error while loading department');
												});
							} else {
								$scope.elecServiceDepartment = [];
							}

						});

		$http.get("/api/departmentsparentisnull", {}).then(
				function(response) {
					return $scope.treeNodes.push.apply($scope.treeNodes,
							response.data);
				}, function(errResponse) {
					console.error('Error while loading department');
				});

		$http.get("/api/customGetAllElectronicService", {}).then(
				function(response) {
					return $scope.electronic.push.apply($scope.electronic,
							response.data);
				}, function(errResponse) {
					console.error('Error while loading department');
				});
		$scope.getEvaluation = function() {
			$http
					.get(
							"/api/getEvaluationGeneral/"
									+ $scope.electronicSelected.id, {})
					.then(function(response) {
						$scope.evaluation = response.data;
						$scope.start();
					}, function(errResponse) {
						console.error('Error while loading department');
					});
		}

		$scope.start = function() {
			localStorage.setItem('eval', JSON.stringify($scope.evaluation));
			$state.go('do-general-evaluation');
		}
	}
})();
