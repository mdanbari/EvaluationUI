(function() {
    'use strict';
    angular
        .module('evaluationApp')
        .factory('GeneralEvaluation', GeneralEvaluation);

    GeneralEvaluation.$inject = ['$resource'];

    function GeneralEvaluation ($resource) {
        var resourceUrl =  'api/evaluation/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
