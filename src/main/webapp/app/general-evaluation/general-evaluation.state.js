(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('general-evaluation', {
            parent: 'general',
            url: '/general',
            data: {
                authorities: ['GENERAL_EVALUATOR']
            },
            views: {
                'content@': {
                    templateUrl: 'app/general-evaluation/general-evaluation.html',
                    controller: 'GeneralEvaluationController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('general-evaluation');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
