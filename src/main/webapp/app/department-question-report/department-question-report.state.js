(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('department-question-report', {
            parent: 'app',
            url: '/department-question-report',
            data: {
                authorities: ['ADMIN']
            },
            views: {
                'content@': {
                    templateUrl: 'app/department-question-report/department-question-report.html',
                    controller: 'DepartmentQuestionReportController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('report');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
