(function() {
	'use strict';

	angular.module('evaluationApp').controller('JhiLanguageController',
			JhiLanguageController);

	JhiLanguageController.$inject = [ '$translate', 'JhiLanguageService',
			'tmhDynamicLocale', 'Locale' ];

	function JhiLanguageController($translate, JhiLanguageService,
			tmhDynamicLocale, Locale) {
		var vm = this;

		vm.changeLanguage = changeLanguage;
		vm.languages = null;

		vm.locale = Locale;
		vm.locale.lang = 'fa';

		JhiLanguageService.getAll().then(function(languages) {
			vm.languages = languages;
		});

		function changeLanguage(languageKey) {
			debugger
			vm.locale.lang = languageKey;
			
			var patternFa = '\'/^(13\\d\\d|14\\d\\d)\/(0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01])$/\'';
			var patternEn = '\'/^(19\\d\\d|20\\d\\d)\/(0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01])$/\'';
			
			vm.locale.datePattern = languageKey == 'en' ? patternEn : patternFa;
			$translate.use(languageKey);
			tmhDynamicLocale.set(languageKey);
		}
	}
})();

angular
		.module('evaluationApp')
		.factory(
				'Locale',
				[
						'$locale',
						function($locale) {
							return {
								
								lang : $locale.id ,
								datePattern : '\'/^(13\\d\\d|14\\d\\d)\/(0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01])$/\'',
								toLocaleDate : function(date) {
									if (date != null && date != undefined) {
										var locale = $locale.id
										date = date.substring(0, 10);

										if (locale == 'fa') {
											date = toJalali(date.split("-")[0],
													date.split("-")[1], date
															.split("-")[2])
										}
										return date;
									}
								}
							};
						} ]);