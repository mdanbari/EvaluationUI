(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .constant('paginationConstants', {
            'itemsPerPage': 5
        });
})();
