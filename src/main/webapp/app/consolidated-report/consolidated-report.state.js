(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('consolidated-report', {
            parent: 'app',
            url: '/consolidated-report',
            data: {
                authorities: ['ADMIN']
            },
            views: {
                'content@': {
                    templateUrl: 'app/consolidated-report/consolidated-report.html',
                    controller: 'ConsolidatedReportController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('report');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
