(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('do-expert-evaluation', {
            parent: 'app',
            url: '/DoExpertevaluation',
            data: {
                authorities: ['EXPERT_EVALUATOR']
            },
            views: {
                'content@': {
                    templateUrl: 'app/expert-evaluation/do-expert-evaluation/do-expert-evaluation.html',
                    controller: 'DoExpertEvaluationController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('evaluation');
                    $translatePartialLoader.addPart('expert-evaluation');
                    $translatePartialLoader.addPart('evaluationState');
                    $translatePartialLoader.addPart('evaluationState');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('do-expert-evaluation.detail', {
            parent: 'do-expert-evaluation',
            url: '/{id}/detail',
            data: {
                authorities: ['EXPERT_EVALUATOR','ADMIN']
            },
            views: {
                'content@': {
                	 templateUrl: 'app/expert-evaluation/do-expert-evaluation/do-expert-evaluation-detail.html',
                     controller: 'DoExpertEvaluationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('do-specialized-evaluation');
                    $translatePartialLoader.addPart('electronicService');
                    $translatePartialLoader.addPart('evaluation');
                    return $translate.refresh();
                }],
                
                entity: ['$stateParams', 'DoExpertEvaluation', function($stateParams, DoExpertEvaluation) {
                    return DoExpertEvaluation.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'do-expert-evaluation',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        });
        ;
    }
})();
