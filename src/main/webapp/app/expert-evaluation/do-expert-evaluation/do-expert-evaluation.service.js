(function() {
    'use strict';
    angular
        .module('evaluationApp')
        .factory('DoExpertEvaluation', DoExpertEvaluation);

    DoExpertEvaluation.$inject = ['$resource'];

    function DoExpertEvaluation ($resource) {
        var resourceUrl =  'api/complete-evaluations/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
