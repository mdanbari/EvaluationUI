(function() {
	'use strict';

	angular.module('evaluationApp').controller(
			'MyExpertEvaluationDetailController',
			MyExpertEvaluationDetailController);

	MyExpertEvaluationDetailController.$inject = [ '$scope', '$state', '$http',
			'MyExpertEvaluation', 'entity', 'MaturityStage', 'Locale' , 'Evaluation'];

	function MyExpertEvaluationDetailController($scope, $state, $http,
			MyExpertEvaluation, entity, MaturityStage, Locale , Evaluation) {
		var vm = this;
		$scope.locale = Locale;
		$scope.userEval = entity;
		$scope.maturitystages = MaturityStage.query();

		$scope.confirm = function() {
			$scope.userEval.evaluationState = 'CONFIRMED';
			$http.post("/api/checkingEvaluation" , $scope.userEval).then(onSaveSuccess, onSaveError);			
		}

		$scope.reject = function() {
			$scope.userEval.evaluationState = 'REJECTED';
			$http.post("/api/checkingEvaluation" , $scope.userEval).then(onSaveSuccess, onSaveError);			
		}
		
        function onSaveSuccess (result) {
            $scope.$emit('evaluationApp:evaluationUpdate', result);
			$state.go('my-expert-evaluation');
        }

        function onSaveError () {
        }

	}
})();
