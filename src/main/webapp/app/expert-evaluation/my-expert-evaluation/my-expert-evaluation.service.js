
(function() {
    'use strict';
    angular
        .module('evaluationApp')
        .factory('MyExpertEvaluation', MyExpertEvaluation);

    MyExpertEvaluation.$inject = ['$resource'];

    function MyExpertEvaluation ($resource) {
        var resourceUrl =  'api/my-expert-evaluations/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
