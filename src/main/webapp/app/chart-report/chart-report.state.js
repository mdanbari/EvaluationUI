(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('chart-report', {
            parent: 'app',
            url: '/chart-report',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/chart-report/chart-report.html',
                    controller: 'ChartReportController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    return $translate.refresh();
                }]
            }
        });
    }
})();
