(function() {
	'use strict';

	angular.module('evaluationApp').controller('ChartReportController',
			ChartReportController);

	ChartReportController.$inject = [ '$scope', 'Principal', 'LoginService',
			'$state', '$http' , 'Locale' ];

	function ChartReportController($scope, Principal, LoginService, $state,
			$http , Locale) {
		var vm = this;

		$scope.locale = Locale;

		$scope.barLabels= JSON.parse(localStorage.getItem("reportLabels"));
		$scope.barSeries  = JSON.parse(localStorage.getItem("reportSeries"));
		$scope.barData = JSON.parse(localStorage.getItem("reportData"));
		$scope.chartColors = JSON.parse(localStorage.getItem("chartColors"));
		$scope.barOptions = JSON.parse(localStorage.getItem("barOptions"));
		$scope.pieOptions = JSON.parse(localStorage.getItem("pieOptions"));
	debugger
		localStorage.removeItem("reportLabels");
		localStorage.removeItem("reportSeries");
		localStorage.removeItem("reportData");
		localStorage.removeItem("chartColors");
		localStorage.removeItem("barOptions");
		localStorage.removeItem("pieOptions");
		
		setTimeout(function()  {
			window.print();
		}, 1000);
		
	}
})();
