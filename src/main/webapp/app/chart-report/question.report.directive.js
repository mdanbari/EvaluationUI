angular
		.module('evaluationApp')
		.directive(
				'questionreport',
				[
						'Question',
						'$http',
						'Locale',
						function(Question, $http, Locale) {

							var directive = {};

							directive.restrict = 'E';

							directive.templateUrl = 'app/chart-report/question.report.template.html';

							directive.scope = false;

							directive.link = function($scope, element, myAttr) {

								$scope.mode = myAttr.mode;

								$scope.questions = Question.query();

								$scope.locale = Locale;

								$scope.report = {};

								$scope.barLabels = [];
								$scope.barSeries = [];
								$scope.barData = [];

								$scope.showBarChart = false;

								$scope.treeNodes = [];
								$scope.elecServiceDepartment = [];

								$scope.electronic = [];
								$scope.electronicSelected = {};

								$scope.report.electronicServiceList = [];
								$scope.report.questions = [];
								$scope.report.departments = [];

								$scope.addService = function(service) {
									if ($scope.report.electronicServiceList
											.indexOf(service) == -1)
										$scope.report.electronicServiceList
												.push(service);
								}
								$scope.removeService = function(service) {
									$scope.report.electronicServiceList = $scope.report.electronicServiceList
											.filter(function(el) {
												return el.id != service[0].id
											});

								}

								$scope.addQuestion = function(question) {
									if ($scope.report.questions
											.indexOf(question) == -1)
										$scope.report.questions.push(question);
								}
								$scope.removeQuestion = function(question) {
									$scope.report.questions = $scope.report.questions
											.filter(function(el) {
												return el.id != question[0].id
											});

								}

								$scope.addDepartment = function(service) {
									if ($scope.report.electronicServiceList
											.indexOf(service) == -1)
										$scope.report.departments
												.push(service);
								}
								$scope.removeDepartment = function(service) {
									$scope.report.departments = $scope.report.departments
											.filter(function(el) {
												return el.id != service[0].id
											});
								}
								
								$scope.printReport = function() {
									localStorage.setItem("reportLabels",
											JSON.stringify($scope.barLabels));
									localStorage.setItem("reportSeries",
											JSON.stringify($scope.barSeries));
									localStorage.setItem("reportData",
											JSON.stringify($scope.barData));
									localStorage.setItem("chartColors",
											JSON.stringify($scope.chartColors));
									localStorage.setItem("barOptions",
											JSON.stringify($scope.barOptions));
									localStorage.setItem("pieOptions",
											JSON.stringify($scope.pieOptions));
									
									window.open("/#/chart-report" , "_blank");
								}

								
								$scope.createReport = function(isDownload) {

									$scope.chartColors = [ '#FF5252',
											'#0000ff', '#00ff00', '#ff00ff' ];

									var yT;
									var mT;
									var dT;

									if ($scope.report.fromDateStr != undefined) {

										var miladiF;
										var split = $scope.report.fromDateStr
												.split("/");

										if ($scope.locale.lang == 'fa') {
											miladiF = toMiladi(split[0],
													split[1], split[2]);
										}

										split = miladiF.split("/");

										var fromDate = new Date(Date.UTC(
												split[0], split[1] - 1,
												split[2], 0, 0, 1));

										$scope.report.fromDate = fromDate;
									}

									if ($scope.report.toDateStr != undefined) {

										var miladiT;
										var split = $scope.report.toDateStr
												.split("/");

										if ($scope.locale.lang == 'fa') {
											miladiT = toMiladi(split[0],
													split[1], split[2]);
										}

										split = miladiT.split("/");

										var toDate = new Date(Date.UTC(
												split[0], split[1] - 1,
												split[2], 23, 59, 59));

										$scope.report.toDate = toDate;
									}

									$scope.report.locale = $scope.locale.lang;
									$scope.report.isDownload = isDownload;

									debugger
									if ($scope.mode == "service") {
										
										$http
										.post("/api/elec-question-report",
												$scope.report)
										.then(
												function(response) {
													//														
													if (response.data != undefined) {
														if (isDownload) {
															var hiddenElement = document
																	.createElement('a');
															hiddenElement.href = 'data:attachment/csv,'
																	+ encodeURI(response.data);
															hiddenElement.target = '_blank';
															hiddenElement.download = 'reportcsv.csv';
															hiddenElement
																	.click();
														} else {
															$scope.barLabels = response.data.label;
															$scope.barData = response.data.data;
															$scope.barSeries = response.data.series;
															$scope.barOptions = {
																	  scales: {
																            yAxes: [{
																                ticks: {
																                    beginAtZero:true
																                }
																            }]
																        },
																legend : {
																	display : true
																},
																title : {
																	display : true
																}
															};
															$scope.pieOptions = {
																	  scales: {
																            yAxes: [{
																                ticks: {
																                    beginAtZero:true
																                }
																            }]
																        },
																legend : {
																	display : true
																},
																title : {
																	display : true
																}
															};
															$scope.showBarChart = true;

														}
													}

												}, function(error) {
													alert("fail");
												});

										
										
									} else if ($scope.mode == "department") {
										
										$http
										.post("/api/dep-question-report",
												$scope.report)
										.then(
												function(response) {
													//														
													if (response.data != undefined) {
														if (isDownload) {
															var hiddenElement = document
																	.createElement('a');
															hiddenElement.href = 'data:attachment/csv,'
																	+ encodeURI(response.data);
															hiddenElement.target = '_blank';
															hiddenElement.download = 'reportcsv.csv';
															hiddenElement
																	.click();
														} else {
															$scope.barLabels = response.data.label;
															$scope.barData = response.data.data;
															$scope.barSeries = response.data.series;
															$scope.barOptions = {scales: {
													            yAxes: [{
													                ticks: {
													                    beginAtZero:true
													                }
													            }]
													        },
																legend : {
																	display : true
																},
																title : {
																	display : true
																}
															};
															$scope.pieOptions = {scales: {
													            yAxes: [{
													                ticks: {
													                    beginAtZero:true
													                }
													            }]
													        },
																legend : {
																	display : true
																},
																title : {
																	display : true
																}
															};
															$scope.showBarChart = true;

														}
													}

												}, function(error) {
													alert("fail");
												});


										
									}

								}

								$scope
										.$on(
												'selection-changed',
												function(e, node) {
													$scope.selectedDepartment = node;
													$http
															.get(
																	"/api/findCountAllChildrenOfDepartment/"
																			+ parseInt(node.id))
															.then(
																	function(
																			response) {
																		if (response.data > 0) {
																			$http
																					.get(
																							"/api/findallchildrenofdepartment/"
																									+ parseInt(node.id))
																					.then(
																							function(
																									response) {
																								if (response.data.length > 0) {
																									node.children = [];
																									node.children.push
																											.apply(
																													node.children,
																													response.data);
																								}
																							},
																							function(
																									errResponse) {
																								console
																										.error('Error while loading department');
																							});

																		}
																	},
																	function(
																			errResponse) {
																		console
																				.error('Error while loading department');
																	});

													if (node.electronicServiceCount > 0) {
														$http
																.get(
																		"/api/findAllElecServiceOfDepartment/"
																				+ parseInt(node.id))
																.then(
																		function(
																				response) {
																			$scope.elecServiceDepartment = response.data;
																		},
																		function(
																				errResponse) {
																			console
																					.error('Error while loading department');
																		});
													} else {
														$scope.elecServiceDepartment = [];
													}

												});

								$http
										.get("/api/departmentsparentisnull", {})
										.then(
												function(response) {
													return $scope.treeNodes.push
															.apply(
																	$scope.treeNodes,
																	response.data);
												},
												function(errResponse) {
													console
															.error('Error while loading department');
												});

								$http
										.get(
												"/api/customGetAllElectronicService",
												{})
										.then(
												function(response) {
													return $scope.electronic.push
															.apply(
																	$scope.electronic,
																	response.data);
												},
												function(errResponse) {
													console
															.error('Error while loading department');
												});

							}
							return directive;
						} ]);
