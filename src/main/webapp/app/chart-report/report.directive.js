/**
 * hossein
 */

angular
		.module('evaluationApp')
		.directive(
				'report',
				[
						'DiamondCategory',
						'MaturityStage',
						'UnMaturityModel',
						'$http',
						'Locale',
						function(DiamondCategory, MaturityStage,
								UnMaturityModel, $http, Locale) {

							var directive = {};

							directive.restrict = 'AE';

							directive.template = function(myVar) {

								var mode = myVar.attr("mode"); // separation,
								// consolidated , //organization
								// //service-question //organization-question

								var html = '';

								html += '<div class="panel panel-success" style="color:black;">';

								html += '<div data-translate="report.main"';
								html += 'class="panel-heading">department</div>';
								html += '<div class="panel-body">';

								html += '<div class="row">';

								html += '<div class="col-md-6">';
								html += '<tree nodes="treeNodes" options="options" locale = "locale.lang">';
								html += '</div>';

								html += '<div class="col-md-6">';

								html += '<div class="col-md-6">';
								html += '<label data-translate="report.fromDate"></label>';
								html += '<datebox ng-model="report.fromDateStr"></datebox>';
								html += '</div>';

								html += '<div class="col-md-6">';
								html += '<label data-translate="report.toDate"></label>';
								html += '<datebox ng-model="report.toDateStr"></datebox>';
								html += '</div>';

								html += '</div>';

								html += '</div>';

								html += '<br>';

								html += '<div class="row">';

								html += '<div class="col-md-6">';
								if (mode != "organization") {
									html += '<label data-translate="report.elecService"></label>';

									html += '<angucomplete ng-show="locale.lang == \'fa\'" pause="100" selectedobject="report.electronicService"';
									html += 'autoname="elecServiceDepartment" localdata="elecServiceDepartment"';
									html += 'searchfields="elecServiceTitlefa" titlefield="elecServiceTitlefa"';
									html += 'minlength="1" inputclass="form-control form-control-small"';
									html += 'matchclass="highlight" />';
									html += '<angucomplete ng-show="locale.lang == \'en\'" pause="100" selectedobject="report.electronicService"';
									html += 'autoname="elecServiceDepartment" localdata="elecServiceDepartment"';
									html += 'searchfields="elecServiceTitleen" titlefield="elecServiceTitleen"';
									html += 'minlength="1" inputclass="form-control form-control-small"';
									html += 'matchclass="highlight" />';

//									html += '<select ng-show = "locale.lang == \'fa\'" class="form-control" id="field_parent"';
//									html += 'name="elecServ" ng-model="report.electronicService"';
//									html += 'ng-options="esd as esd.elecServiceTitlefa for esd in elecServiceDepartment track by esd.id">';
//									html += '<option value="" ></option>';
//									html += '</select>';
//									html += '<select ng-show = "locale.lang == \'en\'" class="form-control" id="field_parent"';
//									html += 'name="elecServ" ng-model="report.electronicService"';
//									html += 'ng-options="esd as esd.elecServiceTitleen for esd in elecServiceDepartment track by esd.id">';
//									html += '<option value="" ></option>';
//									html += '</select>';
								}
								html += '</div>';
								html += '<div class="col-md-6">';

								html += '<div class="col-md-6">';
								html += '<label data-translate="report.modelType"></label>';
								html += '<select class="form-control" id="field_parent"';
								html += 'name="elecServ" ng-model="selectedModel">';
								html += '<option value = "maturity" data-translate="report.maturityStage"></option>';
								html += '<option value = "diamond" data-translate="report.diamondCategory"></option>';
								html += '</select>';
								html += '</div>';

								html += '<div class="col-md-6" ng-show = "selectedModel == \'maturity\'">';
								html += '<label data-translate="report.maturityStage"></label>';

								html += '<select ng-show = "locale.lang == \'fa\'" multiple class="form-control" id="field_parent"';
								html += ' ng-model="report.maturitySelecteds"';
								html += 'ng-options="m as m.maturityStageTitlefa for m in maturityStages track by m.id">';
								html += '<option value="" ></option>';
								html += '</select>';

								html += '<select ng-show = "locale.lang == \'en\'" multiple class="form-control" id="field_parent"';
								html += ' ng-model="report.maturitySelecteds"';
								html += 'ng-options="m as m.maturityStageTitleen for m in maturityStages track by m.id">';
								html += '<option value="" ></option>';
								html += '</select>';

								html += '</div>';

								html += '<div class="col-md-6" ng-show = "selectedModel == \'diamond\'">';
								html += '<label data-translate="report.diamondCategory"></label>';

								html += '<select ng-show = "locale.lang == \'fa\'" multiple class="form-control" id="field_parent"';
								html += ' ng-model="report.diamondSelecteds"';
								html += 'ng-options="d as d.diamondCategoryTitlefa for d in diamondCategories track by d.id">';
								html += '<option value="" ></option>';
								html += '</select>';

								html += '<select ng-show = "locale.lang == \'en\'" multiple class="form-control" id="field_parent"';
								html += ' ng-model="report.diamondSelecteds"';
								html += 'ng-options="d as d.diamondCategoryTitleen for d in diamondCategories track by d.id">';
								html += '<option value="" ></option>';
								html += '</select>';
								html += '</div>';

								html += '</div>';

								if (mode == "consolidated") {
									html += '<div class="col-md-6">';
									html += '<label data-translate="report.selectedElecService"></label>';

									html += '<select ng-show = "locale.lang == \'fa\'" multiple class="form-control" id="field_parent"';
									html += ' ng-model="report.selectedService"';
									html += 'ng-options="e as e.elecServiceTitlefa for e in report.electronicServiceList track by e.id">';
									html += '<option value="" ></option>';
									html += '</select>';

									html += '<select ng-show = "locale.lang == \'en\'" multiple class="form-control" id="field_parent"';
									html += ' ng-model="report.selectedService"';
									html += 'ng-options="e as e.elecServiceTitleen for e in report.electronicServiceList track by e.id">';
									html += '<option value="" ></option>';
									html += '</select>';

									html += '<div class="col-md-6">';
									html += '<button data-translate="entity.action.add" class="btn btn-info" ng-click="addService(report.electronicService)" value="add"></button>';
									html += '</div>';

									html += '<div class="col-md-6">';
									html += '<button data-translate="entity.action.remove" class="btn btn-danger" ng-click="removeService(report.selectedService)" value="remove"></button>';
									html += '</div>';

									html += '</div>';
									
									html += '<div class="col-md-6">';
									html += '&nbsp';
									html += '</div>';
								}

								if (mode == "organization") {
									html += '<div class="col-md-6">';
									html += '<label data-translate="report.selectedDepartment"></label>';

									html += '<select ng-show = "locale.lang == \'fa\'" multiple class="form-control" id="field_parent"';
									html += ' ng-model="report.selectedDepartment"';
									html += 'ng-options="e as e.namefa for e in report.departmentList track by e.id">';
									html += '<option value="" ></option>';
									html += '</select>';

									html += '<select ng-show = "locale.lang == \'en\'" multiple class="form-control" id="field_parent"';
									html += ' ng-model="report.selectedService"';
									html += 'ng-options="e as e.nameen for e in report.departmentList track by e.id">';
									html += '<option value="" ></option>';
									html += '</select>';

									html += '<div class="col-md-6">';
									html += '<button data-translate="entity.action.add" class="btn btn-info" ng-click="addDepartment(selectedDepartment)" value="add"></button>';
									html += '</div>';

									html += '<div class="col-md-6">';
									html += '<button data-translate="entity.action.remove" class="btn btn-danger" ng-click="removeDepartment(report.selectedDepartment)" value="remove"></button>';
									html += '</div>';

									html += '</div>';
								}

								html += '</div>';

								html += '<center>';

								html += '<button type="button" class="btn btn-success" data-translate="report.main" ng-click="createReport(false)"></button>';

								html += '</center>';

								html += '</div>';

								html += '</div>';
								html += '</div>';

								html += '<div class="panel panel-success" ng-show = "showBarChart">';

								html += '<div data-translate="report.result"';
								html += 'class="panel-heading">department</div>';
								html += '<div class="panel-body">';

								html += '<div class="row">';

								html += '<div class="col-md-12">';
								html += '<div >';
								html += '		<canvas id="bar" class="chart chart-bar" chart-options="barOptions" chart-data="barData" chart-colors="chartColors" chart-series="barSeries" chart-labels="barLabels"> </canvas>';
								html += '</div>';
								html += '</div>';
								html += '<br>';

								html += '<div class="col-md-12">';
								html += '<div >';
								html += '		<canvas id="bar" class="chart chart-radar" chart-options="barOptions" chart-data="barData" chart-colors="chartColors" chart-series="barSeries" chart-labels="barLabels"></canvas>';
								html += '</div>';
								html += '</div>';
								html += '<br>';

								html += '<div class="col-md-12">';
								html += '<div >';
								html += '		<canvas id="bar" class="chart chart-pie" chart-options="pieOptions" chart-data="barData" chart-colors="chartColors" chart-series="barSeries" chart-labels="barLabels"> </canvas>';
								html += '</div>';
								html += '</div>';
								html += '<br>';

								html += '<div class="col-md-12">';
								html += '<div >';
								html += '		<canvas id="bar" class="chart chart-doughnut" chart-options="pieOptions" chart-data="barData" chart-colors="chartColors" chart-series="barSeries" chart-labels="barLabels"></canvas>';
								html += '</div>';
								html += '</div>';

								html += '<center>';

								html += '<button type="button" class="btn btn-success" data-translate="report.download" ng-click="createReport(true)"></button>';

								html += '<button type="button" class="btn btn-success" data-translate="report.print" ng-click="printReport()"></button>';

								html += '</center>';

								html += '</div>';
								html += '</div>';

								return html;
							}

							directive.scope = false;

							directive.link = function($scope, element, myAttr) {

								$scope.locale = Locale;
								var mode = myAttr.mode;
								$scope.maturityStages = MaturityStage.query();
								$scope.diamondCategories = DiamondCategory
										.query();

								$scope.report = {};

								$scope.barLabels = [];
								$scope.barSeries = [];
								$scope.barData = [];

								$scope.showBarChart = false;

								$scope.treeNodes = [];
								$scope.elecServiceDepartment = [];

								$scope.electronic = [];
								$scope.electronicSelected = {};

								$scope.report.electronicServiceList = [];
								$scope.report.departmentList = [];

								$scope.addService = function(service) {
									if ($scope.report.electronicServiceList
											.indexOf(service) == -1)
										$scope.report.electronicServiceList
												.push(service);
								}
								$scope.removeService = function(service) {
									$scope.report.electronicServiceList = $scope.report.electronicServiceList
											.filter(function(el) {
												return el.id != service[0].id
											});

								}

								$scope.printReport = function() {
									localStorage.setItem("reportLabels", JSON
											.stringify($scope.barLabels));
									localStorage.setItem("reportSeries", JSON
											.stringify($scope.barSeries));
									localStorage.setItem("reportData", JSON
											.stringify($scope.barData));
									localStorage.setItem("chartColors", JSON
											.stringify($scope.chartColors));
									localStorage.setItem("barOptions", JSON
											.stringify($scope.barOptions));
									localStorage.setItem("pieOptions", JSON
											.stringify($scope.pieOptions));
									window.open("/#/chart-report", "_blank");
								}

								$scope.addDepartment = function(service) {
									if ($scope.report.electronicServiceList
											.indexOf(service) == -1)
										$scope.report.departmentList
												.push(service);
								}
								$scope.removeDepartment = function(service) {
									$scope.report.departmentList = $scope.report.departmentList
											.filter(function(el) {
												return el.id != service[0].id
											});
								}

								$scope.createReport = function(isDownload) {

									$scope.chartColors = [ '#FF5252',
											'#0000ff', '#00ff00', '#ff00ff' ];

									var yT;
									var mT;
									var dT;

									if ($scope.report.fromDateStr != undefined) {

										var miladiF;
										var split = $scope.report.fromDateStr
												.split("/");

										if ($scope.locale.lang == 'fa') {
											miladiF = toMiladi(split[0],
													split[1], split[2]);
										}

										split = miladiF.split("/");

										var fromDate = new Date(Date.UTC(
												split[0], split[1] - 1,
												split[2], 0, 0, 1));

										$scope.report.fromDate = fromDate;
									}

									if ($scope.report.toDateStr != undefined) {

										var miladiT;
										var split = $scope.report.toDateStr
												.split("/");

										if ($scope.locale.lang == 'fa') {
											miladiT = toMiladi(split[0],
													split[1], split[2]);
										}

										split = miladiT.split("/");

										var toDate = new Date(Date.UTC(
												split[0], split[1] - 1,
												split[2], 23, 59, 59));

										$scope.report.toDate = toDate;
									}

									if ($scope.selectedModel == 'maturity') {
										$scope.report.diamondSelecteds = [];

									}
									if ($scope.selectedModel == 'diamond') {
										$scope.report.maturitySelecteds = [];
									}

									$scope.report.typeModel = $scope.selectedModel;
									$scope.report.locale = $scope.locale.lang;
									$scope.report.isDownload = isDownload;

									if (mode == "separation") {

										$http
												.post("/api/separation-report",
														$scope.report)
												.then(
														function(response) {
															//														
															if (response.data != undefined) {
																if (isDownload) {
																	var hiddenElement = document
																			.createElement('a');
																	hiddenElement.href = 'data:attachment/csv,'
																			+ encodeURI(response.data);
																	hiddenElement.target = '_blank';
																	hiddenElement.download = 'reportcsv.csv';
																	hiddenElement
																			.click();
																} else {
																	$scope.barLabels = response.data.label;
																	$scope.barData = response.data.data;
																	$scope.barSeries = response.data.label;
																	$scope.barOptions = {
																		scales : {
																			yAxes : [ {
																				ticks : {
																					beginAtZero : true
																				}
																			} ]
																		},
																		legend : {
																			display : false
																		}
																	};
																	$scope.pieOptions = {
																		scales : {
																			yAxes : [ {
																				ticks : {
																					beginAtZero : true
																				}
																			} ]
																		},
																		legend : {
																			display : true
																		}
																	};

																	$scope.showBarChart = true;
																}
															}

														}, function(error) {
															alert("fail");
														});
									} else if (mode == "consolidated") {

										$http
												.post(
														"/api/consolidated-report",
														$scope.report)
												.then(
														function(response) {

															if (response.data != undefined) {
																if (isDownload) {
																	var hiddenElement = document
																			.createElement('a');
																	hiddenElement.href = 'data:attachment/csv,'
																			+ encodeURI(response.data);
																	hiddenElement.target = '_blank';
																	hiddenElement.download = 'reportcsv.csv';
																	hiddenElement
																			.click();
																} else {

																	$scope.barLabels = response.data.label;
																	$scope.barData = response.data.data;
																	$scope.barSeries = response.data.series;
																	$scope.barOptions = {
																		scales : {
																			yAxes : [ {
																				ticks : {
																					beginAtZero : true
																				}
																			} ]
																		},
																		legend : {
																			display : true
																		},
																		title : {
																			display : true
																		}
																	};
																	$scope.pieOptions = {
																		scales : {
																			yAxes : [ {
																				ticks : {
																					beginAtZero : true
																				}
																			} ]
																		},
																		legend : {
																			display : true
																		},
																		title : {
																			display : true
																		}
																	};
																	$scope.showBarChart = true;
																}
															}

														}, function(error) {
															alert("fail");
														});

									} else if (mode == "organization") {

										$http
												.post("/api/department-report",
														$scope.report)
												.then(
														function(response) {

															if (response.data != undefined) {
																if (isDownload) {
																	var hiddenElement = document
																			.createElement('a');
																	hiddenElement.href = 'data:attachment/csv,'
																			+ encodeURI(response.data);
																	hiddenElement.target = '_blank';
																	hiddenElement.download = 'reportcsv.csv';
																	hiddenElement
																			.click();
																} else {

																	$scope.barLabels = response.data.label;
																	$scope.barData = response.data.data;
																	$scope.barSeries = response.data.series;
																	$scope.barOptions = {
																		scales : {
																			yAxes : [ {
																				ticks : {
																					beginAtZero : true
																				}
																			} ]
																		},
																		legend : {
																			display : true
																		},
																		title : {
																			display : true
																		}
																	};
																	$scope.pieOptions = {
																		scales : {
																			yAxes : [ {
																				ticks : {
																					beginAtZero : true
																				}
																			} ]
																		},
																		legend : {
																			display : true
																		},
																		title : {
																			display : true
																		}
																	};
																	$scope.showBarChart = true;
																}
															}

														}, function(error) {
															alert("fail");
														});

									}
									// call service to get report
								};
								$scope
										.$on(
												'selection-changed',
												function(e, node) {
													$scope.selectedDepartment = node;
													$http
															.get(
																	"/api/findCountAllChildrenOfDepartment/"
																			+ parseInt(node.id))
															.then(
																	function(
																			response) {
																		if (response.data > 0) {
																			$http
																					.get(
																							"/api/findallchildrenofdepartment/"
																									+ parseInt(node.id))
																					.then(
																							function(
																									response) {
																								if (response.data.length > 0) {
																									node.children = [];
																									node.children.push
																											.apply(
																													node.children,
																													response.data);
																								}
																							},
																							function(
																									errResponse) {
																								console
																										.error('Error while loading department');
																							});

																		}
																	},
																	function(
																			errResponse) {
																		console
																				.error('Error while loading department');
																	});

													if (node.electronicServiceCount > 0) {
														$http
																.get(
																		"/api/findAllElecServiceOfDepartment/"
																				+ parseInt(node.id))
																.then(
																		function(
																				response) {
																			$scope.elecServiceDepartment = response.data;
																		},
																		function(
																				errResponse) {
																			console
																					.error('Error while loading department');
																		});
													} else {
														$scope.elecServiceDepartment = [];
													}

												});

								$http
										.get("/api/departmentsparentisnull", {})
										.then(
												function(response) {
													return $scope.treeNodes.push
															.apply(
																	$scope.treeNodes,
																	response.data);
												},
												function(errResponse) {
													console
															.error('Error while loading department');
												});

								$http
										.get(
												"/api/customGetAllElectronicService",
												{})
										.then(
												function(response) {
													return $scope.electronic.push
															.apply(
																	$scope.electronic,
																	response.data);
												},
												function(errResponse) {
													console
															.error('Error while loading department');
												});

							}
							return directive;
						} ]);
