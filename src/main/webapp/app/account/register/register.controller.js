(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('RegisterController', RegisterController);


    RegisterController.$inject = ['$translate', '$timeout', 'Auth', 'LoginService','Captcha'];

    function RegisterController ($translate, $timeout, Auth, LoginService,Captcha) {
        var vm = this;

        vm.doNotMatch = null;
        vm.error = null;
        vm.errorUserExists = null;
        vm.login = LoginService.open;
        vm.register = register;
        vm.registerAccount = {};
        vm.success = null;
        vm.captcha={};
        vm.inputcaptcha=null;
        vm.captchaValid={};
        createCaptcha();

        $timeout(function (){angular.element('#login').focus();});

        function register () {
            if (vm.registerAccount.password !== vm.confirmPassword) {
                vm.doNotMatch = 'ERROR';
            } else {
                vm.registerAccount.langKey = $translate.use();
                vm.doNotMatch = null;
                vm.error = null;
                vm.errorUserExists = null;
                vm.errorEmailExists = null;
                vm.errorNationalIdExists = null;
                Captcha.valid({'captchaValue':vm.inputcaptcha}).$promise.then(function(data){
                	debugger
                	vm.captchaValid=data;
                	if(data.valid)
                		{
                		 Auth.createAccount(vm.registerAccount).then(function () {
                             vm.success = 'OK';
                         }).catch(function (response) {
                             vm.success = null;
                             if (response.status === 400 && response.data === 'login already in use') {
                                 vm.errorUserExists = 'ERROR';
                                 createCaptcha();
                             } else if (response.status === 400 && response.data === 'email address already in use') {
                                 vm.errorEmailExists = 'ERROR';
                                 createCaptcha();
                             }
                             else if (response.status === 400 && response.data === 'NationalId already in use') {
                                     vm.errorNationalIdExists = 'ERROR'; 
                                     createCaptcha();
                             } else {
                                 vm.error = 'ERROR';
                                 createCaptcha();
                             }
                         });
                		}
                	else
                		{
                			createCaptcha();
                			vm.success=null;
                			vm.error='ERROR';
                		}
                }).catch(function(response){
                	createCaptcha();
                	vm.success=null;
        			vm.error='ERROR';
                });

//                Auth.createAccount(vm.registerAccount).then(function () {
//                    vm.success = 'OK';
//                }).catch(function (response) {
//                    vm.success = null;
//                    if (response.status === 400 && response.data === 'login already in use') {
//                        vm.errorUserExists = 'ERROR';
//                    } else if (response.status === 400 && response.data === 'email address already in use') {
//                        vm.errorEmailExists = 'ERROR';
//                    }
//                    else if (response.status === 400 && response.data === 'NationalId already in use') {
//                            vm.errorNationalIdExists = 'ERROR';                      
//                    } else {
//                        vm.error = 'ERROR';
//                    }
//                });
            }
        }
        function createCaptcha()
        {
        	vm.captcha=Captcha.create();
        }
    }
})();
