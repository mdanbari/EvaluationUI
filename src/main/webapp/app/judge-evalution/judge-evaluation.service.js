(function() {
    'use strict';
    angular
        .module('evaluationApp')
        .factory('JudgeEvaluation', JudgeEvaluation);

    JudgeEvaluation.$inject = ['$resource'];

    function JudgeEvaluation ($resource) {
        var resourceUrl =  'api/confirmed-evaluations';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();