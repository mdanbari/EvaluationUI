(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('judge-evaluation', {
            parent: 'app',
            url: '/judge',
            data: {
                authorities: ['OBSERVER'],
                pageTitle: 'evaluationApp.judgeEvaluation.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/judge-evalution/judge-evaluation.html',
                    controller: 'JudgeEvaluationController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('judgeEvaluation');
                    $translatePartialLoader.addPart('evaluation');
                    $translatePartialLoader.addPart('evaluationState');
                    $translatePartialLoader.addPart('evaluationState');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })        
        .state('judge-evaluation.detail', {
            parent: 'judge-evaluation',
            url: '{id}/detail',
            data: {
                authorities: ['OBSERVER']
            },
            views: {
                'content@': {
                	 templateUrl: 'app/judge-evalution/judge-evaluation-detail.html',
                     controller: 'JudgeEvaluationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('judgeEvaluation');
                    $translatePartialLoader.addPart('electronicService');
                    $translatePartialLoader.addPart('evaluation');
                    return $translate.refresh();
                }],
                
                entity: ['$stateParams', 'MyExpertEvaluation', function($stateParams, MyExpertEvaluation) {
                    return MyExpertEvaluation.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'judge-evaluation',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        });
    }

})();
