(function() {
	'use strict';

	angular.module('evaluationApp').controller('SeparationReportController',
			SeparationReportController);

	SeparationReportController.$inject = [ '$scope', 'Principal',
			'LoginService', '$state', '$http', 'Locale' ];

	function SeparationReportController($scope, Principal, LoginService,
			$state, $http, Locale) {
		var vm = this;
		$scope.locale = Locale;
	}
})();
