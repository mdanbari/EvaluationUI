(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('separation-report', {
            parent: 'app',
            url: '/separation-report',
            data: {
                authorities: ['ADMIN']
            },
            views: {
                'content@': {
                    templateUrl: 'app/separation-report/separation-report.html',
                    controller: 'SeparationReportController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('report');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
