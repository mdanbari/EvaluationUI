(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('service-question-report', {
            parent: 'app',
            url: '/service-question-report',
            data: {
                authorities: ['ADMIN']
            },
            views: {
                'content@': {
                    templateUrl: 'app/service-question-report/service-question-report.html',
                    controller: 'ServiceQuestionReportController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('report');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
