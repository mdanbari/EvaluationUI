(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('specialized-evaluation', {
            parent: 'specialized',
            url: '/specialized',
            data: {
                authorities: ['SPECIALISED_EVALUATOR']
            },
            views: {
                'content@': {
                    templateUrl: 'app/specialized-evaluation/specialized-evaluation.html',
                    controller: 'SpecializedEvaluationController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('specialized-evaluation');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
