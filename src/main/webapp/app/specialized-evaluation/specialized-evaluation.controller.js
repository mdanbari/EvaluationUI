(function() {
	'use strict';

	angular.module('evaluationApp').controller('SpecializedEvaluationController',
			SpecializedEvaluationController);

	SpecializedEvaluationController.$inject = [ '$scope', 'Principal',
			'LoginService', '$state', '$http' ];

	function SpecializedEvaluationController($scope, Principal, LoginService,
			$state, $http) {
		var vm = this;

		$http.get("../api/getCurrentEvaluation").then(function(response) {
			if (response.data.id != null) {
				$scope.evaluation = response.data;
				$scope.start();
			}
		}, function(errResponse) {
			console.error('Error while deleting user');
		});

		$scope.evaluation = null;
		$scope.getRandomEvaluation = function() {
			$http.get("../api/getRandomEvaluation").then(function(response) {
				$scope.evaluation = response.data;
				$scope.start();
			}, function(errResponse) {
				console.error('Error while deleting user');
			});
		}

		$scope.start = function() {
			localStorage.setItem('eval', JSON.stringify($scope.evaluation));
			$state.go('do-specialized-evaluation');
		}
	}
})();
