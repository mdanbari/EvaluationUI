(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('do-specialized-evaluation', {
            parent: 'specialized',
            url: '/dospecialized',
            data: {
                authorities: ['SPECIALISED_EVALUATOR']
            },
            views: {
                'content@': {
                    templateUrl: 'app/specialized-evaluation/do-specialized-evaluation/do-specialized-evaluation.html',
                    controller: 'DoSpecializedEvaluationController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('do-specialized-evaluation');
                    $translatePartialLoader.addPart('electronicService');
                    $translatePartialLoader.addPart('evaluation');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
