(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('my-specialized-evaluation', {
            parent: 'app',
            url: '/myspecializedevaluation',
            data: {
                authorities: ['SPECIALISED_EVALUATOR']
            },
            views: {
                'content@': {
                    templateUrl: 'app/specialized-evaluation/my-specialized-evaluation/my-specialized-evaluation.html',
                    controller: 'MySpecializedEvaluationController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('evaluation');
                    $translatePartialLoader.addPart('evaluationState');
                    $translatePartialLoader.addPart('evaluationState');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('my-specialized-evaluation.detail', {
            parent: 'my-specialized-evaluation',
            url: '/{id}/detail',
            data: {
                authorities: ['SPECIALISED_EVALUATOR']
            },
            views: {
                'content@': {
                	 templateUrl: 'app/specialized-evaluation/my-specialized-evaluation/my-specialized-evaluation-detail.html',
                     controller: 'MySpecializedEvaluationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('do-specialized-evaluation');
                    $translatePartialLoader.addPart('electronicService');
                    $translatePartialLoader.addPart('evaluation');
                    return $translate.refresh();
                }],
                
                entity: ['$stateParams', 'MySpecializedEvaluation', function($stateParams, MySpecializedEvaluation) {
                    return MySpecializedEvaluation.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'my-specialized-evaluation',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        });
        ;
    }
})();
