(function() {
	'use strict';

	angular.module('evaluationApp').controller('MySpecializedEvaluationDetailController',
			MySpecializedEvaluationDetailController);

	MySpecializedEvaluationDetailController.$inject = [ '$scope','$state', 'MySpecializedEvaluation', 'entity' , 'MaturityStage' , 'Locale'];

	function MySpecializedEvaluationDetailController($scope , $state, MySpecializedEvaluation, entity , MaturityStage , Locale) {
		   var vm = this;
		   $scope.locale = Locale;
	      $scope.userEval = entity;
	      $scope.maturitystages = MaturityStage.query();

	}
})();
