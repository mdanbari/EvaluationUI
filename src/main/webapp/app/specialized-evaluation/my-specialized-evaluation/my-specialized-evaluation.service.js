(function() {
    'use strict';
    angular
        .module('evaluationApp')
        .factory('MySpecializedEvaluation', MySpecializedEvaluation);

    MySpecializedEvaluation.$inject = ['$resource'];

    function MySpecializedEvaluation ($resource) {
        var resourceUrl =  'api/myevaluations/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
