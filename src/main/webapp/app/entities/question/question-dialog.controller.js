(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('QuestionDialogController', QuestionDialogController);

    QuestionDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Question', 'Answer', 'MaturityStage', 'UnMaturityModel', 'DiamondCategory' , 'Locale'];

    function QuestionDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Question, Answer, MaturityStage, UnMaturityModel, DiamondCategory , Locale) {
        var vm = this;

        $scope.locale = Locale;
        vm.question = entity;
        vm.clear = clear;
        vm.save = save;
        vm.answers = Answer.query();
        vm.maturitystages = MaturityStage.query();
        vm.unmaturitymodels = UnMaturityModel.query();
        vm.diamondcategories = DiamondCategory.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.question.id !== null) {
                Question.update(vm.question, onSaveSuccess, onSaveError);
            } else {
                Question.save(vm.question, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('evaluationApp:questionUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
