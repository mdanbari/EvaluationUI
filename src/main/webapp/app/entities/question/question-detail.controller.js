(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('QuestionDetailController', QuestionDetailController);

    QuestionDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Question', 'Answer', 'MaturityStage', 'UnMaturityModel', 'DiamondCategory' , 'Locale'];

    function QuestionDetailController($scope, $rootScope, $stateParams, previousState, entity, Question, Answer, MaturityStage, UnMaturityModel, DiamondCategory , Locale) {
        var vm = this;

        $scope.locale = Locale;
        vm.question = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('evaluationApp:questionUpdate', function(event, result) {
            vm.question = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
