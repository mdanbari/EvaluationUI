(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('EvaluationArchiveDeleteController',EvaluationArchiveDeleteController);

    EvaluationArchiveDeleteController.$inject = ['$uibModalInstance', 'entity', 'EvaluationArchive'];

    function EvaluationArchiveDeleteController($uibModalInstance, entity, EvaluationArchive) {
        var vm = this;

        vm.evaluationArchive = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            EvaluationArchive.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
