(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('evaluation-archive', {
            parent: 'entity',
            url: '/evaluation-archive?page&sort&search',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.evaluationArchive.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/evaluation-archive/evaluation-archives.html',
                    controller: 'EvaluationArchiveController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('evaluationArchive');
                    $translatePartialLoader.addPart('evaluationState');
                    $translatePartialLoader.addPart('evaluationState');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('evaluation-archive-detail', {
            parent: 'evaluation-archive',
            url: '/evaluation-archive/{id}',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.evaluationArchive.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/evaluation-archive/evaluation-archive-detail.html',
                    controller: 'EvaluationArchiveDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('evaluationArchive');
                    $translatePartialLoader.addPart('evaluationState');
                    $translatePartialLoader.addPart('evaluationState');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'EvaluationArchive', function($stateParams, EvaluationArchive) {
                    return EvaluationArchive.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'evaluation-archive',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('evaluation-archive-detail.edit', {
            parent: 'evaluation-archive-detail',
            url: '/detail/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evaluation-archive/evaluation-archive-dialog.html',
                    controller: 'EvaluationArchiveDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['EvaluationArchive', function(EvaluationArchive) {
                            return EvaluationArchive.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('evaluation-archive.new', {
            parent: 'evaluation-archive',
            url: '/new',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evaluation-archive/evaluation-archive-dialog.html',
                    controller: 'EvaluationArchiveDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                evalArchTitlefa: null,
                                evalArchTitleen: null,
                                evalArchPerson: null,
                                evalArchStartDate: null,
                                evalArchEndDate: null,
                                evalArchDeadLineDate: null,
                                evalArchLastModifiedDate: null,
                                evalArchConfirmedDate: null,
                                evalArchState: null,
                                evalArchDescriptionfa: null,
                                evalArchDescriptionen: null,
                                evalArchConfirmer: null,
                                evalArchScore: null,
                                evalArchuationState: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('evaluation-archive', null, { reload: 'evaluation-archive' });
                }, function() {
                    $state.go('evaluation-archive');
                });
            }]
        })
        .state('evaluation-archive.edit', {
            parent: 'evaluation-archive',
            url: '/{id}/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evaluation-archive/evaluation-archive-dialog.html',
                    controller: 'EvaluationArchiveDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['EvaluationArchive', function(EvaluationArchive) {
                            return EvaluationArchive.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('evaluation-archive', null, { reload: 'evaluation-archive' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('evaluation-archive.delete', {
            parent: 'evaluation-archive',
            url: '/{id}/delete',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evaluation-archive/evaluation-archive-delete-dialog.html',
                    controller: 'EvaluationArchiveDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['EvaluationArchive', function(EvaluationArchive) {
                            return EvaluationArchive.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('evaluation-archive', null, { reload: 'evaluation-archive' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
