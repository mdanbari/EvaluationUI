(function() {
    'use strict';
    angular
        .module('evaluationApp')
        .factory('EvaluationArchive', EvaluationArchive);

    EvaluationArchive.$inject = ['$resource', 'DateUtils'];

    function EvaluationArchive ($resource, DateUtils) {
        var resourceUrl =  'api/evaluation-archives/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.evalArchStartDate = DateUtils.convertDateTimeFromServer(data.evalArchStartDate);
                        data.evalArchEndDate = DateUtils.convertDateTimeFromServer(data.evalArchEndDate);
                        data.evalArchDeadLineDate = DateUtils.convertDateTimeFromServer(data.evalArchDeadLineDate);
                        data.evalArchLastModifiedDate = DateUtils.convertDateTimeFromServer(data.evalArchLastModifiedDate);
                        data.evalArchConfirmedDate = DateUtils.convertDateTimeFromServer(data.evalArchConfirmedDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
