(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('EvaluationArchiveDialogController', EvaluationArchiveDialogController);

    EvaluationArchiveDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'EvaluationArchive', 'QuestionAnswer', 'ElectronicService'];

    function EvaluationArchiveDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, EvaluationArchive, QuestionAnswer, ElectronicService) {
        var vm = this;

        vm.evaluationArchive = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.questionanswers = QuestionAnswer.query();
        vm.electronicservices = ElectronicService.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.evaluationArchive.id !== null) {
                EvaluationArchive.update(vm.evaluationArchive, onSaveSuccess, onSaveError);
            } else {
                EvaluationArchive.save(vm.evaluationArchive, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('evaluationApp:evaluationArchiveUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.evalArchStartDate = false;
        vm.datePickerOpenStatus.evalArchEndDate = false;
        vm.datePickerOpenStatus.evalArchDeadLineDate = false;
        vm.datePickerOpenStatus.evalArchLastModifiedDate = false;
        vm.datePickerOpenStatus.evalArchConfirmedDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
