(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('EvaluationArchiveDetailController', EvaluationArchiveDetailController);

    EvaluationArchiveDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'EvaluationArchive', 'QuestionAnswer', 'ElectronicService'];

    function EvaluationArchiveDetailController($scope, $rootScope, $stateParams, previousState, entity, EvaluationArchive, QuestionAnswer, ElectronicService) {
        var vm = this;

        vm.evaluationArchive = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('evaluationApp:evaluationArchiveUpdate', function(event, result) {
            vm.evaluationArchive = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
