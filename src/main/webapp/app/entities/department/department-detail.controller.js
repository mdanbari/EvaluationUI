(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('DepartmentDetailController', DepartmentDetailController);

    DepartmentDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Department' , 'Locale'];

    function DepartmentDetailController($scope, $rootScope, $stateParams, previousState, entity, Department , Locale) {
        var vm = this;

        $scope.locale = Locale;
        vm.department = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('evaluationApp:departmentUpdate', function(event, result) {
            vm.department = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
