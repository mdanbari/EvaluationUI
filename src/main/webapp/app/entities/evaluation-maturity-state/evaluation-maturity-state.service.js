(function() {
    'use strict';
    angular
        .module('evaluationApp')
        .factory('EvaluationMaturityState', EvaluationMaturityState);

    EvaluationMaturityState.$inject = ['$resource'];

    function EvaluationMaturityState ($resource) {
        var resourceUrl =  'api/evaluation-maturity-states/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
