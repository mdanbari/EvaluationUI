(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('EvaluationMaturityStateDetailController', EvaluationMaturityStateDetailController);

    EvaluationMaturityStateDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'EvaluationMaturityState', 'Evaluation', 'MaturityStage'];

    function EvaluationMaturityStateDetailController($scope, $rootScope, $stateParams, previousState, entity, EvaluationMaturityState, Evaluation, MaturityStage) {
        var vm = this;

        vm.evaluationMaturityState = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('evaluationApp:evaluationMaturityStateUpdate', function(event, result) {
            vm.evaluationMaturityState = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
