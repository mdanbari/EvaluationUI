(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('EvaluationMaturityStateDeleteController',EvaluationMaturityStateDeleteController);

    EvaluationMaturityStateDeleteController.$inject = ['$uibModalInstance', 'entity', 'EvaluationMaturityState'];

    function EvaluationMaturityStateDeleteController($uibModalInstance, entity, EvaluationMaturityState) {
        var vm = this;

        vm.evaluationMaturityState = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            EvaluationMaturityState.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
