(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('EvaluationMaturityStateDialogController', EvaluationMaturityStateDialogController);

    EvaluationMaturityStateDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'EvaluationMaturityState', 'Evaluation', 'MaturityStage'];

    function EvaluationMaturityStateDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, EvaluationMaturityState, Evaluation, MaturityStage) {
        var vm = this;

        vm.evaluationMaturityState = entity;
        vm.clear = clear;
        vm.save = save;
        vm.evaluations = Evaluation.query();
        vm.maturitystages = MaturityStage.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.evaluationMaturityState.id !== null) {
                EvaluationMaturityState.update(vm.evaluationMaturityState, onSaveSuccess, onSaveError);
            } else {
                EvaluationMaturityState.save(vm.evaluationMaturityState, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('evaluationApp:evaluationMaturityStateUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
