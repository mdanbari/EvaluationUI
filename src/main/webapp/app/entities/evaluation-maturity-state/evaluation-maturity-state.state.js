(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('evaluation-maturity-state', {
            parent: 'entity',
            url: '/evaluation-maturity-state?page&sort&search',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.evaluationMaturityState.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/evaluation-maturity-state/evaluation-maturity-states.html',
                    controller: 'EvaluationMaturityStateController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('evaluationMaturityState');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('evaluation-maturity-state-detail', {
            parent: 'evaluation-maturity-state',
            url: '/evaluation-maturity-state/{id}',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.evaluationMaturityState.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/evaluation-maturity-state/evaluation-maturity-state-detail.html',
                    controller: 'EvaluationMaturityStateDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('evaluationMaturityState');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'EvaluationMaturityState', function($stateParams, EvaluationMaturityState) {
                    return EvaluationMaturityState.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'evaluation-maturity-state',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('evaluation-maturity-state-detail.edit', {
            parent: 'evaluation-maturity-state-detail',
            url: '/detail/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evaluation-maturity-state/evaluation-maturity-state-dialog.html',
                    controller: 'EvaluationMaturityStateDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['EvaluationMaturityState', function(EvaluationMaturityState) {
                            return EvaluationMaturityState.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('evaluation-maturity-state.new', {
            parent: 'evaluation-maturity-state',
            url: '/new',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evaluation-maturity-state/evaluation-maturity-state-dialog.html',
                    controller: 'EvaluationMaturityStateDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                evalMaturityStateScore: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('evaluation-maturity-state', null, { reload: 'evaluation-maturity-state' });
                }, function() {
                    $state.go('evaluation-maturity-state');
                });
            }]
        })
        .state('evaluation-maturity-state.edit', {
            parent: 'evaluation-maturity-state',
            url: '/{id}/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evaluation-maturity-state/evaluation-maturity-state-dialog.html',
                    controller: 'EvaluationMaturityStateDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['EvaluationMaturityState', function(EvaluationMaturityState) {
                            return EvaluationMaturityState.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('evaluation-maturity-state', null, { reload: 'evaluation-maturity-state' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('evaluation-maturity-state.delete', {
            parent: 'evaluation-maturity-state',
            url: '/{id}/delete',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evaluation-maturity-state/evaluation-maturity-state-delete-dialog.html',
                    controller: 'EvaluationMaturityStateDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['EvaluationMaturityState', function(EvaluationMaturityState) {
                            return EvaluationMaturityState.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('evaluation-maturity-state', null, { reload: 'evaluation-maturity-state' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
