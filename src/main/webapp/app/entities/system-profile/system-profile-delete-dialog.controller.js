(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('SystemProfileDeleteController',SystemProfileDeleteController);

    SystemProfileDeleteController.$inject = ['$uibModalInstance', 'entity', 'SystemProfile'];

    function SystemProfileDeleteController($uibModalInstance, entity, SystemProfile) {
        var vm = this;

        vm.systemProfile = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SystemProfile.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
