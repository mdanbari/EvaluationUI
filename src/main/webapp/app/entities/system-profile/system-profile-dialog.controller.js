(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('SystemProfileDialogController', SystemProfileDialogController);

    SystemProfileDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SystemProfile'];

    function SystemProfileDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, SystemProfile) {
        var vm = this;

        vm.systemProfile = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.systemProfile.id !== null) {
                SystemProfile.update(vm.systemProfile, onSaveSuccess, onSaveError);
            } else {
                SystemProfile.save(vm.systemProfile, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('evaluationApp:systemProfileUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
