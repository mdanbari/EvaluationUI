(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('SystemProfileDetailController', SystemProfileDetailController);

    SystemProfileDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SystemProfile'];

    function SystemProfileDetailController($scope, $rootScope, $stateParams, previousState, entity, SystemProfile) {
        var vm = this;

        vm.systemProfile = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('evaluationApp:systemProfileUpdate', function(event, result) {
            vm.systemProfile = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
