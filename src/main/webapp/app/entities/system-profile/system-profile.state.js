(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('system-profile', {
            parent: 'entity',
            url: '/system-profile?page&sort&search',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.systemProfile.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/system-profile/system-profiles.html',
                    controller: 'SystemProfileController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('systemProfile');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('system-profile-detail', {
            parent: 'system-profile',
            url: '/system-profile/{id}',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.systemProfile.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/system-profile/system-profile-detail.html',
                    controller: 'SystemProfileDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('systemProfile');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'SystemProfile', function($stateParams, SystemProfile) {
                    return SystemProfile.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'system-profile',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('system-profile-detail.edit', {
            parent: 'system-profile-detail',
            url: '/detail/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/system-profile/system-profile-dialog.html',
                    controller: 'SystemProfileDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SystemProfile', function(SystemProfile) {
                            return SystemProfile.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('system-profile.new', {
            parent: 'system-profile',
            url: '/new',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/system-profile/system-profile-dialog.html',
                    controller: 'SystemProfileDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                systemProfileKey: null,
                                systemProfileValue: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('system-profile', null, { reload: 'system-profile' });
                }, function() {
                    $state.go('system-profile');
                });
            }]
        })
        .state('system-profile.edit', {
            parent: 'system-profile',
            url: '/{id}/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/system-profile/system-profile-dialog.html',
                    controller: 'SystemProfileDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SystemProfile', function(SystemProfile) {
                            return SystemProfile.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('system-profile', null, { reload: 'system-profile' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('system-profile.delete', {
            parent: 'system-profile',
            url: '/{id}/delete',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/system-profile/system-profile-delete-dialog.html',
                    controller: 'SystemProfileDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SystemProfile', function(SystemProfile) {
                            return SystemProfile.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('system-profile', null, { reload: 'system-profile' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
