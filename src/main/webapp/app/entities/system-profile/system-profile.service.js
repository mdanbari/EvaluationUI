(function() {
    'use strict';
    angular
        .module('evaluationApp')
        .factory('SystemProfile', SystemProfile);

    SystemProfile.$inject = ['$resource'];

    function SystemProfile ($resource) {
        var resourceUrl =  'api/system-profiles/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
