(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('EvaluationDetailController', EvaluationDetailController);

    EvaluationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Evaluation', 'QuestionAnswer', 'ElectronicService'];

    function EvaluationDetailController($scope, $rootScope, $stateParams, previousState, entity, Evaluation, QuestionAnswer, ElectronicService) {
        var vm = this;

        vm.evaluation = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('evaluationApp:evaluationUpdate', function(event, result) {
            vm.evaluation = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
