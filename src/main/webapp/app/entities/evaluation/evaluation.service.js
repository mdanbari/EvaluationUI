(function() {
    'use strict';
    angular
        .module('evaluationApp')
        .factory('Evaluation', Evaluation);

    Evaluation.$inject = ['$resource', 'DateUtils'];

    function Evaluation ($resource, DateUtils) {
        var resourceUrl =  'api/evaluations/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.evalStartDate = DateUtils.convertDateTimeFromServer(data.evalStartDate);
                        data.evalEndDate = DateUtils.convertDateTimeFromServer(data.evalEndDate);
                        data.evalDeadLineDate = DateUtils.convertDateTimeFromServer(data.evalDeadLineDate);
                        data.evalLastModifiedDate = DateUtils.convertDateTimeFromServer(data.evalLastModifiedDate);
                        data.evalConfirmedDate = DateUtils.convertDateTimeFromServer(data.evalConfirmedDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
