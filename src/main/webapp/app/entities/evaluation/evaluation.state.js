(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('evaluation', {
            parent: 'entity',
            url: '/evaluation?page&sort&search',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.evaluation.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/evaluation/evaluations.html',
                    controller: 'EvaluationController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('evaluation');
                    $translatePartialLoader.addPart('evaluationState');
                    $translatePartialLoader.addPart('evaluationState');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('evaluation-detail', {
            parent: 'evaluation',
            url: '/evaluation/{id}',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.evaluation.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/evaluation/evaluation-detail.html',
                    controller: 'EvaluationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('evaluation');
                    $translatePartialLoader.addPart('evaluationState');
                    $translatePartialLoader.addPart('evaluationState');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Evaluation', function($stateParams, Evaluation) {
                    return Evaluation.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'evaluation',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('evaluation-detail.edit', {
            parent: 'evaluation-detail',
            url: '/detail/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evaluation/evaluation-dialog.html',
                    controller: 'EvaluationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Evaluation', function(Evaluation) {
                            return Evaluation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('evaluation.new', {
            parent: 'evaluation',
            url: '/new',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evaluation/evaluation-dialog.html',
                    controller: 'EvaluationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                evalTitlefa: null,
                                evalTitleen: null,
                                evalPerson: null,
                                evalStartDate: null,
                                evalEndDate: null,
                                evalDeadLineDate: null,
                                evalLastModifiedDate: null,
                                evalConfirmedDate: null,
                                evalState: null,
                                evalDescriptionfa: null,
                                evalDescriptionen: null,
                                evalConfirmer: null,
                                evalScore: null,
                                evaluationState: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('evaluation', null, { reload: 'evaluation' });
                }, function() {
                    $state.go('evaluation');
                });
            }]
        })
        .state('evaluation.edit', {
            parent: 'evaluation',
            url: '/{id}/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evaluation/evaluation-dialog.html',
                    controller: 'EvaluationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Evaluation', function(Evaluation) {
                            return Evaluation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('evaluation', null, { reload: 'evaluation' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('evaluation.delete', {
            parent: 'evaluation',
            url: '/{id}/delete',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evaluation/evaluation-delete-dialog.html',
                    controller: 'EvaluationDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Evaluation', function(Evaluation) {
                            return Evaluation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('evaluation', null, { reload: 'evaluation' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
