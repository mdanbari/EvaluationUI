(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('MaturityStageDialogController', MaturityStageDialogController);

    MaturityStageDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'MaturityStage'];

    function MaturityStageDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, MaturityStage) {
        var vm = this;

        vm.maturityStage = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.maturityStage.id !== null) {
                MaturityStage.update(vm.maturityStage, onSaveSuccess, onSaveError);
            } else {
                MaturityStage.save(vm.maturityStage, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('evaluationApp:maturityStageUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
