(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('maturity-stage', {
            parent: 'entity',
            url: '/maturity-stage?page&sort&search',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.maturityStage.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/maturity-stage/maturity-stages.html',
                    controller: 'MaturityStageController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('maturityStage');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('maturity-stage-detail', {
            parent: 'maturity-stage',
            url: '/maturity-stage/{id}',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.maturityStage.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/maturity-stage/maturity-stage-detail.html',
                    controller: 'MaturityStageDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('maturityStage');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'MaturityStage', function($stateParams, MaturityStage) {
                    return MaturityStage.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'maturity-stage',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('maturity-stage-detail.edit', {
            parent: 'maturity-stage-detail',
            url: '/detail/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/maturity-stage/maturity-stage-dialog.html',
                    controller: 'MaturityStageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MaturityStage', function(MaturityStage) {
                            return MaturityStage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('maturity-stage.new', {
            parent: 'maturity-stage',
            url: '/new',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/maturity-stage/maturity-stage-dialog.html',
                    controller: 'MaturityStageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                maturityStageTitlefa: null,
                                maturityStageTitleen: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('maturity-stage', null, { reload: 'maturity-stage' });
                }, function() {
                    $state.go('maturity-stage');
                });
            }]
        })
        .state('maturity-stage.edit', {
            parent: 'maturity-stage',
            url: '/{id}/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/maturity-stage/maturity-stage-dialog.html',
                    controller: 'MaturityStageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MaturityStage', function(MaturityStage) {
                            return MaturityStage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('maturity-stage', null, { reload: 'maturity-stage' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('maturity-stage.delete', {
            parent: 'maturity-stage',
            url: '/{id}/delete',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/maturity-stage/maturity-stage-delete-dialog.html',
                    controller: 'MaturityStageDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['MaturityStage', function(MaturityStage) {
                            return MaturityStage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('maturity-stage', null, { reload: 'maturity-stage' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
