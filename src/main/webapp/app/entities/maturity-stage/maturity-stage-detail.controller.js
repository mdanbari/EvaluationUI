(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('MaturityStageDetailController', MaturityStageDetailController);

    MaturityStageDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'MaturityStage'];

    function MaturityStageDetailController($scope, $rootScope, $stateParams, previousState, entity, MaturityStage) {
        var vm = this;

        vm.maturityStage = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('evaluationApp:maturityStageUpdate', function(event, result) {
            vm.maturityStage = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
