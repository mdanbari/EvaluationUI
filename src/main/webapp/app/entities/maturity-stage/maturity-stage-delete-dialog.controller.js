(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('MaturityStageDeleteController',MaturityStageDeleteController);

    MaturityStageDeleteController.$inject = ['$uibModalInstance', 'entity', 'MaturityStage'];

    function MaturityStageDeleteController($uibModalInstance, entity, MaturityStage) {
        var vm = this;

        vm.maturityStage = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            MaturityStage.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
