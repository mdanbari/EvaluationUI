(function() {
    'use strict';
    angular
        .module('evaluationApp')
        .factory('MaturityStage', MaturityStage);

    MaturityStage.$inject = ['$resource'];

    function MaturityStage ($resource) {
        var resourceUrl =  'api/maturity-stages/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
