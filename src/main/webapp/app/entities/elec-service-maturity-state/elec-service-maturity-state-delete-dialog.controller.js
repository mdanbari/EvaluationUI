(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('ElecServiceMaturityStateDeleteController',ElecServiceMaturityStateDeleteController);

    ElecServiceMaturityStateDeleteController.$inject = ['$uibModalInstance', 'entity', 'ElecServiceMaturityState'];

    function ElecServiceMaturityStateDeleteController($uibModalInstance, entity, ElecServiceMaturityState) {
        var vm = this;

        vm.elecServiceMaturityState = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ElecServiceMaturityState.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
