(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('ElecServiceMaturityStateDialogController', ElecServiceMaturityStateDialogController);

    ElecServiceMaturityStateDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ElecServiceMaturityState', 'MaturityStage', 'ElectronicService'];

    function ElecServiceMaturityStateDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ElecServiceMaturityState, MaturityStage, ElectronicService) {
        var vm = this;

        vm.elecServiceMaturityState = entity;
        vm.clear = clear;
        vm.save = save;
        vm.maturitystages = MaturityStage.query();
        vm.electronicservices = ElectronicService.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.elecServiceMaturityState.id !== null) {
                ElecServiceMaturityState.update(vm.elecServiceMaturityState, onSaveSuccess, onSaveError);
            } else {
                ElecServiceMaturityState.save(vm.elecServiceMaturityState, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('evaluationApp:elecServiceMaturityStateUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
