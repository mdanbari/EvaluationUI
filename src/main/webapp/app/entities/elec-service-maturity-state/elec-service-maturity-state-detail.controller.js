(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('ElecServiceMaturityStateDetailController', ElecServiceMaturityStateDetailController);

    ElecServiceMaturityStateDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ElecServiceMaturityState', 'MaturityStage', 'ElectronicService'];

    function ElecServiceMaturityStateDetailController($scope, $rootScope, $stateParams, previousState, entity, ElecServiceMaturityState, MaturityStage, ElectronicService) {
        var vm = this;

        vm.elecServiceMaturityState = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('evaluationApp:elecServiceMaturityStateUpdate', function(event, result) {
            vm.elecServiceMaturityState = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
