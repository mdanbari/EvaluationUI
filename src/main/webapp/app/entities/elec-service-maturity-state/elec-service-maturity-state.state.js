(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('elec-service-maturity-state', {
            parent: 'entity',
            url: '/elec-service-maturity-state?page&sort&search',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.elecServiceMaturityState.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/elec-service-maturity-state/elec-service-maturity-states.html',
                    controller: 'ElecServiceMaturityStateController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('elecServiceMaturityState');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('elec-service-maturity-state-detail', {
            parent: 'elec-service-maturity-state',
            url: '/elec-service-maturity-state/{id}',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.elecServiceMaturityState.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/elec-service-maturity-state/elec-service-maturity-state-detail.html',
                    controller: 'ElecServiceMaturityStateDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('elecServiceMaturityState');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ElecServiceMaturityState', function($stateParams, ElecServiceMaturityState) {
                    return ElecServiceMaturityState.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'elec-service-maturity-state',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('elec-service-maturity-state-detail.edit', {
            parent: 'elec-service-maturity-state-detail',
            url: '/detail/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/elec-service-maturity-state/elec-service-maturity-state-dialog.html',
                    controller: 'ElecServiceMaturityStateDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ElecServiceMaturityState', function(ElecServiceMaturityState) {
                            return ElecServiceMaturityState.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('elec-service-maturity-state.new', {
            parent: 'elec-service-maturity-state',
            url: '/new',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/elec-service-maturity-state/elec-service-maturity-state-dialog.html',
                    controller: 'ElecServiceMaturityStateDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                elecMaturityStateScore: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('elec-service-maturity-state', null, { reload: 'elec-service-maturity-state' });
                }, function() {
                    $state.go('elec-service-maturity-state');
                });
            }]
        })
        .state('elec-service-maturity-state.edit', {
            parent: 'elec-service-maturity-state',
            url: '/{id}/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/elec-service-maturity-state/elec-service-maturity-state-dialog.html',
                    controller: 'ElecServiceMaturityStateDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ElecServiceMaturityState', function(ElecServiceMaturityState) {
                            return ElecServiceMaturityState.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('elec-service-maturity-state', null, { reload: 'elec-service-maturity-state' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('elec-service-maturity-state.delete', {
            parent: 'elec-service-maturity-state',
            url: '/{id}/delete',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/elec-service-maturity-state/elec-service-maturity-state-delete-dialog.html',
                    controller: 'ElecServiceMaturityStateDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ElecServiceMaturityState', function(ElecServiceMaturityState) {
                            return ElecServiceMaturityState.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('elec-service-maturity-state', null, { reload: 'elec-service-maturity-state' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
