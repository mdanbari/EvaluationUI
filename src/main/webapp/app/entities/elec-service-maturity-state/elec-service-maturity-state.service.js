(function() {
    'use strict';
    angular
        .module('evaluationApp')
        .factory('ElecServiceMaturityState', ElecServiceMaturityState);

    ElecServiceMaturityState.$inject = ['$resource'];

    function ElecServiceMaturityState ($resource) {
        var resourceUrl =  'api/elec-service-maturity-states/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
