(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('AnswerDetailController', AnswerDetailController);

    AnswerDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Answer', 'Question' , 'Locale'];

    function AnswerDetailController($scope, $rootScope, $stateParams, previousState, entity, Answer, Question , Locale) {
        var vm = this;
        
        $scope.locale = Locale;
        vm.answer = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('evaluationApp:answerUpdate', function(event, result) {
            vm.answer = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
