(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('AnswerController', AnswerController);

    AnswerController.$inject = ['$state', 'Answer', 'ParseLinks', 'AlertService', 'paginationConstants', 'pagingParams' , '$scope' , 'Locale','$http'];

    function AnswerController($state, Answer, ParseLinks, AlertService, paginationConstants, pagingParams , $scope , Locale,$http) {

        var vm = this;

        $scope.locale = Locale;
        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;
        vm.itemsPerPage = paginationConstants.itemsPerPage;

		vm.search = search;
		vm.searchQuery = null;

		
        loadAll();

        function loadAll () {
            Answer.query({
                page: pagingParams.page - 1,
                size: vm.itemsPerPage,
                sort: sort()
            }, onSuccess, onError);
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.answers = data;
                vm.page = pagingParams.page;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function loadPage(page) {
            vm.page = page;
            vm.transition();
        }

        function transition() {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }
        

		function search() {
			if (!vm.searchQuery) {
				loadAll();
			} else {
				$http.get('/api/answer/search/' + vm.searchQuery + '/' + Locale.lang,
						{
							page : pagingParams.page - 1,
							size : vm.itemsPerPage,
							sort : sort()
						}).then(function(response) {
					vm.links = ParseLinks.parse(response.headers('link'));
					vm.totalItems = response.headers('X-Total-Count');
					vm.queryCount = vm.totalItems;
					vm.answers = response.data;
					vm.page = pagingParams.page;
				}, function(errResponse) {
					console.error('Error while loading department');
				});
			}

			function sort() {
				var result = [ vm.predicate + ','
						+ (vm.reverse ? 'asc' : 'desc') ];
				if (vm.predicate !== 'id') {
					result.push('id');
				}
				return result;
			}
		}
    }
})();
