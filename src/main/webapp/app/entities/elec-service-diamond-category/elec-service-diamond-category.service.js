(function() {
    'use strict';
    angular
        .module('evaluationApp')
        .factory('ElecServiceDiamondCategory', ElecServiceDiamondCategory);

    ElecServiceDiamondCategory.$inject = ['$resource'];

    function ElecServiceDiamondCategory ($resource) {
        var resourceUrl =  'api/elec-service-diamond-categories/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
