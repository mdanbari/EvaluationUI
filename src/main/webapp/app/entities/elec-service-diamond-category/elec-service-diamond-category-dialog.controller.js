(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('ElecServiceDiamondCategoryDialogController', ElecServiceDiamondCategoryDialogController);

    ElecServiceDiamondCategoryDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ElecServiceDiamondCategory', 'DiamondCategory', 'ElectronicService'];

    function ElecServiceDiamondCategoryDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ElecServiceDiamondCategory, DiamondCategory, ElectronicService) {
        var vm = this;

        vm.elecServiceDiamondCategory = entity;
        vm.clear = clear;
        vm.save = save;
        vm.diamondcategories = DiamondCategory.query();
        vm.electronicservices = ElectronicService.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.elecServiceDiamondCategory.id !== null) {
                ElecServiceDiamondCategory.update(vm.elecServiceDiamondCategory, onSaveSuccess, onSaveError);
            } else {
                ElecServiceDiamondCategory.save(vm.elecServiceDiamondCategory, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('evaluationApp:elecServiceDiamondCategoryUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
