(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('ElecServiceDiamondCategoryDeleteController',ElecServiceDiamondCategoryDeleteController);

    ElecServiceDiamondCategoryDeleteController.$inject = ['$uibModalInstance', 'entity', 'ElecServiceDiamondCategory'];

    function ElecServiceDiamondCategoryDeleteController($uibModalInstance, entity, ElecServiceDiamondCategory) {
        var vm = this;

        vm.elecServiceDiamondCategory = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ElecServiceDiamondCategory.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
