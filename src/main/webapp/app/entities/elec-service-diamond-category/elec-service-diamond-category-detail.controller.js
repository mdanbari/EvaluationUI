(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('ElecServiceDiamondCategoryDetailController', ElecServiceDiamondCategoryDetailController);

    ElecServiceDiamondCategoryDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ElecServiceDiamondCategory', 'DiamondCategory', 'ElectronicService'];

    function ElecServiceDiamondCategoryDetailController($scope, $rootScope, $stateParams, previousState, entity, ElecServiceDiamondCategory, DiamondCategory, ElectronicService) {
        var vm = this;

        vm.elecServiceDiamondCategory = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('evaluationApp:elecServiceDiamondCategoryUpdate', function(event, result) {
            vm.elecServiceDiamondCategory = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
