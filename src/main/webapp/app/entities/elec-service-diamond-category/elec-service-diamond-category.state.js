(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('elec-service-diamond-category', {
            parent: 'entity',
            url: '/elec-service-diamond-category?page&sort&search',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.elecServiceDiamondCategory.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/elec-service-diamond-category/elec-service-diamond-categories.html',
                    controller: 'ElecServiceDiamondCategoryController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('elecServiceDiamondCategory');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('elec-service-diamond-category-detail', {
            parent: 'elec-service-diamond-category',
            url: '/elec-service-diamond-category/{id}',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.elecServiceDiamondCategory.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/elec-service-diamond-category/elec-service-diamond-category-detail.html',
                    controller: 'ElecServiceDiamondCategoryDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('elecServiceDiamondCategory');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ElecServiceDiamondCategory', function($stateParams, ElecServiceDiamondCategory) {
                    return ElecServiceDiamondCategory.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'elec-service-diamond-category',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('elec-service-diamond-category-detail.edit', {
            parent: 'elec-service-diamond-category-detail',
            url: '/detail/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/elec-service-diamond-category/elec-service-diamond-category-dialog.html',
                    controller: 'ElecServiceDiamondCategoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ElecServiceDiamondCategory', function(ElecServiceDiamondCategory) {
                            return ElecServiceDiamondCategory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('elec-service-diamond-category.new', {
            parent: 'elec-service-diamond-category',
            url: '/new',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/elec-service-diamond-category/elec-service-diamond-category-dialog.html',
                    controller: 'ElecServiceDiamondCategoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                elecDiamondCategoryScore: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('elec-service-diamond-category', null, { reload: 'elec-service-diamond-category' });
                }, function() {
                    $state.go('elec-service-diamond-category');
                });
            }]
        })
        .state('elec-service-diamond-category.edit', {
            parent: 'elec-service-diamond-category',
            url: '/{id}/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/elec-service-diamond-category/elec-service-diamond-category-dialog.html',
                    controller: 'ElecServiceDiamondCategoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ElecServiceDiamondCategory', function(ElecServiceDiamondCategory) {
                            return ElecServiceDiamondCategory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('elec-service-diamond-category', null, { reload: 'elec-service-diamond-category' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('elec-service-diamond-category.delete', {
            parent: 'elec-service-diamond-category',
            url: '/{id}/delete',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/elec-service-diamond-category/elec-service-diamond-category-delete-dialog.html',
                    controller: 'ElecServiceDiamondCategoryDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ElecServiceDiamondCategory', function(ElecServiceDiamondCategory) {
                            return ElecServiceDiamondCategory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('elec-service-diamond-category', null, { reload: 'elec-service-diamond-category' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
