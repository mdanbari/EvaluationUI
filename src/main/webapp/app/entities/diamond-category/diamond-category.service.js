(function() {
    'use strict';
    angular
        .module('evaluationApp')
        .factory('DiamondCategory', DiamondCategory);

    DiamondCategory.$inject = ['$resource'];

    function DiamondCategory ($resource) {
        var resourceUrl =  'api/diamond-categories/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
