(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('DiamondCategoryDeleteController',DiamondCategoryDeleteController);

    DiamondCategoryDeleteController.$inject = ['$uibModalInstance', 'entity', 'DiamondCategory'];

    function DiamondCategoryDeleteController($uibModalInstance, entity, DiamondCategory) {
        var vm = this;

        vm.diamondCategory = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DiamondCategory.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
