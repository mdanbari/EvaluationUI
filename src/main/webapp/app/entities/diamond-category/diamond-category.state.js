(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('diamond-category', {
            parent: 'entity',
            url: '/diamond-category?page&sort&search',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.diamondCategory.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/diamond-category/diamond-categories.html',
                    controller: 'DiamondCategoryController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('diamondCategory');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('diamond-category-detail', {
            parent: 'diamond-category',
            url: '/diamond-category/{id}',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.diamondCategory.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/diamond-category/diamond-category-detail.html',
                    controller: 'DiamondCategoryDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('diamondCategory');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'DiamondCategory', function($stateParams, DiamondCategory) {
                    return DiamondCategory.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'diamond-category',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('diamond-category-detail.edit', {
            parent: 'diamond-category-detail',
            url: '/detail/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/diamond-category/diamond-category-dialog.html',
                    controller: 'DiamondCategoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DiamondCategory', function(DiamondCategory) {
                            return DiamondCategory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('diamond-category.new', {
            parent: 'diamond-category',
            url: '/new',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/diamond-category/diamond-category-dialog.html',
                    controller: 'DiamondCategoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                diamondCategoryTitlefa: null,
                                diamondCategoryTitleen: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('diamond-category', null, { reload: 'diamond-category' });
                }, function() {
                    $state.go('diamond-category');
                });
            }]
        })
        .state('diamond-category.edit', {
            parent: 'diamond-category',
            url: '/{id}/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/diamond-category/diamond-category-dialog.html',
                    controller: 'DiamondCategoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DiamondCategory', function(DiamondCategory) {
                            return DiamondCategory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('diamond-category', null, { reload: 'diamond-category' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('diamond-category.delete', {
            parent: 'diamond-category',
            url: '/{id}/delete',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/diamond-category/diamond-category-delete-dialog.html',
                    controller: 'DiamondCategoryDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DiamondCategory', function(DiamondCategory) {
                            return DiamondCategory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('diamond-category', null, { reload: 'diamond-category' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
