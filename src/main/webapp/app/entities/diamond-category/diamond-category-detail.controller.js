(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('DiamondCategoryDetailController', DiamondCategoryDetailController);

    DiamondCategoryDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'DiamondCategory', 'Question', 'ElectronicService'];

    function DiamondCategoryDetailController($scope, $rootScope, $stateParams, previousState, entity, DiamondCategory, Question, ElectronicService) {
        var vm = this;

        vm.diamondCategory = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('evaluationApp:diamondCategoryUpdate', function(event, result) {
            vm.diamondCategory = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
