(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('DiamondCategoryDialogController', DiamondCategoryDialogController);

    DiamondCategoryDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'DiamondCategory', 'Question', 'ElectronicService'];

    function DiamondCategoryDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, DiamondCategory, Question, ElectronicService) {
        var vm = this;

        vm.diamondCategory = entity;
        vm.clear = clear;
        vm.save = save;
        vm.questions = Question.query();
        vm.electronicservices = ElectronicService.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.diamondCategory.id !== null) {
                DiamondCategory.update(vm.diamondCategory, onSaveSuccess, onSaveError);
            } else {
                DiamondCategory.save(vm.diamondCategory, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('evaluationApp:diamondCategoryUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
