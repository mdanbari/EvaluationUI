(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('QuestionAnswerDetailController', QuestionAnswerDetailController);

    QuestionAnswerDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'QuestionAnswer', 'Question', 'Answer', 'Evaluation', 'EvaluationArchive'];

    function QuestionAnswerDetailController($scope, $rootScope, $stateParams, previousState, entity, QuestionAnswer, Question, Answer, Evaluation, EvaluationArchive) {
        var vm = this;

        vm.questionAnswer = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('evaluationApp:questionAnswerUpdate', function(event, result) {
            vm.questionAnswer = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
