(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('QuestionAnswerDialogController', QuestionAnswerDialogController);

    QuestionAnswerDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'QuestionAnswer', 'Question', 'Answer', 'Evaluation', 'EvaluationArchive'];

    function QuestionAnswerDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, QuestionAnswer, Question, Answer, Evaluation, EvaluationArchive) {
        var vm = this;

        vm.questionAnswer = entity;
        vm.clear = clear;
        vm.save = save;
        vm.questions = Question.query();
        vm.answers = Answer.query();
        vm.evaluations = Evaluation.query();
        vm.evaluationarchives = EvaluationArchive.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.questionAnswer.id !== null) {
                QuestionAnswer.update(vm.questionAnswer, onSaveSuccess, onSaveError);
            } else {
                QuestionAnswer.save(vm.questionAnswer, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('evaluationApp:questionAnswerUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
