(function() {
    'use strict';
    angular
        .module('evaluationApp')
        .factory('UnMaturityModel', UnMaturityModel);

    UnMaturityModel.$inject = ['$resource'];

    function UnMaturityModel ($resource) {
        var resourceUrl =  'api/un-maturity-models/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
