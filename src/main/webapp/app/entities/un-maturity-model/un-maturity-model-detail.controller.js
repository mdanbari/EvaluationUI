(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('UnMaturityModelDetailController', UnMaturityModelDetailController);

    UnMaturityModelDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UnMaturityModel'];

    function UnMaturityModelDetailController($scope, $rootScope, $stateParams, previousState, entity, UnMaturityModel) {
        var vm = this;

        vm.unMaturityModel = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('evaluationApp:unMaturityModelUpdate', function(event, result) {
            vm.unMaturityModel = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
