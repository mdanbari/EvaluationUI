(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('UnMaturityModelDialogController', UnMaturityModelDialogController);

    UnMaturityModelDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'UnMaturityModel'];

    function UnMaturityModelDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, UnMaturityModel) {
        var vm = this;

        vm.unMaturityModel = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.unMaturityModel.id !== null) {
                UnMaturityModel.update(vm.unMaturityModel, onSaveSuccess, onSaveError);
            } else {
                UnMaturityModel.save(vm.unMaturityModel, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('evaluationApp:unMaturityModelUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
