(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('UnMaturityModelDeleteController',UnMaturityModelDeleteController);

    UnMaturityModelDeleteController.$inject = ['$uibModalInstance', 'entity', 'UnMaturityModel'];

    function UnMaturityModelDeleteController($uibModalInstance, entity, UnMaturityModel) {
        var vm = this;

        vm.unMaturityModel = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            UnMaturityModel.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
