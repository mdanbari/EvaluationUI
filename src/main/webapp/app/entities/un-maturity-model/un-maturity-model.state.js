(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('un-maturity-model', {
            parent: 'entity',
            url: '/un-maturity-model?page&sort&search',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.unMaturityModel.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/un-maturity-model/un-maturity-models.html',
                    controller: 'UnMaturityModelController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('unMaturityModel');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('un-maturity-model-detail', {
            parent: 'un-maturity-model',
            url: '/un-maturity-model/{id}',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.unMaturityModel.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/un-maturity-model/un-maturity-model-detail.html',
                    controller: 'UnMaturityModelDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('unMaturityModel');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'UnMaturityModel', function($stateParams, UnMaturityModel) {
                    return UnMaturityModel.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'un-maturity-model',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('un-maturity-model-detail.edit', {
            parent: 'un-maturity-model-detail',
            url: '/detail/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/un-maturity-model/un-maturity-model-dialog.html',
                    controller: 'UnMaturityModelDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UnMaturityModel', function(UnMaturityModel) {
                            return UnMaturityModel.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('un-maturity-model.new', {
            parent: 'un-maturity-model',
            url: '/new',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/un-maturity-model/un-maturity-model-dialog.html',
                    controller: 'UnMaturityModelDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                unMaturityTitlefa: null,
                                unMaturityTitleen: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('un-maturity-model', null, { reload: 'un-maturity-model' });
                }, function() {
                    $state.go('un-maturity-model');
                });
            }]
        })
        .state('un-maturity-model.edit', {
            parent: 'un-maturity-model',
            url: '/{id}/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/un-maturity-model/un-maturity-model-dialog.html',
                    controller: 'UnMaturityModelDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UnMaturityModel', function(UnMaturityModel) {
                            return UnMaturityModel.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('un-maturity-model', null, { reload: 'un-maturity-model' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('un-maturity-model.delete', {
            parent: 'un-maturity-model',
            url: '/{id}/delete',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/un-maturity-model/un-maturity-model-delete-dialog.html',
                    controller: 'UnMaturityModelDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['UnMaturityModel', function(UnMaturityModel) {
                            return UnMaturityModel.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('un-maturity-model', null, { reload: 'un-maturity-model' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
