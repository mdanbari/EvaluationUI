(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('HelpFileDeleteController',HelpFileDeleteController);

    HelpFileDeleteController.$inject = ['$uibModalInstance', 'entity', 'HelpFile'];

    function HelpFileDeleteController($uibModalInstance, entity, HelpFile) {
        var vm = this;

        vm.helpFile = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            HelpFile.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
