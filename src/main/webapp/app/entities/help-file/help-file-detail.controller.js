(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('HelpFileDetailController', HelpFileDetailController);

    HelpFileDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'HelpFile'];

    function HelpFileDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, HelpFile) {
        var vm = this;

        vm.helpFile = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('evaluationApp:helpFileUpdate', function(event, result) {
            vm.helpFile = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
