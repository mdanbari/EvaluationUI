(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('help-file', {
            parent: 'entity',
            url: '/help-file?page&sort&search',
            data: {
                authorities: [],
                pageTitle: 'evaluationApp.helpFile.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/help-file/help-files.html',
                    controller: 'HelpFileController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('helpFile');
                    $translatePartialLoader.addPart('fileStatus');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('help-file-detail', {
            parent: 'help-file',
            url: '/help-file/{id}',
            data: {
                authorities: [],
                pageTitle: 'evaluationApp.helpFile.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/help-file/help-file-detail.html',
                    controller: 'HelpFileDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('helpFile');
                    $translatePartialLoader.addPart('fileStatus');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'HelpFile', function($stateParams, HelpFile) {
                    return HelpFile.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'help-file',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('help-file-detail.edit', {
            parent: 'help-file-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/help-file/help-file-dialog.html',
                    controller: 'HelpFileDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['HelpFile', function(HelpFile) {
                            return HelpFile.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('help-file.new', {
            parent: 'help-file',
            url: '/new',
            data: {
                authorities: ['ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/help-file/help-file-dialog.html',
                    controller: 'HelpFileDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                fileName: null,
                                fileUrl: null,
                                fileSize: null,
                                fileBlob: null,
                                fileBlobContentType: null,
                                fileIndex: null,
                                fileStatus: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('help-file', null, { reload: 'help-file' });
                }, function() {
                    $state.go('help-file');
                });
            }]
        })
        .state('help-file.edit', {
            parent: 'help-file',
            url: '/{id}/edit',
            data: {
                authorities: ['ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/help-file/help-file-dialog.html',
                    controller: 'HelpFileDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['HelpFile', function(HelpFile) {
                            return HelpFile.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('help-file', null, { reload: 'help-file' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('help-file.delete', {
            parent: 'help-file',
            url: '/{id}/delete',
            data: {
                authorities: ['ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/help-file/help-file-delete-dialog.html',
                    controller: 'HelpFileDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['HelpFile', function(HelpFile) {
                            return HelpFile.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('help-file', null, { reload: 'help-file' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
