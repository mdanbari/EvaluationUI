(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('HelpFileDialogController', HelpFileDialogController);

    HelpFileDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'HelpFile'];

    function HelpFileDialogController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, HelpFile) {
        var vm = this;

        vm.helpFile = entity;
        vm.clear = clear;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.helpFile.id !== null) {
                HelpFile.update(vm.helpFile, onSaveSuccess, onSaveError);
            } else {
                HelpFile.save(vm.helpFile, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('evaluationApp:helpFileUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


        vm.setFileBlob = function ($file, helpFile) {
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        helpFile.fileBlob = base64Data;
                        helpFile.fileBlobContentType = $file.type;
                    });
                });
            }
        };

    }
})();
