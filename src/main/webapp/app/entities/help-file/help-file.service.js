(function() {
    'use strict';
    angular
        .module('evaluationApp')
        .factory('HelpFile', HelpFile);

    HelpFile.$inject = ['$resource'];

    function HelpFile ($resource) {
        var resourceUrl =  'api/help-files/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
