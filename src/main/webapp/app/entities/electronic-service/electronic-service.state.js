(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('electronic-service', {
            parent: 'entity',
            url: '/electronic-service?page&sort&search',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.electronicService.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/electronic-service/electronic-services.html',
                    controller: 'ElectronicServiceController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('electronicService');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('electronic-service-detail', {
            parent: 'electronic-service',
            url: '/electronic-service/{id}',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.electronicService.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/electronic-service/electronic-service-detail.html',
                    controller: 'ElectronicServiceDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('electronicService');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ElectronicService', function($stateParams, ElectronicService) {
                    return ElectronicService.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'electronic-service',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('electronic-service-detail.edit', {
            parent: 'electronic-service-detail',
            url: '/detail/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/electronic-service/electronic-service-dialog.html',
                    controller: 'ElectronicServiceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ElectronicService', function(ElectronicService) {
                            return ElectronicService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('electronic-service.new', {
            parent: 'electronic-service',
            url: '/new',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/electronic-service/electronic-service-dialog.html',
                    controller: 'ElectronicServiceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                elecServiceTitlefa: null,
                                elecServiceTitleen: null,
                                electronicServiceCode: null,
                                elecServiceEvalCount: null,
                                elecServiceEvalTotalScore: null,
                                elecServiceEvalDate: null,
                                parentServicefa: null,
                                parentServiceen: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('electronic-service', null, { reload: 'electronic-service' });
                }, function() {
                    $state.go('electronic-service');
                });
            }]
        })
        .state('electronic-service.edit', {
            parent: 'electronic-service',
            url: '/{id}/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/electronic-service/electronic-service-dialog.html',
                    controller: 'ElectronicServiceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ElectronicService', function(ElectronicService) {
                            return ElectronicService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('electronic-service', null, { reload: 'electronic-service' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('electronic-service.delete', {
            parent: 'electronic-service',
            url: '/{id}/delete',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/electronic-service/electronic-service-delete-dialog.html',
                    controller: 'ElectronicServiceDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ElectronicService', function(ElectronicService) {
                            return ElectronicService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('electronic-service', null, { reload: 'electronic-service' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
