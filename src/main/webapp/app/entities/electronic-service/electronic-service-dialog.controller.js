(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('ElectronicServiceDialogController', ElectronicServiceDialogController);

    ElectronicServiceDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ElectronicService', 'Department', 'DiamondCategory' , 'Locale'];

    function ElectronicServiceDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ElectronicService, Department, DiamondCategory , Locale) {
        var vm = this;

        $scope.locale = Locale;
        vm.electronicService = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.departments = Department.query();
        vm.diamondcategories = DiamondCategory.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.electronicService.id !== null) {
                ElectronicService.update(vm.electronicService, onSaveSuccess, onSaveError);
            } else {
                ElectronicService.save(vm.electronicService, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('evaluationApp:electronicServiceUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.elecServiceEvalDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
