(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('ElectronicServiceDeleteController',ElectronicServiceDeleteController);

    ElectronicServiceDeleteController.$inject = ['$uibModalInstance', 'entity', 'ElectronicService'];

    function ElectronicServiceDeleteController($uibModalInstance, entity, ElectronicService) {
        var vm = this;

        vm.electronicService = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ElectronicService.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
