(function() {
    'use strict';
    angular
        .module('evaluationApp')
        .factory('ElectronicService', ElectronicService);

    ElectronicService.$inject = ['$resource', 'DateUtils'];

    function ElectronicService ($resource, DateUtils) {
        var resourceUrl =  'api/electronic-services/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.elecServiceEvalDate = DateUtils.convertDateTimeFromServer(data.elecServiceEvalDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
