(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('ElectronicServiceDetailController', ElectronicServiceDetailController);

    ElectronicServiceDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ElectronicService', 'Department', 'DiamondCategory' , 'Locale'];

    function ElectronicServiceDetailController($scope, $rootScope, $stateParams, previousState, entity, ElectronicService, Department, DiamondCategory , Locale) {
        var vm = this;

        $scope.locale = Locale;
        vm.electronicService = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('evaluationApp:electronicServiceUpdate', function(event, result) {
            vm.electronicService = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
