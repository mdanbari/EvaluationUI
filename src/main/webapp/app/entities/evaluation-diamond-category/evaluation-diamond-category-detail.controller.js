(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('EvaluationDiamondCategoryDetailController', EvaluationDiamondCategoryDetailController);

    EvaluationDiamondCategoryDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'EvaluationDiamondCategory', 'Evaluation', 'DiamondCategory'];

    function EvaluationDiamondCategoryDetailController($scope, $rootScope, $stateParams, previousState, entity, EvaluationDiamondCategory, Evaluation, DiamondCategory) {
        var vm = this;

        vm.evaluationDiamondCategory = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('evaluationApp:evaluationDiamondCategoryUpdate', function(event, result) {
            vm.evaluationDiamondCategory = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
