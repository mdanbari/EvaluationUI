(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('evaluation-diamond-category', {
            parent: 'entity',
            url: '/evaluation-diamond-category?page&sort&search',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.evaluationDiamondCategory.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/evaluation-diamond-category/evaluation-diamond-categories.html',
                    controller: 'EvaluationDiamondCategoryController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('evaluationDiamondCategory');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('evaluation-diamond-category-detail', {
            parent: 'evaluation-diamond-category',
            url: '/evaluation-diamond-category/{id}',
            data: {
                authorities: ['GENERAL','ADMIN'],
                pageTitle: 'evaluationApp.evaluationDiamondCategory.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/evaluation-diamond-category/evaluation-diamond-category-detail.html',
                    controller: 'EvaluationDiamondCategoryDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('evaluationDiamondCategory');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'EvaluationDiamondCategory', function($stateParams, EvaluationDiamondCategory) {
                    return EvaluationDiamondCategory.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'evaluation-diamond-category',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('evaluation-diamond-category-detail.edit', {
            parent: 'evaluation-diamond-category-detail',
            url: '/detail/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evaluation-diamond-category/evaluation-diamond-category-dialog.html',
                    controller: 'EvaluationDiamondCategoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['EvaluationDiamondCategory', function(EvaluationDiamondCategory) {
                            return EvaluationDiamondCategory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('evaluation-diamond-category.new', {
            parent: 'evaluation-diamond-category',
            url: '/new',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evaluation-diamond-category/evaluation-diamond-category-dialog.html',
                    controller: 'EvaluationDiamondCategoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                evalDiamondCategoryScore: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('evaluation-diamond-category', null, { reload: 'evaluation-diamond-category' });
                }, function() {
                    $state.go('evaluation-diamond-category');
                });
            }]
        })
        .state('evaluation-diamond-category.edit', {
            parent: 'evaluation-diamond-category',
            url: '/{id}/edit',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evaluation-diamond-category/evaluation-diamond-category-dialog.html',
                    controller: 'EvaluationDiamondCategoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['EvaluationDiamondCategory', function(EvaluationDiamondCategory) {
                            return EvaluationDiamondCategory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('evaluation-diamond-category', null, { reload: 'evaluation-diamond-category' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('evaluation-diamond-category.delete', {
            parent: 'evaluation-diamond-category',
            url: '/{id}/delete',
            data: {
                authorities: ['GENERAL','ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evaluation-diamond-category/evaluation-diamond-category-delete-dialog.html',
                    controller: 'EvaluationDiamondCategoryDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['EvaluationDiamondCategory', function(EvaluationDiamondCategory) {
                            return EvaluationDiamondCategory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('evaluation-diamond-category', null, { reload: 'evaluation-diamond-category' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
