(function() {
    'use strict';
    angular
        .module('evaluationApp')
        .factory('EvaluationDiamondCategory', EvaluationDiamondCategory);

    EvaluationDiamondCategory.$inject = ['$resource'];

    function EvaluationDiamondCategory ($resource) {
        var resourceUrl =  'api/evaluation-diamond-categories/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
