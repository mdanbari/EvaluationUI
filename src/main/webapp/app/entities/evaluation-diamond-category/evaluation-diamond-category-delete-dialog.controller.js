(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('EvaluationDiamondCategoryDeleteController',EvaluationDiamondCategoryDeleteController);

    EvaluationDiamondCategoryDeleteController.$inject = ['$uibModalInstance', 'entity', 'EvaluationDiamondCategory'];

    function EvaluationDiamondCategoryDeleteController($uibModalInstance, entity, EvaluationDiamondCategory) {
        var vm = this;

        vm.evaluationDiamondCategory = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            EvaluationDiamondCategory.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
