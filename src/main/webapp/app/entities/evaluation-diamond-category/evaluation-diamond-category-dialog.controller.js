(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .controller('EvaluationDiamondCategoryDialogController', EvaluationDiamondCategoryDialogController);

    EvaluationDiamondCategoryDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'EvaluationDiamondCategory', 'Evaluation', 'DiamondCategory'];

    function EvaluationDiamondCategoryDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, EvaluationDiamondCategory, Evaluation, DiamondCategory) {
        var vm = this;

        vm.evaluationDiamondCategory = entity;
        vm.clear = clear;
        vm.save = save;
        vm.evaluations = Evaluation.query();
        vm.diamondcategories = DiamondCategory.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.evaluationDiamondCategory.id !== null) {
                EvaluationDiamondCategory.update(vm.evaluationDiamondCategory, onSaveSuccess, onSaveError);
            } else {
                EvaluationDiamondCategory.save(vm.evaluationDiamondCategory, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('evaluationApp:evaluationDiamondCategoryUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
