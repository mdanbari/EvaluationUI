/**
 * hossein
 */

angular
		.module('evaluationApp')
		.directive(
				'evaluation',
				[
						'QuestionAnswer',
						'MaturityStage', '$http',
						function(QuestionAnswer, MaturityStage , $http) {

							var directive = {};

							directive.restrict = 'AE';

							directive.template = function(myVar) {
								var evaluation = myVar.attr("eval");

								var mode = myVar.attr("mode"); // general,
								// general-view
								// ,
								// specialized,
								// specialized-view,
								// expert,
								// expert-view,
								// judge

								var summary = '';

								// total score
								summary += '<div class="panel panel-success" style="color:black !important;">';

								summary += '<div class="panel-heading">';
								summary += '<span ng-show ="locale.lang==\'fa\'" data-translate="evaluationApp.evaluation.evalScore"></span><span>: {{'
										+ evaluation + '.evalScore.toFixed(2)}}</span>';
								summary += '</div>';
								summary += '</div>';

								// maturity stage score
								summary += '<div class="panel-group">';
								summary += ' <div class="panel panel-primary">';
								summary += '<div data-translate="global.menu.entities.maturityStage" class="panel-heading"></div>';

								summary += '<div class="panel-body" >';

								summary += ' <ol type="1" ng-repeat="ems in evalMaturityScore">';

								summary += '<div class="col-md-6" ng-show ="locale.lang==\'fa\'" style="font-weight: bold;">';
								summary += '{{ems.maturityStage.maturityStageTitlefa}} : {{ems.evalMaturityStateScore.toFixed(2)}}</div>';
								
								//mehdi
								summary+='<div class="col-md-6"> <uib-progressbar min="0" max="100" value="ems.evalMaturityStateScore" class="progress-striped active" type="{{ptype(ems.evalMaturityStateScore)}}"></div>'

								summary += '<div class="col-md-6" ng-show ="locale.lang==\'en\'" style="font-weight: bold;">';
								
								summary += '{{ems.maturityStage.maturityStageTitleen}} : {{ems.evalMaturityStateScore}}</div>';

								summary += '<hr class="style18">';
								summary += '</ol>';
								summary += '</div>';
								summary += '</div>';
								summary += '</div>';

								// diamond score
								summary += '<div class="panel-group">';
								summary += ' <div class="panel panel-primary">';
								summary += '<div data-translate="global.menu.entities.diamondCategory" class="panel-heading"></div>';

								summary += '<div class="panel-body">';

								summary += ' <ol type="1" ng-repeat="eds in evalDiamondScore">';

								summary += '<div class="col-md-6" ng-show ="locale.lang==\'fa\'" style="font-weight: bold;">';
								summary += '{{eds.diamondCategory.diamondCategoryTitlefa}} : {{eds.evalDiamondCategoryScore.toFixed(2)}}</div>';
								
								//mehdi
								summary+='<div class="col-md-6"> <uib-progressbar min="0" max="100" value="eds.evalDiamondCategoryScore" class="progress-striped active" type="{{ptype(eds.evalDiamondCategoryScore)}}"></div>'
								
								summary += '<div ng-show ="locale.lang==\'en\'" style="font-weight: bold;">';
								summary += '{{eds.diamondCategory.diamondCategoryTitleen}} : {{eds.evalDiamondCategoryScore}}</div>';

								summary += '<hr class="style18">';
								summary += '</ol>';
								summary += '</div>';
								summary += '</div>';
								summary += '</div>';

								var html = '';

								html += '<div class="panel panel-success">';

								html += '<div class="panel-heading">';
								html += '<span ng-show ="locale.lang==\'fa\'" data-translate="evaluationApp.electronicService.elecServiceTitlefa"></span><span ng-show ="locale.lang==\'fa\'">: {{'
										+ evaluation
										+ '.electronicService.elecServiceTitlefa}}</span>';
								html += '<span ng-show ="locale.lang==\'en\'" data-translate="evaluationApp.electronicService.elecServiceTitleen"></span><span ng-show ="locale.lang==\'en\'">: {{'
										+ evaluation
										+ '.electronicService.elecServiceTitleen}}</span>';
								html += '</div>';

								html += '<div class="panel-heading">';
								html += '<span ng-show ="locale.lang==\'fa\'" data-translate="evaluationApp.electronicService.department"></span><span ng-show ="locale.lang==\'fa\'"> : {{'
										+ evaluation
										+ '.electronicService.department.departmentNamefa}}</span>';
								html += '<span ng-show ="locale.lang==\'en\'" data-translate="evaluationApp.electronicService.department"></span><span ng-show ="locale.lang==\'en\'"> : {{'
										+ evaluation
										+ '.electronicService.department.departmentNameen}}</span>';
								html += '</div>';

								html += '<div class="panel-heading">';
								html += '<span data-translate="evaluationApp.evaluation.evalStartDate"></span> : <span ng-bind = "locale.toLocaleDate('
										+ evaluation
										+ '.evalStartDate)"></span>';
								html += '</div>';

								html += '<div class="panel-heading">';
								html += '<span data-translate="evaluationApp.evaluation.evalDeadLineDate"></span> : <span ng-bind = "locale.toLocaleDate('
										+ evaluation
										+ '.evalDeadLineDate)"></span>';
								html += '</div>';
								html += '<br>';

								if (mode == "general" || mode == "specialized") {

									html += ' <div class="panel-group">';
									html += ' <div class="panel panel-primary"';
									html += 'ng-repeat="maturity in maturitystages">';
									html += '<div ng-show ="locale.lang==\'fa\'" class="panel-heading">{{maturity.maturityStageTitlefa}}</div>';
									html += '<div ng-show ="locale.lang==\'en\'" class="panel-heading">{{maturity.maturityStageTitleen}}</div>';
									html += '<div class="panel-body">';
									html += ' <ol type="1" ng-repeat="q in '
											+ evaluation + '.questionAnswers"';
									html += 'ng-if="maturity.id == q.question.maturityStage.id">';

									html += '<div ng-show ="locale.lang==\'fa\'"><label class="question"> {{$index + 1}} -';
									html += '{{q.question.questionTitlefa}}</label></div>';

									html += '<div ng-show ="locale.lang==\'en\'"><label>  {{$index + 1}} -';
									html += '{{q.question.questionTitleen}}</label></div>';
									html += '<br>';
									html += '<div class="radio radio-success radio-inline" ng-repeat="a in q.question.answers"><input';
									html += ' type="radio" name = {{q.question.id}} id={{a.id}} value={{a.id}} ng-model="q.answer.id" '
											+ ' >';
									html += ' <label for={{a.id}} ng-show ="locale.lang==\'fa\'"> {{a.answerTitlefa}} </label><label for={{a.id}} ng-show ="locale.lang==\'en\'"> {{a.answerTitleen}} </label></div>';
									html += '<hr class="style18">';
									html += '</ol>';
									html += '</div>';
									html += '</div>';
									html += '</div>';

								} else if (mode == "specialized-view") {

									html += ' <div class="panel-group">';
									html += ' <div class="panel panel-primary"';
									html += 'ng-repeat="maturity in maturitystages">';
									html += '<div ng-show ="locale.lang==\'fa\'" class="panel-heading">{{maturity.maturityStageTitlefa}}</div>';
									html += '<div ng-show ="locale.lang==\'en\'" class="panel-heading">{{maturity.maturityStageTitleen}}</div>';
									html += '<div class="panel-body">';
									html += ' <ol type="1" ng-repeat="q in '
											+ evaluation	 + '.questionAnswers"';
									html += 'ng-if="maturity.id == q.question.maturityStage.id">';

									html += '<div ng-show ="locale.lang==\'fa\'"><label class="question"> {{$index + 1}} -';
									html += '{{q.question.questionTitlefa}}</label></div>';

									html += '<div ng-show ="locale.lang==\'en\'"><label>  {{$index + 1}} -';
									html += '{{q.question.questionTitleen}}</label></div>';
									html += '<br>';
									html += '<div class="radio radio-success radio-inline" ng-repeat="a in q.question.answers"><input';
									html += ' type="radio" name = {{q.question.id}} id={{a.id}} value={{a.id}} ng-model="q.answer.id" '
											+ ' disabled>';
									html += ' <label for={{a.id}} ng-show ="locale.lang==\'fa\'"> {{a.answerTitlefa}} </label><label for={{a.id}} ng-show ="locale.lang==\'en\'"> {{a.answerTitleen}} </label></div>';
									html += '<hr class="style18">';
									html += '</ol>';
									html += '</div>';
									html += '</div>';
									html += '</div>';
									

									html += ' <div class="panel-group">';
									html += ' <div class="panel panel-primary">';
									html += '<div data-translate="evaluationApp.evaluation.evalDescriptionfa" class="panel-heading"></div>';
									html += '<div class="panel-body">';
									
									html += '<textarea rows="4" style="width: 100%; resize: none;" ng-model="'+evaluation+'.evalDescriptionfa" readOnly></textarea>';

									html += ' </div>';
									html += ' </div>';
									html += ' </div>';

								} else if (mode == "general-view") {
									html += ' <div class="panel-group">';
									html += ' <div class="panel panel-primary"';
									html += 'ng-repeat="maturity in maturitystages">';
									html += '<div ng-show ="locale.lang==\'fa\'" class="panel-heading">{{maturity.maturityStageTitlefa}}</div>';
									html += '<div ng-show ="locale.lang==\'en\'" class="panel-heading">{{maturity.maturityStageTitleen}}</div>';
									html += '<div class="panel-body">';
									html += ' <ol type="1" ng-repeat="q in '
											+ evaluation + '.questionAnswers"';
									html += 'ng-if="maturity.id == q.question.maturityStage.id">';

									html += '<div ng-show ="locale.lang==\'fa\'"><label class="question"> {{$index + 1}} -';
									html += '{{q.question.questionTitlefa}}</label></div>';

									html += '<div ng-show ="locale.lang==\'en\'"><label>  {{$index + 1}} -';
									html += '{{q.question.questionTitleen}}</label></div>';
									html += '<br>';
									html += '<div class="radio radio-success radio-inline" ng-repeat="a in q.question.answers"><input';
									html += ' type="radio" name = {{q.question.id}} id={{a.id}} value={{a.id}} ng-model="q.answer.id" '
											+ ' disabled>';
									html += ' <label for={{a.id}} ng-show ="locale.lang==\'fa\'"> {{a.answerTitlefa}} </label><label for={{a.id}} ng-show ="locale.lang==\'en\'"> {{a.answerTitleen}} </label></div>';
									html += '<hr class="style18">';
									html += '</ol>';
									html += '</div>';
									html += '</div>';
									html += '</div>';
									html += '<br>';

									html += summary;

								} else if (mode == "expert") {
									html += ' <div class="panel-group">';
									html += ' <div class="panel panel-primary"';
									html += 'ng-repeat="maturity in maturitystages">';
									html += '<div ng-show ="locale.lang==\'fa\'" class="panel-heading">{{maturity.maturityStageTitlefa}}</div>';
									html += '<div ng-show ="locale.lang==\'en\'" class="panel-heading">{{maturity.maturityStageTitleen}}</div>';
									html += '<div class="panel-body">';
									html += ' <ol type="1" ng-repeat="q in '
											+ evaluation + '.questionAnswers"';
									html += 'ng-if="maturity.id == q.question.maturityStage.id">';

									html += '<div ng-show ="locale.lang==\'fa\'"><label class="question"> {{$index + 1}} -';
									html += '{{q.question.questionTitlefa}}</label></div>';

									html += '<div ng-show ="locale.lang==\'en\'"><label>  {{$index + 1}} -';
									html += '{{q.question.questionTitleen}}</label></div>';
									html += '<br>';
									html += '<div class="radio radio-success radio-inline" ng-repeat="a in q.question.answers"><input';
									html += ' type="radio" name = {{q.question.id}} id={{a.id}} value={{a.id}} ng-model="q.answer.id" '
											+ ' disabled>';
									html += ' <label for={{a.id}} ng-show ="locale.lang==\'fa\'"> {{a.answerTitlefa}} </label><label for={{a.id}} ng-show ="locale.lang==\'en\'"> {{a.answerTitleen}} </label></div>';
									html += '<hr class="style18">';
									html += '</ol>';
									html += '</div>';
									html += '</div>';
									html += '</div>';

									html += '<br>';
									html += summary;
									html += '<br>';

									html += ' <div class="panel-group">';
									html += ' <div class="panel panel-primary">';
									html += '<div data-translate="evaluationApp.evaluation.evalDescriptionfa" class="panel-heading"></div>';
									html += '<div class="panel-body">';
									
									html += '<textarea rows="4" style="width: 100%; resize: none;" ng-model="'+evaluation+'.evalDescriptionfa"></textarea>';

									html += ' </div>';
									html += ' </div>';
									html += ' </div>';
									
									html += '<center> <div>';

									var cancel = '<button type="button" class="btn btn-default" ng-click="reject()"><span class="glyphicon glyphicon-remove"></span>&nbsp;<span data-translate="entity.action.reject">Cancel</span></button>';

									var ok = '<button type="button" class="btn btn-primary" ng-click="confirm()"><span class="glyphicon glyphicon-ok"></span>&nbsp;<span data-translate="entity.action.confirm">Save</span></button';

									html += cancel;
									html += ok;

									html += '</div></center>';

								} else if (mode == "expert-view"){

									html += ' <div class="panel-group">';
									html += ' <div class="panel panel-primary"';
									html += 'ng-repeat="maturity in maturitystages">';
									html += '<div ng-show ="locale.lang==\'fa\'" class="panel-heading">{{maturity.maturityStageTitlefa}}</div>';
									html += '<div ng-show ="locale.lang==\'en\'" class="panel-heading">{{maturity.maturityStageTitleen}}</div>';
									html += '<div class="panel-body">';
									html += ' <ol type="1" ng-repeat="q in '
											+ evaluation + '.questionAnswers"';
									html += 'ng-if="maturity.id == q.question.maturityStage.id">';

									html += '<div ng-show ="locale.lang==\'fa\'"><label class="question"> {{$index + 1}} -';
									html += '{{q.question.questionTitlefa}}</label></div>';

									html += '<div ng-show ="locale.lang==\'en\'"><label>  {{$index + 1}} -';
									html += '{{q.question.questionTitleen}}</label></div>';
									html += '<br>';
									html += '<div class="radio radio-success radio-inline" ng-repeat="a in q.question.answers"><input';
									html += ' type="radio" name = {{q.question.id}} id={{a.id}} value={{a.id}} ng-model="q.answer.id" '
											+ ' disabled>';
									html += ' <label for={{a.id}} ng-show ="locale.lang==\'fa\'"> {{a.answerTitlefa}} </label><label for={{a.id}} ng-show ="locale.lang==\'en\'"> {{a.answerTitleen}} </label></div>';
									html += '<hr class="style18">';
									html += '</ol>';
									html += '</div>';
									html += '</div>';
									html += '</div>';
									html += '<br>';

									html += '<br>';
									html += summary;
									html += '<br>';

									html += ' <div class="panel-group">';
									html += ' <div class="panel panel-primary">';
									html += '<div data-translate="evaluationApp.evaluation.evalDescriptionfa" class="panel-heading"></div>';
									html += '<div class="panel-body">';
									
									html += '<textarea rows="4" style="width: 100%; resize: none;" ng-model="'+evaluation+'.evalDescriptionfa" readOnly></textarea>';

									html += ' </div>';
									html += ' </div>';
									html += ' </div>';
																		
								}else if(mode == "judge"){
									
									html += ' <div class="panel-group">';
									html += ' <div class="panel panel-primary"';
									html += 'ng-repeat="maturity in maturitystages">';
									html += '<div ng-show ="locale.lang==\'fa\'" class="panel-heading">{{maturity.maturityStageTitlefa}}</div>';
									html += '<div ng-show ="locale.lang==\'en\'" class="panel-heading">{{maturity.maturityStageTitleen}}</div>';
									html += '<div class="panel-body">';
									html += ' <ol type="1" ng-repeat="q in '
											+ evaluation + '.questionAnswers"';
									html += 'ng-if="maturity.id == q.question.maturityStage.id">';

									html += '<div ng-show ="locale.lang==\'fa\'"><label class="question"> {{$index + 1}} -';
									html += '{{q.question.questionTitlefa}}</label></div>';

									html += '<div ng-show ="locale.lang==\'en\'"><label>  {{$index + 1}} -';
									html += '{{q.question.questionTitleen}}</label></div>';
									html += '<br>';
									html += '<div class="radio radio-success radio-inline" ng-repeat="a in q.question.answers"><input';
									html += ' type="radio" name = {{q.question.id}} id={{a.id}} value={{a.id}} ng-model="q.answer.id" '
											+ ' disabled>';
									html += ' <label for={{a.id}} ng-show ="locale.lang==\'fa\'"> {{a.answerTitlefa}} </label><label for={{a.id}} ng-show ="locale.lang==\'en\'"> {{a.answerTitleen}} </label></div>';
									html += '<hr class="style18">';
									html += '</ol>';
									html += '</div>';
									html += '</div>';
									html += '</div>';
									html += '<br>';

									html += '<br>';
									html += summary;
									html += '<br>';

									html += ' <div class="panel-group">';
									html += ' <div class="panel panel-primary">';
									html += '<div data-translate="evaluationApp.evaluation.evalDescriptionfa" class="panel-heading"></div>';
									html += '<div class="panel-body">';
									
									html += '<textarea rows="4" style="width: 100%; resize: none;" ng-model="'+evaluation+'.evalDescriptionfa" readOnly></textarea>';

									html += ' </div>';
									html += ' </div>';
									html += ' </div>';
									
									
									html += ' <div class="panel-group">';
									html += ' <div class="panel panel-primary">';
									html += '<div data-translate="evaluationApp.evaluation.evalJudgeDescriptionfa" class="panel-heading"></div>';
									html += '<div class="panel-body">';
									
									html += '<textarea rows="4" style="width: 100%; resize: none;" ng-model="'+evaluation+'.evalJudgeDescriptionfa" ></textarea>';
									
									html += ' </div>';
									html += ' </div>';
									html += ' </div>';

									html += '<center> <div>';

									var ok = '<button type="button" class="btn btn-primary" ng-click="confirm()"><span class="glyphicon glyphicon-ok"></span>&nbsp;<span data-translate="entity.action.confirm">Save</span></button';

									//html += ok;

									html += '</div></center>';

								}

								return html;

							}

							directive.scope = false;

							directive.link = function($scope , element , myAttr) {
								$scope.maturitystages = MaturityStage.query();
								$scope.ptype=function(val)
								{
									if(val<40)
										return "danger";
										else if(val>40 && val<70)
											return "warning";
											else
												return "success";
								}

								var evaluation = myAttr.eval;
								
//								$scope.updateSelectedAnswer = function(
//										question, answer) {
//									question.answer = answer;
//									question.evaluation = {};
//									question.evaluation.id = $scope[evaluation].id;
//									QuestionAnswer.update(question, function(
//											success) {
//									}, function(error) {
//									});
//								}
								

								$http.get("api/evaluation-maturity-states-by-eval-id/" + parseInt($scope[evaluation].id)).then(
										function(response) {
											return $scope.evalMaturityScore = response.data;
										}, function(errResponse) {
											console.error('Error while loading evaluation maturity state : ' + errResponse.data);
										});
								

								$http.get("/api/evaluation-diamond-categories-by-eval-id/" + parseInt($scope[evaluation].id)).then(
										function(response) {
											return $scope.evalDiamondScore= response.data;
										}, function(errResponse) {
											console.error('Error while loading evaluation diamond category: ' + errResponse.data);
										});
							}
							return directive;
						} ]);
