'use strict';
angular
.module('evaluationApp')
		.directive(
				'datebox',
				[
					'Locale',
				function(Locale) {
					var directive = {};
					directive.restrict = 'AE';
					directive.template = function(myVar) {

						var ngModel = myVar.attr("ng-model");

						var pattern = '\'/^(13\\d\\d|14\\d\\d)\/(0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01])$/\'';

						var text = '<input class="form-control" type="text" style="direction: ltr !important;" ui-mask="9999/99/99"  model-view-value="true" ng-model="'
							+ ngModel
							+ '" placeholder="____/__/__" ng-pattern= '
							+ pattern +  ' />';
					
						return text;

					}

					directive.link = function($scope, elem, attr, ctrl) {
						$scope.locale = Locale;
					}
					directive.scope = false;
					return directive;
				} ]);