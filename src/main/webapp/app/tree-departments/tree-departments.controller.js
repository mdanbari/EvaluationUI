(function() {
	'use strict';

	angular.module('evaluationApp').controller('TreeDepartmentsController',
			TreeDepartmentsController);

	TreeDepartmentsController.$inject = [ '$scope', '$state', '$http' , '$translate' , 'Locale'];

	function TreeDepartmentsController($scope, $state, $http , $translate,Locale) {
		var vm = this;
		debugger;
		$scope.locale = Locale;
		$scope.treeNodes = [];
		$scope.$on('selection-changed', function(e, node) {
			$http.get(
					"/api/findallchildrenofdepartment/"
							+ parseInt(node.id)).then(function(response) {
				if (response.data.length > 0) {
					node.children = [];
					node.children.push.apply(node.children, response.data);
				}
			}, function(errResponse) {
				console.error('Error while loading department');
			});

		});

		$http.get("/api/departmentsparentisnull", {
		}).then(
				function(response) {
					return $scope.treeNodes.push.apply($scope.treeNodes,
							response.data);
				}, function(errResponse) {
					console.error('Error while loading department');
				});
	}
})();
