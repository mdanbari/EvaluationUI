(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('tree-departments', {
            parent: 'entity',
            url: '/tree-departments',
            data: {
                authorities: ['ADMIN']
            },
            views: {
                'content@': {
                    templateUrl: 'app/tree-departments/tree-departments.html',
                    controller: 'TreeDepartmentsController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('tree-departments');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
