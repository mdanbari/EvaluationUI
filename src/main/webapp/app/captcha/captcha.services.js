(function() {
    'use strict';

    angular
        .module('evaluationApp')
        .factory('Captcha', Captcha);

    Captcha.$inject = ['$resource'];

    function Captcha ($resource) {
        return $resource('api/captcha/',{},{
        	create:{method:'GET',url:'api/captcha/createCaptcha'},
        	valid:{method:'GET',url:'api/captcha/validateCaptcha/:captchaValue'}
        });
    }
})();