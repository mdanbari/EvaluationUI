'use strict';
angular
.module('evaluationApp')
		.directive(
				'datebox',
				[

				function() {
					var directive = {};
					directive.restrict = 'AE';
					directive.template = function(myVar) {

						var ngModel = myVar.attr("ng-model");
						var label = myVar.attr("label");
						var required = myVar.attr("required");
						var disabled = myVar.attr("disabled");

						var requiredLab = '';
						
						if (required == undefined){
							required = '';
							requiredLab = '';
						}
						else{
							required = 'required';
							requiredLab = '<span style="color : #b20505 !important">* </span>';
						}

						
						
						if(label== undefined)
							label ='';
						else
							label = '<span class="labelForm icon"><span class="txt">' + requiredLab + label + '</span></span>';
							
						
						if(disabled == undefined)
							disabled = 'false';
						else
							disabled = 'true';

					var	size = "medium-" + 4 + " large-" + 4 + " columns";
						myVar.attr("class", size);

						var pattern = '\'/^(13\\d\\d|14\\d\\d)\/(0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01])$/\'';

						
						var text = '<input class="txtInput" type="text" style="direction: ltr !important;" ui-mask="9999/99/99"  model-view-value="true" ng-model="'
							+ ngModel
							+ '" placeholder="____/__/__" ng-pattern='
							+ pattern + ' ng-disabled = "'+disabled+ '"' + required +  '/>';
						
						var content = '<div class="row collapse rowPad12">' + label + text + '</div>' ;		

						return content;

					}

					directive.link = function($scope, elem, attr, ctrl) {

					}
					directive.scope = false;
					return directive;
				} ]);