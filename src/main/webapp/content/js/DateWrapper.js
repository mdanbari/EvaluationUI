function toJalali(year,month,day)
{
	var param= JalaliDate.gregorianToJalali(year,month,day);
	
	return param[0] + "/"+param[1] + "/"+param[2] ;

}
function toMiladi(year,month,day)
{
	var param= JalaliDate.jalaliToGregorian(year,month,day);
	
	return param[0] + "/"+param[1] + "/"+param[2] ;

}


function toJalaliDate(year, month, day){
	
	var param= JalaliDate.gregorianToJalali(year,getMonth(month),day);
	
	return param[0] + "/"+param[1] + "/"+param[2] ;
	
	
}


function getMonth(month){

	var months = ["", "Jan" , "Feb" , "Mar" , "Apr" , "May" , "Jun" , "Jul" , "Aug" , "Sep" , "Oct" , "Nov" , "Dec"];

	return months.indexOf(month);

	}

