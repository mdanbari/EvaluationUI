package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.EvaluationDiamondCategory;

/**
 * Spring Data JPA repository for the EvaluationDiamondCategory entity.
 */
@Repository
public interface EvaluationDiamondCategoryRepository extends JpaRepository<EvaluationDiamondCategory, Long> {

	public List<EvaluationDiamondCategory> findAllByEvaluationIdIn(List<Long> ids);

}
