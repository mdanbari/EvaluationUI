package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.Department;
import com.mycompany.myapp.service.dto.ChildrenDepartment;

/**
 * Spring Data JPA repository for the Department entity.
 */
@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {

	/**
	 * get root department
	 * 
	 * @return
	 */
	@Query("select new com.mycompany.myapp.service.dto.ChildrenDepartment(d.id , d.departmentNamefa , d.departmentNameen , d.electronicServiceCount) from Department d where d.parent is null")
	public List<ChildrenDepartment> findAllByParentIsNull();

	/**
	 * @param id
	 * @return
	 */
	@Query("select new com.mycompany.myapp.service.dto.ChildrenDepartment(d.id , d.departmentNamefa, d.departmentNameen , d.electronicServiceCount) from Department d where d.parent.id = ?1")
	public List<ChildrenDepartment> findAllChildrenOfDepartment(Long id);

	@Query("select count(d.id) from Department d where d.parent.id = ?1")
	public Long findCountAllChildrenOfDepartment(Long id);

	public Department findTop1ByDepartmentNamefa(String name);

	@Query(value = "select d from Department d left outer join d.parent p where (('fa' = ?2) and (d.departmentNamefa like %?1%  or  p.departmentNamefa like %?1%)) or (('en' = ?2) and (d.departmentNameen like %?1% or  p.departmentNameen like %?1%))", 
		countQuery = "select count(*) from Department d left outer join d.parent p where (('fa' = ?2) and (d.departmentNamefa like %?1%  or  p.departmentNamefa like %?1%)) or (('en' = ?2) and (d.departmentNameen like %?1% or  p.departmentNameen like %?1%))")
	public Page<Department> search(String search, String local , Pageable pageable);
	
	@Query("select d from Department d where d.departmentNamefa like %?1%")
	public List<Department> findByDepartmentName(String departmentName);
}
