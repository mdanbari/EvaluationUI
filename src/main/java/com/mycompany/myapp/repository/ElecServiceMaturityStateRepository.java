package com.mycompany.myapp.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.ElecServiceMaturityState;
import com.mycompany.myapp.domain.MaturityStage;
import com.mycompany.myapp.service.dto.SeparationChartReportJpaDTO;
import com.mycompany.myapp.service.dto.SeparationChartReportResultDTO;

import io.swagger.annotations.ApiParam;

/**
 * Spring Data JPA repository for the ElecServiceMaturityState entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ElecServiceMaturityStateRepository extends JpaRepository<ElecServiceMaturityState, Long> {

	@Query(value = "SELECT new com.mycompany.myapp.service.dto.SeparationChartReportJpaDTO"
			+ "(esm.maturityStage.id , avg(esm.elecMaturityStateScore)) "
			+ "FROM ElecServiceMaturityState esm "
			+ "WHERE esm.electronicService.id = :elecId AND esm.maturityStage.id in :maturitySelecteds "
			+ "GROUP BY esm.maturityStage.id ORDER BY esm.maturityStage.id asc")
	List<SeparationChartReportJpaDTO> generateElecServiceMaturityReport(@Param("elecId") Long elecId,
			@Param("maturitySelecteds")List<Long> maturitySelecteds);

	@Query(value = "SELECT new com.mycompany.myapp.service.dto.SeparationChartReportJpaDTO"
			+ "(esm.maturityStage.id , avg(esm.elecMaturityStateScore)) "
			+ "FROM ElecServiceMaturityState esm "
			+ "WHERE esm.electronicService.id = :elecId AND esm.maturityStage.id in :maturitySelecteds AND esm.elecMaturityDate BETWEEN :fromDate AND :toDate "
			+ "GROUP BY esm.maturityStage.id ORDER BY esm.maturityStage.id asc")
	List<SeparationChartReportJpaDTO>  generateElecServiceMaturityReport(@Param("elecId")Long elecId,
			@Param("maturitySelecteds")List<Long> maturitySelecteds, @Param("fromDate")ZonedDateTime fromDate, @Param("toDate")ZonedDateTime toDate);

	

	

	

}
