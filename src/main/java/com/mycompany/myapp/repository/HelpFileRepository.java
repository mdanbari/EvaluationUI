package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.HelpFile;


/**
 * Spring Data JPA repository for the HelpFile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HelpFileRepository extends JpaRepository<HelpFile, Long> {

}
