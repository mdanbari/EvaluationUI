package com.mycompany.myapp.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.ElecServiceDiamondCategory;
import com.mycompany.myapp.service.dto.SeparationChartReportJpaDTO;


/**
 * Spring Data JPA repository for the ElecServiceDiamondCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ElecServiceDiamondCategoryRepository extends JpaRepository<ElecServiceDiamondCategory, Long> {

	@Query(value = "SELECT new com.mycompany.myapp.service.dto.SeparationChartReportJpaDTO "
			+ "(esd.diamondCategory.id , avg(esd.elecDiamondCategoryScore)) "
			+ "FROM ElecServiceDiamondCategory esd "
			+ "WHERE  esd.electronicService.id = :elecId AND esd.diamondCategory.id in :diamondIdList "
			+ "GROUP BY esd.diamondCategory.id ORDER BY esd.diamondCategory.id asc")
	List<SeparationChartReportJpaDTO> generateElecServiceDiamondReport(@Param("elecId")Long elecId, @Param("diamondIdList")List<Long> diamondIdList);

	@Query(value = "SELECT new com.mycompany.myapp.service.dto.SeparationChartReportJpaDTO "
			+ "(esd.diamondCategory.id,avg(esd.elecDiamondCategoryScore)) "
			+ "FROM ElecServiceDiamondCategory esd "
			+ "WHERE esd.electronicService.id = :elecId AND esd.diamondCategory.id in :diamondIdList AND esd.elecDiamondDate BETWEEN :fromDate AND :toDate "
			+ "GROUP BY esd.diamondCategory.id ORDER BY esd.diamondCategory.id asc")
	List<SeparationChartReportJpaDTO> generateElecServiceDiamondReport(@Param("elecId")Long id, @Param("diamondIdList")List<Long> diamondIdList,
			@Param("fromDate")ZonedDateTime fromDate, @Param("toDate")ZonedDateTime toDate);
	
}
