package com.mycompany.myapp.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.Evaluation;
import com.mycompany.myapp.domain.enumeration.EvaluationState;

/**
 * Spring Data JPA repository for the Evaluation entity.
 */
@Repository
public interface EvaluationRepository extends JpaRepository<Evaluation, Long> {

	List<Evaluation> findByEvaluationStateAndEvalDeadLineDateLessThan(EvaluationState inprogress, ZonedDateTime now);

	@Query("select e.id from Evaluation e where e.electronicService.id =?1 and e.evaluationState =?2 order by e.evalConfirmedDate desc")
	public List<Long> findAllByElecServiceAndEvaluationStateOrderByEvalConfirmedDateDesc(
			Long elecId, EvaluationState evaluationState);

}
