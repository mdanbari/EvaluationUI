package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.UnMaturityModel;


/**
 * Spring Data JPA repository for the UnMaturityModel entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UnMaturityModelRepository extends JpaRepository<UnMaturityModel, Long> {

}
