package com.mycompany.myapp.repository.initial;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mycompany.myapp.config.Constants;
import com.mycompany.myapp.domain.SystemProfile;
import com.mycompany.myapp.repository.SystemProfileRepository;




@Component
public class InitialData {
	
	@Autowired
	SystemProfileRepository systemProfileRepository;
	
	
	@PostConstruct
	private void initial()
	{
		
		SystemProfile systemProfilePurgeTask = new SystemProfile();
		SystemProfile systemProfileArchiveTask = new SystemProfile();
		SystemProfile systemProfileDataInitialTask = new SystemProfile();
		SystemProfile systemProfilePurgeInterval = new SystemProfile();
		SystemProfile systemProfileArchiveInterval = new SystemProfile();
		SystemProfile systemProfileDataInitialInterval = new SystemProfile();
		SystemProfile systemProfileDataInitialFilePath = new SystemProfile();
		SystemProfile systemProfileelecServiceInitialFilePath = new SystemProfile();
		SystemProfile systemProfileElecConfirmCount = new SystemProfile();
		SystemProfile systemProfileDataReportInitialPath=new SystemProfile();
		SystemProfile systemProfileQuestionDataReportInitialPath=new SystemProfile();
		
		
		systemProfilePurgeTask.systemProfileKey(Constants.EVAL_PURG_TASK_ENABLE);
		systemProfilePurgeTask.systemProfileValue("1");
		systemProfileArchiveTask.setSystemProfileKey(Constants.EVAL_ARCHIVE_TASK_ENABLE);
		systemProfileArchiveTask.systemProfileValue("1");
		systemProfileDataInitialTask.setSystemProfileKey(Constants.EVAL_DATA_INIT_TASK_ENABLE);
		systemProfileDataInitialTask.systemProfileValue("1");
		systemProfilePurgeInterval.systemProfileKey(Constants.EVAL_PURG_TASK_INTERVAL);
		systemProfilePurgeInterval.systemProfileValue("100");
		systemProfileArchiveInterval.setSystemProfileKey(Constants.EVAL_ARCHIVE_TASK_INTERVAL);
		systemProfileArchiveInterval.systemProfileValue("100");
		systemProfileDataInitialInterval.setSystemProfileKey(Constants.EVAL_DATA_INIT_TASK_INTERVAL);
		systemProfileDataInitialInterval.systemProfileValue("100");
		systemProfileDataInitialFilePath.setSystemProfileKey(Constants.EVAL_INITIALDATA_QUESTION_FILE_PATH);
		systemProfileDataInitialFilePath.systemProfileValue("D:\\sample.xlsx");
		systemProfileelecServiceInitialFilePath.setSystemProfileKey(Constants.EVAL_INITIALDATA_ELECTRONICSERVICE_FILE_PATH);
		systemProfileelecServiceInitialFilePath.systemProfileValue("D:\\sample_of_khedmat.xlsx");
		systemProfileElecConfirmCount.setSystemProfileKey(Constants.EVAL_DEFAULT_ELEC_CONFIRM_COUNT);
		systemProfileElecConfirmCount.systemProfileValue("2");
		systemProfileDataReportInitialPath.setSystemProfileKey(Constants.EVAL_INITIALDATA_REPORT_FILE_PATH);
		systemProfileDataReportInitialPath.setSystemProfileValue("D:\\sample_of_gozaresh.xlsx");
		systemProfileQuestionDataReportInitialPath.setSystemProfileKey(Constants.EVAL_INITIALDATA_QUESTION_REPORT_FILE_PATH);
		systemProfileQuestionDataReportInitialPath.setSystemProfileValue("D:\\sample_of_question_gozaresh.xlsx");
		
		
		if(systemProfileRepository.findAllBySystemProfileKey(Constants.EVAL_PURG_TASK_ENABLE).size() == 0)
			systemProfileRepository.save(systemProfilePurgeTask);
		if(systemProfileRepository.findAllBySystemProfileKey(Constants.EVAL_ARCHIVE_TASK_ENABLE).size() == 0)
			systemProfileRepository.save(systemProfileArchiveTask);
		if(systemProfileRepository.findAllBySystemProfileKey(Constants.EVAL_DATA_INIT_TASK_ENABLE).size() == 0)
			systemProfileRepository.save(systemProfileDataInitialTask);
		if(systemProfileRepository.findAllBySystemProfileKey(Constants.EVAL_PURG_TASK_INTERVAL).size() == 0)		
			systemProfileRepository.save(systemProfilePurgeInterval);
		if(systemProfileRepository.findAllBySystemProfileKey(Constants.EVAL_ARCHIVE_TASK_INTERVAL).size() == 0)
			systemProfileRepository.save(systemProfileArchiveInterval);
		if(systemProfileRepository.findAllBySystemProfileKey(Constants.EVAL_DATA_INIT_TASK_INTERVAL).size() == 0)
			systemProfileRepository.save(systemProfileDataInitialInterval);
		if(systemProfileRepository.findAllBySystemProfileKey(Constants.EVAL_INITIALDATA_QUESTION_FILE_PATH).size() == 0)
			systemProfileRepository.save(systemProfileDataInitialFilePath);
		if(systemProfileRepository.findAllBySystemProfileKey(Constants.EVAL_INITIALDATA_ELECTRONICSERVICE_FILE_PATH).size() == 0)
			systemProfileRepository.save(systemProfileelecServiceInitialFilePath);
		if(systemProfileRepository.findAllBySystemProfileKey(Constants.EVAL_DEFAULT_ELEC_CONFIRM_COUNT).size() == 0)
			systemProfileRepository.save(systemProfileElecConfirmCount);
		if(systemProfileRepository.findAllBySystemProfileKey(Constants.EVAL_INITIALDATA_REPORT_FILE_PATH).size() == 0)
			systemProfileRepository.save(systemProfileDataReportInitialPath);
		if(systemProfileRepository.findAllBySystemProfileKey(Constants.EVAL_INITIALDATA_QUESTION_REPORT_FILE_PATH).size()==0)
			systemProfileRepository.save(systemProfileQuestionDataReportInitialPath);
	}	
	

}
