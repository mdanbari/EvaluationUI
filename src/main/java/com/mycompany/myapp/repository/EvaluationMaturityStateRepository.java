package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.EvaluationMaturityState;

/**
 * Spring Data JPA repository for the EvaluationMaturityState entity.
 */
@Repository
public interface EvaluationMaturityStateRepository extends JpaRepository<EvaluationMaturityState, Long> {

	public List<EvaluationMaturityState> findAllByEvaluationIdIn(List<Long> ids);

}
