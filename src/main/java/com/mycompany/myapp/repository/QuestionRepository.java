package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.Question;

/**
 * Spring Data JPA repository for the Question entity.
 */
@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
	@Query("select distinct question from Question question left join fetch question.diamondCategories")
	List<Question> findAllWithEagerRelationships();

	@Query("select question from Question question left join fetch question.diamondCategories where question.id =:id")
	Question findOneWithEagerRelationships(@Param("id") Long id);

	@Query(value = "select q from Question q where (('fa' = ?2) and (q.questionTitlefa like %?1% )) or (('en' = ?2) and (q.questionTitleen like %?1%))", 
			countQuery = "select count(*) from Question q where (('fa' = ?2) and (q.questionTitlefa like %?1% )) or (('en' = ?2) and (q.questionTitleen like %?1%))")
	public Page<Question> search(String search, String local, Pageable pageable);
	
	public Question findTop1ByQuestionTitlefa(String questionTittle);
	
	@Query(value="select q from Question q where q.questionTitlefa like %?1% ")
	public List<Question> findQuestionByquestionName(String questionTittle);
}
