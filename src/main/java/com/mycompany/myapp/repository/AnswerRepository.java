package com.mycompany.myapp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.Answer;
import com.mycompany.myapp.domain.Question;


/**
 * Spring Data JPA repository for the Answer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {
	
	@Query(value = "select q from Answer q where (('fa' = ?2) and (q.answerTitlefa like %?1% )) or (('en' = ?2) and (q.answerTitleen like %?1%))", 
			countQuery = "select count(*) from Answer q where (('fa' = ?2) and (q.answerTitlefa like %?1% )) or (('en' = ?2) and (q.answerTitleen like %?1%))")
	public Page<Answer> search(String search, String local, Pageable pageable);

}
