package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.SystemProfile;


/**
 * Spring Data JPA repository for the SystemProfile entity.
 */
@Repository
public interface SystemProfileRepository extends JpaRepository<SystemProfile, Long> {
	
	List<SystemProfile> findAllBySystemProfileKey(String evalPurgTaskEnable);

}
