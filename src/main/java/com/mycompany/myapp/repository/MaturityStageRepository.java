package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.MaturityStage;


/**
 * Spring Data JPA repository for the MaturityStage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaturityStageRepository extends JpaRepository<MaturityStage, Long> {
	
	List<MaturityStage> findAll();
	
	MaturityStage findOneByMaturityStageTitlefa(String title);

	List<MaturityStage> findAllByOrderByIdAsc();

}
