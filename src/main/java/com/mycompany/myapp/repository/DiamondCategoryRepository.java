package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.DiamondCategory;

/**
 * Spring Data JPA repository for the DiamondCategory entity.
 */
@Repository
public interface DiamondCategoryRepository extends JpaRepository<DiamondCategory, Long> {
	public List<DiamondCategory> findAll();

	public DiamondCategory findOneByDiamondCategoryTitlefa(String title);	
	

}
