package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.EvaluationArchive;


/**
 * Spring Data JPA repository for the EvaluationArchive entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EvaluationArchiveRepository extends JpaRepository<EvaluationArchive, Long> {

}
