package com.mycompany.myapp.repository;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.ElecServiceQuestion;
import com.mycompany.myapp.domain.MaturityStage;
import com.mycompany.myapp.service.dto.SeparationChartReportJpaDTO;
import com.mycompany.myapp.service.dto.SeparationChartReportResultDTO;

import io.swagger.annotations.ApiParam;

/**
 * Spring Data JPA repository for the ElecServiceQuestion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ElecServiceQuestionRepository extends JpaRepository<ElecServiceQuestion, Long> {

	@Query(value = "SELECT new com.mycompany.myapp.service.dto.SeparationChartReportJpaDTO"
			+ "(esm.maturityStage.id , avg(esm.elecMaturityStateScore)) "
			+ "FROM ElecServiceMaturityState esm "
			+ "WHERE esm.electronicService.id = :elecId AND esm.maturityStage.id in :maturitySelecteds "
			+ "GROUP BY esm.maturityStage.id ORDER BY esm.maturityStage.id asc")
	List<SeparationChartReportJpaDTO> generateElecServiceMaturityReport(@Param("elecId") Long elecId,
			@Param("maturitySelecteds")List<Long> maturitySelecteds);

	@Query(value = "SELECT new com.mycompany.myapp.service.dto.SeparationChartReportJpaDTO"
			+ "(esm.maturityStage.id , avg(esm.elecMaturityStateScore)) "
			+ "FROM ElecServiceMaturityState esm "
			+ "WHERE esm.electronicService.id = :elecId AND esm.maturityStage.id in :maturitySelecteds AND esm.elecMaturityDate BETWEEN :fromDate AND :toDate "
			+ "GROUP BY esm.maturityStage.id ORDER BY esm.maturityStage.id asc")
	List<SeparationChartReportJpaDTO>  generateElecServiceMaturityReport(@Param("elecId")Long elecId,
			@Param("maturitySelecteds")List<Long> maturitySelecteds, @Param("fromDate")ZonedDateTime fromDate, @Param("toDate")ZonedDateTime toDate);

	@Query(value = "SELECT new com.mycompany.myapp.service.dto.SeparationChartReportJpaDTO"
			+ "(esq.question.id , esq.elecServiceQuestionScore) "
			+ "FROM ElecServiceQuestion esq "
			+ "WHERE esq.electronicService.id = :elecId AND esq.question.id in :questionIds "
			+ "GROUP BY esq.question.id ORDER BY esq.question.id asc")
	List<SeparationChartReportJpaDTO> generateElecQuestionReport(@Param("elecId") Long elecId,
			@Param("questionIds")List<Long> questionIds);
	
	@Query(value = "SELECT new com.mycompany.myapp.service.dto.SeparationChartReportJpaDTO"
			+ "(esq.question.id , esq.elecServiceQuestionScore) "
			+ "FROM ElecServiceQuestion esq "
			+ "WHERE esq.electronicService.id = :elecId AND esq.question.id in :questionIds AND esq.elecServiceQuestionDate BETWEEN :fromDate AND :toDate "
			+ "GROUP BY esq.question.id ORDER BY esq.question.id asc")
	List<SeparationChartReportJpaDTO> generateElecQuestionReport(@Param("elecId") Long elecId,
			@Param("questionIds")List<Long> questionIds,@Param("fromDate")ZonedDateTime fromDate, @Param("toDate")ZonedDateTime toDate);

	@Query(value = "SELECT new com.mycompany.myapp.service.dto.SeparationChartReportJpaDTO"
			+ "(esq.question.id , avg(esq.elecServiceQuestionScore)) "
			+ "FROM ElecServiceQuestion esq "
			+ "WHERE esq.electronicService.department.id = :depId AND esq.question.id in :questionIds "
			+ "GROUP BY esq.question.id ORDER BY esq.question.id asc")
	List<SeparationChartReportJpaDTO> generateDepQuestionReport(@Param("depId") Long elecId,
			@Param("questionIds")List<Long> questionIds);

	@Query(value = "SELECT new com.mycompany.myapp.service.dto.SeparationChartReportJpaDTO"
			+ "(esq.question.id , avg(esq.elecServiceQuestionScore)) "
			+ "FROM ElecServiceQuestion esq "
			+ "WHERE esq.electronicService.department.id = :depId AND esq.question.id in :questionIds AND esq.elecServiceQuestionDate BETWEEN :fromDate AND :toDate "
			+ "GROUP BY esq.question.id ORDER BY esq.question.id asc")
	List<SeparationChartReportJpaDTO> generateDepQuestionReport(@Param("depId") Long depId,
			@Param("questionIds")List<Long> questionIds,@Param("fromDate")ZonedDateTime fromDate, @Param("toDate")ZonedDateTime toDate);

	

	

	

}
