package com.mycompany.myapp.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.ElectronicService;

/**
 * Spring Data JPA repository for the ElectronicService entity.
 */
@Repository
public interface ElectronicServiceRepository extends JpaRepository<ElectronicService, Long> {
	@Query("select distinct electronic_service from ElectronicService electronic_service left join fetch electronic_service.diamondCategories")
	List<ElectronicService> findAllWithEagerRelationships();

	@Query("select electronic_service from ElectronicService electronic_service left join fetch electronic_service.diamondCategories where electronic_service.id =:id")
	ElectronicService findOneWithEagerRelationships(@Param("id") Long id);

	@Query("select new com.mycompany.myapp.domain.ElectronicService(es.id as id , es.elecServiceTitlefa as elecServiceTitlefa , es.elecServiceTitleen as elecServiceTitleen) from ElectronicService es")
	public List<ElectronicService> customFindAll();

	@Query("select new com.mycompany.myapp.domain.ElectronicService(es.id as id , es.elecServiceTitlefa as elecServiceTitlefa , es.elecServiceTitleen as elecServiceTitleen) from ElectronicService es where es.department.id = ?1")
	public List<ElectronicService> findAllElecServiceOfDepartment(Long id);

	public ElectronicService findTop1ByElecServiceEvalCountAndElecServiceEvalDateLessThanEqualOrderByElecServiceEvalDateDesc(
			int i, ZonedDateTime now);

	public ElectronicService findTop1ByElecServiceEvalCount(int i);

	public ElectronicService findTop1ByElecServiceTitlefa(String title);
	
	@Query(value = "select q from ElectronicService q where (('fa' = ?2) and (q.elecServiceTitlefa like %?1% or q.parentServicefa like %?1%)) or (('en' = ?2) and (q.elecServiceTitleen like %?1% or q.parentServiceen like %?1%))", 
			countQuery = "select count(*) from ElectronicService q where (('fa' = ?2) and (q.elecServiceTitlefa like %?1% or q.parentServicefa like %?1%)) or (('en' = ?2) and (q.elecServiceTitleen like %?1% or q.parentServiceen like %?1%))")
	public Page<ElectronicService> search(String search, String local, Pageable pageable);	
	
	@Query(value = "select es.id from ElectronicService es")
	List<Long> findAllById();
	
	@Query("select q from ElectronicService q where q.elecServiceTitlefa like %?1% ")
	public List<ElectronicService> findByElecServiceName(String elecServiceTitlefa);

}
