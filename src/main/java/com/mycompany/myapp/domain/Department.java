package com.mycompany.myapp.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Department.
 */
@Entity
@Table(name = "department")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Department implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "department_namefa", nullable = false)
    private String departmentNamefa;

    
    @Column(name = "department_nameen", nullable = false)
    private String departmentNameen;

    @Column(name = "electronic_service_count")
    private Integer electronicServiceCount;

    @ManyToOne
    private Department parent;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDepartmentNamefa() {
        return departmentNamefa;
    }

    public Department departmentNamefa(String departmentNamefa) {
        this.departmentNamefa = departmentNamefa;
        return this;
    }

    public void setDepartmentNamefa(String departmentNamefa) {
        this.departmentNamefa = departmentNamefa;
    }

    public String getDepartmentNameen() {
        return departmentNameen;
    }

    public Department departmentNameen(String departmentNameen) {
        this.departmentNameen = departmentNameen;
        return this;
    }

    public void setDepartmentNameen(String departmentNameen) {
        this.departmentNameen = departmentNameen;
    }

    public Integer getElectronicServiceCount() {
        return electronicServiceCount;
    }

    public Department electronicServiceCount(Integer electronicServiceCount) {
        this.electronicServiceCount = electronicServiceCount;
        return this;
    }

    public void setElectronicServiceCount(Integer electronicServiceCount) {
        this.electronicServiceCount = electronicServiceCount;
    }

    public Department getParent() {
        return parent;
    }

    public Department parent(Department department) {
        this.parent = department;
        return this;
    }

    public void setParent(Department department) {
        this.parent = department;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Department department = (Department) o;
        if (department.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), department.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Department{" +
            "id=" + getId() +
            ", departmentNamefa='" + getDepartmentNamefa() + "'" +
            ", departmentNameen='" + getDepartmentNameen() + "'" +
            ", electronicServiceCount='" + getElectronicServiceCount() + "'" +
            "}";
    }
}
