package com.mycompany.myapp.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mycompany.myapp.domain.enumeration.EvaluationState;

/**
 * A Evaluation.
 */
@Entity
@Table(name = "evaluation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Evaluation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "eval_titlefa")
	private String evalTitlefa;

	@Column(name = "eval_titleen")
	private String evalTitleen;

	@Column(name = "eval_person")
	private String evalPerson;

	@Column(name = "eval_start_date")
	private ZonedDateTime evalStartDate;

	@Column(name = "eval_end_date")
	private ZonedDateTime evalEndDate;

	@Column(name = "eval_dead_line_date")
	private ZonedDateTime evalDeadLineDate;

	@Column(name = "eval_last_modified_date")
	private ZonedDateTime evalLastModifiedDate;

	@Column(name = "eval_confirmed_date")
	private ZonedDateTime evalConfirmedDate;

	@Enumerated(EnumType.STRING)
	@Column(name = "eval_state")
	private EvaluationState evalState;

	@Column(name = "eval_descriptionfa")
	private String evalDescriptionfa;

	@Column(name = "eval_judgedescriptionfa")
	private String evalJudgeDescriptionfa;

	@Column(name = "eval_descriptionen")
	private String evalDescriptionen;

	@Column(name = "eval_confirmer")
	private String evalConfirmer;

	@Column(name = "eval_score")
	private Double evalScore;

	@Enumerated(EnumType.STRING)
	@Column(name = "evaluation_state")
	private EvaluationState evaluationState;

	@OneToMany(mappedBy = "evaluation")
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<QuestionAnswer> questionAnswers = new HashSet<>();

	@ManyToOne
	private ElectronicService electronicService;

	// jhipster-needle-entity-add-field - Jhipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEvalTitlefa() {
		return evalTitlefa;
	}

	public Evaluation evalTitlefa(String evalTitlefa) {
		this.evalTitlefa = evalTitlefa;
		return this;
	}

	public void setEvalTitlefa(String evalTitlefa) {
		this.evalTitlefa = evalTitlefa;
	}

	public String getEvalTitleen() {
		return evalTitleen;
	}

	public Evaluation evalTitleen(String evalTitleen) {
		this.evalTitleen = evalTitleen;
		return this;
	}

	public void setEvalTitleen(String evalTitleen) {
		this.evalTitleen = evalTitleen;
	}

	public String getEvalPerson() {
		return evalPerson;
	}

	public Evaluation evalPerson(String evalPerson) {
		this.evalPerson = evalPerson;
		return this;
	}

	public void setEvalPerson(String evalPerson) {
		this.evalPerson = evalPerson;
	}

	public ZonedDateTime getEvalStartDate() {
		return evalStartDate;
	}

	public Evaluation evalStartDate(ZonedDateTime evalStartDate) {
		this.evalStartDate = evalStartDate;
		return this;
	}

	public void setEvalStartDate(ZonedDateTime evalStartDate) {
		this.evalStartDate = evalStartDate;
	}

	public ZonedDateTime getEvalEndDate() {
		return evalEndDate;
	}

	public Evaluation evalEndDate(ZonedDateTime evalEndDate) {
		this.evalEndDate = evalEndDate;
		return this;
	}

	public void setEvalEndDate(ZonedDateTime evalEndDate) {
		this.evalEndDate = evalEndDate;
	}

	public ZonedDateTime getEvalDeadLineDate() {
		return evalDeadLineDate;
	}

	public Evaluation evalDeadLineDate(ZonedDateTime evalDeadLineDate) {
		this.evalDeadLineDate = evalDeadLineDate;
		return this;
	}

	public void setEvalDeadLineDate(ZonedDateTime evalDeadLineDate) {
		this.evalDeadLineDate = evalDeadLineDate;
	}

	public ZonedDateTime getEvalLastModifiedDate() {
		return evalLastModifiedDate;
	}

	public Evaluation evalLastModifiedDate(ZonedDateTime evalLastModifiedDate) {
		this.evalLastModifiedDate = evalLastModifiedDate;
		return this;
	}

	public void setEvalLastModifiedDate(ZonedDateTime evalLastModifiedDate) {
		this.evalLastModifiedDate = evalLastModifiedDate;
	}

	public ZonedDateTime getEvalConfirmedDate() {
		return evalConfirmedDate;
	}

	public Evaluation evalConfirmedDate(ZonedDateTime evalConfirmedDate) {
		this.evalConfirmedDate = evalConfirmedDate;
		return this;
	}

	public void setEvalConfirmedDate(ZonedDateTime evalConfirmedDate) {
		this.evalConfirmedDate = evalConfirmedDate;
	}

	public EvaluationState getEvalState() {
		return evalState;
	}

	public Evaluation evalState(EvaluationState evalState) {
		this.evalState = evalState;
		return this;
	}

	public void setEvalState(EvaluationState evalState) {
		this.evalState = evalState;
	}

	public String getEvalDescriptionfa() {
		return evalDescriptionfa;
	}

	public Evaluation evalDescriptionfa(String evalDescriptionfa) {
		this.evalDescriptionfa = evalDescriptionfa;
		return this;
	}

	public void setEvalDescriptionfa(String evalDescriptionfa) {
		this.evalDescriptionfa = evalDescriptionfa;
	}

	public String getEvalDescriptionen() {
		return evalDescriptionen;
	}

	public Evaluation evalDescriptionen(String evalDescriptionen) {
		this.evalDescriptionen = evalDescriptionen;
		return this;
	}

	public void setEvalDescriptionen(String evalDescriptionen) {
		this.evalDescriptionen = evalDescriptionen;
	}

	public String getEvalConfirmer() {
		return evalConfirmer;
	}

	public Evaluation evalConfirmer(String evalConfirmer) {
		this.evalConfirmer = evalConfirmer;
		return this;
	}

	public void setEvalConfirmer(String evalConfirmer) {
		this.evalConfirmer = evalConfirmer;
	}

	public Double getEvalScore() {
		return evalScore;
	}

	public Evaluation evalScore(Double evalScore) {
		this.evalScore = evalScore;
		return this;
	}

	public void setEvalScore(Double evalScore) {
		this.evalScore = evalScore;
	}

	public EvaluationState getEvaluationState() {
		return evaluationState;
	}

	public Evaluation evaluationState(EvaluationState evaluationState) {
		this.evaluationState = evaluationState;
		return this;
	}

	public void setEvaluationState(EvaluationState evaluationState) {
		this.evaluationState = evaluationState;
	}

	public Set<QuestionAnswer> getQuestionAnswers() {
		return questionAnswers;
	}

	public Evaluation questionAnswers(Set<QuestionAnswer> questionAnswers) {
		this.questionAnswers = questionAnswers;
		return this;
	}

	public Evaluation addQuestionAnswer(QuestionAnswer questionAnswer) {
		this.questionAnswers.add(questionAnswer);
		questionAnswer.setEvaluation(this);
		return this;
	}

	public Evaluation removeQuestionAnswer(QuestionAnswer questionAnswer) {
		this.questionAnswers.remove(questionAnswer);
		questionAnswer.setEvaluation(null);
		return this;
	}

	public void setQuestionAnswers(Set<QuestionAnswer> questionAnswers) {
		this.questionAnswers = questionAnswers;
	}

	public ElectronicService getElectronicService() {
		return electronicService;
	}

	public Evaluation electronicService(ElectronicService electronicService) {
		this.electronicService = electronicService;
		return this;
	}

	public void setElectronicService(ElectronicService electronicService) {
		this.electronicService = electronicService;
	}
	// jhipster-needle-entity-add-getters-setters - Jhipster will add getters
	// and setters here, do not remove
	
	public String getEvalJudgeDescriptionfa() {
		return evalJudgeDescriptionfa;
	}
	
	public void setEvalJudgeDescriptionfa(String evalJudgeDescriptionfa) {
		this.evalJudgeDescriptionfa = evalJudgeDescriptionfa;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Evaluation evaluation = (Evaluation) o;
		if (evaluation.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), evaluation.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "Evaluation{" + "id=" + getId() + ", evalTitlefa='" + getEvalTitlefa() + "'" + ", evalTitleen='"
				+ getEvalTitleen() + "'" + ", evalPerson='" + getEvalPerson() + "'" + ", evalStartDate='"
				+ getEvalStartDate() + "'" + ", evalEndDate='" + getEvalEndDate() + "'" + ", evalDeadLineDate='"
				+ getEvalDeadLineDate() + "'" + ", evalLastModifiedDate='" + getEvalLastModifiedDate() + "'"
				+ ", evalConfirmedDate='" + getEvalConfirmedDate() + "'" + ", evalState='" + getEvalState() + "'"
				+ ", evalDescriptionfa='" + getEvalDescriptionfa() + "'" + ", evalDescriptionen='"
				+ getEvalDescriptionen() + "'" + ", evalConfirmer='" + getEvalConfirmer() + "'" + ", evalScore='"
				+ getEvalScore() + "'" + ", evaluationState='" + getEvaluationState() + "'" + "}";
	}
}
