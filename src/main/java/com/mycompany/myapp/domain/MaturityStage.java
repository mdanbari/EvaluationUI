package com.mycompany.myapp.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A MaturityStage.
 */
@Entity
@Table(name = "maturity_stage")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MaturityStage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "maturity_stage_titlefa", nullable = false)
    private String maturityStageTitlefa;

    
    @Column(name = "maturity_stage_titleen", nullable = false)
    private String maturityStageTitleen;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMaturityStageTitlefa() {
        return maturityStageTitlefa;
    }

    public MaturityStage maturityStageTitlefa(String maturityStageTitlefa) {
        this.maturityStageTitlefa = maturityStageTitlefa;
        return this;
    }

    public void setMaturityStageTitlefa(String maturityStageTitlefa) {
        this.maturityStageTitlefa = maturityStageTitlefa;
    }

    public String getMaturityStageTitleen() {
        return maturityStageTitleen;
    }

    public MaturityStage maturityStageTitleen(String maturityStageTitleen) {
        this.maturityStageTitleen = maturityStageTitleen;
        return this;
    }

    public void setMaturityStageTitleen(String maturityStageTitleen) {
        this.maturityStageTitleen = maturityStageTitleen;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MaturityStage maturityStage = (MaturityStage) o;
        if (maturityStage.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), maturityStage.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MaturityStage{" +
            "id=" + getId() +
            ", maturityStageTitlefa='" + getMaturityStageTitlefa() + "'" +
            ", maturityStageTitleen='" + getMaturityStageTitleen() + "'" +
            "}";
    }
}
