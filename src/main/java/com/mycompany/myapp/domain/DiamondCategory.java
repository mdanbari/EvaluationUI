package com.mycompany.myapp.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A DiamondCategory.
 */
@Entity
@Table(name = "diamond_category")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DiamondCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "diamond_category_titlefa", nullable = false)
    private String diamondCategoryTitlefa;

    
    @Column(name = "diamond_category_titleen", nullable = false)
    private String diamondCategoryTitleen;

    @ManyToMany(mappedBy = "diamondCategories")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Question> questions = new HashSet<>();

    @ManyToMany(mappedBy = "diamondCategories")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ElectronicService> electronicServices = new HashSet<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDiamondCategoryTitlefa() {
        return diamondCategoryTitlefa;
    }

    public DiamondCategory diamondCategoryTitlefa(String diamondCategoryTitlefa) {
        this.diamondCategoryTitlefa = diamondCategoryTitlefa;
        return this;
    }

    public void setDiamondCategoryTitlefa(String diamondCategoryTitlefa) {
        this.diamondCategoryTitlefa = diamondCategoryTitlefa;
    }

    public String getDiamondCategoryTitleen() {
        return diamondCategoryTitleen;
    }

    public DiamondCategory diamondCategoryTitleen(String diamondCategoryTitleen) {
        this.diamondCategoryTitleen = diamondCategoryTitleen;
        return this;
    }

    public void setDiamondCategoryTitleen(String diamondCategoryTitleen) {
        this.diamondCategoryTitleen = diamondCategoryTitleen;
    }

    public Set<Question> getQuestions() {
        return questions;
    }

    public DiamondCategory questions(Set<Question> questions) {
        this.questions = questions;
        return this;
    }

    public DiamondCategory addQuestion(Question question) {
        this.questions.add(question);
        question.getDiamondCategories().add(this);
        return this;
    }

    public DiamondCategory removeQuestion(Question question) {
        this.questions.remove(question);
        question.getDiamondCategories().remove(this);
        return this;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }

    public Set<ElectronicService> getElectronicServices() {
        return electronicServices;
    }

    public DiamondCategory electronicServices(Set<ElectronicService> electronicServices) {
        this.electronicServices = electronicServices;
        return this;
    }

    public DiamondCategory addElectronicService(ElectronicService electronicService) {
        this.electronicServices.add(electronicService);
        electronicService.getDiamondCategories().add(this);
        return this;
    }

    public DiamondCategory removeElectronicService(ElectronicService electronicService) {
        this.electronicServices.remove(electronicService);
        electronicService.getDiamondCategories().remove(this);
        return this;
    }

    public void setElectronicServices(Set<ElectronicService> electronicServices) {
        this.electronicServices = electronicServices;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DiamondCategory diamondCategory = (DiamondCategory) o;
        if (diamondCategory.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), diamondCategory.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DiamondCategory{" +
            "id=" + getId() +
            ", diamondCategoryTitlefa='" + getDiamondCategoryTitlefa() + "'" +
            ", diamondCategoryTitleen='" + getDiamondCategoryTitleen() + "'" +
            "}";
    }
}
