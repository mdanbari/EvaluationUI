package com.mycompany.myapp.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A UnMaturityModel.
 */
@Entity
@Table(name = "un_maturity_model")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UnMaturityModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "un_maturity_titlefa", nullable = false)
    private String unMaturityTitlefa;

    @Column(name = "un_maturity_titleen", nullable = false)
    private String unMaturityTitleen;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUnMaturityTitlefa() {
        return unMaturityTitlefa;
    }

    public UnMaturityModel unMaturityTitlefa(String unMaturityTitlefa) {
        this.unMaturityTitlefa = unMaturityTitlefa;
        return this;
    }

    public void setUnMaturityTitlefa(String unMaturityTitlefa) {
        this.unMaturityTitlefa = unMaturityTitlefa;
    }

    public String getUnMaturityTitleen() {
        return unMaturityTitleen;
    }

    public UnMaturityModel unMaturityTitleen(String unMaturityTitleen) {
        this.unMaturityTitleen = unMaturityTitleen;
        return this;
    }

    public void setUnMaturityTitleen(String unMaturityTitleen) {
        this.unMaturityTitleen = unMaturityTitleen;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UnMaturityModel unMaturityModel = (UnMaturityModel) o;
        if (unMaturityModel.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), unMaturityModel.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UnMaturityModel{" +
            "id=" + getId() +
            ", unMaturityTitlefa='" + getUnMaturityTitlefa() + "'" +
            ", unMaturityTitleen='" + getUnMaturityTitleen() + "'" +
            "}";
    }
}
