package com.mycompany.myapp.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ElecServiceMaturityState.
 */
@Entity
@Table(name = "elec_service_maturity_state")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ElecServiceMaturityState implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "elec_maturity_state_score")
	private Double elecMaturityStateScore;

	@ManyToOne
	private MaturityStage maturityStage;

	@ManyToOne
	private ElectronicService electronicService;

	@Column(name = "elec_maturity_date")
	private ZonedDateTime elecMaturityDate;

	// jhipster-needle-entity-add-field - Jhipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getElecMaturityStateScore() {
		return elecMaturityStateScore;
	}

	public ElecServiceMaturityState elecMaturityStateScore(Double elecMaturityStateScore) {
		this.elecMaturityStateScore = elecMaturityStateScore;
		return this;
	}

	public void setElecMaturityStateScore(Double elecMaturityStateScore) {
		this.elecMaturityStateScore = elecMaturityStateScore;
	}

	public ZonedDateTime getElecMaturityDate() {
		return elecMaturityDate;
	}

	public void setElecMaturityDate(ZonedDateTime elecMaturityDate) {
		this.elecMaturityDate = elecMaturityDate;
	}

	public MaturityStage getMaturityStage() {
		return maturityStage;
	}

	public ElecServiceMaturityState maturityStage(MaturityStage maturityStage) {
		this.maturityStage = maturityStage;
		return this;
	}

	public void setMaturityStage(MaturityStage maturityStage) {
		this.maturityStage = maturityStage;
	}

	public ElectronicService getElectronicService() {
		return electronicService;
	}

	public ElecServiceMaturityState electronicService(ElectronicService electronicService) {
		this.electronicService = electronicService;
		return this;
	}

	public void setElectronicService(ElectronicService electronicService) {
		this.electronicService = electronicService;
	}
	// jhipster-needle-entity-add-getters-setters - Jhipster will add getters
	// and setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ElecServiceMaturityState elecServiceMaturityState = (ElecServiceMaturityState) o;
		if (elecServiceMaturityState.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), elecServiceMaturityState.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "ElecServiceMaturityState{" + "id=" + getId() + ", elecMaturityStateScore='"
				+ getElecMaturityStateScore() + "'" + "}";
	}
}
