package com.mycompany.myapp.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mycompany.myapp.domain.enumeration.QuestionStatus;

/**
 * A Question.
 */
@Entity
@Table(name = "question")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Question implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "question_titlefa", nullable = false)
    @Size(max=3000)
    private String questionTitlefa;

    
    @Column(name = "question_titleen", nullable = false)
    @Size(max=3000)
    private String questionTitleen;

    @Column(name = "question_descriptionfa")
    @Size(max=3000)
    private String questionDescriptionfa;

    @Column(name = "question_descriptionen")
    @Size(max=3000)
    private String questionDescriptionen;

    @NotNull
    @Column(name = "question_weight", nullable = false)
    private Double questionWeight;

    @Column(name = "question_percentage")
    private Double questionPercentage;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "question_status", nullable = false)
    private QuestionStatus questionStatus;

    @OneToMany(mappedBy = "question")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Answer> answers = new HashSet<>();

    @ManyToOne
    private MaturityStage maturityStage;

    @ManyToOne
    private UnMaturityModel unMaturityModel;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "question_diamond_category",
               joinColumns = @JoinColumn(name="questions_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="diamond_categories_id", referencedColumnName="id"))
    private Set<DiamondCategory> diamondCategories = new HashSet<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestionTitlefa() {
        return questionTitlefa;
    }

    public Question questionTitlefa(String questionTitlefa) {
        this.questionTitlefa = questionTitlefa;
        return this;
    }

    public void setQuestionTitlefa(String questionTitlefa) {
        this.questionTitlefa = questionTitlefa;
    }

    public String getQuestionTitleen() {
        return questionTitleen;
    }

    public Question questionTitleen(String questionTitleen) {
        this.questionTitleen = questionTitleen;
        return this;
    }

    public void setQuestionTitleen(String questionTitleen) {
        this.questionTitleen = questionTitleen;
    }

    public String getQuestionDescriptionfa() {
        return questionDescriptionfa;
    }

    public Question questionDescriptionfa(String questionDescriptionfa) {
        this.questionDescriptionfa = questionDescriptionfa;
        return this;
    }

    public void setQuestionDescriptionfa(String questionDescriptionfa) {
        this.questionDescriptionfa = questionDescriptionfa;
    }

    public String getQuestionDescriptionen() {
        return questionDescriptionen;
    }

    public Question questionDescriptionen(String questionDescriptionen) {
        this.questionDescriptionen = questionDescriptionen;
        return this;
    }

    public void setQuestionDescriptionen(String questionDescriptionen) {
        this.questionDescriptionen = questionDescriptionen;
    }

    public Double getQuestionWeight() {
        return questionWeight;
    }

    public Question questionWeight(Double questionWeight) {
        this.questionWeight = questionWeight;
        return this;
    }

    public void setQuestionWeight(Double questionWeight) {
        this.questionWeight = questionWeight;
    }

    public Double getQuestionPercentage() {
        return questionPercentage;
    }

    public Question questionPercentage(Double questionPercentage) {
        this.questionPercentage = questionPercentage;
        return this;
    }

    public void setQuestionPercentage(Double questionPercentage) {
        this.questionPercentage = questionPercentage;
    }

    public QuestionStatus getQuestionStatus() {
        return questionStatus;
    }

    public Question questionStatus(QuestionStatus questionStatus) {
        this.questionStatus = questionStatus;
        return this;
    }

    public void setQuestionStatus(QuestionStatus questionStatus) {
        this.questionStatus = questionStatus;
    }

    public Set<Answer> getAnswers() {
        return answers;
    }

    public Question answers(Set<Answer> answers) {
        this.answers = answers;
        return this;
    }

    public Question addAnswer(Answer answer) {
        this.answers.add(answer);
        answer.setQuestion(this);
        return this;
    }

    public Question removeAnswer(Answer answer) {
        this.answers.remove(answer);
        answer.setQuestion(null);
        return this;
    }

    public void setAnswers(Set<Answer> answers) {
        this.answers = answers;
    }

    public MaturityStage getMaturityStage() {
        return maturityStage;
    }

    public Question maturityStage(MaturityStage maturityStage) {
        this.maturityStage = maturityStage;
        return this;
    }

    public void setMaturityStage(MaturityStage maturityStage) {
        this.maturityStage = maturityStage;
    }

    public UnMaturityModel getUnMaturityModel() {
        return unMaturityModel;
    }

    public Question unMaturityModel(UnMaturityModel unMaturityModel) {
        this.unMaturityModel = unMaturityModel;
        return this;
    }

    public void setUnMaturityModel(UnMaturityModel unMaturityModel) {
        this.unMaturityModel = unMaturityModel;
    }

    public Set<DiamondCategory> getDiamondCategories() {
        return diamondCategories;
    }

    public Question diamondCategories(Set<DiamondCategory> diamondCategories) {
        this.diamondCategories = diamondCategories;
        return this;
    }

    public Question addDiamondCategory(DiamondCategory diamondCategory) {
        this.diamondCategories.add(diamondCategory);
        diamondCategory.getQuestions().add(this);
        return this;
    }

    public Question removeDiamondCategory(DiamondCategory diamondCategory) {
        this.diamondCategories.remove(diamondCategory);
        diamondCategory.getQuestions().remove(this);
        return this;
    }

    public void setDiamondCategories(Set<DiamondCategory> diamondCategories) {
        this.diamondCategories = diamondCategories;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Question question = (Question) o;
        if (question.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), question.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Question{" +
            "id=" + getId() +
            ", questionTitlefa='" + getQuestionTitlefa() + "'" +
            ", questionTitleen='" + getQuestionTitleen() + "'" +
            ", questionDescriptionfa='" + getQuestionDescriptionfa() + "'" +
            ", questionDescriptionen='" + getQuestionDescriptionen() + "'" +
            ", questionWeight='" + getQuestionWeight() + "'" +
            ", questionPercentage='" + getQuestionPercentage() + "'" +
            ", questionStatus='" + getQuestionStatus() + "'" +
            "}";
    }
}
