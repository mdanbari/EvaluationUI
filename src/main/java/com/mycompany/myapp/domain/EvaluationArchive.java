package com.mycompany.myapp.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mycompany.myapp.domain.enumeration.EvaluationState;

/**
 * A EvaluationArchive.
 */
@Entity
@Table(name = "evaluation_archive")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EvaluationArchive implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "eval_arch_titlefa")
    private String evalArchTitlefa;

    @Column(name = "eval_arch_titleen")
    private String evalArchTitleen;

    @Column(name = "eval_arch_person")
    private String evalArchPerson;

    @Column(name = "eval_arch_start_date")
    private ZonedDateTime evalArchStartDate;

    @Column(name = "eval_arch_end_date")
    private ZonedDateTime evalArchEndDate;

    @Column(name = "eval_arch_dead_line_date")
    private ZonedDateTime evalArchDeadLineDate;

    @Column(name = "eval_arch_last_modified_date")
    private ZonedDateTime evalArchLastModifiedDate;

    @Column(name = "eval_arch_confirmed_date")
    private ZonedDateTime evalArchConfirmedDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "eval_arch_state")
    private EvaluationState evalArchState;

    @Column(name = "eval_arch_descriptionfa")
    private String evalArchDescriptionfa;

    @Column(name = "eval_arch_descriptionen")
    private String evalArchDescriptionen;

    @Column(name = "eval_arch_confirmer")
    private String evalArchConfirmer;

    @Column(name = "eval_arch_score")
    private Double evalArchScore;

    @Enumerated(EnumType.STRING)
    @Column(name = "eval_archuation_state")
    private EvaluationState evalArchuationState;

    @OneToMany(mappedBy = "evaluationArchive")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<QuestionAnswer> questionAnswers = new HashSet<>();

    @ManyToOne
    private ElectronicService electronicService;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEvalArchTitlefa() {
        return evalArchTitlefa;
    }

    public EvaluationArchive evalArchTitlefa(String evalArchTitlefa) {
        this.evalArchTitlefa = evalArchTitlefa;
        return this;
    }

    public void setEvalArchTitlefa(String evalArchTitlefa) {
        this.evalArchTitlefa = evalArchTitlefa;
    }

    public String getEvalArchTitleen() {
        return evalArchTitleen;
    }

    public EvaluationArchive evalArchTitleen(String evalArchTitleen) {
        this.evalArchTitleen = evalArchTitleen;
        return this;
    }

    public void setEvalArchTitleen(String evalArchTitleen) {
        this.evalArchTitleen = evalArchTitleen;
    }

    public String getEvalArchPerson() {
        return evalArchPerson;
    }

    public EvaluationArchive evalArchPerson(String evalArchPerson) {
        this.evalArchPerson = evalArchPerson;
        return this;
    }

    public void setEvalArchPerson(String evalArchPerson) {
        this.evalArchPerson = evalArchPerson;
    }

    public ZonedDateTime getEvalArchStartDate() {
        return evalArchStartDate;
    }

    public EvaluationArchive evalArchStartDate(ZonedDateTime evalArchStartDate) {
        this.evalArchStartDate = evalArchStartDate;
        return this;
    }

    public void setEvalArchStartDate(ZonedDateTime evalArchStartDate) {
        this.evalArchStartDate = evalArchStartDate;
    }

    public ZonedDateTime getEvalArchEndDate() {
        return evalArchEndDate;
    }

    public EvaluationArchive evalArchEndDate(ZonedDateTime evalArchEndDate) {
        this.evalArchEndDate = evalArchEndDate;
        return this;
    }

    public void setEvalArchEndDate(ZonedDateTime evalArchEndDate) {
        this.evalArchEndDate = evalArchEndDate;
    }

    public ZonedDateTime getEvalArchDeadLineDate() {
        return evalArchDeadLineDate;
    }

    public EvaluationArchive evalArchDeadLineDate(ZonedDateTime evalArchDeadLineDate) {
        this.evalArchDeadLineDate = evalArchDeadLineDate;
        return this;
    }

    public void setEvalArchDeadLineDate(ZonedDateTime evalArchDeadLineDate) {
        this.evalArchDeadLineDate = evalArchDeadLineDate;
    }

    public ZonedDateTime getEvalArchLastModifiedDate() {
        return evalArchLastModifiedDate;
    }

    public EvaluationArchive evalArchLastModifiedDate(ZonedDateTime evalArchLastModifiedDate) {
        this.evalArchLastModifiedDate = evalArchLastModifiedDate;
        return this;
    }

    public void setEvalArchLastModifiedDate(ZonedDateTime evalArchLastModifiedDate) {
        this.evalArchLastModifiedDate = evalArchLastModifiedDate;
    }

    public ZonedDateTime getEvalArchConfirmedDate() {
        return evalArchConfirmedDate;
    }

    public EvaluationArchive evalArchConfirmedDate(ZonedDateTime evalArchConfirmedDate) {
        this.evalArchConfirmedDate = evalArchConfirmedDate;
        return this;
    }

    public void setEvalArchConfirmedDate(ZonedDateTime evalArchConfirmedDate) {
        this.evalArchConfirmedDate = evalArchConfirmedDate;
    }

    public EvaluationState getEvalArchState() {
        return evalArchState;
    }

    public EvaluationArchive evalArchState(EvaluationState evalArchState) {
        this.evalArchState = evalArchState;
        return this;
    }

    public void setEvalArchState(EvaluationState evalArchState) {
        this.evalArchState = evalArchState;
    }

    public String getEvalArchDescriptionfa() {
        return evalArchDescriptionfa;
    }

    public EvaluationArchive evalArchDescriptionfa(String evalArchDescriptionfa) {
        this.evalArchDescriptionfa = evalArchDescriptionfa;
        return this;
    }

    public void setEvalArchDescriptionfa(String evalArchDescriptionfa) {
        this.evalArchDescriptionfa = evalArchDescriptionfa;
    }

    public String getEvalArchDescriptionen() {
        return evalArchDescriptionen;
    }

    public EvaluationArchive evalArchDescriptionen(String evalArchDescriptionen) {
        this.evalArchDescriptionen = evalArchDescriptionen;
        return this;
    }

    public void setEvalArchDescriptionen(String evalArchDescriptionen) {
        this.evalArchDescriptionen = evalArchDescriptionen;
    }

    public String getEvalArchConfirmer() {
        return evalArchConfirmer;
    }

    public EvaluationArchive evalArchConfirmer(String evalArchConfirmer) {
        this.evalArchConfirmer = evalArchConfirmer;
        return this;
    }

    public void setEvalArchConfirmer(String evalArchConfirmer) {
        this.evalArchConfirmer = evalArchConfirmer;
    }

    public Double getEvalArchScore() {
        return evalArchScore;
    }

    public EvaluationArchive evalArchScore(Double evalArchScore) {
        this.evalArchScore = evalArchScore;
        return this;
    }

    public void setEvalArchScore(Double evalArchScore) {
        this.evalArchScore = evalArchScore;
    }

    public EvaluationState getEvalArchuationState() {
        return evalArchuationState;
    }

    public EvaluationArchive evalArchuationState(EvaluationState evalArchuationState) {
        this.evalArchuationState = evalArchuationState;
        return this;
    }

    public void setEvalArchuationState(EvaluationState evalArchuationState) {
        this.evalArchuationState = evalArchuationState;
    }

    public Set<QuestionAnswer> getQuestionAnswers() {
        return questionAnswers;
    }

    public EvaluationArchive questionAnswers(Set<QuestionAnswer> questionAnswers) {
        this.questionAnswers = questionAnswers;
        return this;
    }

    public EvaluationArchive addQuestionAnswer(QuestionAnswer questionAnswer) {
        this.questionAnswers.add(questionAnswer);
        questionAnswer.setEvaluationArchive(this);
        return this;
    }

    public EvaluationArchive removeQuestionAnswer(QuestionAnswer questionAnswer) {
        this.questionAnswers.remove(questionAnswer);
        questionAnswer.setEvaluationArchive(null);
        return this;
    }

    public void setQuestionAnswers(Set<QuestionAnswer> questionAnswers) {
        this.questionAnswers = questionAnswers;
    }

    public ElectronicService getElectronicService() {
        return electronicService;
    }

    public EvaluationArchive electronicService(ElectronicService electronicService) {
        this.electronicService = electronicService;
        return this;
    }

    public void setElectronicService(ElectronicService electronicService) {
        this.electronicService = electronicService;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EvaluationArchive evaluationArchive = (EvaluationArchive) o;
        if (evaluationArchive.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), evaluationArchive.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EvaluationArchive{" +
            "id=" + getId() +
            ", evalArchTitlefa='" + getEvalArchTitlefa() + "'" +
            ", evalArchTitleen='" + getEvalArchTitleen() + "'" +
            ", evalArchPerson='" + getEvalArchPerson() + "'" +
            ", evalArchStartDate='" + getEvalArchStartDate() + "'" +
            ", evalArchEndDate='" + getEvalArchEndDate() + "'" +
            ", evalArchDeadLineDate='" + getEvalArchDeadLineDate() + "'" +
            ", evalArchLastModifiedDate='" + getEvalArchLastModifiedDate() + "'" +
            ", evalArchConfirmedDate='" + getEvalArchConfirmedDate() + "'" +
            ", evalArchState='" + getEvalArchState() + "'" +
            ", evalArchDescriptionfa='" + getEvalArchDescriptionfa() + "'" +
            ", evalArchDescriptionen='" + getEvalArchDescriptionen() + "'" +
            ", evalArchConfirmer='" + getEvalArchConfirmer() + "'" +
            ", evalArchScore='" + getEvalArchScore() + "'" +
            ", evalArchuationState='" + getEvalArchuationState() + "'" +
            "}";
    }
}
