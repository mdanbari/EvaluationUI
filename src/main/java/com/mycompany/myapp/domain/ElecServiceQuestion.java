package com.mycompany.myapp.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ElecServiceMaturityState.
 */
@Entity
@Table(name = "elec_service_question")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ElecServiceQuestion implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "elec_service_question_score")
	private Double elecServiceQuestionScore;
	
	@Column(name = "elec_service_question_count")
	private Integer count;

	@ManyToOne
	private Question question;

	@ManyToOne
	private ElectronicService electronicService;

	@Column(name = "elec_question_date")
	private ZonedDateTime elecServiceQuestionDate;

	// jhipster-needle-entity-add-field - Jhipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	

	public Double getElecServiceQuestionScore() {
		return elecServiceQuestionScore;
	}

	public void setElecServiceQuestionScore(Double elecServiceQuestionScore) {
		this.elecServiceQuestionScore = elecServiceQuestionScore;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public ZonedDateTime getElecServiceQuestionDate() {
		return elecServiceQuestionDate;
	}

	public void setElecServiceQuestionDate(ZonedDateTime elecServiceQuestionDate) {
		this.elecServiceQuestionDate = elecServiceQuestionDate;
	}

	public ElectronicService getElectronicService() {
		return electronicService;
	}

	public ElecServiceQuestion electronicService(ElectronicService electronicService) {
		this.electronicService = electronicService;
		return this;
	}

	public void setElectronicService(ElectronicService electronicService) {
		this.electronicService = electronicService;
	}
	// jhipster-needle-entity-add-getters-setters - Jhipster will add getters
	// and setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ElecServiceQuestion elecServiceMaturityState = (ElecServiceQuestion) o;
		if (elecServiceMaturityState.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), elecServiceMaturityState.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "ElecServiceQuestion{" + "id=" + getId() + "}";
	}
}
