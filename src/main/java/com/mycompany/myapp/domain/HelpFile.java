package com.mycompany.myapp.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mycompany.myapp.domain.enumeration.FileStatus;

/**
 * A HelpFile.
 */
@Entity
@Table(name = "help_file")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class HelpFile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "file_url")
    private String fileUrl;

    @Column(name = "file_size")
    private Double fileSize;

    @Lob
    @Column(name = "file_blob")
    private byte[] fileBlob;

    @Column(name = "file_blob_content_type")
    private String fileBlobContentType;

    @Column(name = "file_index")
    private Integer fileIndex;

    @Enumerated(EnumType.STRING)
    @Column(name = "file_status")
    private FileStatus fileStatus;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public HelpFile fileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public HelpFile fileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
        return this;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public Double getFileSize() {
        return fileSize;
    }

    public HelpFile fileSize(Double fileSize) {
        this.fileSize = fileSize;
        return this;
    }

    public void setFileSize(Double fileSize) {
        this.fileSize = fileSize;
    }

    public byte[] getFileBlob() {
        return fileBlob;
    }

    public HelpFile fileBlob(byte[] fileBlob) {
        this.fileBlob = fileBlob;
        return this;
    }

    public void setFileBlob(byte[] fileBlob) {
        this.fileBlob = fileBlob;
    }

    public String getFileBlobContentType() {
        return fileBlobContentType;
    }

    public HelpFile fileBlobContentType(String fileBlobContentType) {
        this.fileBlobContentType = fileBlobContentType;
        return this;
    }

    public void setFileBlobContentType(String fileBlobContentType) {
        this.fileBlobContentType = fileBlobContentType;
    }

    public Integer getFileIndex() {
        return fileIndex;
    }

    public HelpFile fileIndex(Integer fileIndex) {
        this.fileIndex = fileIndex;
        return this;
    }

    public void setFileIndex(Integer fileIndex) {
        this.fileIndex = fileIndex;
    }

    public FileStatus getFileStatus() {
        return fileStatus;
    }

    public HelpFile fileStatus(FileStatus fileStatus) {
        this.fileStatus = fileStatus;
        return this;
    }

    public void setFileStatus(FileStatus fileStatus) {
        this.fileStatus = fileStatus;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HelpFile helpFile = (HelpFile) o;
        if (helpFile.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), helpFile.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "HelpFile{" +
            "id=" + getId() +
            ", fileName='" + getFileName() + "'" +
            ", fileUrl='" + getFileUrl() + "'" +
            ", fileSize='" + getFileSize() + "'" +
            ", fileBlob='" + getFileBlob() + "'" +
            ", fileBlobContentType='" + fileBlobContentType + "'" +
            ", fileIndex='" + getFileIndex() + "'" +
            ", fileStatus='" + getFileStatus() + "'" +
            "}";
    }
}
