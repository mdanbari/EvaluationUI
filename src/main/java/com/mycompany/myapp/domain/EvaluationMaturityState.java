package com.mycompany.myapp.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A EvaluationMaturityState.
 */
@Entity
@Table(name = "evaluation_maturity_state")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EvaluationMaturityState implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "eval_maturity_state_score")
    private Double evalMaturityStateScore;

    @ManyToOne
    private Evaluation evaluation;

    @ManyToOne
    private MaturityStage maturityStage;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getEvalMaturityStateScore() {
        return evalMaturityStateScore;
    }

    public EvaluationMaturityState evalMaturityStateScore(Double evalMaturityStateScore) {
        this.evalMaturityStateScore = evalMaturityStateScore;
        return this;
    }

    public void setEvalMaturityStateScore(Double evalMaturityStateScore) {
        this.evalMaturityStateScore = evalMaturityStateScore;
    }

    public Evaluation getEvaluation() {
        return evaluation;
    }

    public EvaluationMaturityState evaluation(Evaluation evaluation) {
        this.evaluation = evaluation;
        return this;
    }

    public void setEvaluation(Evaluation evaluation) {
        this.evaluation = evaluation;
    }

    public MaturityStage getMaturityStage() {
        return maturityStage;
    }

    public EvaluationMaturityState maturityStage(MaturityStage maturityStage) {
        this.maturityStage = maturityStage;
        return this;
    }

    public void setMaturityStage(MaturityStage maturityStage) {
        this.maturityStage = maturityStage;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EvaluationMaturityState evaluationMaturityState = (EvaluationMaturityState) o;
        if (evaluationMaturityState.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), evaluationMaturityState.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EvaluationMaturityState{" +
            "id=" + getId() +
            ", evalMaturityStateScore='" + getEvalMaturityStateScore() + "'" +
            "}";
    }
}
