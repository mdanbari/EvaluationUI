package com.mycompany.myapp.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ElecServiceDiamondCategory.
 */
@Entity
@Table(name = "elec_service_diamond_category")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ElecServiceDiamondCategory implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "elec_diamond_category_score")
	private Double elecDiamondCategoryScore;

	@ManyToOne
	private DiamondCategory diamondCategory;

	@ManyToOne
	private ElectronicService electronicService;

	@Column(name = "elec_diamond_date")
	private ZonedDateTime elecDiamondDate;

	// jhipster-needle-entity-add-field - Jhipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getElecDiamondCategoryScore() {
		return elecDiamondCategoryScore;
	}

	public ElecServiceDiamondCategory elecDiamondCategoryScore(Double elecDiamondCategoryScore) {
		this.elecDiamondCategoryScore = elecDiamondCategoryScore;
		return this;

	}

	public void setElecDiamondCategoryScore(Double elecDiamondCategoryScore) {
		this.elecDiamondCategoryScore = elecDiamondCategoryScore;
	}

	public ZonedDateTime getElecDiamondDate() {
		return elecDiamondDate;
	}

	public void setElecDiamondDate(ZonedDateTime elecDiamondDate) {
		this.elecDiamondDate = elecDiamondDate;
	}

	public DiamondCategory getDiamondCategory() {
		return diamondCategory;
	}

	public ElecServiceDiamondCategory diamondCategory(DiamondCategory diamondCategory) {
		this.diamondCategory = diamondCategory;
		return this;
	}

	public void setDiamondCategory(DiamondCategory diamondCategory) {
		this.diamondCategory = diamondCategory;
	}

	public ElectronicService getElectronicService() {
		return electronicService;
	}

	public ElecServiceDiamondCategory electronicService(ElectronicService electronicService) {
		this.electronicService = electronicService;
		return this;
	}

	public void setElectronicService(ElectronicService electronicService) {
		this.electronicService = electronicService;
	}
	// jhipster-needle-entity-add-getters-setters - Jhipster will add getters
	// and setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ElecServiceDiamondCategory elecServiceDiamondCategory = (ElecServiceDiamondCategory) o;
		if (elecServiceDiamondCategory.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), elecServiceDiamondCategory.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "ElecServiceDiamondCategory{" + "id=" + getId() + ", elecDiamondCategoryScore='"
				+ getElecDiamondCategoryScore() + "'" + "}";
	}
}
