package com.mycompany.myapp.domain.enumeration;

public enum SheetName {
	hozurDarWeb,taamoli,tarakoneshi,yeparchegi,mosharekat;
	public String toString()
	{
		switch(this)
		{
		case hozurDarWeb:
		return "حضور در وب";
		case taamoli:
			return "تعاملی";
		case tarakoneshi:
			return "تراکنشی";
		case yeparchegi:
			return "یکپارچگی";
		case mosharekat:
			return "مشارکتی";
		}
		return null;
	}
}
