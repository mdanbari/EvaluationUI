package com.mycompany.myapp.domain.enumeration;

/**
 * The EvaluationState enumeration.
 */
public enum EvaluationUserType {
    GENERAL_EVALUATOR, EXPERT_EVALUATOR, OBSERVER,SPECIALIZED_EVALUATOR
    }
