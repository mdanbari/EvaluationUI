package com.mycompany.myapp.domain.enumeration;

/**
 * The QuestionStatus enumeration.
 */
public enum QuestionStatus {
    ENABLE, DISABLE
}
