package com.mycompany.myapp.domain.enumeration;

/**
 * The FileStatus enumeration.
 */
public enum FileStatus {
    ENABLE, DISABLE
}
