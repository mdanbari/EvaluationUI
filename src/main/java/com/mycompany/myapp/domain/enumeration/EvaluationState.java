package com.mycompany.myapp.domain.enumeration;

/**
 * The EvaluationState enumeration.
 */
public enum EvaluationState {
    GENERAL, INPROGRESS, COMPLETE, EXPIRED, CONFIRMED, REJECTED
}
