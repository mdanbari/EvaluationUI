package com.mycompany.myapp.domain.enumeration;

public enum DiamondCategoryName {
	OMOOMI,EKHTESASI_AMN,EKHTESASII_NAAMN,MOSTAGHEL,MORTABET_EDGHAAM,MORTABET_HAMRASTA,AMALKARDI,ETELAATI_ENTEKHAABI,ETELLAATI_TANZIMSHODE;
	public String toString()
	{
		switch(this)
		{
		case OMOOMI:
		return "عمومی";
		case EKHTESASI_AMN:
			return "اختصاصی امن";
		case EKHTESASII_NAAMN:
			return "اختصاصی نا امن";
		case MOSTAGHEL:
			return "مستقل";
		case MORTABET_EDGHAAM:
			return "مرتبط ادغام";
		case MORTABET_HAMRASTA:
			return "مرتبط هم راستا";
			
		case AMALKARDI:
			return "عملکردی";
			
		case ETELAATI_ENTEKHAABI:
			return "اطلاعاتی انتخابی";
		case ETELLAATI_TANZIMSHODE:
			return "اطلاعاتی تنطیم شده";
		}
		return null;
	}
}
