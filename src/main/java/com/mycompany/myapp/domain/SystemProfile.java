package com.mycompany.myapp.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A SystemProfile.
 */
@Entity
@Table(name = "system_profile")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SystemProfile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "system_profile_key")
    private String systemProfileKey;

    @Column(name = "system_profile_value")
    private String systemProfileValue;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSystemProfileKey() {
        return systemProfileKey;
    }

    public SystemProfile systemProfileKey(String systemProfileKey) {
        this.systemProfileKey = systemProfileKey;
        return this;
    }

    public void setSystemProfileKey(String systemProfileKey) {
        this.systemProfileKey = systemProfileKey;
    }

    public String getSystemProfileValue() {
        return systemProfileValue;
    }

    public SystemProfile systemProfileValue(String systemProfileValue) {
        this.systemProfileValue = systemProfileValue;
        return this;
    }

    public void setSystemProfileValue(String systemProfileValue) {
        this.systemProfileValue = systemProfileValue;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SystemProfile systemProfile = (SystemProfile) o;
        if (systemProfile.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), systemProfile.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SystemProfile{" +
            "id=" + getId() +
            ", systemProfileKey='" + getSystemProfileKey() + "'" +
            ", systemProfileValue='" + getSystemProfileValue() + "'" +
            "}";
    }
}
