package com.mycompany.myapp.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ElectronicService.
 */
@Entity
@Table(name = "electronic_service")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ElectronicService implements Serializable {

	private static final long serialVersionUID = 1L;

	public ElectronicService() {
	}

	public ElectronicService(Long id, String elecServiceTitlefa, String elecServiceTitleen) {
		this.id = id;
		this.elecServiceTitlefa = elecServiceTitlefa;
		this.elecServiceTitleen = elecServiceTitleen;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "elec_service_titlefa", nullable = false)
	private String elecServiceTitlefa;

	@Column(name = "elec_service_titleen", nullable = false)
	private String elecServiceTitleen;

	@Column(name = "electronic_service_code")
	private String electronicServiceCode;

	@Column(name = "electronic_service_Link")
	private String electronicServiceLink;

	@NotNull
	@Column(name = "elec_service_eval_count", nullable = false)
	private Integer elecServiceEvalCount;

	@Column(name = "elec_service_eval_total_score")
	private Double elecServiceEvalTotalScore;

	@Column(name = "elec_service_eval_date")
	private ZonedDateTime elecServiceEvalDate;

	@Column(name = "parent_servicefa")
	private String parentServicefa;

	@Column(name = "parent_serviceen")
	private String parentServiceen;

	@Column(name = "parent_servicecode")
	private String parentServiceCode;

	@ManyToOne
	private Department department;

	@ManyToMany
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@JoinTable(name = "electronic_service_diamond_category", joinColumns = @JoinColumn(name = "electronic_services_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "diamond_categories_id", referencedColumnName = "id"))
	private Set<DiamondCategory> diamondCategories = new HashSet<>();

	// jhipster-needle-entity-add-field - Jhipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getElecServiceTitlefa() {
		return elecServiceTitlefa;
	}

	public ElectronicService elecServiceTitlefa(String elecServiceTitlefa) {
		this.elecServiceTitlefa = elecServiceTitlefa;
		return this;
	}

	public void setElecServiceTitlefa(String elecServiceTitlefa) {
		this.elecServiceTitlefa = elecServiceTitlefa;
	}

	public String getElecServiceTitleen() {
		return elecServiceTitleen;
	}

	public ElectronicService elecServiceTitleen(String elecServiceTitleen) {
		this.elecServiceTitleen = elecServiceTitleen;
		return this;
	}

	public void setElecServiceTitleen(String elecServiceTitleen) {
		this.elecServiceTitleen = elecServiceTitleen;
	}

	public String getElectronicServiceCode() {
		return electronicServiceCode;
	}

	public ElectronicService electronicServiceCode(String electronicServiceCode) {
		this.electronicServiceCode = electronicServiceCode;
		return this;
	}

	public void setElectronicServiceCode(String electronicServiceCode) {
		this.electronicServiceCode = electronicServiceCode;
	}

	public Integer getElecServiceEvalCount() {
		return elecServiceEvalCount;
	}

	public ElectronicService elecServiceEvalCount(Integer elecServiceEvalCount) {
		this.elecServiceEvalCount = elecServiceEvalCount;
		return this;
	}

	public void setElecServiceEvalCount(Integer elecServiceEvalCount) {
		this.elecServiceEvalCount = elecServiceEvalCount;
	}

	public Double getElecServiceEvalTotalScore() {
		return elecServiceEvalTotalScore;
	}

	public ElectronicService elecServiceEvalTotalScore(Double elecServiceEvalTotalScore) {
		this.elecServiceEvalTotalScore = elecServiceEvalTotalScore;
		return this;
	}

	public void setElecServiceEvalTotalScore(Double elecServiceEvalTotalScore) {
		this.elecServiceEvalTotalScore = elecServiceEvalTotalScore;
	}

	public ZonedDateTime getElecServiceEvalDate() {
		return elecServiceEvalDate;
	}

	public ElectronicService elecServiceEvalDate(ZonedDateTime elecServiceEvalDate) {
		this.elecServiceEvalDate = elecServiceEvalDate;
		return this;
	}

	public void setElecServiceEvalDate(ZonedDateTime elecServiceEvalDate) {
		this.elecServiceEvalDate = elecServiceEvalDate;
	}

	public String getParentServicefa() {
		return parentServicefa;
	}

	public ElectronicService parentServicefa(String parentServicefa) {
		this.parentServicefa = parentServicefa;
		return this;
	}

	public void setParentServicefa(String parentServicefa) {
		this.parentServicefa = parentServicefa;
	}

	public String getParentServiceen() {
		return parentServiceen;
	}

	public ElectronicService parentServiceen(String parentServiceen) {
		this.parentServiceen = parentServiceen;
		return this;
	}

	public void setParentServiceen(String parentServiceen) {
		this.parentServiceen = parentServiceen;
	}

	public Department getDepartment() {
		return department;
	}

	public ElectronicService department(Department department) {
		this.department = department;
		return this;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Set<DiamondCategory> getDiamondCategories() {
		return diamondCategories;
	}

	public ElectronicService diamondCategories(Set<DiamondCategory> diamondCategories) {
		this.diamondCategories = diamondCategories;
		return this;
	}

	public String getParentServiceCode() {
		return parentServiceCode;
	}

	public void setParentServiceCode(String parentServiceCode) {
		this.parentServiceCode = parentServiceCode;
	}

	public ElectronicService addDiamondCategory(DiamondCategory diamondCategory) {
		this.diamondCategories.add(diamondCategory);
		diamondCategory.getElectronicServices().add(this);
		return this;
	}

	public ElectronicService removeDiamondCategory(DiamondCategory diamondCategory) {
		this.diamondCategories.remove(diamondCategory);
		diamondCategory.getElectronicServices().remove(this);
		return this;
	}

	public void setDiamondCategories(Set<DiamondCategory> diamondCategories) {
		this.diamondCategories = diamondCategories;
	}

	// jhipster-needle-entity-add-getters-setters - Jhipster will add getters
	// and setters here, do not remove

	public String getElectronicServiceLink() {
		return electronicServiceLink;
	}

	public void setElectronicServiceLink(String electronicServiceLink) {
		this.electronicServiceLink = electronicServiceLink;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ElectronicService electronicService = (ElectronicService) o;
		if (electronicService.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), electronicService.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "ElectronicService{" + "id=" + getId() + ", elecServiceTitlefa='" + getElecServiceTitlefa() + "'"
				+ ", elecServiceTitleen='" + getElecServiceTitleen() + "'" + ", electronicServiceCode='"
				+ getElectronicServiceCode() + "'" + ", elecServiceEvalCount='" + getElecServiceEvalCount() + "'"
				+ ", elecServiceEvalTotalScore='" + getElecServiceEvalTotalScore() + "'" + ", elecServiceEvalDate='"
				+ getElecServiceEvalDate() + "'" + ", parentServicefa='" + getParentServicefa() + "'"
				+ ", parentServiceen='" + getParentServiceen() + "'" + "}";
	}
}
