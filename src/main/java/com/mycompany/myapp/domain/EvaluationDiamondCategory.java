package com.mycompany.myapp.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A EvaluationDiamondCategory.
 */
@Entity
@Table(name = "evaluation_diamond_category")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EvaluationDiamondCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "eval_diamond_category_score")
    private Double evalDiamondCategoryScore;

    @ManyToOne
    private Evaluation evaluation;

    @ManyToOne
    private DiamondCategory diamondCategory;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getEvalDiamondCategoryScore() {
        return evalDiamondCategoryScore;
    }

    public EvaluationDiamondCategory evalDiamondCategoryScore(Double evalDiamondCategoryScore) {
        this.evalDiamondCategoryScore = evalDiamondCategoryScore;
        return this;
    }

    public void setEvalDiamondCategoryScore(Double evalDiamondCategoryScore) {
        this.evalDiamondCategoryScore = evalDiamondCategoryScore;
    }

    public Evaluation getEvaluation() {
        return evaluation;
    }

    public EvaluationDiamondCategory evaluation(Evaluation evaluation) {
        this.evaluation = evaluation;
        return this;
    }

    public void setEvaluation(Evaluation evaluation) {
        this.evaluation = evaluation;
    }

    public DiamondCategory getDiamondCategory() {
        return diamondCategory;
    }

    public EvaluationDiamondCategory diamondCategory(DiamondCategory diamondCategory) {
        this.diamondCategory = diamondCategory;
        return this;
    }

    public void setDiamondCategory(DiamondCategory diamondCategory) {
        this.diamondCategory = diamondCategory;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EvaluationDiamondCategory evaluationDiamondCategory = (EvaluationDiamondCategory) o;
        if (evaluationDiamondCategory.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), evaluationDiamondCategory.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EvaluationDiamondCategory{" +
            "id=" + getId() +
            ", evalDiamondCategoryScore='" + getEvalDiamondCategoryScore() + "'" +
            "}";
    }
}
