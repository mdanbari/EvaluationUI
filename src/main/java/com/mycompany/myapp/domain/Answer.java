package com.mycompany.myapp.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Answer.
 */
@Entity
@Table(name = "answer")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Answer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "answer_titlefa", nullable = false)
    private String answerTitlefa;

   
    @Column(name = "answer_titleen", nullable = false)
    private String answerTitleen;

    @NotNull
    @Column(name = "answer_value", nullable = false)
    private Double answerValue;

    @ManyToOne
    private Question question;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAnswerTitlefa() {
        return answerTitlefa;
    }

    public Answer answerTitlefa(String answerTitlefa) {
        this.answerTitlefa = answerTitlefa;
        return this;
    }

    public void setAnswerTitlefa(String answerTitlefa) {
        this.answerTitlefa = answerTitlefa;
    }

    public String getAnswerTitleen() {
        return answerTitleen;
    }

    public Answer answerTitleen(String answerTitleen) {
        this.answerTitleen = answerTitleen;
        return this;
    }

    public void setAnswerTitleen(String answerTitleen) {
        this.answerTitleen = answerTitleen;
    }

    public Double getAnswerValue() {
        return answerValue;
    }

    public Answer answerValue(Double answerValue) {
        this.answerValue = answerValue;
        return this;
    }

    public void setAnswerValue(Double answerValue) {
        this.answerValue = answerValue;
    }

    public Question getQuestion() {
        return question;
    }

    public Answer question(Question question) {
        this.question = question;
        return this;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Answer answer = (Answer) o;
        if (answer.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), answer.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Answer{" +
            "id=" + getId() +
            ", answerTitlefa='" + getAnswerTitlefa() + "'" +
            ", answerTitleen='" + getAnswerTitleen() + "'" +
            ", answerValue='" + getAnswerValue() + "'" +
            "}";
    }
}
