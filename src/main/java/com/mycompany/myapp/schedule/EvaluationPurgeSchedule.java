package com.mycompany.myapp.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mycompany.myapp.service.EvaluationService;

@Component
public class EvaluationPurgeSchedule {
	
	@Autowired
	EvaluationService evaluationService;
	
	public void runTask() {
		//evaluationService.purgeOldEvaluations();
	}
	

}
