package com.mycompany.myapp.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mycompany.myapp.config.Constants;
import com.mycompany.myapp.domain.SystemProfile;
import com.mycompany.myapp.repository.SystemProfileRepository;
import com.mycompany.myapp.service.util.ExcelReaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class DataInitializerSchedule {
//	@Autowired
//	public MaturityStageService maturityservice;
//	@Autowired
//	public QuestionService questionService;
//	@Autowired
//	public AnswerService answerService;
//	@Autowired
//	public DiamondCategoryService diamondService;
//	@Autowired
//	public DepartmentService departmentService;
//	@Autowired
//	public ElectronicServiceService elecService;
//	@Autowired
//	public ElecServiceDiamondCategoryService elecDiamondService;
//	@Autowired
//	public ElecServiceMaturityStateService elecMaturityService;
//	@Autowired
//	public ElecServiceQuestionService elecQuestionService;
	@Autowired
	private SystemProfileRepository systemProfileRepository;
	
	@Autowired
	private ExcelReaderUtil excelReader;
	
	private Logger log=LoggerFactory.getLogger(ExcelReaderUtil.class);
	

	public void runTask() {
		
		SystemProfile systemProfile = systemProfileRepository
				.findAllBySystemProfileKey(Constants.EVAL_INITIALDATA_QUESTION_FILE_PATH).get(0);
		SystemProfile systemProfile1=systemProfileRepository.findAllBySystemProfileKey(Constants.EVAL_INITIALDATA_ELECTRONICSERVICE_FILE_PATH).get(0);
		SystemProfile systemProfile2=systemProfileRepository.findAllBySystemProfileKey(Constants.EVAL_INITIALDATA_REPORT_FILE_PATH).get(0);
		SystemProfile systemProfile3=systemProfileRepository.findAllBySystemProfileKey(Constants.EVAL_INITIALDATA_QUESTION_REPORT_FILE_PATH).get(0);
		try{
		excelReader.readQyestionAnswer_Fa_File(systemProfile.getSystemProfileValue());
		}catch(Exception e)
		{
			System.err.println(e.getMessage());
			log.error(e.getMessage());
		}
		try{
		excelReader.initializeElectronicSevice(systemProfile1.getSystemProfileValue());
		}
		catch(Exception e)
		{
			System.err.println(e.getMessage());
			log.error(e.getMessage());
		}
		try{
		excelReader.insertAutomateReport(systemProfile2.getSystemProfileValue());
		}
		catch(Exception e)
		{
			System.err.println(e.getMessage());
			log.error(e.getMessage());
		}
		try{
		excelReader.inserElecServiceReportOnQuestion(systemProfile3.getSystemProfileValue());
		}
		catch(Exception e)
		{
			System.err.println(e.getMessage());
			log.error(e.getMessage());
		}
		
		
		
	}

}
