package com.mycompany.myapp.config;

/**
 * Application constants.
 */
public final class Constants {

	 public static final String LOGIN_REGEX = "^[_'.@A-Za-z0-9-]*$";

	    public static final String SYSTEM_ACCOUNT = "system";
	    public static final String ANONYMOUS_USER = "anonymoususer";
	    public static final String EVAL_PURG_TASK_INTERVAL = "eval.purge.task.interval";
	   	public static final String EVAL_DATA_INIT_TASK_INTERVAL = "eval.datainit.task.interval"; 
	   	public static final String EVAL_ARCHIVE_TASK_INTERVAL = "eval.archive.task.interval";

	   	public static final String EVAL_PURG_TASK_ENABLE = "eval.purge.task.enable";
	   	public static final String EVAL_DATA_INIT_TASK_ENABLE = "eval.datainit.task.enable";
	   	public static final String EVAL_ARCHIVE_TASK_ENABLE = "eval.archive.task.enable"; 
	   	public static final String EVAL_DEFAULT_INTERVAL = "10000"; //10s
	   	public static final String EVAL_INITIALDATA_QUESTION_FILE_PATH="eval.xls.path";
	   	public static final String EVAL_INITIALDATA_ELECTRONICSERVICE_FILE_PATH="elec.xls.path";
	   	public static final String EVAL_DEFAULT_ELEC_CONFIRM_COUNT = "2";
	   	public static final String EVAL_INITIALDATA_REPORT_FILE_PATH = "report.xls.path"; 
	   	public static final String EVAL_INITIALDATA_QUESTION_REPORT_FILE_PATH="question.report.xls.path";
	   	
    private Constants() {
    }
}
