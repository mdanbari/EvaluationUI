package com.mycompany.myapp.config;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.util.StringUtils;

import com.mycompany.myapp.domain.SystemProfile;
import com.mycompany.myapp.schedule.DataInitializerSchedule;
import com.mycompany.myapp.schedule.EvaluationArchiveSchedule;
import com.mycompany.myapp.schedule.EvaluationPurgeSchedule;


@Configuration
@EnableScheduling
public class DynamicScheduleConfiguration implements SchedulingConfigurer{	

	@Autowired
    JdbcTemplate jdbcTemplate;
	

	@Bean
	public EvaluationArchiveSchedule evaluationArchiveSchedule() {
		return new EvaluationArchiveSchedule();
	}

	@Bean
	public EvaluationPurgeSchedule evaluationPurgeSchedule() {
		return new EvaluationPurgeSchedule();
	}

	@Bean
	public DataInitializerSchedule dataInitializerSchedule() {
		return new DataInitializerSchedule();
	}

	@Bean(destroyMethod = "shutdown")
	public Executor taskExecutor() {
		ThreadPoolTaskScheduler executor = new ThreadPoolTaskScheduler();
		executor.setPoolSize(10);
		return executor;
	}

	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
		taskRegistrar.setScheduler(taskExecutor());
		taskRegistrar.addTriggerTask(new Runnable() {
			@Override
			public void run() {
				try {
					evaluationPurgeSchedule().runTask();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, new Trigger() {
			@Override
			public Date nextExecutionTime(TriggerContext triggerContext) {
				        if(isEnableTask(Constants.EVAL_PURG_TASK_ENABLE))
				        {
							Calendar nextExecutionTime = new GregorianCalendar();
							Date lastActualExecutionTime = triggerContext.lastActualExecutionTime();
							nextExecutionTime.setTime(lastActualExecutionTime != null ? lastActualExecutionTime : new Date());
							nextExecutionTime.add(Calendar.MILLISECOND,Integer.valueOf(getNextTimeExecution(Constants.EVAL_PURG_TASK_INTERVAL)) * 1000);
							return nextExecutionTime.getTime();
				        }
						return null;

			}
		}

		);
		taskRegistrar.addTriggerTask(new Runnable() {
			@Override
			public void run() {
				try {
					dataInitializerSchedule().runTask();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, new Trigger() {
			@Override
			public Date nextExecutionTime(TriggerContext triggerContext) {
				 if(isEnableTask(Constants.EVAL_DATA_INIT_TASK_ENABLE)){
						Calendar nextExecutionTime = new GregorianCalendar();
						Date lastActualExecutionTime = triggerContext.lastActualExecutionTime();
						nextExecutionTime.setTime(lastActualExecutionTime != null ? lastActualExecutionTime : new Date());
						nextExecutionTime.add(Calendar.MILLISECOND,Integer.valueOf(getNextTimeExecution(Constants.EVAL_DATA_INIT_TASK_INTERVAL)) * 1000);
						return nextExecutionTime.getTime();
			        }
				return null;
			}
		}

		);
		taskRegistrar.addTriggerTask(new Runnable() {
			@Override
			public void run() {
				try {
					evaluationArchiveSchedule().runTask();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, new Trigger() {
			@Override
			public Date nextExecutionTime(TriggerContext triggerContext) {
				if(isEnableTask(Constants.EVAL_ARCHIVE_TASK_ENABLE)){
					Calendar nextExecutionTime = new GregorianCalendar();
					Date lastActualExecutionTime = triggerContext.lastActualExecutionTime();
					nextExecutionTime.setTime(lastActualExecutionTime != null ? lastActualExecutionTime : new Date());
					nextExecutionTime.add(Calendar.MILLISECOND,Integer.valueOf(getNextTimeExecution(Constants.EVAL_ARCHIVE_TASK_INTERVAL)) * 1000);
					return nextExecutionTime.getTime();
		        }
				return null;
			}
		}

		);

	}
	
	
	private String getNextTimeExecution(String keyName)
	{	
		    String selectQuery = "select * from  system_profile sp where sp.system_profile_key = ? ";
		    SystemProfile  systemProfile = new SystemProfile();
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(selectQuery, keyName);
			for (Map row : rows) {
				systemProfile.setSystemProfileKey((String)row.get("system_profile_key"));
				systemProfile.setSystemProfileValue((String)row.get("system_profile_value"));
			}
			if (!StringUtils.isEmpty(systemProfile.getSystemProfileKey()) && !StringUtils.isEmpty(systemProfile.getSystemProfileValue()) && org.apache.commons.lang3.StringUtils.isNumeric(systemProfile.getSystemProfileValue())) 
				return systemProfile.getSystemProfileValue();				
			return Constants.EVAL_DEFAULT_INTERVAL;
	}
	
	private Boolean isEnableTask(String keyName)
	{
		String selectQuery = "select * from  system_profile sp where sp.system_profile_key = ? ";
	    SystemProfile  systemProfile = new SystemProfile();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(selectQuery, keyName);
		for (Map row : rows) {
			systemProfile.setSystemProfileKey((String)row.get("system_profile_key"));
			systemProfile.setSystemProfileValue((String)row.get("system_profile_value"));
		}
		if (!StringUtils.isEmpty(systemProfile.getSystemProfileKey()) && !StringUtils.isEmpty(systemProfile.getSystemProfileValue())) 
			return true;
		return false;		
	}

	

}
