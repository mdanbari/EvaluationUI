package com.mycompany.myapp.config;

import java.util.concurrent.TimeUnit;

import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.github.jhipster.config.JHipsterProperties;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.mycompany.myapp.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.MaturityStage.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.DiamondCategory.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.DiamondCategory.class.getName() + ".questions", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.DiamondCategory.class.getName() + ".electronicServices", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.UnMaturityModel.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Question.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Question.class.getName() + ".answers", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Question.class.getName() + ".diamondCategories", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Answer.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Department.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.ElectronicService.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.ElectronicService.class.getName() + ".diamondCategories", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.SystemProfile.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Evaluation.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Evaluation.class.getName() + ".questionAnswers", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.EvaluationMaturityState.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.EvaluationDiamondCategory.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.ElecServiceMaturityState.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.ElecServiceDiamondCategory.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.EvaluationArchive.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.EvaluationArchive.class.getName() + ".questionAnswers", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.QuestionAnswer.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.HelpFile.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.ElecServiceQuestion.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
