package com.mycompany.myapp.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mycompany.myapp.domain.EvaluationMaturityState;

/**
 * Service Interface for managing EvaluationMaturityState.
 */
public interface EvaluationMaturityStateService {

	/**
	 * Save a evaluationMaturityState.
	 *
	 * @param evaluationMaturityState
	 *            the entity to save
	 * @return the persisted entity
	 */
	EvaluationMaturityState save(EvaluationMaturityState evaluationMaturityState);

	/**
	 * Get all the evaluationMaturityStates.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	Page<EvaluationMaturityState> findAll(Pageable pageable);

	/**
	 * Get the "id" evaluationMaturityState.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	EvaluationMaturityState findOne(Long id);

	/**
	 * Delete the "id" evaluationMaturityState.
	 *
	 * @param id
	 *            the id of the entity
	 */
	void delete(Long id);

	/**
	 *
	 * findAllByEvaluationIdIn
	 *
	 * @param ids
	 * @return
	 *
	 * 		List<EvaluationMaturityState>
	 */
	List<EvaluationMaturityState> findAllByEvaluationIdIn(List<Long> ids);

}
