package com.mycompany.myapp.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.DiamondCategory;
import com.mycompany.myapp.repository.DiamondCategoryRepository;
import com.mycompany.myapp.service.DiamondCategoryService;


/**
 * Service Implementation for managing DiamondCategory.
 */
@Service
@Transactional
public class DiamondCategoryServiceImpl implements DiamondCategoryService{

    private final Logger log = LoggerFactory.getLogger(DiamondCategoryServiceImpl.class);

    private final DiamondCategoryRepository diamondCategoryRepository;
    public DiamondCategoryServiceImpl(DiamondCategoryRepository diamondCategoryRepository) {
        this.diamondCategoryRepository = diamondCategoryRepository;
    }

    /**
     * Save a diamondCategory.
     *
     * @param diamondCategory the entity to save
     * @return the persisted entity
     */
    @Override
    public DiamondCategory save(DiamondCategory diamondCategory) {
        log.debug("Request to save DiamondCategory : {}", diamondCategory);
        return diamondCategoryRepository.save(diamondCategory);
    }

    /**
     *  Get all the diamondCategories.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DiamondCategory> findAll(Pageable pageable) {
        log.debug("Request to get all DiamondCategories");
        return diamondCategoryRepository.findAll(pageable);
    }

    /**
     *  Get one diamondCategory by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public DiamondCategory findOne(Long id) {
        log.debug("Request to get DiamondCategory : {}", id);
        return diamondCategoryRepository.findOne(id);
    }

    /**
     *  Delete the  diamondCategory by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DiamondCategory : {}", id);
        diamondCategoryRepository.delete(id);
    }

	@Override
	public List<DiamondCategory> findAll() {
		return diamondCategoryRepository.findAll();
	}

	@Override
	public DiamondCategory findByTitlefa(String title) {
		return diamondCategoryRepository.findOneByDiamondCategoryTitlefa(title);
	}
}
