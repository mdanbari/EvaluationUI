package com.mycompany.myapp.service.impl;

import java.time.Period;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.config.Constants;
import com.mycompany.myapp.domain.Answer;
import com.mycompany.myapp.domain.DiamondCategory;
import com.mycompany.myapp.domain.ElecServiceDiamondCategory;
import com.mycompany.myapp.domain.ElecServiceMaturityState;
import com.mycompany.myapp.domain.ElecServiceQuestion;
import com.mycompany.myapp.domain.ElectronicService;
import com.mycompany.myapp.domain.Evaluation;
import com.mycompany.myapp.domain.EvaluationDiamondCategory;
import com.mycompany.myapp.domain.EvaluationMaturityState;
import com.mycompany.myapp.domain.MaturityStage;
import com.mycompany.myapp.domain.Question;
import com.mycompany.myapp.domain.QuestionAnswer;
import com.mycompany.myapp.domain.SystemProfile;
import com.mycompany.myapp.domain.enumeration.EvaluationState;
import com.mycompany.myapp.repository.ElectronicServiceRepository;
import com.mycompany.myapp.repository.EvaluationRepository;
import com.mycompany.myapp.repository.SystemProfileRepository;
import com.mycompany.myapp.security.SecurityUtils;
import com.mycompany.myapp.service.ElecServiceDiamondCategoryService;
import com.mycompany.myapp.service.ElecServiceMaturityStateService;
import com.mycompany.myapp.service.ElecServiceQuestionService;
import com.mycompany.myapp.service.ElectronicServiceService;
import com.mycompany.myapp.service.EvaluationDiamondCategoryService;
import com.mycompany.myapp.service.EvaluationMaturityStateService;
import com.mycompany.myapp.service.EvaluationService;
import com.mycompany.myapp.service.QuestionAnswerService;
import com.mycompany.myapp.service.QuestionService;
import com.mycompany.myapp.service.dto.EvaluationDTO;
import com.mycompany.myapp.service.dto.QuestionAnswerDTO;

/**
 * Service Implementation for managing Evaluation.
 */
@Service
@Transactional
public class EvaluationServiceImpl implements EvaluationService {

	private final Logger log = LoggerFactory.getLogger(EvaluationServiceImpl.class);

	private final EvaluationRepository evaluationRepository;
	private final ElectronicServiceService electronicService;
	private final QuestionService questionService;
	private final QuestionAnswerService questionAnswerService;

	@Autowired
	private ElectronicServiceRepository electronicServiceRepository;

	private EvaluationMaturityStateService evaluationMaturityStateService;

	private EvaluationDiamondCategoryService evaluationDiamondCategoryService;

	private SystemProfileRepository systemProfileRepository;

	private ElecServiceMaturityStateService elecServiceMaturityStateService;

	private ElecServiceDiamondCategoryService elecServiceDiamondCategoryService;
	
	private ElecServiceQuestionService elecServiceQuestionService;
	


	public EvaluationServiceImpl(EvaluationRepository evaluationRepository, ElectronicServiceService electronicService,
			QuestionService questionService, QuestionAnswerService questionAnswerService,
			ElectronicServiceRepository electronicServiceRepository,
			EvaluationMaturityStateService evaluationMaturityStateService,
			EvaluationDiamondCategoryService evaluationDiamondCategoryService,
			SystemProfileRepository systemProfileRepository,
			ElecServiceMaturityStateService elecServiceMaturityStateService,
			ElecServiceDiamondCategoryService elecServiceDiamondCategoryService,
			ElecServiceQuestionService elecServiceQuestionService) {
		this.evaluationRepository = evaluationRepository;
		this.electronicService = electronicService;
		this.questionService = questionService;
		this.questionAnswerService = questionAnswerService;
		this.electronicServiceRepository = electronicServiceRepository;
		this.evaluationMaturityStateService = evaluationMaturityStateService;
		this.evaluationDiamondCategoryService = evaluationDiamondCategoryService;
		this.systemProfileRepository = systemProfileRepository;
		this.elecServiceMaturityStateService = elecServiceMaturityStateService;
		this.elecServiceDiamondCategoryService = elecServiceDiamondCategoryService;
		this.elecServiceQuestionService = elecServiceQuestionService;
	}

	/**
	 * Save a evaluation.
	 *
	 * @param evaluation
	 *            the entity to save
	 * @return the persisted entity
	 */
	@Override
	public Evaluation save(Evaluation evaluation) {
		log.debug("Request to save Evaluation : {}", evaluation);
		return evaluationRepository.save(evaluation);
	}

	/**
	 * Get all the evaluations.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<Evaluation> findAll(Pageable pageable) {
		log.debug("Request to get all Evaluations");
		return evaluationRepository.findAll(pageable);
	}

	/**
	 * Get one evaluation by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public Evaluation findOne(Long id) {
		log.debug("Request to get Evaluation : {}", id);
		return evaluationRepository.findOne(id);
	}

	/**
	 * Delete the evaluation by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Evaluation : {}", id);
		evaluationRepository.delete(id);
	}

	@Override
	public Page<Evaluation> findAllMyEvaluation(Pageable pageable) {
		log.debug("Request to find all my Evaluation : {}", SecurityUtils.getCurrentUserLogin());

		Evaluation evaluation = new Evaluation();
		evaluation.setEvalPerson(SecurityUtils.getCurrentUserLogin());
		Example<Evaluation> example = Example.of(evaluation);

		return evaluationRepository.findAll(example, pageable);
	}

	@Override
	public EvaluationDTO getRandomEvaluation() {

		Evaluation evaluation = new Evaluation();

		evaluation.setEvalPerson(SecurityUtils.getCurrentUserLogin());

		evaluation.setEvalStartDate(ZonedDateTime.now());
		evaluation.setEvalLastModifiedDate(ZonedDateTime.now());

		// FIXME load period from properties
		evaluation.setEvalDeadLineDate(ZonedDateTime.now().plus(Period.ofDays(3)));

		evaluation.setEvaluationState(EvaluationState.INPROGRESS);

		evaluation.setElectronicService(getRandomElectronicService());

		evaluation = evaluationRepository.save(evaluation);

		evaluation = wrapDataAndQuestionForClient(evaluation);

		return new EvaluationDTO(evaluation);
	}

	private Evaluation wrapDataAndQuestionForClient(Evaluation evaluation) {

		Page<Question> page = questionService.findAll(null);

		Set<QuestionAnswer> questionAnswers = new HashSet<>();

		for (Question question : page) {

			QuestionAnswer questionAnswer = new QuestionAnswer();

			question.getAnswers();
			questionAnswer.setQuestion(question);
			questionAnswer.setEvaluation(evaluation);

			questionAnswer = questionAnswerService.save(questionAnswer);
			questionAnswers.add(questionAnswer);

		}

		evaluation.setQuestionAnswers(questionAnswers);

		evaluation = evaluationRepository.save(evaluation);

		return evaluation;
	}

	private ElectronicService getRandomElectronicService() {
		return electronicService.getRandomElectronicServiceForEvaluation();
	}

	@Override
	public EvaluationDTO getCurrentEvaluation() {
		Evaluation evaluation = new Evaluation();
		evaluation.setEvalPerson(SecurityUtils.getCurrentUserLogin());
		evaluation.setEvaluationState(EvaluationState.INPROGRESS);
		Example<Evaluation> example = Example.of(evaluation);
		evaluation = evaluationRepository.findOne(example);
		return new EvaluationDTO(evaluation);
	}

	@Override
	public EvaluationDTO getCurrentGeneralEvaluation() {
		Evaluation evaluation = new Evaluation();
		evaluation.setEvalPerson(SecurityUtils.getCurrentUserLogin());
		evaluation.setEvaluationState(EvaluationState.GENERAL);
		Example<Evaluation> example = Example.of(evaluation);
		evaluation = evaluationRepository.findOne(example);
		return new EvaluationDTO(evaluation);
	}

	@Override
	public EvaluationDTO findMyEvaluation(Long id) {
		Evaluation evaluation = evaluationRepository.findOne(id);
		return new EvaluationDTO(evaluation);
	}

	@Override
	public Page<Evaluation> findAllCompleteEvaluation(Pageable pageable) {
		log.debug("Request to find all complete Evaluation : {}", SecurityUtils.getCurrentUserLogin());

		Evaluation evaluation = new Evaluation();
		evaluation.setEvaluationState(EvaluationState.COMPLETE);
		Example<Evaluation> example = Example.of(evaluation);

		return evaluationRepository.findAll(example, pageable);
	}

	@Override
	public Page<Evaluation> findAllMyCompleteEvaluation(Pageable pageable) {
		log.debug("Request to find all complete Evaluation : {}", SecurityUtils.getCurrentUserLogin());
		
		Evaluation evaluation = new Evaluation();
		evaluation.setEvalConfirmer(SecurityUtils.getCurrentUserLogin());
		Example<Evaluation> example = Example.of(evaluation);
		
		return evaluationRepository.findAll(example, pageable);
	}

	@Override
	public EvaluationDTO getEvaluationGeneral(Long id) {

		Evaluation evaluation = new Evaluation();

		evaluation.setEvalPerson(SecurityUtils.getCurrentUserLogin());

		evaluation.setEvalStartDate(ZonedDateTime.now());
		evaluation.setEvalLastModifiedDate(ZonedDateTime.now());

		// FIXME load period from properties
		evaluation.setEvalDeadLineDate(ZonedDateTime.now().plus(Period.ofDays(3)));

		evaluation.setEvaluationState(EvaluationState.GENERAL);

		evaluation.setElectronicService(electronicService.findOne(id));

		evaluation = evaluationRepository.save(evaluation);

		evaluation = wrapDataAndQuestionForClient(evaluation);

		return new EvaluationDTO(evaluation);

	}

	public void purgeOldEvaluations() {
		List<Evaluation> evaluationList = evaluationRepository
				.findByEvaluationStateAndEvalDeadLineDateLessThan(EvaluationState.INPROGRESS, ZonedDateTime.now());
		for (Evaluation evaluation : evaluationList) {
			ElectronicService electronicService = evaluation.getElectronicService();
			electronicService.setElecServiceEvalCount(electronicService.getElecServiceEvalCount() + 1);
			electronicServiceRepository.save(electronicService);
			evaluation.setEvaluationState(EvaluationState.EXPIRED);
			evaluationRepository.save(evaluation);
		}
	}

	@Override
	public void answeringEvaluation(Long evalId) {

		Evaluation evaluation = findOne(evalId);

		evaluation.getQuestionAnswers();

		if (evaluation.getEvaluationState() == EvaluationState.GENERAL) {
			evaluation.setEvaluationState(EvaluationState.CONFIRMED);

		} else if (evaluation.getEvaluationState() == EvaluationState.INPROGRESS) {
			evaluation.setEvaluationState(EvaluationState.COMPLETE);
		}

		evaluation.setEvalEndDate(ZonedDateTime.now());

		double score = calculateEvaluationScore(evaluation);

		evaluation.setEvalScore(score);

		evaluationRepository.save(evaluation);
	}

	private double calculateEvaluationScore(Evaluation evaluation) {

		double score = calculateEvaluationMaturityScore(evaluation);

		calculateEvaluationDiamondScore(evaluation);
		
		calculateEvaluationQuestionScore(evaluation);

		return score;
	}

	private void calculateEvaluationQuestionScore(Evaluation evaluation) {
		Set<QuestionAnswer> questionAnswers = evaluation.getQuestionAnswers();
		for (QuestionAnswer questionAnswer : questionAnswers) {
			ElecServiceQuestion elecServiceQuestion = new ElecServiceQuestion();
			elecServiceQuestion.setCount(1);
			elecServiceQuestion.setQuestion(questionAnswer.getQuestion());
			elecServiceQuestion.setElecServiceQuestionDate(ZonedDateTime.now());
			elecServiceQuestion.setElectronicService(evaluation.getElectronicService());
			elecServiceQuestion.setElecServiceQuestionScore(questionAnswer.getAnswer().getAnswerValue() * 100);
			elecServiceQuestionService.save(elecServiceQuestion);
		}
		
	}

	private void calculateEvaluationDiamondScore(Evaluation evaluation) {

		Map<DiamondCategory, List<QuestionAnswer>> map = new HashMap<DiamondCategory, List<QuestionAnswer>>();

		Set<QuestionAnswer> questionAnswers = evaluation.getQuestionAnswers();

		for (QuestionAnswer questionAnswer : questionAnswers) {

			for (DiamondCategory diamondCategory : questionAnswer.getQuestion().getDiamondCategories()) {
				List<QuestionAnswer> list = map.get(diamondCategory);

				if (list == null)
					list = new ArrayList<>();

				list.add(questionAnswer);

				map.put(diamondCategory, list);
			}
		}
		for (DiamondCategory diamondCategory : map.keySet()) {
			EvaluationDiamondCategory evaluationDiamondCategory = new EvaluationDiamondCategory();
			evaluationDiamondCategory.setEvaluation(evaluation);
			evaluationDiamondCategory.setDiamondCategory(diamondCategory);

			double score = 0;

			double sum = 0;

			double sumWeight = 0;

			for (QuestionAnswer questionAnswer : map.get(diamondCategory)) {

				Double answerValue = 0.0;

				if (questionAnswer.getAnswer() != null)
					answerValue = questionAnswer.getAnswer().getAnswerValue();

				Double questionWeight = questionAnswer.getQuestion().getQuestionWeight();

				double multiply = answerValue.doubleValue() * questionWeight.doubleValue();

				sumWeight += questionWeight.doubleValue();

				sum += multiply;
			}

			score = sum * 100 / sumWeight;

			evaluationDiamondCategory.setEvalDiamondCategoryScore(score);

			evaluationDiamondCategoryService.save(evaluationDiamondCategory);
		}

	}

	private double calculateEvaluationMaturityScore(Evaluation evaluation) {

		Map<MaturityStage, List<QuestionAnswer>> map = new HashMap<MaturityStage, List<QuestionAnswer>>();

		Set<QuestionAnswer> questionAnswers = evaluation.getQuestionAnswers();

		double totalScore = 0;

		for (QuestionAnswer questionAnswer : questionAnswers) {
			List<QuestionAnswer> list = map.get(questionAnswer.getQuestion().getMaturityStage());

			if (list == null)
				list = new ArrayList<>();

			list.add(questionAnswer);

			map.put(questionAnswer.getQuestion().getMaturityStage(), list);
		}

		for (MaturityStage maturityStage : map.keySet()) {
			EvaluationMaturityState evaluationMaturityState = new EvaluationMaturityState();
			evaluationMaturityState.setEvaluation(evaluation);
			evaluationMaturityState.setMaturityStage(maturityStage);

			double score = 0;

			double sum = 0;

			double sumWeight = 0;

			for (QuestionAnswer questionAnswer : map.get(maturityStage)) {

				Double answerValue = 0.0;

				if (questionAnswer.getAnswer() != null)
					answerValue = questionAnswer.getAnswer().getAnswerValue();

				Double questionWeight = questionAnswer.getQuestion().getQuestionWeight();

				double multiply = answerValue.doubleValue() * questionWeight.doubleValue();

				sumWeight += questionWeight.doubleValue();

				sum += multiply;
			}

			score = sum * 100 / sumWeight;

			evaluationMaturityState.setEvalMaturityStateScore(score);

			totalScore += score;
			evaluationMaturityStateService.save(evaluationMaturityState);
		}

		return totalScore / map.keySet().size();

	}

	@Override
	public void checkingEvaluation(EvaluationDTO evaluation) {

		Evaluation entity = findOne(evaluation.getId());

		entity.setEvalConfirmer(SecurityUtils.getCurrentUserLogin());
		entity.setEvalConfirmedDate(ZonedDateTime.now());
		entity.setEvaluationState(evaluation.getEvaluationState());

		if (evaluation.getEvalDescriptionfa() != null && !"".equals(evaluation.getEvalDescriptionfa())) {
			entity.setEvalDescriptionen(evaluation.getEvalDescriptionfa());
			entity.setEvalDescriptionfa(evaluation.getEvalDescriptionfa());
		} else if (evaluation.getEvalDescriptionen() != null && !"".equals(evaluation.getEvalDescriptionen())) {
			entity.setEvalDescriptionen(evaluation.getEvalDescriptionen());
			entity.setEvalDescriptionfa(evaluation.getEvalDescriptionen());
		}

		evaluationRepository.save(entity);

		calculateNewScoreToElectronicService(entity);
	}

	private void calculateNewScoreToElectronicService(Evaluation evaluation) {
		if (evaluation.getEvaluationState() == EvaluationState.CONFIRMED) {
			SystemProfile profileKey = systemProfileRepository
					.findAllBySystemProfileKey(Constants.EVAL_DEFAULT_ELEC_CONFIRM_COUNT).get(0);

			evaluation.getElectronicService()
					.setElecServiceEvalCount(evaluation.getElectronicService().getElecServiceEvalCount() - 1);
			electronicService.save(evaluation.getElectronicService());
			if (evaluation.getElectronicService().getElecServiceEvalCount() == 0) {

				double score = calculateElectronicServiceScore(
						findLastConfirmedEvaluationByeElectronicService(evaluation.getElectronicService()),
						evaluation.getElectronicService());

				evaluation.getElectronicService().setElecServiceEvalTotalScore(score);
				evaluation.getElectronicService().setElecServiceEvalDate(ZonedDateTime.now());
				evaluation.getElectronicService()
						.setElecServiceEvalCount(Integer.valueOf(profileKey.getSystemProfileValue()));
				electronicService.save(evaluation.getElectronicService());

			}
		}
	}

	private List<Long> findLastConfirmedEvaluationByeElectronicService(ElectronicService electronicService2) {
		return evaluationRepository.findAllByElecServiceAndEvaluationStateOrderByEvalConfirmedDateDesc(
				electronicService2.getId(), EvaluationState.CONFIRMED);
	}

	private double calculateElectronicServiceScore(List<Long> ids, ElectronicService electronicService) {

		double score = calculateElectronicServiceMaturityStageScore(ids, electronicService);

		calcualteElectronicServiceDiamondScore(ids, electronicService);

		return score;
	}

	private double calculateElectronicServiceMaturityStageScore(List<Long> ids, ElectronicService electronicService2) {

		List<EvaluationMaturityState> list = evaluationMaturityStateService.findAllByEvaluationIdIn(ids);

		Map<MaturityStage, List<EvaluationMaturityState>> map = new HashMap<MaturityStage, List<EvaluationMaturityState>>();

		double totalScore = 0;

		for (EvaluationMaturityState evaluationMaturityState : list) {
			List<EvaluationMaturityState> evaluationMaturityStates = map
					.get(evaluationMaturityState.getMaturityStage());

			if (evaluationMaturityStates == null)
				evaluationMaturityStates = new ArrayList<>();

			evaluationMaturityStates.add(evaluationMaturityState);

			map.put(evaluationMaturityState.getMaturityStage(), evaluationMaturityStates);
		}

		for (MaturityStage maturityStage : map.keySet()) {
			ElecServiceMaturityState elecServiceMaturityState = new ElecServiceMaturityState();
			elecServiceMaturityState.setElectronicService(electronicService2);
			elecServiceMaturityState.setMaturityStage(maturityStage);

			double score = 0;

			double sum = 0;

			double sumWeight = 0;

			for (EvaluationMaturityState evaluationMaturityState : map.get(maturityStage)) {

				sum += evaluationMaturityState.getEvalMaturityStateScore();
				sumWeight++;
			}

			score = sum / sumWeight;

			elecServiceMaturityState.setElecMaturityStateScore(score);
			elecServiceMaturityState.setElecMaturityDate(ZonedDateTime.now());

			totalScore += score;
			elecServiceMaturityStateService.save(elecServiceMaturityState);
		}

		return totalScore / map.keySet().size();
	}

	private void calcualteElectronicServiceDiamondScore(List<Long> ids, ElectronicService electronicService2) {

		List<EvaluationDiamondCategory> list = evaluationDiamondCategoryService.findAllByEvaluationIdIn(ids);

		Map<DiamondCategory, List<EvaluationDiamondCategory>> map = new HashMap<DiamondCategory, List<EvaluationDiamondCategory>>();

		for (EvaluationDiamondCategory evaluationDiamondCategory : list) {
			List<EvaluationDiamondCategory> evaluationDiamondCategorys = map
					.get(evaluationDiamondCategory.getDiamondCategory());

			if (evaluationDiamondCategorys == null)
				evaluationDiamondCategorys = new ArrayList<>();

			evaluationDiamondCategorys.add(evaluationDiamondCategory);

			map.put(evaluationDiamondCategory.getDiamondCategory(), evaluationDiamondCategorys);
		}

		for (DiamondCategory diamondCategory : map.keySet()) {
			ElecServiceDiamondCategory elecServiceDiamondCategory = new ElecServiceDiamondCategory();
			elecServiceDiamondCategory.setElectronicService(electronicService2);
			elecServiceDiamondCategory.setDiamondCategory(diamondCategory);

			double score = 0;

			double sum = 0;

			double sumWeight = 0;

			for (EvaluationDiamondCategory evaluationDiamondCategory : map.get(diamondCategory)) {

				sum += evaluationDiamondCategory.getEvalDiamondCategoryScore();
				sumWeight++;
			}

			score = sum / sumWeight;

			elecServiceDiamondCategory.setElecDiamondCategoryScore(score);
			elecServiceDiamondCategory.setElecDiamondDate(ZonedDateTime.now());

			elecServiceDiamondCategoryService.save(elecServiceDiamondCategory);
		}

	}

	@Override
	public Page<Evaluation> findAllConfirmedEvaluation(Pageable pageable) {
		log.debug("Request to find all complete Evaluation : {}", SecurityUtils.getCurrentUserLogin());
		Evaluation evaluation = new Evaluation();
		evaluation.setEvaluationState(EvaluationState.CONFIRMED);
		Example<Evaluation> example = Example.of(evaluation);
		return evaluationRepository.findAll(example, pageable);
	}

	@Override
	public void temporarySaveEvaluation(EvaluationDTO evaluation) {
		List<QuestionAnswerDTO> questionAnswers = evaluation.getQuestionAnswers();
		
		for (QuestionAnswerDTO questionAnswerDTO : questionAnswers) {
			if(questionAnswerDTO.getAnswer()!=null)
			{
				QuestionAnswer questionAnswerEntity = questionAnswerService.findOne(questionAnswerDTO.getId());
				Answer answer = new Answer();
				answer.setId(questionAnswerDTO.getAnswer().getId());
				questionAnswerEntity.setAnswer(answer);
				questionAnswerService.save(questionAnswerEntity);
			}
		}
		
		Evaluation evanulationEntity = findOne(evaluation.getId());
		evanulationEntity.setEvalLastModifiedDate(ZonedDateTime.now());
		save(evanulationEntity);
		
	}

}
