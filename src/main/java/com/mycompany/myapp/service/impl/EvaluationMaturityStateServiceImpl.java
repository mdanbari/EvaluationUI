package com.mycompany.myapp.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.EvaluationMaturityState;
import com.mycompany.myapp.repository.EvaluationMaturityStateRepository;
import com.mycompany.myapp.service.EvaluationMaturityStateService;


/**
 * Service Implementation for managing EvaluationMaturityState.
 */
@Service
@Transactional
public class EvaluationMaturityStateServiceImpl implements EvaluationMaturityStateService{

    private final Logger log = LoggerFactory.getLogger(EvaluationMaturityStateServiceImpl.class);

    private final EvaluationMaturityStateRepository evaluationMaturityStateRepository;
    public EvaluationMaturityStateServiceImpl(EvaluationMaturityStateRepository evaluationMaturityStateRepository) {
        this.evaluationMaturityStateRepository = evaluationMaturityStateRepository;
    }

    /**
     * Save a evaluationMaturityState.
     *
     * @param evaluationMaturityState the entity to save
     * @return the persisted entity
     */
    @Override
    public EvaluationMaturityState save(EvaluationMaturityState evaluationMaturityState) {
        log.debug("Request to save EvaluationMaturityState : {}", evaluationMaturityState);
        return evaluationMaturityStateRepository.save(evaluationMaturityState);
    }

    /**
     *  Get all the evaluationMaturityStates.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EvaluationMaturityState> findAll(Pageable pageable) {
        log.debug("Request to get all EvaluationMaturityStates");
        return evaluationMaturityStateRepository.findAll(pageable);
    }

    /**
     *  Get one evaluationMaturityState by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public EvaluationMaturityState findOne(Long id) {
        log.debug("Request to get EvaluationMaturityState : {}", id);
        return evaluationMaturityStateRepository.findOne(id);
    }

    /**
     *  Delete the  evaluationMaturityState by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EvaluationMaturityState : {}", id);
        evaluationMaturityStateRepository.delete(id);
    }

	@Override
	public List<EvaluationMaturityState> findAllByEvaluationIdIn(List<Long> ids) {
		return evaluationMaturityStateRepository.findAllByEvaluationIdIn(ids);
	}
}
