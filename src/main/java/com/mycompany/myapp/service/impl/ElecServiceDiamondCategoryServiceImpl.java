package com.mycompany.myapp.service.impl;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.Department;
import com.mycompany.myapp.domain.DiamondCategory;
import com.mycompany.myapp.domain.ElecServiceDiamondCategory;
import com.mycompany.myapp.domain.ElectronicService;
import com.mycompany.myapp.domain.DiamondCategory;
import com.mycompany.myapp.repository.DepartmentRepository;
import com.mycompany.myapp.repository.DiamondCategoryRepository;
import com.mycompany.myapp.repository.ElecServiceDiamondCategoryRepository;
import com.mycompany.myapp.repository.ElectronicServiceRepository;
import com.mycompany.myapp.service.ElecServiceDiamondCategoryService;
import com.mycompany.myapp.service.dto.ConsolidatedChartReportResultDTO;
import com.mycompany.myapp.service.dto.ConsolidatedReportInputDTO;
import com.mycompany.myapp.service.dto.DepartmentReportInputDTO;
import com.mycompany.myapp.service.dto.DepartmentReportResultDTO;
import com.mycompany.myapp.service.dto.SeparationChartReportJpaDTO;
import com.mycompany.myapp.service.dto.SeparationChartReportResultDTO;
import com.mycompany.myapp.service.dto.SeparationReportInputDTO;


/**
 * Service Implementation for managing ElecServiceDiamondCategory.
 */
@Service
@Transactional
public class ElecServiceDiamondCategoryServiceImpl implements ElecServiceDiamondCategoryService{

    private final Logger log = LoggerFactory.getLogger(ElecServiceDiamondCategoryServiceImpl.class);

    private final ElecServiceDiamondCategoryRepository elecServiceDiamondCategoryRepository;
    private final DiamondCategoryRepository diamondCategoryRepository;
    private final ElectronicServiceRepository electronicServiceRepository;
    private final DepartmentRepository departmentRepository;
    


   

	public ElecServiceDiamondCategoryServiceImpl(
			ElecServiceDiamondCategoryRepository elecServiceDiamondCategoryRepository,
			DiamondCategoryRepository diamondCategoryRepository,
			ElectronicServiceRepository electronicServiceRepository, DepartmentRepository departmentRepository) {
		this.elecServiceDiamondCategoryRepository = elecServiceDiamondCategoryRepository;
		this.diamondCategoryRepository = diamondCategoryRepository;
		this.electronicServiceRepository = electronicServiceRepository;
		this.departmentRepository = departmentRepository;
	}

	/**
     * Save a elecServiceDiamondCategory.
     *
     * @param elecServiceDiamondCategory the entity to save
     * @return the persisted entity
     */
    @Override
    public ElecServiceDiamondCategory save(ElecServiceDiamondCategory elecServiceDiamondCategory) {
        log.debug("Request to save ElecServiceDiamondCategory : {}", elecServiceDiamondCategory);
        return elecServiceDiamondCategoryRepository.save(elecServiceDiamondCategory);
    }

    /**
     *  Get all the elecServiceDiamondCategories.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ElecServiceDiamondCategory> findAll(Pageable pageable) {
        log.debug("Request to get all ElecServiceDiamondCategories");
        return elecServiceDiamondCategoryRepository.findAll(pageable);
    }

    /**
     *  Get one elecServiceDiamondCategory by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ElecServiceDiamondCategory findOne(Long id) {
        log.debug("Request to get ElecServiceDiamondCategory : {}", id);
        return elecServiceDiamondCategoryRepository.findOne(id);
    }

    /**
     *  Delete the  elecServiceDiamondCategory by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ElecServiceDiamondCategory : {}", id);
        elecServiceDiamondCategoryRepository.delete(id);
    }

	@Override
	public SeparationChartReportResultDTO generateElecServiceSeprationDiamondReport(SeparationReportInputDTO separationReportInputDTO) throws Exception {
		ElectronicService electronicService = separationReportInputDTO.getElectronicService();
		List<String> lables = new ArrayList<>();
		String locale = separationReportInputDTO.getLocale();
		SeparationChartReportResultDTO resultDTO = new SeparationChartReportResultDTO();
		ZonedDateTime fromDate = separationReportInputDTO.getFromDate();
		ZonedDateTime toDate = separationReportInputDTO.getToDate();
		HashMap<Long,Double> listScoreEachElecService =new HashMap<Long,Double>();
		
	    HashMap<Long, String> diamondCategoryList = getSelectedDiamondIdList(separationReportInputDTO.getDiamondSelecteds(),locale);
	    for(Map.Entry<Long, String> entry : diamondCategoryList.entrySet()) {
			lables.add(entry.getValue());
			listScoreEachElecService.put(entry.getKey(),0.0);
		}

		if (fromDate == null && toDate == null) {
			List<SeparationChartReportJpaDTO> generateElecServiceDiamondReport = elecServiceDiamondCategoryRepository
					.generateElecServiceDiamondReport(electronicService.getId(), new ArrayList<>(diamondCategoryList.keySet()));
			for (SeparationChartReportJpaDTO separationChartReportJpaDTO : generateElecServiceDiamondReport) {
				listScoreEachElecService.put(separationChartReportJpaDTO.getLabelId(),separationChartReportJpaDTO.getData());
			}	
		} else if (fromDate != null && toDate != null){
		
			if (fromDate.isAfter(toDate))
				throw new Exception("fromDate is After toDate");
			List<SeparationChartReportJpaDTO> generateElecServiceDiamondReport = elecServiceDiamondCategoryRepository
					.generateElecServiceDiamondReport(electronicService.getId(), new ArrayList<>(diamondCategoryList.keySet()), fromDate, toDate);
			for (SeparationChartReportJpaDTO separationChartReportJpaDTO : generateElecServiceDiamondReport) {
				listScoreEachElecService.put(separationChartReportJpaDTO.getLabelId(),separationChartReportJpaDTO.getData());
			}	
		}
		resultDTO.setLabel(lables);
		resultDTO.setData(new ArrayList<>(listScoreEachElecService.values()));
		return resultDTO;
	}
		
	

	

	private HashMap<Long, String> getSelectedDiamondIdList(List<DiamondCategory> maturitySelecteds, String locale) {
		HashMap<Long,String> diamondCategoryIds = new HashMap<>();
		if (maturitySelecteds.size() == 0) {
			List<DiamondCategory> diamondCategoryList = diamondCategoryRepository.findAll();
			for (DiamondCategory diamondCategory : diamondCategoryList) {
				diamondCategoryIds.put(diamondCategory.getId(),"en".equals(locale) ?  diamondCategory.getDiamondCategoryTitleen() : diamondCategory.getDiamondCategoryTitlefa());
			}
		} else {
			for (DiamondCategory diamondCategory : maturitySelecteds) {
				diamondCategoryIds.put(diamondCategory.getId(),"en".equals(locale) ?  diamondCategory.getDiamondCategoryTitleen() : diamondCategory.getDiamondCategoryTitlefa());
			}
		}

		return diamondCategoryIds;
	}

	@Override
	public ConsolidatedChartReportResultDTO generateElecServiceConsolatedDiamondReport(
			ConsolidatedReportInputDTO consolidatedReportInputDTO) throws Exception {
		
		ZonedDateTime fromDate = consolidatedReportInputDTO.getFromDate();
		ZonedDateTime toDate = consolidatedReportInputDTO.getToDate();
		String locale = consolidatedReportInputDTO.getLocale();
		HashMap<Long, String> diamondCategoryList = getSelectedDiamondIdList(consolidatedReportInputDTO.getDiamondSelecteds(),locale);
		HashMap<Long,Double> listScoreEachElecService =new HashMap<Long,Double>(); 
		ConsolidatedChartReportResultDTO resultDTO = new ConsolidatedChartReportResultDTO();
		List<String> series = new ArrayList<>();
		List<String> lables = new ArrayList<>();
		List<List<Double>> data = new ArrayList<>();
		for(Map.Entry<Long, String> entry : diamondCategoryList.entrySet()) {
			lables.add(entry.getValue());
		}
		List<ElectronicService> electronicServiceList = consolidatedReportInputDTO.getElectronicServiceList();
		for (ElectronicService electronicService : electronicServiceList) {
			for(Map.Entry<Long, String> entry : diamondCategoryList.entrySet()) {
				listScoreEachElecService.put(entry.getKey(),0.0);
			}
			if (fromDate == null && toDate == null) {
				List<SeparationChartReportJpaDTO> generateElecServiceDiamondReport = elecServiceDiamondCategoryRepository.generateElecServiceDiamondReport(electronicService.getId(), new ArrayList<>(diamondCategoryList.keySet()));
				for (SeparationChartReportJpaDTO separationChartReportJpaDTO : generateElecServiceDiamondReport) {
					listScoreEachElecService.put(separationChartReportJpaDTO.getLabelId(),separationChartReportJpaDTO.getData());
				}	
			}			
			else if (fromDate != null && toDate != null) {
				if (fromDate.isAfter(toDate))
					throw new Exception("fromDate is After toDate");
				List<SeparationChartReportJpaDTO> generateElecServiceDiamondReport = elecServiceDiamondCategoryRepository.generateElecServiceDiamondReport(electronicService.getId(), new ArrayList<>(diamondCategoryList.keySet()), fromDate, toDate);
				for (SeparationChartReportJpaDTO separationChartReportJpaDTO : generateElecServiceDiamondReport) {
					listScoreEachElecService.put(separationChartReportJpaDTO.getLabelId(),separationChartReportJpaDTO.getData());
				}	
			}
			series.add("en".equals(locale) ?  electronicService.getElecServiceTitleen() : electronicService.getElecServiceTitlefa());
			data.add(new ArrayList<>(listScoreEachElecService.values()));
			
		}
		resultDTO.setLabel(lables);
		resultDTO.setSeries(series);
		resultDTO.setData(data);	
		return resultDTO;
	}

	@Override
	public DepartmentReportResultDTO generateElecServiceDepartmentDiamondReport(
			DepartmentReportInputDTO departmentReportInputDTO) throws Exception {
		List<Department> departmentList = departmentReportInputDTO.getDepartmentList();
		String locale = departmentReportInputDTO.getLocale();
		
		HashMap<Long, String> diamondIdList = getSelectedDiamondIdList(departmentReportInputDTO.getDiamondSelecteds(),locale);
		HashMap<Long,Double> listScoreEachDepartment =new HashMap<Long,Double>();
		DepartmentReportResultDTO resultDTO = new DepartmentReportResultDTO();
		List<String> series = new ArrayList<>();
		List<String> lables = new ArrayList<>();
		List<List<Double>> data = new ArrayList<>();
		for(Map.Entry<Long, String> entry : diamondIdList.entrySet()) {
			lables.add(entry.getValue());
		}
		for (Department department : departmentList) {
			for(Map.Entry<Long, String> entry : diamondIdList.entrySet()) {
				listScoreEachDepartment.put(entry.getKey(),0.0);
			}
			List<Double> avgDataPerDepartment = new ArrayList<>();
			List<ElectronicService> elecServiceListOfDepartment = electronicServiceRepository.findAllElecServiceOfDepartment(department.getId());
			ConsolidatedReportInputDTO consolidatedReportInputDTO = new ConsolidatedReportInputDTO();
			consolidatedReportInputDTO.setDiamondSelecteds(departmentReportInputDTO.getDiamondSelecteds());
			consolidatedReportInputDTO.setElectronicServiceList(elecServiceListOfDepartment);
			consolidatedReportInputDTO.setFromDate(departmentReportInputDTO.getFromDate());
			consolidatedReportInputDTO.setToDate(departmentReportInputDTO.getToDate());
			consolidatedReportInputDTO.setLocale(departmentReportInputDTO.getLocale());
			ConsolidatedChartReportResultDTO consolidatedChartReportResultDTO = generateElecServiceConsolatedDiamondReport(consolidatedReportInputDTO);
			List<List<Double>> resultChartPerElecServiceOfEachDepartment = consolidatedChartReportResultDTO.getData();
			
			int listSize = resultChartPerElecServiceOfEachDepartment.size() == 0 ? 0 :  resultChartPerElecServiceOfEachDepartment.get(0).size();
			
			for (int i = 0; i < listSize; i++)
				avgDataPerDepartment.add(0.0);		
				
			for (List<Double> list : resultChartPerElecServiceOfEachDepartment)
			{
				listSize = list.size();			
				for (int i = 0; i < listSize; i++)
					avgDataPerDepartment.set(i,avgDataPerDepartment.get(i) + list.get(i));
			}				
					
			for (int i = 0; i < listSize; i++)
				avgDataPerDepartment.set(i,avgDataPerDepartment.get(i) / listSize);			
			
			series.add("fa".equals(locale) ?  departmentRepository.findOne(department.getId()).getDepartmentNamefa() : departmentRepository.findOne(department.getId()).getDepartmentNameen());
			data.add(avgDataPerDepartment);
			
			
		}
		
		resultDTO.setLabel(lables);
		resultDTO.setSeries(series);
		resultDTO.setData(data);	
		return resultDTO;
	}
	

	
	


}
