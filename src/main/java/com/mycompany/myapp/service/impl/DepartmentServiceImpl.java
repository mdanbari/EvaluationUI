package com.mycompany.myapp.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.Department;
import com.mycompany.myapp.repository.DepartmentRepository;
import com.mycompany.myapp.service.DepartmentService;
import com.mycompany.myapp.service.dto.ChildrenDepartment;

/**
 * Service Implementation for managing Department.
 */
@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

	private final Logger log = LoggerFactory.getLogger(DepartmentServiceImpl.class);

	private final DepartmentRepository departmentRepository;

	public DepartmentServiceImpl(DepartmentRepository departmentRepository) {
		this.departmentRepository = departmentRepository;
	}

	/**
	 * Save a department.
	 *
	 * @param department
	 *            the entity to save
	 * @return the persisted entity
	 */
	@Override
	public Department save(Department department) {
		log.debug("Request to save Department : {}", department);
		return departmentRepository.save(department);
	}

	/**
	 * Get all the departments.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<Department> findAll(Pageable pageable) {
		log.debug("Request to get all Departments");
		return departmentRepository.findAll(pageable);
	}

	/**
	 * Get one department by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public Department findOne(Long id) {
		log.debug("Request to get Department : {}", id);
		return departmentRepository.findOne(id);
	}

	/**
	 * Delete the department by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Department : {}", id);
		departmentRepository.delete(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ChildrenDepartment> findAllByParentIsNull() {
		log.debug("Requset to get root departments");
		return departmentRepository.findAllByParentIsNull();
	}

	@Override
	@Transactional(readOnly = true)
	public List<ChildrenDepartment> findAllChildrenOfDepartment(Long id) {
		log.debug("Requset to get children of department");
		return departmentRepository.findAllChildrenOfDepartment(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Long findCountAllChildrenOfDepartment(Long id) {
		log.debug("Requset to count children of department");
		return departmentRepository.findCountAllChildrenOfDepartment(id);
	}

	@Override
	@Transactional(readOnly=true)
	public Department findByDepartmentNamefa(String name) {
		return departmentRepository.findTop1ByDepartmentNamefa(name);
	}
	
	
	@Override
	@Transactional
	public Page<Department> search(String search , String local , Pageable pageable){
		return departmentRepository.search(search , local, pageable);
	}

	@Override
	@Transactional(readOnly=true)
	public Department findByDepartmentName(String departmentTitlefa) {
		List<Department> departmentList=departmentRepository.findByDepartmentName(departmentTitlefa);
		if(departmentList!=null && departmentList.size()!=0)
			return departmentList.get(0);
		return null;
	}
}
