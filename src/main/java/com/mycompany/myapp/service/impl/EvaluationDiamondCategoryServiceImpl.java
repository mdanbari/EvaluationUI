package com.mycompany.myapp.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.EvaluationDiamondCategory;
import com.mycompany.myapp.repository.EvaluationDiamondCategoryRepository;
import com.mycompany.myapp.service.EvaluationDiamondCategoryService;

/**
 * Service Implementation for managing EvaluationDiamondCategory.
 */
@Service
@Transactional
public class EvaluationDiamondCategoryServiceImpl implements EvaluationDiamondCategoryService {

	private final Logger log = LoggerFactory.getLogger(EvaluationDiamondCategoryServiceImpl.class);

	private final EvaluationDiamondCategoryRepository evaluationDiamondCategoryRepository;

	public EvaluationDiamondCategoryServiceImpl(
			EvaluationDiamondCategoryRepository evaluationDiamondCategoryRepository) {
		this.evaluationDiamondCategoryRepository = evaluationDiamondCategoryRepository;
	}

	/**
	 * Save a evaluationDiamondCategory.
	 *
	 * @param evaluationDiamondCategory
	 *            the entity to save
	 * @return the persisted entity
	 */
	@Override
	public EvaluationDiamondCategory save(EvaluationDiamondCategory evaluationDiamondCategory) {
		log.debug("Request to save EvaluationDiamondCategory : {}", evaluationDiamondCategory);
		return evaluationDiamondCategoryRepository.save(evaluationDiamondCategory);
	}

	/**
	 * Get all the evaluationDiamondCategories.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<EvaluationDiamondCategory> findAll(Pageable pageable) {
		log.debug("Request to get all EvaluationDiamondCategories");
		return evaluationDiamondCategoryRepository.findAll(pageable);
	}

	/**
	 * Get one evaluationDiamondCategory by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public EvaluationDiamondCategory findOne(Long id) {
		log.debug("Request to get EvaluationDiamondCategory : {}", id);
		return evaluationDiamondCategoryRepository.findOne(id);
	}

	/**
	 * Delete the evaluationDiamondCategory by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete EvaluationDiamondCategory : {}", id);
		evaluationDiamondCategoryRepository.delete(id);
	}

	@Override
	public List<EvaluationDiamondCategory> findAllByEvaluationIdIn(List<Long> ids) {
		return evaluationDiamondCategoryRepository.findAllByEvaluationIdIn(ids);
	}
}
