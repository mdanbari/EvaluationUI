package com.mycompany.myapp.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.MaturityStage;
import com.mycompany.myapp.repository.MaturityStageRepository;
import com.mycompany.myapp.service.MaturityStageService;


/**
 * Service Implementation for managing MaturityStage.
 */
@Service
@Transactional
public class MaturityStageServiceImpl implements MaturityStageService{

    private final Logger log = LoggerFactory.getLogger(MaturityStageServiceImpl.class);

    private final MaturityStageRepository maturityStageRepository;
    public MaturityStageServiceImpl(MaturityStageRepository maturityStageRepository) {
        this.maturityStageRepository = maturityStageRepository;
    }

    /**
     * Save a maturityStage.
     *
     * @param maturityStage the entity to save
     * @return the persisted entity
     */
    @Override
    public MaturityStage save(MaturityStage maturityStage) {
        log.debug("Request to save MaturityStage : {}", maturityStage);
        return maturityStageRepository.save(maturityStage);
    }

    /**
     *  Get all the maturityStages.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MaturityStage> findAll(Pageable pageable) {
        log.debug("Request to get all MaturityStages");
        return maturityStageRepository.findAll(pageable);
    }

    /**
     *  Get one maturityStage by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public MaturityStage findOne(Long id) {
        log.debug("Request to get MaturityStage : {}", id);
        return maturityStageRepository.findOne(id);
    }

    /**
     *  Delete the  maturityStage by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete MaturityStage : {}", id);
        maturityStageRepository.delete(id);
    }

	@Override
	public List<MaturityStage> findAll() {
		log.debug("Request to get all MaturityStages");
		return maturityStageRepository.findAll();
	}

	@Override
	public MaturityStage findByFATiltle(String title) {
		return maturityStageRepository.findOneByMaturityStageTitlefa(title);
	}
}
