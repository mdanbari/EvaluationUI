package com.mycompany.myapp.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.QuestionAnswer;
import com.mycompany.myapp.repository.QuestionAnswerRepository;
import com.mycompany.myapp.service.QuestionAnswerService;


/**
 * Service Implementation for managing QuestionAnswer.
 */
@Service
@Transactional
public class QuestionAnswerServiceImpl implements QuestionAnswerService{

    private final Logger log = LoggerFactory.getLogger(QuestionAnswerServiceImpl.class);

    private final QuestionAnswerRepository questionAnswerRepository;
    public QuestionAnswerServiceImpl(QuestionAnswerRepository questionAnswerRepository) {
        this.questionAnswerRepository = questionAnswerRepository;
    }

    /**
     * Save a questionAnswer.
     *
     * @param questionAnswer the entity to save
     * @return the persisted entity
     */
    @Override
    public QuestionAnswer save(QuestionAnswer questionAnswer) {
        log.debug("Request to save QuestionAnswer : {}", questionAnswer);
        return questionAnswerRepository.save(questionAnswer);
    }

    /**
     *  Get all the questionAnswers.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<QuestionAnswer> findAll(Pageable pageable) {
        log.debug("Request to get all QuestionAnswers");
        return questionAnswerRepository.findAll(pageable);
    }

    /**
     *  Get one questionAnswer by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public QuestionAnswer findOne(Long id) {
        log.debug("Request to get QuestionAnswer : {}", id);
        return questionAnswerRepository.findOne(id);
    }

    /**
     *  Delete the  questionAnswer by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete QuestionAnswer : {}", id);
        questionAnswerRepository.delete(id);
    }
}
