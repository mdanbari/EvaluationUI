package com.mycompany.myapp.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.EvaluationArchive;
import com.mycompany.myapp.repository.EvaluationArchiveRepository;
import com.mycompany.myapp.service.EvaluationArchiveService;


/**
 * Service Implementation for managing EvaluationArchive.
 */
@Service
@Transactional
public class EvaluationArchiveServiceImpl implements EvaluationArchiveService{

    private final Logger log = LoggerFactory.getLogger(EvaluationArchiveServiceImpl.class);

    private final EvaluationArchiveRepository evaluationArchiveRepository;
    public EvaluationArchiveServiceImpl(EvaluationArchiveRepository evaluationArchiveRepository) {
        this.evaluationArchiveRepository = evaluationArchiveRepository;
    }

    /**
     * Save a evaluationArchive.
     *
     * @param evaluationArchive the entity to save
     * @return the persisted entity
     */
    @Override
    public EvaluationArchive save(EvaluationArchive evaluationArchive) {
        log.debug("Request to save EvaluationArchive : {}", evaluationArchive);
        return evaluationArchiveRepository.save(evaluationArchive);
    }

    /**
     *  Get all the evaluationArchives.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EvaluationArchive> findAll(Pageable pageable) {
        log.debug("Request to get all EvaluationArchives");
        return evaluationArchiveRepository.findAll(pageable);
    }

    /**
     *  Get one evaluationArchive by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public EvaluationArchive findOne(Long id) {
        log.debug("Request to get EvaluationArchive : {}", id);
        return evaluationArchiveRepository.findOne(id);
    }

    /**
     *  Delete the  evaluationArchive by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EvaluationArchive : {}", id);
        evaluationArchiveRepository.delete(id);
    }
}
