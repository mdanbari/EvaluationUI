package com.mycompany.myapp.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.SystemProfile;
import com.mycompany.myapp.repository.SystemProfileRepository;
import com.mycompany.myapp.service.SystemProfileService;


/**
 * Service Implementation for managing SystemProfile.
 */
@Service
@Transactional
public class SystemProfileServiceImpl implements SystemProfileService{

    private final Logger log = LoggerFactory.getLogger(SystemProfileServiceImpl.class);

    private final SystemProfileRepository systemProfileRepository;
    public SystemProfileServiceImpl(SystemProfileRepository systemProfileRepository) {
        this.systemProfileRepository = systemProfileRepository;
    }

    /**
     * Save a systemProfile.
     *
     * @param systemProfile the entity to save
     * @return the persisted entity
     */
    @Override
    public SystemProfile save(SystemProfile systemProfile) {
        log.debug("Request to save SystemProfile : {}", systemProfile);
        return systemProfileRepository.save(systemProfile);
    }

    /**
     *  Get all the systemProfiles.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SystemProfile> findAll(Pageable pageable) {
        log.debug("Request to get all SystemProfiles");
        return systemProfileRepository.findAll(pageable);
    }

    /**
     *  Get one systemProfile by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public SystemProfile findOne(Long id) {
        log.debug("Request to get SystemProfile : {}", id);
        return systemProfileRepository.findOne(id);
    }

    /**
     *  Delete the  systemProfile by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SystemProfile : {}", id);
        systemProfileRepository.delete(id);
    }
}
