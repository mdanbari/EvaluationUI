package com.mycompany.myapp.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.UnMaturityModel;
import com.mycompany.myapp.repository.UnMaturityModelRepository;
import com.mycompany.myapp.service.UnMaturityModelService;


/**
 * Service Implementation for managing UnMaturityModel.
 */
@Service
@Transactional
public class UnMaturityModelServiceImpl implements UnMaturityModelService{

    private final Logger log = LoggerFactory.getLogger(UnMaturityModelServiceImpl.class);

    private final UnMaturityModelRepository unMaturityModelRepository;
    public UnMaturityModelServiceImpl(UnMaturityModelRepository unMaturityModelRepository) {
        this.unMaturityModelRepository = unMaturityModelRepository;
    }

    /**
     * Save a unMaturityModel.
     *
     * @param unMaturityModel the entity to save
     * @return the persisted entity
     */
    @Override
    public UnMaturityModel save(UnMaturityModel unMaturityModel) {
        log.debug("Request to save UnMaturityModel : {}", unMaturityModel);
        return unMaturityModelRepository.save(unMaturityModel);
    }

    /**
     *  Get all the unMaturityModels.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UnMaturityModel> findAll(Pageable pageable) {
        log.debug("Request to get all UnMaturityModels");
        return unMaturityModelRepository.findAll(pageable);
    }

    /**
     *  Get one unMaturityModel by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public UnMaturityModel findOne(Long id) {
        log.debug("Request to get UnMaturityModel : {}", id);
        return unMaturityModelRepository.findOne(id);
    }

    /**
     *  Delete the  unMaturityModel by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UnMaturityModel : {}", id);
        unMaturityModelRepository.delete(id);
    }
}
