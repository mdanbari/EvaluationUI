package com.mycompany.myapp.service.impl;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.Department;
import com.mycompany.myapp.domain.ElecServiceQuestion;
import com.mycompany.myapp.domain.ElectronicService;
import com.mycompany.myapp.domain.Question;
import com.mycompany.myapp.repository.DepartmentRepository;
import com.mycompany.myapp.repository.ElecServiceQuestionRepository;
import com.mycompany.myapp.repository.QuestionRepository;
import com.mycompany.myapp.service.ElecServiceQuestionService;
import com.mycompany.myapp.service.dto.DepartmentQuestionChartReportResultDTO;
import com.mycompany.myapp.service.dto.DepartmentQuestionReportInputDTO;
import com.mycompany.myapp.service.dto.ElecServiceQuestionChartReportResultDTO;
import com.mycompany.myapp.service.dto.ElecServiceQuestionReportInputDTO;
import com.mycompany.myapp.service.dto.SeparationChartReportJpaDTO;

/**
 * Service Implementation for managing ElecServiceQuestion.
 */
@Service
@Transactional
public class ElecServiceQuestionServiceImpl implements ElecServiceQuestionService {

	private final Logger log = LoggerFactory.getLogger(ElecServiceQuestionServiceImpl.class);

	private final ElecServiceQuestionRepository elecServiceQuestionRepository;
	private final QuestionRepository questionRepository;
	private final DepartmentRepository departmentRepository;

	

	public ElecServiceQuestionServiceImpl(ElecServiceQuestionRepository elecServiceQuestionRepository,
			QuestionRepository questionRepository, DepartmentRepository departmentRepository) {
		this.elecServiceQuestionRepository = elecServiceQuestionRepository;
		this.questionRepository = questionRepository;
		this.departmentRepository = departmentRepository;
	}

	private HashMap<Long, String> getSelectedQuestionIdList(List<Question> questionSelecteds, String locale) {
		HashMap<Long, String> questionMapList = new HashMap<>();

		for (Question question : questionSelecteds) {
			Question findOne = questionRepository.findOne(question.getId());
			questionMapList.put(findOne.getId(),
					"en".equals(locale) ? question.getQuestionTitleen() : question.getQuestionTitlefa());
		}

		return questionMapList;
	}

	@Override
	public ElecServiceQuestionChartReportResultDTO generateElecServiceQuestionReport(
			ElecServiceQuestionReportInputDTO elecServiceQuestionReportInputDTO) throws Exception {
		ZonedDateTime fromDate = elecServiceQuestionReportInputDTO.getFromDate();
		ZonedDateTime toDate = elecServiceQuestionReportInputDTO.getToDate();
		String locale = elecServiceQuestionReportInputDTO.getLocale();

		HashMap<Long, String> questionMapList = getSelectedQuestionIdList(
				elecServiceQuestionReportInputDTO.getQuestions(), locale);
		HashMap<Long, Double> listScoreEachElecService = new HashMap<Long, Double>();
		ElecServiceQuestionChartReportResultDTO resultDTO = new ElecServiceQuestionChartReportResultDTO();
		List<String> series = new ArrayList<>();
		List<String> lables = new ArrayList<>();
		List<List<Double>> data = new ArrayList<>();
		for (Map.Entry<Long, String> entry : questionMapList.entrySet()) {
			lables.add(entry.getValue());
		}
		List<ElectronicService> electronicServiceList = elecServiceQuestionReportInputDTO.getElectronicServiceList();
		for (ElectronicService electronicService : electronicServiceList) {
			for (Map.Entry<Long, String> entry : questionMapList.entrySet()) {
				listScoreEachElecService.put(entry.getKey(), 0.0);
			}
			if (fromDate == null && toDate == null) {

				List<SeparationChartReportJpaDTO> generateElecQuestionReport = elecServiceQuestionRepository
						.generateElecQuestionReport(electronicService.getId(),
								new ArrayList<>(questionMapList.keySet()));
				for (SeparationChartReportJpaDTO separationChartReportJpaDTO : generateElecQuestionReport) {
					listScoreEachElecService.put(separationChartReportJpaDTO.getLabelId(),
							separationChartReportJpaDTO.getData());
				}

			} else if (fromDate != null && toDate != null) {
				if (fromDate.isAfter(toDate))
					throw new Exception("fromDate is After toDate");
				List<SeparationChartReportJpaDTO> generateElecQuestionReport = elecServiceQuestionRepository
						.generateElecQuestionReport(electronicService.getId(),
								new ArrayList<>(questionMapList.keySet()), fromDate, toDate);
				for (SeparationChartReportJpaDTO separationChartReportJpaDTO : generateElecQuestionReport) {
					listScoreEachElecService.put(separationChartReportJpaDTO.getLabelId(),
							separationChartReportJpaDTO.getData());
				}

			}
			series.add("fa".equals(locale) ? electronicService.getElecServiceTitlefa()
					: electronicService.getElecServiceTitleen());
			data.add(new ArrayList<>(listScoreEachElecService.values()));

		}

		resultDTO.setLabel(lables);
		resultDTO.setSeries(series);
		resultDTO.setData(data);
		return resultDTO;
	}

	@Override
	public DepartmentQuestionChartReportResultDTO generateDepartmentQuestionReport(
			DepartmentQuestionReportInputDTO departmentQuestionReportInputDTO) throws Exception {
		ZonedDateTime fromDate = departmentQuestionReportInputDTO.getFromDate();
		ZonedDateTime toDate = departmentQuestionReportInputDTO.getToDate();
		String locale = departmentQuestionReportInputDTO.getLocale();

		HashMap<Long, String> questionMapList = getSelectedQuestionIdList(
				departmentQuestionReportInputDTO.getQuestions(), locale);
		HashMap<Long, Double> listScoreEachElecService = new HashMap<Long, Double>();
		DepartmentQuestionChartReportResultDTO resultDTO = new DepartmentQuestionChartReportResultDTO();
		List<String> series = new ArrayList<>();
		List<String> lables = new ArrayList<>();
		List<List<Double>> data = new ArrayList<>();
		for (Map.Entry<Long, String> entry : questionMapList.entrySet()) {
			lables.add(entry.getValue());
		}
		List<Department> departments = departmentQuestionReportInputDTO.getDepartments();
		for (Department department : departments) {
			for (Map.Entry<Long, String> entry : questionMapList.entrySet()) {
				listScoreEachElecService.put(entry.getKey(), 0.0);
			}
			if (fromDate == null && toDate == null) {

				List<SeparationChartReportJpaDTO> generateDepQuestionReport = elecServiceQuestionRepository
						.generateDepQuestionReport(department.getId(),
								new ArrayList<>(questionMapList.keySet()));
				for (SeparationChartReportJpaDTO separationChartReportJpaDTO : generateDepQuestionReport) {
					listScoreEachElecService.put(separationChartReportJpaDTO.getLabelId(),
							separationChartReportJpaDTO.getData());
				}

			} else if (fromDate != null && toDate != null) {
				if (fromDate.isAfter(toDate))
					throw new Exception("fromDate is After toDate");
				List<SeparationChartReportJpaDTO> generatedepQuestionReport = elecServiceQuestionRepository
						.generateDepQuestionReport(department.getId(),
								new ArrayList<>(questionMapList.keySet()), fromDate, toDate);
				for (SeparationChartReportJpaDTO separationChartReportJpaDTO : generatedepQuestionReport) {
					listScoreEachElecService.put(separationChartReportJpaDTO.getLabelId(),
							separationChartReportJpaDTO.getData());
				}

			}
			//series.add("fa".equals(locale) ? department.getDepartmentNamefa()
				//	: department.getDepartmentNameen());
			
			series.add("fa".equals(locale) ?  departmentRepository.findOne(department.getId()).getDepartmentNamefa() : departmentRepository.findOne(department.getId()).getDepartmentNameen());
			data.add(new ArrayList<>(listScoreEachElecService.values()));

		}

		resultDTO.setLabel(lables);
		resultDTO.setSeries(series);
		resultDTO.setData(data);
		return resultDTO;
	}

	@Override
	public ElecServiceQuestion save(ElecServiceQuestion elecServiceQuestion) {
		return elecServiceQuestionRepository.save(elecServiceQuestion);
	}

}
