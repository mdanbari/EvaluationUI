package com.mycompany.myapp.service.impl;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.ElectronicService;
import com.mycompany.myapp.repository.ElectronicServiceRepository;
import com.mycompany.myapp.service.ElectronicServiceService;
import com.mycompany.myapp.service.dto.SeparationChartReportResultDTO;
import com.mycompany.myapp.service.dto.SeparationReportInputDTO;

/**
 * Service Implementation for managing ElectronicService.
 */
@Service
@Transactional
public class ElectronicServiceServiceImpl implements ElectronicServiceService {

	private final Logger log = LoggerFactory.getLogger(ElectronicServiceServiceImpl.class);

	private final ElectronicServiceRepository electronicServiceRepository;

	public ElectronicServiceServiceImpl(ElectronicServiceRepository electronicServiceRepository) {
		this.electronicServiceRepository = electronicServiceRepository;
	}

	/**
	 * Save a electronicService.
	 *
	 * @param electronicService
	 *            the entity to save
	 * @return the persisted entity
	 */
	@Override
	public ElectronicService save(ElectronicService electronicService) {
		log.debug("Request to save ElectronicService : {}", electronicService);
		return electronicServiceRepository.save(electronicService);
	}

	/**
	 * Get all the electronicServices.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<ElectronicService> findAll(Pageable pageable) {
		log.debug("Request to get all ElectronicServices");
		return electronicServiceRepository.findAll(pageable);
	}

	/**
	 * Get one electronicService by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public ElectronicService findOne(Long id) {
		log.debug("Request to get ElectronicService : {}", id);
		return electronicServiceRepository.findOneWithEagerRelationships(id);
	}

	/**
	 * Delete the electronicService by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete ElectronicService : {}", id);
		electronicServiceRepository.delete(id);
	}

	@Override
	public ElectronicService getRandomElectronicServiceForEvaluation() {
		// In the Memory of big head
	//	ElectronicService electronicService;
//		electronicService = electronicServiceRepository
//				.findTop1ByElecServiceEvalCountAndElecServiceEvalDateLessThanEqualOrderByElecServiceEvalDateDesc(1,
//						ZonedDateTime.now());
		List<Long> findAllById = electronicServiceRepository.findAllById();
		if(findAllById != null)
		{
			Random random = new Random();
			Long randomElecServiceId = findAllById.get(random.nextInt(findAllById.size()));
			ElectronicService electronicService = electronicServiceRepository.findOne(randomElecServiceId);
			return electronicService;
		}
//		if (electronicService == null) {
//			electronicService = electronicServiceRepository.findTop1ByElecServiceEvalCount(2);
//			// findOneByElecServiceEvalCount(2);
//			if (electronicService == null)
//				electronicService = findOne(1L);
//		}

		return null;
	}

	@Transactional(readOnly = true)
	@Override
	public List<ElectronicService> customFindAll() {
		return electronicServiceRepository.customFindAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<ElectronicService> findAllElecServiceOfDepartment(Long id) {
		return electronicServiceRepository.findAllElecServiceOfDepartment(id);
	}

	@Override
	@Transactional
	public ElectronicService findElecServiceByTitlefa(String title) {
		return electronicServiceRepository.findTop1ByElecServiceTitlefa(title);
	}

	@Override
	@Transactional
	public Page<ElectronicService> search(String search, String local, Pageable pageable) {
		return electronicServiceRepository.search(search, local, pageable);
	}

	@Override
	public SeparationChartReportResultDTO generateElecServiceTotalScore(SeparationReportInputDTO separationReportInputDTO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=true)
	public ElectronicService findByElectronicserviceName(String elecServiceTitlefa) {
		List<ElectronicService> elecServiceList=electronicServiceRepository.findByElecServiceName(elecServiceTitlefa);
		if(elecServiceList!=null && elecServiceList.size()!=0)
			return elecServiceList.get(0);
		return null;
	}

	
}
