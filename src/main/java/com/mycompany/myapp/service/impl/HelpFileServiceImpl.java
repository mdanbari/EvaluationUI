package com.mycompany.myapp.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.HelpFile;
import com.mycompany.myapp.repository.HelpFileRepository;
import com.mycompany.myapp.service.HelpFileService;


/**
 * Service Implementation for managing HelpFile.
 */
@Service
@Transactional
public class HelpFileServiceImpl implements HelpFileService{

    private final Logger log = LoggerFactory.getLogger(HelpFileServiceImpl.class);

    private final HelpFileRepository helpFileRepository;
    public HelpFileServiceImpl(HelpFileRepository helpFileRepository) {
        this.helpFileRepository = helpFileRepository;
    }

    /**
     * Save a helpFile.
     *
     * @param helpFile the entity to save
     * @return the persisted entity
     */
    @Override
    public HelpFile save(HelpFile helpFile) {
        log.debug("Request to save HelpFile : {}", helpFile);
        return helpFileRepository.save(helpFile);
    }

    /**
     *  Get all the helpFiles.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<HelpFile> findAll(Pageable pageable) {
        log.debug("Request to get all HelpFiles");
        return helpFileRepository.findAll(pageable);
    }

    /**
     *  Get one helpFile by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public HelpFile findOne(Long id) {
        log.debug("Request to get HelpFile : {}", id);
        return helpFileRepository.findOne(id);
    }

    /**
     *  Delete the  helpFile by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete HelpFile : {}", id);
        helpFileRepository.delete(id);
    }
}
