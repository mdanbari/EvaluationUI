package com.mycompany.myapp.service.impl;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.Department;
import com.mycompany.myapp.domain.DiamondCategory;
import com.mycompany.myapp.domain.ElecServiceMaturityState;
import com.mycompany.myapp.domain.ElectronicService;
import com.mycompany.myapp.domain.MaturityStage;
import com.mycompany.myapp.repository.DepartmentRepository;
import com.mycompany.myapp.repository.ElecServiceMaturityStateRepository;
import com.mycompany.myapp.repository.ElectronicServiceRepository;
import com.mycompany.myapp.repository.MaturityStageRepository;
import com.mycompany.myapp.service.ElecServiceMaturityStateService;
import com.mycompany.myapp.service.dto.ConsolidatedChartReportResultDTO;
import com.mycompany.myapp.service.dto.ConsolidatedReportInputDTO;
import com.mycompany.myapp.service.dto.DepartmentReportInputDTO;
import com.mycompany.myapp.service.dto.DepartmentReportResultDTO;
import com.mycompany.myapp.service.dto.SeparationChartReportJpaDTO;
import com.mycompany.myapp.service.dto.SeparationChartReportResultDTO;
import com.mycompany.myapp.service.dto.SeparationReportInputDTO;

/**
 * Service Implementation for managing ElecServiceMaturityState.
 */
@Service
@Transactional
public class ElecServiceMaturityStateServiceImpl implements ElecServiceMaturityStateService {

	private final Logger log = LoggerFactory.getLogger(ElecServiceMaturityStateServiceImpl.class);

	private final ElecServiceMaturityStateRepository elecServiceMaturityStateRepository;
	private final MaturityStageRepository maturityStageRepository;
	private final ElectronicServiceRepository electronicServiceRepository;
	private final DepartmentRepository departmentRepository;
	



	public ElecServiceMaturityStateServiceImpl(ElecServiceMaturityStateRepository elecServiceMaturityStateRepository,
			MaturityStageRepository maturityStageRepository, ElectronicServiceRepository electronicServiceRepository,
			DepartmentRepository departmentRepository) {
		this.elecServiceMaturityStateRepository = elecServiceMaturityStateRepository;
		this.maturityStageRepository = maturityStageRepository;
		this.electronicServiceRepository = electronicServiceRepository;
		this.departmentRepository = departmentRepository;
	}

	/**
	 * Save a elecServiceMaturityState.
	 *
	 * @param elecServiceMaturityState
	 *            the entity to save
	 * @return the persisted entity
	 */
	@Override
	public ElecServiceMaturityState save(ElecServiceMaturityState elecServiceMaturityState) {
		log.debug("Request to save ElecServiceMaturityState : {}", elecServiceMaturityState);
		return elecServiceMaturityStateRepository.save(elecServiceMaturityState);
	}

	/**
	 * Get all the elecServiceMaturityStates.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<ElecServiceMaturityState> findAll(Pageable pageable) {
		log.debug("Request to get all ElecServiceMaturityStates");
		return elecServiceMaturityStateRepository.findAll(pageable);
	}

	/**
	 * Get one elecServiceMaturityState by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public ElecServiceMaturityState findOne(Long id) {
		log.debug("Request to get ElecServiceMaturityState : {}", id);
		return elecServiceMaturityStateRepository.findOne(id);
	}

	/**
	 * Delete the elecServiceMaturityState by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete ElecServiceMaturityState : {}", id);
		elecServiceMaturityStateRepository.delete(id);
	}

	@Override
	public SeparationChartReportResultDTO generateElecServiceSeprationMaturityReport(SeparationReportInputDTO separationReportInputDTO) throws Exception {
		ElectronicService electronicService = separationReportInputDTO.getElectronicService();
		ZonedDateTime fromDate = separationReportInputDTO.getFromDate();
		ZonedDateTime toDate = separationReportInputDTO.getToDate();
		String locale = separationReportInputDTO.getLocale();
		SeparationChartReportResultDTO resultDTO = new SeparationChartReportResultDTO();
		List<String> lables = new ArrayList<>();
		HashMap<Long,Double> listScoreEachElecService =new HashMap<Long,Double>();
	
		HashMap<Long, String> maturityIdList = getSelectedMaturityIdList(separationReportInputDTO.getMaturitySelecteds(),locale);
		for(Map.Entry<Long, String> entry : maturityIdList.entrySet()) {
			lables.add(entry.getValue());
			listScoreEachElecService.put(entry.getKey(),0.0);
		}	

		if (fromDate == null && toDate == null){
			List<SeparationChartReportJpaDTO> generateElecServiceMaturityReport = elecServiceMaturityStateRepository.generateElecServiceMaturityReport(electronicService.getId(),new ArrayList<>(maturityIdList.keySet()));
			for (SeparationChartReportJpaDTO separationChartReportJpaDTO : generateElecServiceMaturityReport)
				listScoreEachElecService.put(separationChartReportJpaDTO.getLabelId(),separationChartReportJpaDTO.getData());
			
		}
		
		else if (fromDate != null && toDate != null){
			if (fromDate.isAfter(toDate))
				throw new Exception("fromDate is After toDate");
			List<SeparationChartReportJpaDTO> generateElecServiceMaturityReport = elecServiceMaturityStateRepository.generateElecServiceMaturityReport(electronicService.getId(), new ArrayList<>(maturityIdList.keySet()), fromDate, toDate);
			for (SeparationChartReportJpaDTO separationChartReportJpaDTO : generateElecServiceMaturityReport)
				listScoreEachElecService.put(separationChartReportJpaDTO.getLabelId(),separationChartReportJpaDTO.getData());
		}
		resultDTO.setLabel(lables);
		resultDTO.setData(new ArrayList<>(listScoreEachElecService.values()));
		return resultDTO;

	}

	private HashMap<Long,String> getSelectedMaturityIdList(List<MaturityStage> maturitySelecteds, String locale) {
		HashMap<Long,String> maturityStateIds = new HashMap<>();
		if (maturitySelecteds.size() == 0) {
			List<MaturityStage> maturityStageList = maturityStageRepository.findAllByOrderByIdAsc();
			for (MaturityStage maturityStage : maturityStageList) {
				maturityStateIds.put(maturityStage.getId(),"en".equals(locale) ?  maturityStage.getMaturityStageTitleen() : maturityStage.getMaturityStageTitlefa());
			}
		} else {
			for (MaturityStage maturityStage : maturitySelecteds) {
				maturityStateIds.put(maturityStage.getId(),"en".equals(locale) ?  maturityStage.getMaturityStageTitleen() : maturityStage.getMaturityStageTitlefa());
			}
		}

		return maturityStateIds;
	}

	@Override
	public ConsolidatedChartReportResultDTO generateElecServiceConsolatedMaturityReport(
			ConsolidatedReportInputDTO consolidatedReportInputDTO) throws Exception {
		
		ZonedDateTime fromDate = consolidatedReportInputDTO.getFromDate();
		ZonedDateTime toDate = consolidatedReportInputDTO.getToDate();
		String locale = consolidatedReportInputDTO.getLocale();
		HashMap<Long, String> maturityIdList = getSelectedMaturityIdList(consolidatedReportInputDTO.getMaturitySelecteds(),locale);
		HashMap<Long,Double> listScoreEachElecService =new HashMap<Long,Double>(); 
		ConsolidatedChartReportResultDTO resultDTO = new ConsolidatedChartReportResultDTO();
		List<String> series = new ArrayList<>();
		List<String> lables = new ArrayList<>();
		List<List<Double>> data = new ArrayList<>();
		for(Map.Entry<Long, String> entry : maturityIdList.entrySet()) {
			lables.add(entry.getValue());
		}
		List<ElectronicService> electronicServiceList = consolidatedReportInputDTO.getElectronicServiceList();
		for (ElectronicService electronicService : electronicServiceList) {
			for(Map.Entry<Long, String> entry : maturityIdList.entrySet()) {
				listScoreEachElecService.put(entry.getKey(),0.0);
			}
			if (fromDate == null && toDate == null) {
				List<SeparationChartReportJpaDTO> generateElecServiceMaturityReport = elecServiceMaturityStateRepository.generateElecServiceMaturityReport(electronicService.getId(), new ArrayList<>(maturityIdList.keySet()));
				for (SeparationChartReportJpaDTO separationChartReportJpaDTO : generateElecServiceMaturityReport) {
					listScoreEachElecService.put(separationChartReportJpaDTO.getLabelId(),separationChartReportJpaDTO.getData());
				}		
				
				
			}			
			else if (fromDate != null && toDate != null) {
				if (fromDate.isAfter(toDate))
					throw new Exception("fromDate is After toDate");
				List<SeparationChartReportJpaDTO> generateElecServiceMaturityReport = elecServiceMaturityStateRepository.generateElecServiceMaturityReport(electronicService.getId(), new ArrayList<>(maturityIdList.keySet()), fromDate, toDate);
				for (SeparationChartReportJpaDTO separationChartReportJpaDTO : generateElecServiceMaturityReport) {
					listScoreEachElecService.put(separationChartReportJpaDTO.getLabelId(),separationChartReportJpaDTO.getData());
				}				
				
			}
			series.add("fa".equals(locale) ?  electronicService.getElecServiceTitlefa() : electronicService.getElecServiceTitleen());
			data.add(new ArrayList<>(listScoreEachElecService.values()));
			
		}		
		
		resultDTO.setLabel(lables);
		resultDTO.setSeries(series);
		resultDTO.setData(data);	
		return resultDTO;
	}

	@Override
	public DepartmentReportResultDTO generateElecServiceDepartmentMaturityReport(
			DepartmentReportInputDTO departmentReportInputDTO) throws Exception {
		List<Department> departmentList = departmentReportInputDTO.getDepartmentList();
		String locale = departmentReportInputDTO.getLocale();
		
		HashMap<Long, String> maturityIdList = getSelectedMaturityIdList(departmentReportInputDTO.getMaturitySelecteds(),locale);
		HashMap<Long,Double> listScoreEachDepartment =new HashMap<Long,Double>();
		DepartmentReportResultDTO resultDTO = new DepartmentReportResultDTO();
		List<String> series = new ArrayList<>();
		List<String> lables = new ArrayList<>();
		List<List<Double>> data = new ArrayList<>();
		for(Map.Entry<Long, String> entry : maturityIdList.entrySet()) {
			lables.add(entry.getValue());
		}
		for (Department department : departmentList) {
			for(Map.Entry<Long, String> entry : maturityIdList.entrySet()) {
				listScoreEachDepartment.put(entry.getKey(),0.0);
			}
			List<Double> avgDataPerDepartment = new ArrayList<>();
			List<ElectronicService> elecServiceListOfDepartment = electronicServiceRepository.findAllElecServiceOfDepartment(department.getId());
			ConsolidatedReportInputDTO consolidatedReportInputDTO = new ConsolidatedReportInputDTO();
			consolidatedReportInputDTO.setMaturitySelecteds(departmentReportInputDTO.getMaturitySelecteds());
			consolidatedReportInputDTO.setElectronicServiceList(elecServiceListOfDepartment);
			consolidatedReportInputDTO.setFromDate(departmentReportInputDTO.getFromDate());
			consolidatedReportInputDTO.setToDate(departmentReportInputDTO.getToDate());
			consolidatedReportInputDTO.setLocale(departmentReportInputDTO.getLocale());
			ConsolidatedChartReportResultDTO consolidatedChartReportResultDTO = generateElecServiceConsolatedMaturityReport(consolidatedReportInputDTO);
			List<List<Double>> resultChartPerElecServiceOfEachDepartment = consolidatedChartReportResultDTO.getData();
			
			int listSize = resultChartPerElecServiceOfEachDepartment.size() == 0 ? 0 :  resultChartPerElecServiceOfEachDepartment.get(0).size();
			
			for (int i = 0; i < listSize; i++)
				avgDataPerDepartment.add(0.0);	
			
			for (List<Double> list : resultChartPerElecServiceOfEachDepartment)
			{
				listSize = list.size();
				for (int i = 0; i < listSize; i++)
					avgDataPerDepartment.set(i,avgDataPerDepartment.get(i) + list.get(i));
			}				
					
			for (int i = 0; i < listSize; i++)
				avgDataPerDepartment.set(i,avgDataPerDepartment.get(i) / listSize);			
			
			series.add("fa".equals(locale) ?  departmentRepository.findOne(department.getId()).getDepartmentNamefa() : departmentRepository.findOne(department.getId()).getDepartmentNameen());
			data.add(avgDataPerDepartment);
			
			
		}
		
		resultDTO.setLabel(lables);
		resultDTO.setSeries(series);
		resultDTO.setData(data);	
		return resultDTO;
		
		
	}

	

}
