package com.mycompany.myapp.service.dto;

public class ChartReportDTO {

	private String label;

	private String seri;

	private Integer data;

	public ChartReportDTO() {
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getSeri() {
		return seri;
	}

	public void setSeri(String seri) {
		this.seri = seri;
	}

	public Integer getData() {
		return data;
	}

	public void setData(Integer data) {
		this.data = data;
	}

}
