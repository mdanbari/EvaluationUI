package com.mycompany.myapp.service.dto;

import java.util.List;

public class SeparationChartReportResultDTO {


	private List<String> label;

	private List<Double> data;

	public SeparationChartReportResultDTO() {
		// TODO Auto-generated constructor stub
	}

	public List<String> getLabel() {
		return label;
	}

	public void setLabel(List<String> label) {
		this.label = label;
	}

	public List<Double> getData() {
		return data;
	}

	public void setData(List<Double> data) {
		this.data = data;
	}
	
	

	

}
