package com.mycompany.myapp.service.dto;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import com.mycompany.myapp.domain.Department;
import com.mycompany.myapp.domain.DiamondCategory;
import com.mycompany.myapp.domain.MaturityStage;

public class DepartmentReportInputDTO {

	private List<Department> departmentList;

	private List<MaturityStage> maturitySelecteds = new ArrayList<>();

	private List<DiamondCategory> diamondSelecteds = new ArrayList<>();

	private ZonedDateTime fromDate;

	private ZonedDateTime toDate;

	private String typeModel;// maturity //diamond

	private String locale;

	private Boolean isDownload;

	public DepartmentReportInputDTO() {
		// TODO Auto-generated constructor stub
	}

	public List<MaturityStage> getMaturitySelecteds() {
		return maturitySelecteds;
	}

	public void setMaturitySelecteds(List<MaturityStage> maturitySelecteds) {
		this.maturitySelecteds = maturitySelecteds;
	}

	public List<DiamondCategory> getDiamondSelecteds() {
		return diamondSelecteds;
	}

	public void setDiamondSelecteds(List<DiamondCategory> diamondSelecteds) {
		this.diamondSelecteds = diamondSelecteds;
	}

	public ZonedDateTime getFromDate() {
		return fromDate;
	}

	public void setFromDate(ZonedDateTime fromDate) {
		this.fromDate = fromDate;
	}

	public ZonedDateTime getToDate() {
		return toDate;
	}

	public void setToDate(ZonedDateTime toDate) {
		this.toDate = toDate;
	}

	public String getTypeModel() {
		return typeModel;
	}

	public void setTypeModel(String typeModel) {
		this.typeModel = typeModel;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public Boolean getIsDownload() {
		return isDownload;
	}

	public void setIsDownload(Boolean isDownload) {
		this.isDownload = isDownload;
	}

	public List<Department> getDepartmentList() {
		return departmentList;
	}

	public void setDepartmentList(List<Department> departmentList) {
		this.departmentList = departmentList;
	}

	
}
