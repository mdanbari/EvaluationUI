package com.mycompany.myapp.service.dto;

import java.util.List;

public class DepartmentQuestionChartReportResultDTO {

	private List<String> label;

	private List<String> series;

	private List<List<Double>> data;

	public DepartmentQuestionChartReportResultDTO() {
	}

	public List<String> getLabel() {
		return label;
	}

	public void setLabel(List<String> label) {
		this.label = label;
	}
	
	public List<String> getSeries() {
		return series;
	}

	public void setSeries(List<String> series) {
		this.series = series;
	}

	public List<List<Double>> getData() {
		return data;
	}

	public void setData(List<List<Double>> data) {
		this.data = data;
	}
	
	
	
	

}