package com.mycompany.myapp.service.dto;

import org.springframework.beans.BeanUtils;

import com.mycompany.myapp.domain.Answer;
import com.mycompany.myapp.domain.QuestionAnswer;

public class QuestionAnswerDTO {

	private Long id;

	private QuestionDTO question;

	private Answer answer;

	public QuestionAnswerDTO() {
	}
	
	public QuestionAnswerDTO(QuestionAnswer questionAnswer) {
		super();
		if (questionAnswer != null) {
			BeanUtils.copyProperties(questionAnswer, this);
			if (questionAnswer.getQuestion() != null) {
				setQuestion(new QuestionDTO(questionAnswer.getQuestion()));
			}
		}
	}

	public QuestionDTO getQuestion() {
		return question;
	}

	public void setQuestion(QuestionDTO question) {
		this.question = question;
	}

	public Answer getAnswer() {
		return answer;
	}

	public void setAnswer(Answer answer) {
		this.answer = answer;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
