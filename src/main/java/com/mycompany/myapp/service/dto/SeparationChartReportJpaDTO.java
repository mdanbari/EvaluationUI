package com.mycompany.myapp.service.dto;

public class SeparationChartReportJpaDTO {

	private Long labelId;

	private Double data;

	public SeparationChartReportJpaDTO() {
		// TODO Auto-generated constructor stub
	}

	public SeparationChartReportJpaDTO(Long labelId, Double data) {
		this.data = data;
		this.labelId = labelId;
	}

	public Double getData() {
		return data;
	}

	public void setData(Double data) {
		this.data = data;
	}

	public Long getLabelId() {
		return labelId;
	}

	public void setLabelId(Long labelId) {
		this.labelId = labelId;
	}

}
