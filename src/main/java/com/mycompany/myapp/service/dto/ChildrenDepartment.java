package com.mycompany.myapp.service.dto;

public class ChildrenDepartment {

	private Long id;

	private String namefa;
	private String nameen;

	private Integer electronicServiceCount;

	public ChildrenDepartment() {

	}

	public ChildrenDepartment(Long id, String namefa, String nameen, Integer electronicServiceCount) {
		this.id = id;
		if (electronicServiceCount != null) {
			this.namefa = namefa + "(" + electronicServiceCount + ")";
			this.nameen = nameen + "(" + electronicServiceCount + ")";
		} else {
			this.namefa = namefa + "(0)";
			this.nameen = nameen + "(0)";
		}
		this.electronicServiceCount = electronicServiceCount;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNamefa() {
		return namefa;
	}

	public void setNamefa(String namefa) {
		this.namefa = namefa;
	}

	public String getNameen() {
		return nameen;
	}

	public void setNameen(String nameen) {
		this.nameen = nameen;
	}

	public Integer getElectronicServiceCount() {
		return electronicServiceCount;
	}

	public void setElectronicServiceCount(Integer electronicServiceCount) {
		this.electronicServiceCount = electronicServiceCount;
	}

}
