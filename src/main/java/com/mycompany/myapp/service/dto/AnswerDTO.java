package com.mycompany.myapp.service.dto;

import org.springframework.beans.BeanUtils;

import com.mycompany.myapp.domain.Answer;

public class AnswerDTO {

	private Long id;

	private String answerTitlefa;

	private String answerTitleen;

	public AnswerDTO() {
	}

	public AnswerDTO(Answer answer) {
		super();
		if (answer != null) {
			BeanUtils.copyProperties(answer, this);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAnswerTitlefa() {
		return answerTitlefa;
	}

	public void setAnswerTitlefa(String answerTitlefa) {
		this.answerTitlefa = answerTitlefa;
	}

	public String getAnswerTitleen() {
		return answerTitleen;
	}

	public void setAnswerTitleen(String answerTitleen) {
		this.answerTitleen = answerTitleen;
	}

}
