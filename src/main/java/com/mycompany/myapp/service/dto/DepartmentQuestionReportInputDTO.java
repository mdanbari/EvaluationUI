package com.mycompany.myapp.service.dto;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import com.mycompany.myapp.domain.Department;
import com.mycompany.myapp.domain.Question;

public class DepartmentQuestionReportInputDTO {

	private List<Department> departments;

	private List<Question> questions = new ArrayList<>();

	private ZonedDateTime fromDate;

	private ZonedDateTime toDate;

	private String locale;

	private Boolean isDownload;

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public DepartmentQuestionReportInputDTO() {
	}

	public List<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

	public ZonedDateTime getFromDate() {
		return fromDate;
	}

	public void setFromDate(ZonedDateTime fromDate) {
		this.fromDate = fromDate;
	}

	public ZonedDateTime getToDate() {
		return toDate;
	}

	public void setToDate(ZonedDateTime toDate) {
		this.toDate = toDate;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public Boolean getIsDownload() {
		return isDownload;
	}

	public void setIsDownload(Boolean isDownload) {
		this.isDownload = isDownload;
	}

}
