package com.mycompany.myapp.service.dto;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.mycompany.myapp.domain.ElectronicService;
import com.mycompany.myapp.domain.Evaluation;
import com.mycompany.myapp.domain.EvaluationArchive;
import com.mycompany.myapp.domain.QuestionAnswer;
import com.mycompany.myapp.domain.enumeration.EvaluationState;

public class EvaluationDTO {

	private Long id;

	private String evalPerson;

	private ZonedDateTime evalStartDate;

	private ZonedDateTime evalEndDate;

	private ZonedDateTime evalDeadLineDate;

	private ZonedDateTime evalLastModifiedDate;

	private ZonedDateTime evalConfirmedDate;

	private String evalDescriptionfa;

	private String evalJudgeDescriptionfa;

	private String evalDescriptionen;

	private String evalConfirmer;

	private Double evalScore;

	private EvaluationState evaluationState;

	private List<QuestionAnswerDTO> questionAnswers = new ArrayList<>();

	private ElectronicService electronicService;

	public EvaluationDTO() {
	}

	public EvaluationDTO(Evaluation evaluation) {
		super();
		if (evaluation != null) {
			BeanUtils.copyProperties(evaluation, this);
			if (evaluation.getQuestionAnswers() != null) {
				List<QuestionAnswerDTO> questionAnswerDTOs = new ArrayList<>();
				for (QuestionAnswer questionAnswer : evaluation.getQuestionAnswers()) {
					questionAnswerDTOs.add(new QuestionAnswerDTO(questionAnswer));
				}
				setQuestionAnswers(questionAnswerDTOs);
			}
		}
	}

	public EvaluationDTO(EvaluationArchive evaluation) {
		super();
		// if (evaluation != null) {
		// BeanUtils.copyProperties(evaluation, this);
		// if (evaluation.getQuestionAnswers() != null) {
		// List<QuestionAnswerDTO> questionAnswerDTOs = new ArrayList<>();
		// for (QuestionAnswer questionAnswer : evaluation.getQuestionAnswers())
		// {
		// questionAnswerDTOs.add(new QuestionAnswerDTO(questionAnswer));
		// }
		// setQuestionAnswers(questionAnswerDTOs);
		// }
		// }
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEvalPerson() {
		return evalPerson;
	}

	public void setEvalPerson(String evalPerson) {
		this.evalPerson = evalPerson;
	}

	public ZonedDateTime getEvalStartDate() {
		return evalStartDate;
	}

	public void setEvalStartDate(ZonedDateTime evalStartDate) {
		this.evalStartDate = evalStartDate;
	}

	public ZonedDateTime getEvalEndDate() {
		return evalEndDate;
	}

	public void setEvalEndDate(ZonedDateTime evalEndDate) {
		this.evalEndDate = evalEndDate;
	}

	public ZonedDateTime getEvalDeadLineDate() {
		return evalDeadLineDate;
	}

	public void setEvalDeadLineDate(ZonedDateTime evalDeadLineDate) {
		this.evalDeadLineDate = evalDeadLineDate;
	}

	public ZonedDateTime getEvalLastModifiedDate() {
		return evalLastModifiedDate;
	}

	public void setEvalLastModifiedDate(ZonedDateTime evalLastModifiedDate) {
		this.evalLastModifiedDate = evalLastModifiedDate;
	}

	public ZonedDateTime getEvalConfirmedDate() {
		return evalConfirmedDate;
	}

	public void setEvalConfirmedDate(ZonedDateTime evalConfirmedDate) {
		this.evalConfirmedDate = evalConfirmedDate;
	}

	public String getEvalDescriptionfa() {
		return evalDescriptionfa;
	}

	public void setEvalDescriptionfa(String evalDescriptionfa) {
		this.evalDescriptionfa = evalDescriptionfa;
	}

	public String getEvalDescriptionen() {
		return evalDescriptionen;
	}

	public void setEvalDescriptionen(String evalDescriptionen) {
		this.evalDescriptionen = evalDescriptionen;
	}

	public String getEvalConfirmer() {
		return evalConfirmer;
	}

	public void setEvalConfirmer(String evalConfirmer) {
		this.evalConfirmer = evalConfirmer;
	}

	public Double getEvalScore() {
		return evalScore;
	}

	public void setEvalScore(Double evalScore) {
		this.evalScore = evalScore;
	}

	public EvaluationState getEvaluationState() {
		return evaluationState;
	}

	public void setEvaluationState(EvaluationState evaluationState) {
		this.evaluationState = evaluationState;
	}

	public List<QuestionAnswerDTO> getQuestionAnswers() {
		return questionAnswers;
	}

	public void setQuestionAnswers(List<QuestionAnswerDTO> questionAnswers) {
		this.questionAnswers = questionAnswers;
	}

	public ElectronicService getElectronicService() {
		return electronicService;
	}

	public void setElectronicService(ElectronicService electronicService) {
		this.electronicService = electronicService;
	}

	public String getEvalJudgeDescriptionfa() {
		return evalJudgeDescriptionfa;
	}

	public void setEvalJudgeDescriptionfa(String evalJudgeDescriptionfa) {
		this.evalJudgeDescriptionfa = evalJudgeDescriptionfa;
	}

}
