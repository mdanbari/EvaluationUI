package com.mycompany.myapp.service.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.mycompany.myapp.domain.Answer;
import com.mycompany.myapp.domain.MaturityStage;
import com.mycompany.myapp.domain.Question;

public class QuestionDTO {

	private Long id;

	private String questionTitlefa;

	private String questionTitleen;

	private String questionDescription;

	private Long questionWeight;

	private Integer questionPercentage;

	private List<AnswerDTO> answers = new ArrayList<>();

	private MaturityStage maturityStage;

	private MaturityStage diamondCategory;

	public QuestionDTO() {
	}
	
	public QuestionDTO(Question question) {
		super();
		if (question != null) {
			BeanUtils.copyProperties(question, this);
			if (question.getAnswers() != null) {
				List<AnswerDTO> answersDTO = new ArrayList<>();

				for (Answer answer : question.getAnswers()) {
					answersDTO.add(new AnswerDTO(answer));
				}

				setAnswers(answersDTO);
			}
		}

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQuestionTitlefa() {
		return questionTitlefa;
	}

	public void setQuestionTitlefa(String questionTitlefa) {
		this.questionTitlefa = questionTitlefa;
	}

	public String getQuestionTitleen() {
		return questionTitleen;
	}

	public void setQuestionTitleen(String questionTitleen) {
		this.questionTitleen = questionTitleen;
	}

	public String getQuestionDescription() {
		return questionDescription;
	}

	public void setQuestionDescription(String questionDescription) {
		this.questionDescription = questionDescription;
	}

	public Long getQuestionWeight() {
		return questionWeight;
	}

	public void setQuestionWeight(Long questionWeight) {
		this.questionWeight = questionWeight;
	}

	public Integer getQuestionPercentage() {
		return questionPercentage;
	}

	public void setQuestionPercentage(Integer questionPercentage) {
		this.questionPercentage = questionPercentage;
	}

	public List<AnswerDTO> getAnswers() {
		return answers;
	}

	public void setAnswers(List<AnswerDTO> answers) {
		this.answers = answers;
	}

	public MaturityStage getMaturityStage() {
		return maturityStage;
	}

	public void setMaturityStage(MaturityStage maturityStage) {
		this.maturityStage = maturityStage;
	}

	public MaturityStage getDiamondCategory() {
		return diamondCategory;
	}

	public void setDiamondCategory(MaturityStage diamondCategory) {
		this.diamondCategory = diamondCategory;
	}

}
