package com.mycompany.myapp.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mycompany.myapp.domain.EvaluationArchive;

/**
 * Service Interface for managing EvaluationArchive.
 */
public interface EvaluationArchiveService {

    /**
     * Save a evaluationArchive.
     *
     * @param evaluationArchive the entity to save
     * @return the persisted entity
     */
    EvaluationArchive save(EvaluationArchive evaluationArchive);

    /**
     *  Get all the evaluationArchives.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<EvaluationArchive> findAll(Pageable pageable);

    /**
     *  Get the "id" evaluationArchive.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    EvaluationArchive findOne(Long id);

    /**
     *  Delete the "id" evaluationArchive.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
