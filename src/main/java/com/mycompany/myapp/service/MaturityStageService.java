package com.mycompany.myapp.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mycompany.myapp.domain.MaturityStage;

/**
 * Service Interface for managing MaturityStage.
 */
public interface MaturityStageService {

    /**
     * Save a maturityStage.
     *
     * @param maturityStage the entity to save
     * @return the persisted entity
     */
    MaturityStage save(MaturityStage maturityStage);

    /**
     *  Get all the maturityStages.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<MaturityStage> findAll(Pageable pageable);

    /**
     *  Get the "id" maturityStage.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    MaturityStage findOne(Long id);

    /**
     *  Delete the "id" maturityStage.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
    
    
    List<MaturityStage> findAll();
    
    MaturityStage findByFATiltle(String title);
}
