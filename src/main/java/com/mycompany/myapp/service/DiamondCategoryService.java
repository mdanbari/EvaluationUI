package com.mycompany.myapp.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mycompany.myapp.domain.DiamondCategory;

/**
 * Service Interface for managing DiamondCategory.
 */
public interface DiamondCategoryService {

	/**
	 * Save a diamondCategory.
	 *
	 * @param diamondCategory
	 *            the entity to save
	 * @return the persisted entity
	 */
	DiamondCategory save(DiamondCategory diamondCategory);

	/**
	 * Get all the diamondCategories.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	Page<DiamondCategory> findAll(Pageable pageable);

	/**
	 * Get the "id" diamondCategory.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	DiamondCategory findOne(Long id);

	/**
	 * Delete the "id" diamondCategory.
	 *
	 * @param id
	 *            the id of the entity
	 */
	void delete(Long id);

	List<DiamondCategory> findAll();

	DiamondCategory findByTitlefa(String title);
}
