package com.mycompany.myapp.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mycompany.myapp.domain.Evaluation;
import com.mycompany.myapp.service.dto.EvaluationDTO;

/**
 * Service Interface for managing Evaluation.
 */
public interface EvaluationService {

    /**
     * Save a evaluation.
     *
     * @param evaluation the entity to save
     * @return the persisted entity
     */
    Evaluation save(Evaluation evaluation);

    /**
     *  Get all the evaluations.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Evaluation> findAll(Pageable pageable);

    /**
     *  Get the "id" evaluation.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Evaluation findOne(Long id);

    /**
     *  Delete the "id" evaluation.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
    
	/**
	 * findAllMyEvaluation
	 * 
	 * @param pageable
	 * 
	 * @return Evaluation
	 */
	Page<Evaluation> findAllMyEvaluation(Pageable pageable);

	/**
	 * 
	 * getRandomEvaluation
	 * 
	 * @return Evaluation
	 */
	EvaluationDTO getRandomEvaluation();

	/**
	 * 
	 * getCurrentEvaluation
	 * 
	 * @return EvaluationDTO
	 */
	EvaluationDTO getCurrentEvaluation();

	/**
	 *
	 * findMyEvaluation
	 *
	 * @param id
	 * 
	 * @return EvaluationDTO
	 * 		
	 */
	EvaluationDTO findMyEvaluation(Long id);

	/**
	 *
	 * findAllCompleteEvaluation
	 *
	 * @param pageable
	 * @return
	 *
	 *  Page<Evaluation>
	 */
	Page<Evaluation> findAllCompleteEvaluation(Pageable pageable);
	
	/**
	 * @param id
	 * @return
	 */
	EvaluationDTO getEvaluationGeneral(Long id);

	void purgeOldEvaluations();

	
	
	/**
	 *
	 * answeringEvaluation for general evaluator or specialized evaluator
	 *
	 * @param evalId
	 *
	 *  void
	 */
	void answeringEvaluation(Long evalId);
	
	
	/**
	 *
	 * checkingEvaluation for expert evaluator to checking an evaluation from a specilized evaluator
	 *
	 * @param evaluation
	 *
	 *  void
	 */
	void checkingEvaluation(EvaluationDTO evaluation);
	
	/**
	 *
	 * findAllConfirmedEvaluation
	 *
	 * @param pageable
	 * @return
	 *
	 *  Page<Evaluation>
	 */
	Page<Evaluation> findAllConfirmedEvaluation(Pageable pageable);

	/**
	 *
	 * findAllMyCompleteEvaluation
	 *
	 * @param pageable
	 * @return
	 *
	 *  Page<Evaluation>
	 */
	Page<Evaluation> findAllMyCompleteEvaluation(Pageable pageable);

	/**
	 *
	 * temporarySaveEvaluation
	 *
	 * @param evaluation
	 *
	 *  void
	 */
	void temporarySaveEvaluation(EvaluationDTO evaluation);

	EvaluationDTO getCurrentGeneralEvaluation();
}
