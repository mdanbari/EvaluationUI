package com.mycompany.myapp.service.util;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;

import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
@Component
@Scope(value="session")
public class CaptchaUtil {

	public static String captchaValue = null;
	// Defining Character Array you can change accordingly
	private static final char[] chars = { '1', 'A', 'a', 'B', 'b', 'C', 'c', '2', 'D', 'd', 'E', 'e', 'F', 'f', '3',
			'G', 'g', 'H', 'h', 'I', 'i', 'J', 'j', 'K', 'k', 'L', 'l', '4', 'M', 'm', 'N', 'n', 'O', 'o', '5', 'P',
			'p', 'Q', 'q', 'R', 'r', 'S', 's', 'T', 't', '6', '7', 'U', 'u', 'V', 'v', 'U', 'u', 'W', 'w', '8', 'X',
			'x', 'Y', 'y', 'Z', 'z', '9' };

	public static Integer getRandomNumber() {
		Random rand = new Random();
		int max = 9999;
		int min = 1111;
		return rand.nextInt((max - min) + 1) + min;
	}

	public static byte[] createImageCaptcha() {
		// String base64Captcha=getRandomNumber().toString();
		// byte[] imagebyte=DatatypeConverter.parseBase64Binary(base64Captcha);
		// return imagebyte;
		captchaValue = getRandomNumber().toString();
		return renderImage(captchaValue).toByteArray();

	}
	
	public static Boolean validateCaptcha(String captcha)
	{
		if(captcha.equals(captchaValue))
			return true;
		return false;
	}

	// public static String createImageCaptcha()
	// {
	// return getRandomNumber().toString();
	// }

	private static final Color[] colors = { Color.red, Color.black, Color.blue };

	// Method for generating the Captcha Code
	public static String generateCaptchaText() {

		String randomStrValue = "";

		final int LENGTH = 6; // Character Length

		StringBuffer sb = new StringBuffer();

		int index = 0;

		for (int i = 0; i < LENGTH; i++) {
			// Getting Random Number with in range(ie: 60 total character
			// present)
			index = (int) (Math.random() * (chars.length - 1));
			sb.append(chars[index]); // Appending the character using
										// StringBuffer
		}

		randomStrValue = String.valueOf(sb); // Assigning the Generated Password
												// to String variable

		return randomStrValue;
	}

	// Method used to render the Image for Captcha
	public static ByteArrayOutputStream renderImage(String value) {
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		if (value != null && !value.isEmpty()) {

			BufferedImage image = null;

			try {
				Resource[] resource = new ClassPathResource[] { new ClassPathResource("background.jpg") };

				image = ImageIO.read(resource[0].getInputStream()); // Background
																	// Image

			} catch (IOException e) {

				System.out.println(e.getMessage());
				e.printStackTrace();
			}

			Graphics g = image.getGraphics();

			g.setFont(g.getFont().deriveFont(30f));

			char[] c = value.toCharArray();

			int x = 20;
			int y = 50;

			for (int i = 0; i < c.length; i++) {
				x = x + 30;
				g.setColor(colors[(int) (Math.random() * 3)]);
				g.drawString(String.valueOf(c[i]), x, y);
			}

			g.dispose();

			try {
				ImageIO.write(image, "png", buffer); // Output Image

			} catch (IOException e) {

				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return buffer;
	}

}
