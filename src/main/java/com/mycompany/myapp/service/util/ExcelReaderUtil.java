package com.mycompany.myapp.service.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.Answer;
import com.mycompany.myapp.domain.Department;
import com.mycompany.myapp.domain.DiamondCategory;
import com.mycompany.myapp.domain.ElecServiceDiamondCategory;
import com.mycompany.myapp.domain.ElecServiceMaturityState;
import com.mycompany.myapp.domain.ElecServiceQuestion;
import com.mycompany.myapp.domain.ElectronicService;
import com.mycompany.myapp.domain.MaturityStage;
import com.mycompany.myapp.domain.Question;
import com.mycompany.myapp.domain.enumeration.DiamondCategoryName;
import com.mycompany.myapp.domain.enumeration.QuestionStatus;
import com.mycompany.myapp.domain.enumeration.SheetName;
import com.mycompany.myapp.service.AnswerService;
import com.mycompany.myapp.service.DepartmentService;
import com.mycompany.myapp.service.DiamondCategoryService;
import com.mycompany.myapp.service.ElecServiceDiamondCategoryService;
import com.mycompany.myapp.service.ElecServiceMaturityStateService;
import com.mycompany.myapp.service.ElecServiceQuestionService;
import com.mycompany.myapp.service.ElectronicServiceService;
import com.mycompany.myapp.service.MaturityStageService;
import com.mycompany.myapp.service.QuestionService;

@Service
public class ExcelReaderUtil {
	public MaturityStageService maturityservice;

	public QuestionService questionService;
	
	public AnswerService answerService;

	public DiamondCategoryService diamondService;

	public DepartmentService departmentService;

	public ElectronicServiceService elecService;

	public ElecServiceDiamondCategoryService elecDiamondService;

	public ElecServiceMaturityStateService elecMaturityService;

	public ElecServiceQuestionService elecServiceQuestionService;

	@Autowired
	public ExcelReaderUtil(MaturityStageService matService, QuestionService quService, AnswerService ansService,
			DiamondCategoryService dmndService, DepartmentService depSerivice, ElectronicServiceService elecService,
			ElecServiceDiamondCategoryService elecDiamondService, ElecServiceMaturityStateService elecMaturityService,
			ElecServiceQuestionService elecQuestionService) {
		this.maturityservice = matService;
		this.questionService = quService;
		this.answerService = ansService;
		this.diamondService = dmndService;
		this.departmentService = depSerivice;
		this.elecService = elecService;
		this.elecDiamondService = elecDiamondService;
		this.elecMaturityService = elecMaturityService;
		this.elecServiceQuestionService = elecQuestionService;
	}

	// private Question question = null;

	// public static String fileName;

	/*
	 * public static String getFileName() { return fileName; }
	 * 
	 * public static void setFileName(String fileName) {
	 * ExcelReaderUtil.fileName = fileName; }
	 */

	@SuppressWarnings({ "deprecation", "deprecation" })
	@Transactional(rollbackFor = Throwable.class)
	public void readQyestionAnswer_Fa_File(String fileName) throws Exception {
		try {
			Question question = null;
			File file = new File(fileName);
			if (!file.exists()) {
				throw new FileNotFoundException();
			}
			FileInputStream excelFile = new FileInputStream(new File(fileName));
			XSSFWorkbook workbook = new XSSFWorkbook(excelFile);
			createMaturityStage();
			createDiamondCategory();
			for (Sheet sheet : workbook) {
				if (isAccept(sheet.getSheetName())) {
					MaturityStage maturity = new MaturityStage();
					maturity = maturityservice.findByFATiltle(sheet.getSheetName());
					// maturity.setMaturityStageTitlefa(sheet.getSheetName());
					// maturity = maturityservice.save(maturity);
					Iterator<Row> iterator = sheet.iterator();
					int rowIndex = 0;
					while (rowIndex < 1) {
						iterator.next();
						rowIndex++;
					}
					while (iterator.hasNext()) {
						Row currentRow = iterator.next();
						Iterator<Cell> cellIterator = currentRow.iterator();
						// Cell firstCellofRow=currentRow.getCell(0);

						if (currentRow.getCell(2).getCellTypeEnum() != CellType.BLANK) {
							System.out.println(currentRow.getCell(2).getStringCellValue());
							Answer answer = null;
							Set<DiamondCategory> diamondCategories = new HashSet<>();
							question = new Question();
							question.setMaturityStage(maturity);
							int cellIndex = 0;
							while (cellIndex < 1) {
								cellIndex++;
								cellIterator.next();
							}
							cellIndex++;
							while (cellIterator.hasNext() && cellIndex < 23) {
								Cell currentCell = cellIterator.next();
								switch (cellIndex) {
								case 2: {
									if (currentCell.getCellTypeEnum() == CellType.STRING)
										question.setQuestionTitlefa(currentCell.getStringCellValue());
									break;
								}
								case 3: {
									if (currentCell.getCellTypeEnum() == CellType.STRING)
										question.setQuestionDescriptionfa(currentCell.getStringCellValue());
									break;
								}
								case 4: {
									if (currentCell.getCellTypeEnum() == CellType.NUMERIC)
										question.setQuestionWeight(currentCell.getNumericCellValue());
									break;
								}
								case 5: {
									if (currentCell.getCellTypeEnum() == CellType.NUMERIC)
										question.setQuestionPercentage(currentCell.getNumericCellValue());
									question.setQuestionStatus(QuestionStatus.ENABLE);
									question = questionService.save(question);
									break;
								}
								case 6:
								case 8:
								case 10:
								case 12: {
									if (currentCell.getCellTypeEnum() != CellType.BLANK
											&& currentCell.getCellTypeEnum() == CellType.STRING) {
										answer = new Answer();
										answer.setAnswerTitlefa(currentCell.getStringCellValue());
									}
									break;
								}
								case 7:
								case 9:
								case 11:
								case 13: {
									if (currentCell.getCellTypeEnum() != CellType.BLANK
											&& currentCell.getCellTypeEnum() == CellType.NUMERIC) {
										answer.setAnswerValue(currentCell.getNumericCellValue());
										answer.setQuestion(question);
										answerService.save(answer);
									}
									break;
								}
								case 14: {
									if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
										if (currentCell.getNumericCellValue() != 0) {
											diamondCategories = question.getDiamondCategories();
											diamondCategories.add(diamondService
													.findByTitlefa(DiamondCategoryName.OMOOMI.toString()));
											question.setDiamondCategories(diamondCategories);
										}
									}
									break;
								}
								case 15: {
									if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
										if (currentCell.getNumericCellValue() != 0) {
											diamondCategories = question.getDiamondCategories();
											diamondCategories.add(diamondService
													.findByTitlefa(DiamondCategoryName.EKHTESASI_AMN.toString()));
											question.setDiamondCategories(diamondCategories);
										}
									}
									break;
								}
								case 16: {
									if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
										if (currentCell.getNumericCellValue() != 0) {
											diamondCategories = question.getDiamondCategories();
											diamondCategories.add(diamondService
													.findByTitlefa(DiamondCategoryName.EKHTESASII_NAAMN.toString()));
											question.setDiamondCategories(diamondCategories);
										}
									}
									break;
								}
								case 17: {
									if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
										if (currentCell.getNumericCellValue() != 0) {
											diamondCategories = question.getDiamondCategories();
											diamondCategories.add(diamondService
													.findByTitlefa(DiamondCategoryName.MOSTAGHEL.toString()));
											question.setDiamondCategories(diamondCategories);
										}
									}
									break;
								}

								case 18: {
									if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
										if (currentCell.getNumericCellValue() != 0) {
											diamondCategories = question.getDiamondCategories();
											diamondCategories.add(diamondService
													.findByTitlefa(DiamondCategoryName.MORTABET_EDGHAAM.toString()));
											question.setDiamondCategories(diamondCategories);
										}
									}
									break;
								}

								case 19: {
									if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
										if (currentCell.getNumericCellValue() != 0) {
											diamondCategories = question.getDiamondCategories();
											diamondCategories.add(diamondService
													.findByTitlefa(DiamondCategoryName.MORTABET_HAMRASTA.toString()));
											question.setDiamondCategories(diamondCategories);
										}
									}
									break;
								}

								case 20: {
									if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
										if (currentCell.getNumericCellValue() != 0) {
											diamondCategories = question.getDiamondCategories();
											diamondCategories.add(diamondService
													.findByTitlefa(DiamondCategoryName.AMALKARDI.toString()));
											question.setDiamondCategories(diamondCategories);
										}
									}
									break;
								}

								case 21: {
									if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
										if (currentCell.getNumericCellValue() != 0) {
											diamondCategories = question.getDiamondCategories();
											diamondCategories.add(diamondService
													.findByTitlefa(DiamondCategoryName.ETELAATI_ENTEKHAABI.toString()));
											question.setDiamondCategories(diamondCategories);
										}
									}
									break;
								}
								case 22: {
									if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
										if (currentCell.getNumericCellValue() != 0) {
											diamondCategories = question.getDiamondCategories();
											diamondCategories.add(diamondService.findByTitlefa(
													DiamondCategoryName.ETELLAATI_TANZIMSHODE.toString()));
											question.setDiamondCategories(diamondCategories);
										}
									}
									break;
								}

								}
								cellIndex++;
							}
							questionService.save(question);
						} else {
							break;
						}
					}
				}

			}
			if (file.exists())
				file.delete();
		} catch (FileNotFoundException e) {
			System.err.println("QuestionFileNotFound");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	@Transactional(rollbackFor = Throwable.class)
	public void initializeElectronicSevice(String fileName) throws Exception {
		try {
			Department department = null;
			Department parentDepartment = null;
			createDiamondCategory();
			FileInputStream excelFile = new FileInputStream(new File(fileName));
			XSSFWorkbook workbook = new XSSFWorkbook(excelFile);
			for (Sheet sheet : workbook) {
				Iterator<Row> iterator = sheet.iterator();
				iterator.next();
				while (iterator.hasNext()) {
					Row currentRow = iterator.next();
					Iterator<Cell> cellIterator = currentRow.iterator();
					if (currentRow.getCell(0).getCellTypeEnum() != CellType.BLANK) {
						String depName = currentRow.getCell(0).getStringCellValue();
						ElectronicService electronicService = new ElectronicService();
						Set<DiamondCategory> diamondCategories = new HashSet<>();
						int cellIndex = 0;
						while (cellIterator.hasNext()) {
							Cell currentCell = cellIterator.next();
							switch (cellIndex) {
							case 0: {
								if (currentCell.getCellTypeEnum() == CellType.STRING
										&& !currentCell.getStringCellValue().trim().equals("ندارد")) {
									department = departmentService.findByDepartmentNamefa(depName);
									if (department == null) {
										department = new Department();
										department.setDepartmentNamefa(depName);
										departmentService.save(department);
									}
									electronicService.setDepartment(department);
								}
								break;
							}
							case 1: {
								if (currentCell.getCellTypeEnum() == CellType.STRING
										&& !currentCell.getStringCellValue().trim().equals("ندارد")) {
									String parentDepName = currentCell.getStringCellValue();
									parentDepartment = departmentService.findByDepartmentNamefa(parentDepName);
									if (parentDepartment == null) {
										parentDepartment = new Department();
										parentDepartment.setDepartmentNamefa(parentDepName);
										departmentService.save(parentDepartment);
									}
									department = departmentService.findByDepartmentNamefa(depName);
									department.setParent(parentDepartment);
									departmentService.save(department);

								}
								break;
							}
							case 3: {
								if (currentCell.getCellTypeEnum() == CellType.STRING) {
									electronicService.setElecServiceTitlefa(currentCell.getStringCellValue());
									System.out.println(currentCell.getStringCellValue());
								}
								break;
							}
							case 4: {
								String elecIdentify = null;
								if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									double number = currentCell.getNumericCellValue();
									elecIdentify = String.valueOf(number);
								} else if (currentCell.getCellTypeEnum() == CellType.STRING
										&& !currentCell.getStringCellValue().trim().equals("ندارد"))
									elecIdentify = currentCell.getStringCellValue();
								electronicService.setElectronicServiceCode(elecIdentify);
								break;
							}
							case 5: {
								if (currentCell.getCellTypeEnum() == CellType.STRING
										&& !currentCell.getStringCellValue().trim().equals("ندارد")) {

									electronicService.setParentServicefa(currentCell.getStringCellValue());
								}
								break;
							}

							case 6: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK)

								{
									if (currentCell.getCellTypeEnum() == CellType.NUMERIC)
										electronicService.setParentServiceCode(
												String.valueOf(currentCell.getNumericCellValue()));
									else
										electronicService
												.setParentServiceCode(String.valueOf(currentCell.getStringCellValue()));
								}
								break;
							}

							case 7: {
								if (electronicService.getDiamondCategories() == null) {
									electronicService.setDiamondCategories(diamondCategories);
								}
								if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									if (currentCell.getNumericCellValue() != 0) {
										diamondCategories = electronicService.getDiamondCategories();
										diamondCategories.add(
												diamondService.findByTitlefa(DiamondCategoryName.OMOOMI.toString()));
										electronicService.setDiamondCategories(diamondCategories);
									}
								}
								break;
							}
							case 8: {
								if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									if (currentCell.getNumericCellValue() != 0) {
										diamondCategories = electronicService.getDiamondCategories();
										diamondCategories.add(diamondService
												.findByTitlefa(DiamondCategoryName.EKHTESASI_AMN.toString()));
										electronicService.setDiamondCategories(diamondCategories);
									}
								}
								break;
							}
							case 9: {
								if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {

									if (currentCell.getNumericCellValue() != 0) {
										diamondCategories = electronicService.getDiamondCategories();
										diamondCategories.add(diamondService
												.findByTitlefa(DiamondCategoryName.EKHTESASII_NAAMN.toString()));
										electronicService.setDiamondCategories(diamondCategories);
									}
								}
								break;
							}
							case 10: {
								if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									if (currentCell.getNumericCellValue() != 0) {
										diamondCategories = electronicService.getDiamondCategories();
										diamondCategories.add(
												diamondService.findByTitlefa(DiamondCategoryName.MOSTAGHEL.toString()));
										electronicService.setDiamondCategories(diamondCategories);
									}
								}
								break;
							}
							case 11: {
								if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									if (currentCell.getNumericCellValue() != 0) {
										diamondCategories = electronicService.getDiamondCategories();
										diamondCategories.add(diamondService
												.findByTitlefa(DiamondCategoryName.MORTABET_EDGHAAM.toString()));
										electronicService.setDiamondCategories(diamondCategories);
									}
								}
								break;
							}
							case 12: {
								if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									if (currentCell.getNumericCellValue() != 0) {
										diamondCategories = electronicService.getDiamondCategories();
										diamondCategories.add(diamondService
												.findByTitlefa(DiamondCategoryName.MORTABET_HAMRASTA.toString()));
										electronicService.setDiamondCategories(diamondCategories);
									}
								}
								break;
							}
							case 13: {
								if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									if (currentCell.getNumericCellValue() != 0) {
										diamondCategories = electronicService.getDiamondCategories();
										diamondCategories.add(
												diamondService.findByTitlefa(DiamondCategoryName.AMALKARDI.toString()));
										electronicService.setDiamondCategories(diamondCategories);
									}

								}
								break;
							}
							case 14: {

								if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									if (currentCell.getNumericCellValue() != 0) {
										diamondCategories = electronicService.getDiamondCategories();
										diamondCategories.add(diamondService
												.findByTitlefa(DiamondCategoryName.ETELAATI_ENTEKHAABI.toString()));
										electronicService.setDiamondCategories(diamondCategories);
									}
								}
								break;
							}
							case 15: {

								if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									if (currentCell.getNumericCellValue() != 0) {
										diamondCategories = electronicService.getDiamondCategories();
										diamondCategories.add(diamondService
												.findByTitlefa(DiamondCategoryName.ETELLAATI_TANZIMSHODE.toString()));
										electronicService.setDiamondCategories(diamondCategories);
									}

								}
								break;
							}

							}
							cellIndex++;
						}
						electronicService.setElecServiceEvalCount(Integer.valueOf(2));
						Integer electronicServiceCount = department.getElectronicServiceCount();
						department.setElectronicServiceCount(
								electronicServiceCount == null ? 1 : electronicServiceCount + 1);
						departmentService.save(department);
						elecService.save(electronicService);
					} else {
						break;
					}
				}
			}
			File file = new File(fileName);
			if (file.exists())
				file.delete();
		} catch (

		FileNotFoundException e) {
			System.err.println("electronicFileNotFound");
		} catch (IOException e) {
			e.printStackTrace();

		}

	}

	@Transactional(rollbackFor = Throwable.class)
	public void insertAutomateReport(String fileName) throws Exception {
		try {
			Department department = null;
			ElectronicService elec = null;
			ElecServiceDiamondCategory elecServiceDiamond = null;
			ElecServiceMaturityState elecServiceMaturityState = null;
			FileInputStream excelFile = new FileInputStream(new File(fileName));
			XSSFWorkbook workbook = new XSSFWorkbook(excelFile);
			for (Sheet sheet : workbook) {
				Iterator<Row> iterator = sheet.iterator();
				iterator.next();
				while (iterator.hasNext()) {
					Row currentRow = iterator.next();
					Iterator<Cell> cellIterator = currentRow.iterator();
					Date date = new Date();
					if (currentRow.getCell(0).getCellTypeEnum() != CellType.BLANK) {
						int cellIndex = 0;
						while (cellIterator.hasNext()) {
							Cell currentCell = cellIterator.next();
							switch (cellIndex) {
							case 0: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.STRING) {
									/*department = departmentService
											.findByDepartmentNamefa(currentCell.getStringCellValue());*/
									department=departmentService.findByDepartmentName(currentCell.getStringCellValue());
									if (department == null)
										throw new Exception("چنین سازمانی دز سیستم دخیره نشده است"+currentCell.getStringCellValue()+currentRow.getRowNum());
								}
								break;
							}
							case 1: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.STRING) {
									//elec = elecService.findElecServiceByTitlefa(currentCell.getStringCellValue());
									elec=elecService.findByElectronicserviceName(currentCell.getStringCellValue());
									if (elec==null || (elec != null && elec.getDepartment().getId() != department.getId()))
										throw new Exception(department.getDepartmentNamefa()+"برای این سازمان جنین خدمتی تعریف نشده است"+currentCell.getStringCellValue());
								}
								break;
							}
							case 2: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.STRING) {
									date = CalanderUtil.getTime(currentCell.getStringCellValue());
								}
								break;
							}
							case 3: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									elecServiceDiamond = new ElecServiceDiamondCategory();
									elecServiceDiamond.setDiamondCategory(
											diamondService.findByTitlefa(DiamondCategoryName.OMOOMI.toString()));
									elecServiceDiamond.setElectronicService(elec);
									elecServiceDiamond.setElecDiamondCategoryScore(currentCell.getNumericCellValue());
									elecServiceDiamond.setElecDiamondDate(
											ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()));
									elecDiamondService.save(elecServiceDiamond);
								} else
									throw new Exception(
											"خطا در نمره مدل الماسی عمومی خدمت" + elec.getElecServiceTitlefa());
								break;
							}
							case 4: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									elecServiceDiamond = new ElecServiceDiamondCategory();
									elecServiceDiamond.setDiamondCategory(
											diamondService.findByTitlefa(DiamondCategoryName.EKHTESASI_AMN.toString()));
									elecServiceDiamond.setElectronicService(elec);
									elecServiceDiamond.setElecDiamondCategoryScore(currentCell.getNumericCellValue());
									elecServiceDiamond.setElecDiamondDate(
											ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()));
									elecDiamondService.save(elecServiceDiamond);
								} else
									throw new Exception(
											"خطا در نمره ندل الماسی اختصاصی امن" + elec.getElecServiceTitlefa());
								break;
							}
							case 5: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									elecServiceDiamond = new ElecServiceDiamondCategory();
									elecServiceDiamond.setDiamondCategory(diamondService
											.findByTitlefa(DiamondCategoryName.EKHTESASII_NAAMN.toString()));
									elecServiceDiamond.setElectronicService(elec);
									elecServiceDiamond.setElecDiamondCategoryScore(currentCell.getNumericCellValue());
									elecServiceDiamond.setElecDiamondDate(
											ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()));
									elecDiamondService.save(elecServiceDiamond);
								} else
									throw new Exception(
											"خطا در نمره مدل الماسی اختصاصی نا امن" + elec.getElecServiceTitlefa());
								break;
							}
							case 6: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									elecServiceDiamond = new ElecServiceDiamondCategory();
									elecServiceDiamond.setDiamondCategory(
											diamondService.findByTitlefa(DiamondCategoryName.MOSTAGHEL.toString()));
									elecServiceDiamond.setElectronicService(elec);
									elecServiceDiamond.setElecDiamondCategoryScore(currentCell.getNumericCellValue());
									elecServiceDiamond.setElecDiamondDate(ZonedDateTime.now());
									elecDiamondService.save(elecServiceDiamond);
								} else
									throw new Exception(
											"خطا در نمره مدل الماسی اختصاصی مستقل" + elec.getElecServiceTitlefa());
								break;
							}

							case 7: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									elecServiceDiamond = new ElecServiceDiamondCategory();
									elecServiceDiamond.setDiamondCategory(diamondService
											.findByTitlefa(DiamondCategoryName.MORTABET_EDGHAAM.toString()));
									elecServiceDiamond.setElectronicService(elec);
									elecServiceDiamond.setElecDiamondCategoryScore(currentCell.getNumericCellValue());
									elecServiceDiamond.setElecDiamondDate(
											ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()));
									elecDiamondService.save(elecServiceDiamond);
								} else
									throw new Exception(
											"خطا در نمره مدل الماسی مرتبط ادغام" + elec.getElecServiceTitlefa());
								break;
							}

							case 8: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									elecServiceDiamond = new ElecServiceDiamondCategory();
									elecServiceDiamond.setDiamondCategory(diamondService
											.findByTitlefa(DiamondCategoryName.MORTABET_HAMRASTA.toString()));
									elecServiceDiamond.setElectronicService(elec);
									elecServiceDiamond.setElecDiamondCategoryScore(currentCell.getNumericCellValue());
									elecServiceDiamond.setElecDiamondDate(
											ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()));
									elecDiamondService.save(elecServiceDiamond);
								} else
									throw new Exception(
											"خطا در نمره مدل الماسی مرتبط هم راستا" + elec.getElecServiceTitlefa());
								break;
							}

							case 9: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									elecServiceDiamond = new ElecServiceDiamondCategory();
									elecServiceDiamond.setDiamondCategory(
											diamondService.findByTitlefa(DiamondCategoryName.AMALKARDI.toString()));
									elecServiceDiamond.setElectronicService(elec);
									elecServiceDiamond.setElecDiamondCategoryScore(currentCell.getNumericCellValue());
									elecServiceDiamond.setElecDiamondDate(
											ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()));
									elecDiamondService.save(elecServiceDiamond);
								} else
									throw new Exception(
											"خطا در نمره مدل الماسی مرتبط هم عملکردی" + elec.getElecServiceTitlefa());
								break;
							}

							case 10: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									elecServiceDiamond = new ElecServiceDiamondCategory();
									elecServiceDiamond.setDiamondCategory(diamondService
											.findByTitlefa(DiamondCategoryName.ETELAATI_ENTEKHAABI.toString()));
									elecServiceDiamond.setElectronicService(elec);
									elecServiceDiamond.setElecDiamondCategoryScore(currentCell.getNumericCellValue());
									elecServiceDiamond.setElecDiamondDate(
											ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()));
									elecDiamondService.save(elecServiceDiamond);
								} else
									throw new Exception(
											"خطا در نمره مدل الماسی  اطلاعاتی انتخابی" + elec.getElecServiceTitlefa());
								break;
							}

							case 11: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									elecServiceDiamond = new ElecServiceDiamondCategory();
									elecServiceDiamond.setDiamondCategory(diamondService
											.findByTitlefa(DiamondCategoryName.ETELLAATI_TANZIMSHODE.toString()));
									elecServiceDiamond.setElectronicService(elec);
									elecServiceDiamond.setElecDiamondCategoryScore(currentCell.getNumericCellValue());
									elecServiceDiamond.setElecDiamondDate(
											ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()));
									elecDiamondService.save(elecServiceDiamond);
								} else
									throw new Exception("خطا در نمره مدل الماسی اطلا عاتی تنظیم شده"
											+ elec.getElecServiceTitlefa());
								break;
							}
							case 12: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									elecServiceMaturityState = new ElecServiceMaturityState();
									elecServiceMaturityState.electronicService(elec);
									elecServiceMaturityState.setMaturityStage(
											maturityservice.findByFATiltle(SheetName.hozurDarWeb.toString()));
									elecServiceMaturityState
											.setElecMaturityStateScore(currentCell.getNumericCellValue());
									elecServiceMaturityState.setElecMaturityDate(
											ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()));
									elecMaturityService.save(elecServiceMaturityState);
								} else
									throw new Exception(
											"خطا در نمره مدل بلوغ حضور در وب" + elec.getElecServiceTitlefa());
								break;
							}
							case 13: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									elecServiceMaturityState = new ElecServiceMaturityState();
									elecServiceMaturityState.electronicService(elec);
									elecServiceMaturityState.setMaturityStage(
											maturityservice.findByFATiltle(SheetName.taamoli.toString()));
									elecServiceMaturityState
											.setElecMaturityStateScore(currentCell.getNumericCellValue());
									elecServiceMaturityState.setElecMaturityDate(
											ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()));
									elecMaturityService.save(elecServiceMaturityState);
								} else
									throw new Exception("خطا در نمره مدل بلوغ تعاملی" + elec.getElecServiceTitlefa());
								break;
							}
							case 14: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									elecServiceMaturityState = new ElecServiceMaturityState();
									elecServiceMaturityState.electronicService(elec);
									elecServiceMaturityState.setMaturityStage(
											maturityservice.findByFATiltle(SheetName.tarakoneshi.toString()));
									elecServiceMaturityState
											.setElecMaturityStateScore(currentCell.getNumericCellValue());
									elecServiceMaturityState.setElecMaturityDate(
											ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()));
									elecMaturityService.save(elecServiceMaturityState);
								} else
									throw new Exception("خطا در نمره مدل بلوغ تراکنشی" + elec.getElecServiceTitlefa());
								break;
							}
							case 15: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									elecServiceMaturityState = new ElecServiceMaturityState();
									elecServiceMaturityState.electronicService(elec);
									elecServiceMaturityState.setMaturityStage(
											maturityservice.findByFATiltle(SheetName.yeparchegi.toString()));
									elecServiceMaturityState
											.setElecMaturityStateScore(currentCell.getNumericCellValue());
									elecServiceMaturityState.setElecMaturityDate(
											ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()));
									elecMaturityService.save(elecServiceMaturityState);
								} else
									throw new Exception("خطا در نمره مدل یکپارچگی" + elec.getElecServiceTitlefa());
								break;
							}
							case 16: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									elecServiceMaturityState = new ElecServiceMaturityState();
									elecServiceMaturityState.electronicService(elec);
									elecServiceMaturityState.setMaturityStage(
											maturityservice.findByFATiltle(SheetName.mosharekat.toString()));
									elecServiceMaturityState
											.setElecMaturityStateScore(currentCell.getNumericCellValue());
									elecServiceMaturityState.setElecMaturityDate(
											ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()));
									elecMaturityService.save(elecServiceMaturityState);
								} else
									throw new Exception("خطا در نمره مدل بلوغ مشارکتی" + elec.getElecServiceTitlefa());
								break;
							}

							}
							cellIndex++;
						}
					} else
						break;
				}
			}
			File file = new File(fileName);
			if (file.exists())
				file.delete();
		} catch (FileNotFoundException e) {
			System.err.println("ReportFileNotFound");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Transactional(rollbackFor = Throwable.class)
	public void inserElecServiceReportOnQuestion(String fileName) throws Exception {
		try {
			ElecServiceQuestion elecQuestion = null;
			Department department = null;
			ElectronicService elec = null;
			Question question = null;
			File file = new File(fileName);
			if (!file.exists()) {
				throw new FileNotFoundException();
			}
			FileInputStream excelFile = new FileInputStream(new File(fileName));
			XSSFWorkbook workbook = new XSSFWorkbook(excelFile);
			for (Sheet sheet : workbook) {
				Iterator<Row> iterator = sheet.iterator();
				iterator.next();
				while (iterator.hasNext()) {
					Row currentRow = iterator.next();
					Iterator<Cell> cellIterator = currentRow.iterator();
					Date date = new Date();
					if (currentRow.getCell(0).getCellTypeEnum() != CellType.BLANK) {
						int cellIndex = 0;
						while (cellIterator.hasNext()) {
							Cell currentCell = cellIterator.next();
							switch (cellIndex) {
							case 0: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.STRING) {
									department = departmentService
											.findByDepartmentName(currentCell.getStringCellValue());
									if (department == null)
										throw new Exception(" چنین سازمانی دز سیستم دخیره نشده است"
												+ currentCell.getStringCellValue()+currentRow.getRowNum());
								}
								break;
							}
							case 1: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.STRING) {
									elec = elecService.findByElectronicserviceName(currentCell.getStringCellValue());
									if (elec==null || (elec != null && elec.getDepartment().getId() != department.getId()))
										throw new Exception(department.getDepartmentNamefa()+" برای این سازمان جنین خدمتی تعریف نشده است"
												+ currentCell.getStringCellValue());
								}
								break;
							}
							case 2: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.STRING) {
									question = questionService.findByQuestionName(currentCell.getStringCellValue());
									System.out.println(currentCell.getStringCellValue());
									if (question == null)
										throw new Exception(
												" چنین شاخصی موجود نیست" + currentCell.getStringCellValue()+currentRow.getRowNum());
								}
								break;
							}
							case 3: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.NUMERIC) {
									elecQuestion = new ElecServiceQuestion();
									elecQuestion.setCount(1);
									elecQuestion.setElectronicService(elec);
									elecQuestion.setQuestion(question);
									elecQuestion.setElecServiceQuestionScore(currentCell.getNumericCellValue() * 100);
								} else
									throw new Exception(currentRow.getRowNum() + "نمره در این سطر درست وارد نشده است");
								break;
							}
							case 4: {
								if (currentCell.getCellTypeEnum() != CellType.BLANK
										&& currentCell.getCellTypeEnum() == CellType.STRING) {
									date = CalanderUtil.getTime(currentCell.getStringCellValue());
									elecQuestion.setElecServiceQuestionDate(
											ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()));
									elecServiceQuestionService.save(elecQuestion);
								}
								break;
							}
							}
							cellIndex++;
						}
					} else
						break;
				}

			}

			if (file.exists())
				file.delete();
		} catch (FileNotFoundException e) {
			System.err.println("QuestionReportFileNotFound");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * check the dataBase if have diamonCategory do nothing else creae and
	 * saveDiamondCategory
	 */
	@Transactional
	private void createDiamondCategory() {
		List<DiamondCategory> diamondList = diamondService.findAll();
		if (diamondList.isEmpty() || diamondList == null) {
			for (DiamondCategoryName diamondName : DiamondCategoryName.values()) {
				DiamondCategory diamond = new DiamondCategory();
				diamond.setDiamondCategoryTitlefa(diamondName.toString());
				diamondService.save(diamond);
			}
		}
	}

	@Transactional
	private void createMaturityStage() {
		List<MaturityStage> maturityList = maturityservice.findAll();
		if (maturityList.isEmpty() || maturityList == null) {
			for (SheetName maturityName : SheetName.values()) {
				MaturityStage maturity = new MaturityStage();
				maturity.setMaturityStageTitlefa(maturityName.toString());
				maturityservice.save(maturity);
			}
		}
	}

	private Boolean isAccept(String sheetname) {
		if (sheetname != null) {
			if (sheetname.equals(SheetName.hozurDarWeb.toString()))
				return true;
			else if (sheetname.equals(SheetName.mosharekat.toString()))
				return true;
			else if (sheetname.equals(SheetName.taamoli.toString()))
				return true;
			else if (sheetname.equals(SheetName.tarakoneshi.toString()))
				return true;
			else if (sheetname.equals(SheetName.yeparchegi.toString()))
				return true;
			else
				return false;
		}

		return false;
	}

}
