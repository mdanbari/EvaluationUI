package com.mycompany.myapp.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mycompany.myapp.domain.QuestionAnswer;

/**
 * Service Interface for managing QuestionAnswer.
 */
public interface QuestionAnswerService {

    /**
     * Save a questionAnswer.
     *
     * @param questionAnswer the entity to save
     * @return the persisted entity
     */
    QuestionAnswer save(QuestionAnswer questionAnswer);

    /**
     *  Get all the questionAnswers.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<QuestionAnswer> findAll(Pageable pageable);

    /**
     *  Get the "id" questionAnswer.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    QuestionAnswer findOne(Long id);

    /**
     *  Delete the "id" questionAnswer.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
