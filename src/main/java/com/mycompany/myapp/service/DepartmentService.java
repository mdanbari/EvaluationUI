package com.mycompany.myapp.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mycompany.myapp.domain.Department;
import com.mycompany.myapp.service.dto.ChildrenDepartment;

/**
 * Service Interface for managing Department.
 */
public interface DepartmentService {

    /**
     * Save a department.
     *
     * @param department the entity to save
     * @return the persisted entity
     */
    Department save(Department department);

    /**
     *  Get all the departments.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Department> findAll(Pageable pageable);

    /**
     *  Get the "id" department.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Department findOne(Long id);

    /**
     *  Delete the "id" department.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
    
    /**
     * 
     * get root departments
     * @return
     */
    List<ChildrenDepartment> findAllByParentIsNull();
    
    
    /**
     * 
     * get childern of department
     * 
     * @param id
     * @return
     */
    public List<ChildrenDepartment> findAllChildrenOfDepartment(Long id);
    
    /**
     * 
     * check exist child for department
     * 
     * @param id
     * @return
     */
    public Long findCountAllChildrenOfDepartment(Long id);
    
    
    /**
     * get the name of persian department and return department if exist
     * @param name
     * @return department
     */
    public Department findByDepartmentNamefa(String name);
    
    
    /**
     * @param search
     * @param local
     * @param pageable
     * @return
     */
    public Page<Department> search(String search  ,String local, Pageable pageable);
    
    public Department findByDepartmentName(String departmentTitlefa);
}
