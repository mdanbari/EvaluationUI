package com.mycompany.myapp.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mycompany.myapp.domain.EvaluationDiamondCategory;

/**
 * Service Interface for managing EvaluationDiamondCategory.
 */
public interface EvaluationDiamondCategoryService {

	/**
	 * Save a evaluationDiamondCategory.
	 *
	 * @param evaluationDiamondCategory
	 *            the entity to save
	 * @return the persisted entity
	 */
	EvaluationDiamondCategory save(EvaluationDiamondCategory evaluationDiamondCategory);

	/**
	 * Get all the evaluationDiamondCategories.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	Page<EvaluationDiamondCategory> findAll(Pageable pageable);

	/**
	 * Get the "id" evaluationDiamondCategory.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	EvaluationDiamondCategory findOne(Long id);

	/**
	 * Delete the "id" evaluationDiamondCategory.
	 *
	 * @param id
	 *            the id of the entity
	 */
	void delete(Long id);

	/**
	 *
	 * findAllByEvaluationIdIn
	 *
	 * @param ids
	 * @return
	 *
	 * 		List<EvaluationDiamondCategory>
	 */
	List<EvaluationDiamondCategory> findAllByEvaluationIdIn(List<Long> ids);
}
