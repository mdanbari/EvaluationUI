package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.ElecServiceQuestion;
import com.mycompany.myapp.service.dto.DepartmentQuestionChartReportResultDTO;
import com.mycompany.myapp.service.dto.DepartmentQuestionReportInputDTO;
import com.mycompany.myapp.service.dto.ElecServiceQuestionChartReportResultDTO;
import com.mycompany.myapp.service.dto.ElecServiceQuestionReportInputDTO;

/**
 * Service Interface for managing ElecServiceMaturityState.
 */
public interface ElecServiceQuestionService {

    ElecServiceQuestion save(ElecServiceQuestion elecServiceQuestion);
	
	ElecServiceQuestionChartReportResultDTO generateElecServiceQuestionReport(
			ElecServiceQuestionReportInputDTO elecServiceQuestionReportInputDTO) throws Exception;

	DepartmentQuestionChartReportResultDTO generateDepartmentQuestionReport(
			DepartmentQuestionReportInputDTO departmentQuestionReportInputDTO) throws Exception;

	

}
