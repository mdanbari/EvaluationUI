package com.mycompany.myapp.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mycompany.myapp.domain.UnMaturityModel;

/**
 * Service Interface for managing UnMaturityModel.
 */
public interface UnMaturityModelService {

    /**
     * Save a unMaturityModel.
     *
     * @param unMaturityModel the entity to save
     * @return the persisted entity
     */
    UnMaturityModel save(UnMaturityModel unMaturityModel);

    /**
     *  Get all the unMaturityModels.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<UnMaturityModel> findAll(Pageable pageable);

    /**
     *  Get the "id" unMaturityModel.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    UnMaturityModel findOne(Long id);

    /**
     *  Delete the "id" unMaturityModel.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
