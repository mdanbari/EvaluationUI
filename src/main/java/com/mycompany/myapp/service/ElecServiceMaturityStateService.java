package com.mycompany.myapp.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mycompany.myapp.domain.ElecServiceMaturityState;
import com.mycompany.myapp.service.dto.ConsolidatedChartReportResultDTO;
import com.mycompany.myapp.service.dto.ConsolidatedReportInputDTO;
import com.mycompany.myapp.service.dto.DepartmentReportInputDTO;
import com.mycompany.myapp.service.dto.DepartmentReportResultDTO;
import com.mycompany.myapp.service.dto.SeparationChartReportJpaDTO;
import com.mycompany.myapp.service.dto.SeparationChartReportResultDTO;
import com.mycompany.myapp.service.dto.SeparationReportInputDTO;

/**
 * Service Interface for managing ElecServiceMaturityState.
 */
public interface ElecServiceMaturityStateService {

    /**
     * Save a elecServiceMaturityState.
     *
     * @param elecServiceMaturityState the entity to save
     * @return the persisted entity
     */
    ElecServiceMaturityState save(ElecServiceMaturityState elecServiceMaturityState);

    /**
     *  Get all the elecServiceMaturityStates.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ElecServiceMaturityState> findAll(Pageable pageable);
 
    /**
     *  Get the "id" elecServiceMaturityState.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ElecServiceMaturityState findOne(Long id);

    /**
     *  Delete the "id" elecServiceMaturityState.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

	SeparationChartReportResultDTO generateElecServiceSeprationMaturityReport(SeparationReportInputDTO separationReportInputDTO) throws Exception;

	ConsolidatedChartReportResultDTO generateElecServiceConsolatedMaturityReport(
			ConsolidatedReportInputDTO consolidatedReportInputDTO) throws Exception;

	DepartmentReportResultDTO generateElecServiceDepartmentMaturityReport(
			DepartmentReportInputDTO departmentReportInputDTO) throws Exception;

}
