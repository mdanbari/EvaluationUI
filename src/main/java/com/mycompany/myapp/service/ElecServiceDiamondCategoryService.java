package com.mycompany.myapp.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mycompany.myapp.domain.ElecServiceDiamondCategory;
import com.mycompany.myapp.service.dto.ConsolidatedChartReportResultDTO;
import com.mycompany.myapp.service.dto.ConsolidatedReportInputDTO;
import com.mycompany.myapp.service.dto.DepartmentReportInputDTO;
import com.mycompany.myapp.service.dto.DepartmentReportResultDTO;
import com.mycompany.myapp.service.dto.SeparationChartReportResultDTO;
import com.mycompany.myapp.service.dto.SeparationReportInputDTO;

/**
 * Service Interface for managing ElecServiceDiamondCategory.
 */
public interface ElecServiceDiamondCategoryService {

    /**
     * Save a elecServiceDiamondCategory.
     *
     * @param elecServiceDiamondCategory the entity to save
     * @return the persisted entity
     */
    ElecServiceDiamondCategory save(ElecServiceDiamondCategory elecServiceDiamondCategory);

    /**
     *  Get all the elecServiceDiamondCategories.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ElecServiceDiamondCategory> findAll(Pageable pageable);

    /**
     *  Get the "id" elecServiceDiamondCategory.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ElecServiceDiamondCategory findOne(Long id);

    /**
     *  Delete the "id" elecServiceDiamondCategory.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);



	SeparationChartReportResultDTO generateElecServiceSeprationDiamondReport(SeparationReportInputDTO separationReportInputDTO) throws Exception;

	ConsolidatedChartReportResultDTO generateElecServiceConsolatedDiamondReport(
			ConsolidatedReportInputDTO consolidatedReportInputDTO) throws Exception;

	DepartmentReportResultDTO generateElecServiceDepartmentDiamondReport(
			DepartmentReportInputDTO departmentReportInputDTO) throws Exception;

}
