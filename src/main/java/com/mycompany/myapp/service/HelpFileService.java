package com.mycompany.myapp.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mycompany.myapp.domain.HelpFile;

/**
 * Service Interface for managing HelpFile.
 */
public interface HelpFileService {

    /**
     * Save a helpFile.
     *
     * @param helpFile the entity to save
     * @return the persisted entity
     */
    HelpFile save(HelpFile helpFile);

    /**
     *  Get all the helpFiles.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<HelpFile> findAll(Pageable pageable);

    /**
     *  Get the "id" helpFile.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    HelpFile findOne(Long id);

    /**
     *  Delete the "id" helpFile.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
