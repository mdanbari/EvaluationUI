package com.mycompany.myapp.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mycompany.myapp.domain.ElectronicService;
import com.mycompany.myapp.service.dto.SeparationChartReportResultDTO;
import com.mycompany.myapp.service.dto.SeparationReportInputDTO;

/**
 * Service Interface for managing ElectronicService.
 */
public interface ElectronicServiceService {

    /**
     * Save a electronicService.
     *
     * @param electronicService the entity to save
     * @return the persisted entity
     */
    ElectronicService save(ElectronicService electronicService);

    /**
     *  Get all the electronicServices.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ElectronicService> findAll(Pageable pageable);

    /**
     *  Get the "id" electronicService.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ElectronicService findOne(Long id);

    /**
     *  Delete the "id" electronicService.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
	 * getRandomElectronicServiceForEvaluation
	 * 
	 * @return ElectronicService
	 */
	ElectronicService getRandomElectronicServiceForEvaluation();

	/**
	 *  get all data by projection
	 * 
	 * @return ElectronicService
	 */
	List<ElectronicService> customFindAll();
	
	
	
	/**
	 * get all elecService of a department
	 * @param id
	 */
	List<ElectronicService> findAllElecServiceOfDepartment(Long id);
	
	ElectronicService findElecServiceByTitlefa(String title);
	
	  /**
     * @param search
     * @param local
     * @param pageable
     * @return
     */
    public Page<ElectronicService> search(String search  ,String local, Pageable pageable);

	SeparationChartReportResultDTO generateElecServiceTotalScore(SeparationReportInputDTO separationReportInputDTO);
	
	public ElectronicService findByElectronicserviceName(String elecServiceTitlefa);
}
