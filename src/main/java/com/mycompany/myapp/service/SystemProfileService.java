package com.mycompany.myapp.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mycompany.myapp.domain.SystemProfile;

/**
 * Service Interface for managing SystemProfile.
 */
public interface SystemProfileService {

    /**
     * Save a systemProfile.
     *
     * @param systemProfile the entity to save
     * @return the persisted entity
     */
    SystemProfile save(SystemProfile systemProfile);

    /**
     *  Get all the systemProfiles.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<SystemProfile> findAll(Pageable pageable);

    /**
     *  Get the "id" systemProfile.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    SystemProfile findOne(Long id);

    /**
     *  Delete the "id" systemProfile.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
