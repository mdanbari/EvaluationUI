package com.mycompany.myapp.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

	public static final String ADMIN = "ADMIN";

	public static final String ANONYMOUS = "ROLE_ANONYMOUS";

	public static final String GENERAL = "GENERAL_EVALUATOR";
	public static final String SPECIALISED = "SPECIALISED_EVALUATOR";
	public static final String EXPERT = "EXPERT_EVALUATOR";
	public static final String OBSERVER = "OBSERVER";

    private AuthoritiesConstants() {
    }
}
