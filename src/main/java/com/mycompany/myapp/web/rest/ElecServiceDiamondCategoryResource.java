package com.mycompany.myapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.ElecServiceDiamondCategory;
import com.mycompany.myapp.service.ElecServiceDiamondCategoryService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing ElecServiceDiamondCategory.
 */
@RestController
@RequestMapping("/api")
public class ElecServiceDiamondCategoryResource {

    private final Logger log = LoggerFactory.getLogger(ElecServiceDiamondCategoryResource.class);

    private static final String ENTITY_NAME = "elecServiceDiamondCategory";

    private final ElecServiceDiamondCategoryService elecServiceDiamondCategoryService;

    public ElecServiceDiamondCategoryResource(ElecServiceDiamondCategoryService elecServiceDiamondCategoryService) {
        this.elecServiceDiamondCategoryService = elecServiceDiamondCategoryService;
    }

    /**
     * POST  /elec-service-diamond-categories : Create a new elecServiceDiamondCategory.
     *
     * @param elecServiceDiamondCategory the elecServiceDiamondCategory to create
     * @return the ResponseEntity with status 201 (Created) and with body the new elecServiceDiamondCategory, or with status 400 (Bad Request) if the elecServiceDiamondCategory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/elec-service-diamond-categories")
    @Timed
    public ResponseEntity<ElecServiceDiamondCategory> createElecServiceDiamondCategory(@RequestBody ElecServiceDiamondCategory elecServiceDiamondCategory) throws URISyntaxException {
        log.debug("REST request to save ElecServiceDiamondCategory : {}", elecServiceDiamondCategory);
        if (elecServiceDiamondCategory.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new elecServiceDiamondCategory cannot already have an ID")).body(null);
        }
        ElecServiceDiamondCategory result = elecServiceDiamondCategoryService.save(elecServiceDiamondCategory);
        return ResponseEntity.created(new URI("/api/elec-service-diamond-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /elec-service-diamond-categories : Updates an existing elecServiceDiamondCategory.
     *
     * @param elecServiceDiamondCategory the elecServiceDiamondCategory to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated elecServiceDiamondCategory,
     * or with status 400 (Bad Request) if the elecServiceDiamondCategory is not valid,
     * or with status 500 (Internal Server Error) if the elecServiceDiamondCategory couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/elec-service-diamond-categories")
    @Timed
    public ResponseEntity<ElecServiceDiamondCategory> updateElecServiceDiamondCategory(@RequestBody ElecServiceDiamondCategory elecServiceDiamondCategory) throws URISyntaxException {
        log.debug("REST request to update ElecServiceDiamondCategory : {}", elecServiceDiamondCategory);
        if (elecServiceDiamondCategory.getId() == null) {
            return createElecServiceDiamondCategory(elecServiceDiamondCategory);
        }
        ElecServiceDiamondCategory result = elecServiceDiamondCategoryService.save(elecServiceDiamondCategory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, elecServiceDiamondCategory.getId().toString()))
            .body(result);
    }

    /**
     * GET  /elec-service-diamond-categories : get all the elecServiceDiamondCategories.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of elecServiceDiamondCategories in body
     */
    @GetMapping("/elec-service-diamond-categories")
    @Timed
    public ResponseEntity<List<ElecServiceDiamondCategory>> getAllElecServiceDiamondCategories(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ElecServiceDiamondCategories");
        Page<ElecServiceDiamondCategory> page = elecServiceDiamondCategoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/elec-service-diamond-categories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /elec-service-diamond-categories/:id : get the "id" elecServiceDiamondCategory.
     *
     * @param id the id of the elecServiceDiamondCategory to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the elecServiceDiamondCategory, or with status 404 (Not Found)
     */
    @GetMapping("/elec-service-diamond-categories/{id}")
    @Timed
    public ResponseEntity<ElecServiceDiamondCategory> getElecServiceDiamondCategory(@PathVariable Long id) {
        log.debug("REST request to get ElecServiceDiamondCategory : {}", id);
        ElecServiceDiamondCategory elecServiceDiamondCategory = elecServiceDiamondCategoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(elecServiceDiamondCategory));
    }

    /**
     * DELETE  /elec-service-diamond-categories/:id : delete the "id" elecServiceDiamondCategory.
     *
     * @param id the id of the elecServiceDiamondCategory to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/elec-service-diamond-categories/{id}")
    @Timed
    public ResponseEntity<Void> deleteElecServiceDiamondCategory(@PathVariable Long id) {
        log.debug("REST request to delete ElecServiceDiamondCategory : {}", id);
        elecServiceDiamondCategoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
