package com.mycompany.myapp.web.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.dto.CaptchaDTO;
import com.mycompany.myapp.service.util.CaptchaUtil;

@RestController
@RequestMapping("/api/captcha")
public class CaptchaResource {	
	@GetMapping("/createCaptcha")
	@Timed
	public ResponseEntity<CaptchaDTO> createCaptcha()
	{
		CaptchaDTO captcha=new CaptchaDTO();
		byte[] temp=CaptchaUtil.createImageCaptcha();
		if(temp!=null)
		{
			captcha.setData(temp);
			return new ResponseEntity<CaptchaDTO>(captcha, HttpStatus.OK);
		}
		else
			return new ResponseEntity<CaptchaDTO>(HttpStatus.METHOD_FAILURE);
	}
	
	@GetMapping("/validateCaptcha/{captchaValue}")
	@Timed
	public ResponseEntity<CaptchaDTO> validateCaptcha(@PathVariable String captchaValue)
	{
		CaptchaDTO captchaDto=new CaptchaDTO();
		captchaDto.setValid(CaptchaUtil.validateCaptcha(captchaValue));
			return new ResponseEntity<CaptchaDTO>(captchaDto, HttpStatus.OK);
		
	}
	
//	@GetMapping("/createCaptcha")
//	@Timed
//	public ResponseEntity<CaptchaDTO> createCaptcha()
//	{
//		CaptchaDTO captcha=new CaptchaDTO();
//		String temp=CaptchaUtil.createImageCaptcha();
//		if(temp!=null)
//		{
//			captcha.setStringData(temp);
//			return new ResponseEntity<CaptchaDTO>(captcha, HttpStatus.OK);
//		}
//		else
//			return new ResponseEntity<CaptchaDTO>(HttpStatus.METHOD_FAILURE);
//	}
}
