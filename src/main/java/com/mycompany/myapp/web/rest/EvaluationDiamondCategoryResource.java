package com.mycompany.myapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.EvaluationDiamondCategory;
import com.mycompany.myapp.service.EvaluationDiamondCategoryService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing EvaluationDiamondCategory.
 */
@RestController
@RequestMapping("/api")
public class EvaluationDiamondCategoryResource {

    private final Logger log = LoggerFactory.getLogger(EvaluationDiamondCategoryResource.class);

    private static final String ENTITY_NAME = "evaluationDiamondCategory";

    private final EvaluationDiamondCategoryService evaluationDiamondCategoryService;

    public EvaluationDiamondCategoryResource(EvaluationDiamondCategoryService evaluationDiamondCategoryService) {
        this.evaluationDiamondCategoryService = evaluationDiamondCategoryService;
    }

    /**
     * POST  /evaluation-diamond-categories : Create a new evaluationDiamondCategory.
     *
     * @param evaluationDiamondCategory the evaluationDiamondCategory to create
     * @return the ResponseEntity with status 201 (Created) and with body the new evaluationDiamondCategory, or with status 400 (Bad Request) if the evaluationDiamondCategory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/evaluation-diamond-categories")
    @Timed
    public ResponseEntity<EvaluationDiamondCategory> createEvaluationDiamondCategory(@RequestBody EvaluationDiamondCategory evaluationDiamondCategory) throws URISyntaxException {
        log.debug("REST request to save EvaluationDiamondCategory : {}", evaluationDiamondCategory);
        if (evaluationDiamondCategory.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new evaluationDiamondCategory cannot already have an ID")).body(null);
        }
        EvaluationDiamondCategory result = evaluationDiamondCategoryService.save(evaluationDiamondCategory);
        return ResponseEntity.created(new URI("/api/evaluation-diamond-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /evaluation-diamond-categories : Updates an existing evaluationDiamondCategory.
     *
     * @param evaluationDiamondCategory the evaluationDiamondCategory to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated evaluationDiamondCategory,
     * or with status 400 (Bad Request) if the evaluationDiamondCategory is not valid,
     * or with status 500 (Internal Server Error) if the evaluationDiamondCategory couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/evaluation-diamond-categories")
    @Timed
    public ResponseEntity<EvaluationDiamondCategory> updateEvaluationDiamondCategory(@RequestBody EvaluationDiamondCategory evaluationDiamondCategory) throws URISyntaxException {
        log.debug("REST request to update EvaluationDiamondCategory : {}", evaluationDiamondCategory);
        if (evaluationDiamondCategory.getId() == null) {
            return createEvaluationDiamondCategory(evaluationDiamondCategory);
        }
        EvaluationDiamondCategory result = evaluationDiamondCategoryService.save(evaluationDiamondCategory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, evaluationDiamondCategory.getId().toString()))
            .body(result);
    }

    /**
     * GET  /evaluation-diamond-categories : get all the evaluationDiamondCategories.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of evaluationDiamondCategories in body
     */
    @GetMapping("/evaluation-diamond-categories")
    @Timed
    public ResponseEntity<List<EvaluationDiamondCategory>> getAllEvaluationDiamondCategories(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of EvaluationDiamondCategories");
        Page<EvaluationDiamondCategory> page = evaluationDiamondCategoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/evaluation-diamond-categories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /evaluation-diamond-categories/:id : get the "id" evaluationDiamondCategory.
     *
     * @param id the id of the evaluationDiamondCategory to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the evaluationDiamondCategory, or with status 404 (Not Found)
     */
    @GetMapping("/evaluation-diamond-categories/{id}")
    @Timed
    public ResponseEntity<EvaluationDiamondCategory> getEvaluationDiamondCategory(@PathVariable Long id) {
        log.debug("REST request to get EvaluationDiamondCategory : {}", id);
        EvaluationDiamondCategory evaluationDiamondCategory = evaluationDiamondCategoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(evaluationDiamondCategory));
    }

    /**
     * DELETE  /evaluation-diamond-categories/:id : delete the "id" evaluationDiamondCategory.
     *
     * @param id the id of the evaluationDiamondCategory to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/evaluation-diamond-categories/{id}")
    @Timed
    public ResponseEntity<Void> deleteEvaluationDiamondCategory(@PathVariable Long id) {
        log.debug("REST request to delete EvaluationDiamondCategory : {}", id);
        evaluationDiamondCategoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    
    @GetMapping("/evaluation-diamond-categories-by-eval-id/{evalId}")
    @Timed
    public ResponseEntity<List<EvaluationDiamondCategory>> getAllEvaluationDiamondCategoriesByEvalId(@PathVariable Long evalId) {
        log.debug("REST request to get a page of EvaluationDiamondCategories");
        List<Long> ids = new ArrayList<>();
        ids.add(evalId);
		List<EvaluationDiamondCategory> list = evaluationDiamondCategoryService.findAllByEvaluationIdIn(ids );
        return new ResponseEntity<>(list , HttpStatus.OK);
    }
}
