package com.mycompany.myapp.web.rest;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.sql.Blob;
import java.util.List;
import java.util.Optional;

import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.HelpFile;
import com.mycompany.myapp.service.HelpFileService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing HelpFile.
 */
@RestController
@RequestMapping("/api")
public class HelpFileResource {

    private final Logger log = LoggerFactory.getLogger(HelpFileResource.class);

    private static final String ENTITY_NAME = "helpFile";

    private final HelpFileService helpFileService;

    public HelpFileResource(HelpFileService helpFileService) {
        this.helpFileService = helpFileService;
    }

    /**
     * POST  /help-files : Create a new helpFile.
     *
     * @param helpFile the helpFile to create
     * @return the ResponseEntity with status 201 (Created) and with body the new helpFile, or with status 400 (Bad Request) if the helpFile has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/help-files")
    @Timed
    public ResponseEntity<HelpFile> createHelpFile(@RequestBody HelpFile helpFile) throws URISyntaxException {
        log.debug("REST request to save HelpFile : {}", helpFile);
        if (helpFile.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new helpFile cannot already have an ID")).body(null);
        }
        HelpFile result = helpFileService.save(helpFile);
        return ResponseEntity.created(new URI("/api/help-files/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /help-files : Updates an existing helpFile.
     *
     * @param helpFile the helpFile to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated helpFile,
     * or with status 400 (Bad Request) if the helpFile is not valid,
     * or with status 500 (Internal Server Error) if the helpFile couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/help-files")
    @Timed
    public ResponseEntity<HelpFile> updateHelpFile(@RequestBody HelpFile helpFile) throws URISyntaxException {
        log.debug("REST request to update HelpFile : {}", helpFile);
        if (helpFile.getId() == null) {
            return createHelpFile(helpFile);
        }
        HelpFile result = helpFileService.save(helpFile);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, helpFile.getId().toString()))
            .body(result);
    }

    /**
     * GET  /help-files : get all the helpFiles.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of helpFiles in body
     */
    @GetMapping("/help-files")
    @Timed
    public ResponseEntity<List<HelpFile>> getAllHelpFiles(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of HelpFiles");
        Page<HelpFile> page = helpFileService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/help-files");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /help-files/:id : get the "id" helpFile.
     *
     * @param id the id of the helpFile to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the helpFile, or with status 404 (Not Found)
     */
    @GetMapping("/help-files/{id}")
    @Timed
    public ResponseEntity<HelpFile> getHelpFile(@PathVariable Long id) {
        log.debug("REST request to get HelpFile : {}", id);
        HelpFile helpFile = helpFileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(helpFile));
    }

    /**
     * DELETE  /help-files/:id : delete the "id" helpFile.
     *
     * @param id the id of the helpFile to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/help-files/{id}")
    @Timed
    public ResponseEntity<Void> deleteHelpFile(@PathVariable Long id) {
        log.debug("REST request to delete HelpFile : {}", id);
        helpFileService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    
    
    
    @GetMapping("/download-help-files")
    @Timed
    public HttpServletResponse downloadFile(@RequestParam Long id,HttpServletRequest request ,HttpServletResponse httpServletResponse) throws Exception{
    	HelpFile helpFile = helpFileService.findOne(id);
    	HttpServletResponse response = httpServletResponse;
        InputStream in=new ByteArrayInputStream(helpFile.getFileBlob());
        String fileName=helpFile.getFileName();
        String agent = request.getHeader("USER-AGENT");
        if (agent != null && agent.indexOf("MSIE") != -1)
        {
          fileName = URLEncoder.encode(fileName, "UTF8");
          response.setContentType("application/pdf");
          response.setHeader("Content-Disposition","attachment;filename=" + fileName);
        }
        else if ( agent != null && agent.indexOf("Mozilla") != -1)
        {
          response.setCharacterEncoding("UTF-8");
          fileName = MimeUtility.encodeText(fileName, "UTF8", "B");
         // response.setContentType("application/force-download");
          response.setContentType("application/pdf");
          response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        }


        BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream());
        byte by[] = new byte[32768];
        int index = in.read(by, 0, 32768);
        while (index != -1) {
            out.write(by, 0, index);
            index = in.read(by, 0, 32768);
        }
        out.flush();

        return response;
}
    
}
