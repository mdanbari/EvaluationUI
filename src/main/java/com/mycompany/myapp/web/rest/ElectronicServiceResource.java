package com.mycompany.myapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.Department;
import com.mycompany.myapp.domain.ElectronicService;
import com.mycompany.myapp.service.DepartmentService;
import com.mycompany.myapp.service.ElectronicServiceService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing ElectronicService.
 */
@RestController
@RequestMapping("/api")
public class ElectronicServiceResource {

	private final Logger log = LoggerFactory.getLogger(ElectronicServiceResource.class);

	private static final String ENTITY_NAME = "electronicService";

	private final ElectronicServiceService electronicServiceService;
	private final  DepartmentService departmentService;

	public ElectronicServiceResource(ElectronicServiceService electronicServiceService,DepartmentService departmentService) {
		this.electronicServiceService = electronicServiceService;
		this.departmentService = departmentService;
	}

	/**
	 * POST /electronic-services : Create a new electronicService.
	 *
	 * @param electronicService
	 *            the electronicService to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new electronicService, or with status 400 (Bad Request) if the
	 *         electronicService has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/electronic-services")
	@Timed
	public ResponseEntity<ElectronicService> createElectronicService(
			@Valid @RequestBody ElectronicService electronicService) throws URISyntaxException {
		log.debug("REST request to save ElectronicService : {}", electronicService);
		if (electronicService.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new electronicService cannot already have an ID")).body(null);
		}
		Department department = electronicService.getDepartment();
		Integer electronicServiceCount = department.getElectronicServiceCount();
		department.setElectronicServiceCount(electronicServiceCount == null ? 1 : electronicServiceCount + 1);
		departmentService.save(department);
		ElectronicService result = electronicServiceService.save(electronicService);
		return ResponseEntity.created(new URI("/api/electronic-services/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /electronic-services : Updates an existing electronicService.
	 *
	 * @param electronicService
	 *            the electronicService to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         electronicService, or with status 400 (Bad Request) if the
	 *         electronicService is not valid, or with status 500 (Internal
	 *         Server Error) if the electronicService couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/electronic-services")
	@Timed
	public ResponseEntity<ElectronicService> updateElectronicService(
			@Valid @RequestBody ElectronicService electronicService) throws URISyntaxException {
		log.debug("REST request to update ElectronicService : {}", electronicService);
		if (electronicService.getId() == null) {
			return createElectronicService(electronicService);
		}
		ElectronicService result = electronicServiceService.save(electronicService);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, electronicService.getId().toString()))
				.body(result);
	}

	/**
	 * GET /electronic-services : get all the electronicServices.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         electronicServices in body
	 */
	@GetMapping("/electronic-services")
	@Timed
	public ResponseEntity<List<ElectronicService>> getAllElectronicServices(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of ElectronicServices");
		Page<ElectronicService> page = electronicServiceService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/electronic-services");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /electronic-services/:id : get the "id" electronicService.
	 *
	 * @param id
	 *            the id of the electronicService to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         electronicService, or with status 404 (Not Found)
	 */
	@GetMapping("/electronic-services/{id}")
	@Timed
	public ResponseEntity<ElectronicService> getElectronicService(@PathVariable Long id) {
		log.debug("REST request to get ElectronicService : {}", id);
		ElectronicService electronicService = electronicServiceService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(electronicService));
	}

	/**
	 * DELETE /electronic-services/:id : delete the "id" electronicService.
	 *
	 * @param id
	 *            the id of the electronicService to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/electronic-services/{id}")
	@Timed
	public ResponseEntity<Void> deleteElectronicService(@PathVariable Long id) {
		log.debug("REST request to delete ElectronicService : {}", id);
		electronicServiceService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * Get All ElecService by Projection id and title
	 * 
	 * @return
	 */
	@GetMapping("/customGetAllElectronicService")
	@Timed
	public ResponseEntity<List<ElectronicService>> customGetAllElectronicService() {
		log.debug("REST request to get all data by projection : {}");
		return new ResponseEntity<>(electronicServiceService.customFindAll(), HttpStatus.OK);
	}

	/**
	 * 
	 * get all elecService of a department
	 * 
	 * @return
	 */
	@GetMapping("/findAllElecServiceOfDepartment/{id}")
	@Timed
	public ResponseEntity<List<ElectronicService>> findAllElecServiceOfDepartment(@PathVariable Long id) {
		log.debug("REST request to get all elecserivce of a department : {}");
		return new ResponseEntity<>(electronicServiceService.findAllElecServiceOfDepartment(id), HttpStatus.OK);
	}
	
	@GetMapping("elecService/search/{search}/{local}")
	@Timed
	public ResponseEntity<List<ElectronicService>> search(@PathVariable String search, @PathVariable String local,
			@ApiParam Pageable pageable) {
		log.debug("REST request to search a page of ElectronicService");
		Page<ElectronicService> page = electronicServiceService.search(search, local, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "api/elecService/search/");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
}
