package com.mycompany.myapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.SystemProfile;
import com.mycompany.myapp.service.SystemProfileService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing SystemProfile.
 */
@RestController
@RequestMapping("/api")
public class SystemProfileResource {

    private final Logger log = LoggerFactory.getLogger(SystemProfileResource.class);

    private static final String ENTITY_NAME = "systemProfile";

    private final SystemProfileService systemProfileService;

    public SystemProfileResource(SystemProfileService systemProfileService) {
        this.systemProfileService = systemProfileService;
    }

    /**
     * POST  /system-profiles : Create a new systemProfile.
     *
     * @param systemProfile the systemProfile to create
     * @return the ResponseEntity with status 201 (Created) and with body the new systemProfile, or with status 400 (Bad Request) if the systemProfile has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/system-profiles")
    @Timed
    public ResponseEntity<SystemProfile> createSystemProfile(@RequestBody SystemProfile systemProfile) throws URISyntaxException {
        log.debug("REST request to save SystemProfile : {}", systemProfile);
        if (systemProfile.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new systemProfile cannot already have an ID")).body(null);
        }
        SystemProfile result = systemProfileService.save(systemProfile);
        return ResponseEntity.created(new URI("/api/system-profiles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /system-profiles : Updates an existing systemProfile.
     *
     * @param systemProfile the systemProfile to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated systemProfile,
     * or with status 400 (Bad Request) if the systemProfile is not valid,
     * or with status 500 (Internal Server Error) if the systemProfile couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/system-profiles")
    @Timed
    public ResponseEntity<SystemProfile> updateSystemProfile(@RequestBody SystemProfile systemProfile) throws URISyntaxException {
        log.debug("REST request to update SystemProfile : {}", systemProfile);
        if (systemProfile.getId() == null) {
            return createSystemProfile(systemProfile);
        }
        SystemProfile result = systemProfileService.save(systemProfile);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, systemProfile.getId().toString()))
            .body(result);
    }

    /**
     * GET  /system-profiles : get all the systemProfiles.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of systemProfiles in body
     */
    @GetMapping("/system-profiles")
    @Timed
    public ResponseEntity<List<SystemProfile>> getAllSystemProfiles(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of SystemProfiles");
        Page<SystemProfile> page = systemProfileService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/system-profiles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /system-profiles/:id : get the "id" systemProfile.
     *
     * @param id the id of the systemProfile to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the systemProfile, or with status 404 (Not Found)
     */
    @GetMapping("/system-profiles/{id}")
    @Timed
    public ResponseEntity<SystemProfile> getSystemProfile(@PathVariable Long id) {
        log.debug("REST request to get SystemProfile : {}", id);
        SystemProfile systemProfile = systemProfileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(systemProfile));
    }

    /**
     * DELETE  /system-profiles/:id : delete the "id" systemProfile.
     *
     * @param id the id of the systemProfile to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/system-profiles/{id}")
    @Timed
    public ResponseEntity<Void> deleteSystemProfile(@PathVariable Long id) {
        log.debug("REST request to delete SystemProfile : {}", id);
        systemProfileService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
