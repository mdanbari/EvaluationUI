package com.mycompany.myapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.EvaluationMaturityState;
import com.mycompany.myapp.service.EvaluationMaturityStateService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing EvaluationMaturityState.
 */
@RestController
@RequestMapping("/api")
public class EvaluationMaturityStateResource {

	private final Logger log = LoggerFactory.getLogger(EvaluationMaturityStateResource.class);

	private static final String ENTITY_NAME = "evaluationMaturityState";

	private final EvaluationMaturityStateService evaluationMaturityStateService;

	public EvaluationMaturityStateResource(EvaluationMaturityStateService evaluationMaturityStateService) {
		this.evaluationMaturityStateService = evaluationMaturityStateService;
	}

	/**
	 * POST /evaluation-maturity-states : Create a new evaluationMaturityState.
	 *
	 * @param evaluationMaturityState
	 *            the evaluationMaturityState to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new evaluationMaturityState, or with status 400 (Bad Request) if
	 *         the evaluationMaturityState has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/evaluation-maturity-states")
	@Timed
	public ResponseEntity<EvaluationMaturityState> createEvaluationMaturityState(
			@RequestBody EvaluationMaturityState evaluationMaturityState) throws URISyntaxException {
		log.debug("REST request to save EvaluationMaturityState : {}", evaluationMaturityState);
		if (evaluationMaturityState.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new evaluationMaturityState cannot already have an ID")).body(null);
		}
		EvaluationMaturityState result = evaluationMaturityStateService.save(evaluationMaturityState);
		return ResponseEntity.created(new URI("/api/evaluation-maturity-states/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /evaluation-maturity-states : Updates an existing
	 * evaluationMaturityState.
	 *
	 * @param evaluationMaturityState
	 *            the evaluationMaturityState to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         evaluationMaturityState, or with status 400 (Bad Request) if the
	 *         evaluationMaturityState is not valid, or with status 500
	 *         (Internal Server Error) if the evaluationMaturityState couldn't
	 *         be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/evaluation-maturity-states")
	@Timed
	public ResponseEntity<EvaluationMaturityState> updateEvaluationMaturityState(
			@RequestBody EvaluationMaturityState evaluationMaturityState) throws URISyntaxException {
		log.debug("REST request to update EvaluationMaturityState : {}", evaluationMaturityState);
		if (evaluationMaturityState.getId() == null) {
			return createEvaluationMaturityState(evaluationMaturityState);
		}
		EvaluationMaturityState result = evaluationMaturityStateService.save(evaluationMaturityState);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, evaluationMaturityState.getId().toString()))
				.body(result);
	}

	/**
	 * GET /evaluation-maturity-states : get all the evaluationMaturityStates.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         evaluationMaturityStates in body
	 */
	@GetMapping("/evaluation-maturity-states")
	@Timed
	public ResponseEntity<List<EvaluationMaturityState>> getAllEvaluationMaturityStates(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of EvaluationMaturityStates");
		Page<EvaluationMaturityState> page = evaluationMaturityStateService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/evaluation-maturity-states");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /evaluation-maturity-states/:id : get the "id"
	 * evaluationMaturityState.
	 *
	 * @param id
	 *            the id of the evaluationMaturityState to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         evaluationMaturityState, or with status 404 (Not Found)
	 */
	@GetMapping("/evaluation-maturity-states/{id}")
	@Timed
	public ResponseEntity<EvaluationMaturityState> getEvaluationMaturityState(@PathVariable Long id) {
		log.debug("REST request to get EvaluationMaturityState : {}", id);
		EvaluationMaturityState evaluationMaturityState = evaluationMaturityStateService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(evaluationMaturityState));
	}

	/**
	 * DELETE /evaluation-maturity-states/:id : delete the "id"
	 * evaluationMaturityState.
	 *
	 * @param id
	 *            the id of the evaluationMaturityState to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/evaluation-maturity-states/{id}")
	@Timed
	public ResponseEntity<Void> deleteEvaluationMaturityState(@PathVariable Long id) {
		log.debug("REST request to delete EvaluationMaturityState : {}", id);
		evaluationMaturityStateService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	@GetMapping("/evaluation-maturity-states-by-eval-id/{evalId}")
	@Timed
	public ResponseEntity<List<EvaluationMaturityState>> getAllEvaluationMaturityStatesByEvaluationId(
			@PathVariable Long evalId) {
		log.debug("REST request to get a page of EvaluationMaturityStates");

		ArrayList<Long> ids = new ArrayList<Long>();
		ids.add(evalId);
		List<EvaluationMaturityState> list = evaluationMaturityStateService.findAllByEvaluationIdIn(ids);
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

}
