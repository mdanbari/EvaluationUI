package com.mycompany.myapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.DiamondCategory;
import com.mycompany.myapp.service.DiamondCategoryService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing DiamondCategory.
 */
@RestController
@RequestMapping("/api")
public class DiamondCategoryResource {

    private final Logger log = LoggerFactory.getLogger(DiamondCategoryResource.class);

    private static final String ENTITY_NAME = "diamondCategory";

    private final DiamondCategoryService diamondCategoryService;

    public DiamondCategoryResource(DiamondCategoryService diamondCategoryService) {
        this.diamondCategoryService = diamondCategoryService;
    }

    /**
     * POST  /diamond-categories : Create a new diamondCategory.
     *
     * @param diamondCategory the diamondCategory to create
     * @return the ResponseEntity with status 201 (Created) and with body the new diamondCategory, or with status 400 (Bad Request) if the diamondCategory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/diamond-categories")
    @Timed
    public ResponseEntity<DiamondCategory> createDiamondCategory(@Valid @RequestBody DiamondCategory diamondCategory) throws URISyntaxException {
        log.debug("REST request to save DiamondCategory : {}", diamondCategory);
        if (diamondCategory.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new diamondCategory cannot already have an ID")).body(null);
        }
        DiamondCategory result = diamondCategoryService.save(diamondCategory);
        return ResponseEntity.created(new URI("/api/diamond-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /diamond-categories : Updates an existing diamondCategory.
     *
     * @param diamondCategory the diamondCategory to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated diamondCategory,
     * or with status 400 (Bad Request) if the diamondCategory is not valid,
     * or with status 500 (Internal Server Error) if the diamondCategory couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/diamond-categories")
    @Timed
    public ResponseEntity<DiamondCategory> updateDiamondCategory(@Valid @RequestBody DiamondCategory diamondCategory) throws URISyntaxException {
        log.debug("REST request to update DiamondCategory : {}", diamondCategory);
        if (diamondCategory.getId() == null) {
            return createDiamondCategory(diamondCategory);
        }
        DiamondCategory result = diamondCategoryService.save(diamondCategory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, diamondCategory.getId().toString()))
            .body(result);
    }

    /**
     * GET  /diamond-categories : get all the diamondCategories.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of diamondCategories in body
     */
    @GetMapping("/diamond-categories")
    @Timed
    public ResponseEntity<List<DiamondCategory>> getAllDiamondCategories(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of DiamondCategories");
        Page<DiamondCategory> page = diamondCategoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/diamond-categories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /diamond-categories/:id : get the "id" diamondCategory.
     *
     * @param id the id of the diamondCategory to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the diamondCategory, or with status 404 (Not Found)
     */
    @GetMapping("/diamond-categories/{id}")
    @Timed
    public ResponseEntity<DiamondCategory> getDiamondCategory(@PathVariable Long id) {
        log.debug("REST request to get DiamondCategory : {}", id);
        DiamondCategory diamondCategory = diamondCategoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(diamondCategory));
    }

    /**
     * DELETE  /diamond-categories/:id : delete the "id" diamondCategory.
     *
     * @param id the id of the diamondCategory to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/diamond-categories/{id}")
    @Timed
    public ResponseEntity<Void> deleteDiamondCategory(@PathVariable Long id) {
        log.debug("REST request to delete DiamondCategory : {}", id);
        diamondCategoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
