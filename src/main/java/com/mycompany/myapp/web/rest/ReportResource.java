package com.mycompany.myapp.web.rest;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.ElecServiceDiamondCategoryService;
import com.mycompany.myapp.service.ElecServiceMaturityStateService;
import com.mycompany.myapp.service.ElecServiceQuestionService;
import com.mycompany.myapp.service.dto.ConsolidatedChartReportResultDTO;
import com.mycompany.myapp.service.dto.ConsolidatedReportInputDTO;
import com.mycompany.myapp.service.dto.DepartmentQuestionChartReportResultDTO;
import com.mycompany.myapp.service.dto.DepartmentQuestionReportInputDTO;
import com.mycompany.myapp.service.dto.DepartmentReportInputDTO;
import com.mycompany.myapp.service.dto.DepartmentReportResultDTO;
import com.mycompany.myapp.service.dto.ElecServiceQuestionChartReportResultDTO;
import com.mycompany.myapp.service.dto.ElecServiceQuestionReportInputDTO;
import com.mycompany.myapp.service.dto.SeparationChartReportResultDTO;
import com.mycompany.myapp.service.dto.SeparationReportInputDTO;

/**
 * Controller for view and managing Log Level at runtime.
 */
@RestController
@RequestMapping("/api")
public class ReportResource {

	private static final String UTF_8 = "UTF-8";
	private static final String APPLICATION_CSV_CHARSET = "application/csv;charset=UTF-8";
	private static final String DEPARTMENT_NAME = "نام سازمان";
	private static final String SERVICE_NAME = "نام خدمت الکترونیکی";
	private final ElecServiceMaturityStateService elecServiceMaturityStateService;
	private final ElecServiceDiamondCategoryService elecServiceDiamondCategoryService;
	private final ElecServiceQuestionService elecServiceQuestionService;

	

	public ReportResource(ElecServiceMaturityStateService elecServiceMaturityStateService,
			ElecServiceDiamondCategoryService elecServiceDiamondCategoryService,
			ElecServiceQuestionService elecServiceQuestionService) {
		this.elecServiceMaturityStateService = elecServiceMaturityStateService;
		this.elecServiceDiamondCategoryService = elecServiceDiamondCategoryService;
		this.elecServiceQuestionService = elecServiceQuestionService;
	}

	@SuppressWarnings("resource")
	@PostMapping("/separation-report")
	@Timed
	public SeparationChartReportResultDTO getSeperationReport(@RequestBody SeparationReportInputDTO separationReportInputDTO,HttpServletResponse response) throws Exception {
		
		String typeModel = separationReportInputDTO.getTypeModel();	
		Boolean isDownload = separationReportInputDTO.getIsDownload();	
		if("maturity".equals(typeModel)) {
			SeparationChartReportResultDTO generateElecServiceSeprationMaturityReport = elecServiceMaturityStateService.generateElecServiceSeprationMaturityReport(separationReportInputDTO);
			List<Double> data = generateElecServiceSeprationMaturityReport.getData();
			List<String> label = generateElecServiceSeprationMaturityReport.getLabel();
			if (isDownload != null && isDownload) {
				response.setContentType(APPLICATION_CSV_CHARSET);
				response.setCharacterEncoding(UTF_8);
				//PrintWriter writer = response.getWriter();				
	
				PrintWriter writer = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), "UTF8"));	
				
				writer = generateSeprationReportFile(data, label, writer,typeModel);
				writer.flush();
				writer.close();		

			}
			else
				return generateElecServiceSeprationMaturityReport;
		} else {
			SeparationChartReportResultDTO generateElecServiceSeprationDiamondReport = elecServiceDiamondCategoryService.generateElecServiceSeprationDiamondReport(separationReportInputDTO);
			List<Double> data = generateElecServiceSeprationDiamondReport.getData();
			List<String> label = generateElecServiceSeprationDiamondReport.getLabel();
			if(isDownload != null && isDownload)
			{
				response.setContentType(APPLICATION_CSV_CHARSET);
				response.setCharacterEncoding(UTF_8);
				//PrintWriter writer = response.getWriter();
				PrintWriter writer = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), "UTF8"));
				writer = generateSeprationReportFile(data, label, writer,typeModel);
				writer.flush();
				writer.close();
			}
			else
				return generateElecServiceSeprationDiamondReport;
		}
		return null;		


	}

	private PrintWriter  generateSeprationReportFile(List<Double> data, List<String> label, PrintWriter writer,String typeModel)
			throws IOException {
		writer.print("maturity".equals(typeModel) ? "مدل بلوغ" : "مدل الماسی");
		writer.print(',');
		writer.println("نمره");
		for (int j = 0; j < label.size(); j++) {
			writer.print(label.get(j));
			writer.print(',');
			writer.print(data.get(j));
			writer.println();
		}		
		return writer;
	}

	@SuppressWarnings("resource")
	@PostMapping("/consolidated-report")
	@Timed
	public ConsolidatedChartReportResultDTO getConsolidatedReport(
			@RequestBody ConsolidatedReportInputDTO consolidatedReportInputDTO, HttpServletResponse response)
			throws Exception {

		String typeModel = consolidatedReportInputDTO.getTypeModel();
		Boolean isDownload = consolidatedReportInputDTO.getIsDownload();	
		if ("maturity".equals(typeModel)) {
			ConsolidatedChartReportResultDTO generateElecServiceConsolatedMaturityReport = elecServiceMaturityStateService.generateElecServiceConsolatedMaturityReport(consolidatedReportInputDTO);
			List<List<Double>> data = generateElecServiceConsolatedMaturityReport.getData();
			List<String> label = generateElecServiceConsolatedMaturityReport.getLabel();
			List<String> series = generateElecServiceConsolatedMaturityReport.getSeries();
			if (isDownload != null && isDownload) {
				response.setContentType(APPLICATION_CSV_CHARSET);
				response.setCharacterEncoding(UTF_8);
				//PrintWriter writer = response.getWriter();
				PrintWriter writer = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), "UTF8"));
				writer = generateConsolidatedReportFile(data, label,series, writer,SERVICE_NAME);
				writer.flush();
				writer.close();		

			}else
				return generateElecServiceConsolatedMaturityReport;
		} else {
			ConsolidatedChartReportResultDTO generateElecServiceConsolatedDiamondReport = elecServiceDiamondCategoryService.generateElecServiceConsolatedDiamondReport(consolidatedReportInputDTO);
			List<List<Double>> data = generateElecServiceConsolatedDiamondReport.getData();
			List<String> label = generateElecServiceConsolatedDiamondReport.getLabel();
			List<String> series = generateElecServiceConsolatedDiamondReport.getSeries();
			if (isDownload != null && isDownload) {
				response.setContentType(APPLICATION_CSV_CHARSET);
				response.setCharacterEncoding(UTF_8);
				//PrintWriter writer = response.getWriter();
				PrintWriter writer = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), "UTF8"));
				writer = generateConsolidatedReportFile(data, label,series, writer,SERVICE_NAME);
				writer.flush();
				writer.close();		

			}else
				return generateElecServiceConsolatedDiamondReport;
		}
		return null;
	}
	
	@SuppressWarnings("resource")
	@PostMapping("/department-report")
	@Timed
	public DepartmentReportResultDTO getDepartmentComparisonReport(
			@RequestBody DepartmentReportInputDTO departmentReportInputDTO, HttpServletResponse response)
			throws Exception {

		String typeModel = departmentReportInputDTO.getTypeModel();
		Boolean isDownload = departmentReportInputDTO.getIsDownload();	
		if ("maturity".equals(typeModel)) {
			DepartmentReportResultDTO generateElecServiceConsolatedMaturityReport = elecServiceMaturityStateService.generateElecServiceDepartmentMaturityReport(departmentReportInputDTO);
			List<List<Double>> data = generateElecServiceConsolatedMaturityReport.getData();
			List<String> label = generateElecServiceConsolatedMaturityReport.getLabel();
			List<String> series = generateElecServiceConsolatedMaturityReport.getSeries();
			if (isDownload != null && isDownload) {
				response.setContentType(APPLICATION_CSV_CHARSET);
				response.setCharacterEncoding(UTF_8);
				//PrintWriter writer = response.getWriter();
				PrintWriter writer = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), "UTF8"));
				writer = generateConsolidatedReportFile(data, label,series, writer,DEPARTMENT_NAME);
				writer.flush();
				writer.close();		

			}else
				return generateElecServiceConsolatedMaturityReport;
		} else {
			DepartmentReportResultDTO generateElecServiceConsolatedDiamondReport = elecServiceDiamondCategoryService.generateElecServiceDepartmentDiamondReport(departmentReportInputDTO);
			List<List<Double>> data = generateElecServiceConsolatedDiamondReport.getData();
			List<String> label = generateElecServiceConsolatedDiamondReport.getLabel();
			List<String> series = generateElecServiceConsolatedDiamondReport.getSeries();
			if (isDownload != null && isDownload) {
				response.setContentType(APPLICATION_CSV_CHARSET);
				response.setCharacterEncoding(UTF_8);
				//PrintWriter writer = response.getWriter();
				PrintWriter writer = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), "UTF8"));
				writer = generateConsolidatedReportFile(data, label,series, writer,DEPARTMENT_NAME);
				writer.flush();
				writer.close();		

			}else
				return generateElecServiceConsolatedDiamondReport;
		}
		return null;
	}
	
	
	
	@SuppressWarnings("resource")
	@PostMapping("/elec-question-report")
	@Timed
	public ElecServiceQuestionChartReportResultDTO getElecServiceQuestionReport(
			@RequestBody ElecServiceQuestionReportInputDTO elecServiceQuestionReportInputDTO, HttpServletResponse response)
			throws Exception {

			Boolean isDownload = elecServiceQuestionReportInputDTO.getIsDownload();	
			ElecServiceQuestionChartReportResultDTO generateElecServiceConsolatedMaturityReport = elecServiceQuestionService.generateElecServiceQuestionReport(elecServiceQuestionReportInputDTO);
			List<List<Double>> data = generateElecServiceConsolatedMaturityReport.getData();
			List<String> label = generateElecServiceConsolatedMaturityReport.getLabel();
			List<String> series = generateElecServiceConsolatedMaturityReport.getSeries();
			if (isDownload != null && isDownload) {
				response.setContentType(APPLICATION_CSV_CHARSET);
				response.setCharacterEncoding(UTF_8);
				//PrintWriter writer = response.getWriter();
				PrintWriter writer = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), "UTF8"));
				writer = generateConsolidatedReportFile(data, label,series, writer,SERVICE_NAME);
				writer.flush();
				writer.close();		

			}else
				return generateElecServiceConsolatedMaturityReport;
		
		return null;
	}
	
	@SuppressWarnings("resource")
	@PostMapping("/dep-question-report")
	@Timed
	public DepartmentQuestionChartReportResultDTO getDepartmentQuestionReport(
			@RequestBody DepartmentQuestionReportInputDTO departmentQuestionReportInputDTO, HttpServletResponse response)
			throws Exception {

			Boolean isDownload = departmentQuestionReportInputDTO.getIsDownload();	
			DepartmentQuestionChartReportResultDTO generateDepartmentQuestionReport = elecServiceQuestionService.generateDepartmentQuestionReport(departmentQuestionReportInputDTO);
			List<List<Double>> data = generateDepartmentQuestionReport.getData();
			List<String> label = generateDepartmentQuestionReport.getLabel();
			List<String> series = generateDepartmentQuestionReport.getSeries();
			if (isDownload != null && isDownload) {
				response.setContentType(APPLICATION_CSV_CHARSET);
				response.setCharacterEncoding(UTF_8);
				//PrintWriter writer = response.getWriter();
				PrintWriter writer = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), "UTF8"));
				writer = generateConsolidatedReportFile(data, label,series, writer,DEPARTMENT_NAME);
				writer.flush();
				writer.close();		

			}else
				return generateDepartmentQuestionReport;
		
		return null;
	}

	private PrintWriter generateConsolidatedReportFile(List<List<Double>> data, List<String> label,List<String> series, PrintWriter writer,String name) {
		writer.print(name);					
		for (int j = 0; j < label.size(); j++) {
			writer.print(',');
			writer.print(label.get(j));
		}
		writer.println();
		for (int i = 0; i < series.size(); i++) {
			writer.print(series.get(i));
			for (int j = 0; j < data.get(i).size(); j++) {
				writer.print(',');
				writer.print(data.get(i).get(j));
			}
			writer.println();
		}	
		return writer;
	}

}
