package com.mycompany.myapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.EvaluationArchive;
import com.mycompany.myapp.service.EvaluationArchiveService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing EvaluationArchive.
 */
@RestController
@RequestMapping("/api")
public class EvaluationArchiveResource {

    private final Logger log = LoggerFactory.getLogger(EvaluationArchiveResource.class);

    private static final String ENTITY_NAME = "evaluationArchive";

    private final EvaluationArchiveService evaluationArchiveService;

    public EvaluationArchiveResource(EvaluationArchiveService evaluationArchiveService) {
        this.evaluationArchiveService = evaluationArchiveService;
    }

    /**
     * POST  /evaluation-archives : Create a new evaluationArchive.
     *
     * @param evaluationArchive the evaluationArchive to create
     * @return the ResponseEntity with status 201 (Created) and with body the new evaluationArchive, or with status 400 (Bad Request) if the evaluationArchive has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/evaluation-archives")
    @Timed
    public ResponseEntity<EvaluationArchive> createEvaluationArchive(@RequestBody EvaluationArchive evaluationArchive) throws URISyntaxException {
        log.debug("REST request to save EvaluationArchive : {}", evaluationArchive);
        if (evaluationArchive.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new evaluationArchive cannot already have an ID")).body(null);
        }
        EvaluationArchive result = evaluationArchiveService.save(evaluationArchive);
        return ResponseEntity.created(new URI("/api/evaluation-archives/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /evaluation-archives : Updates an existing evaluationArchive.
     *
     * @param evaluationArchive the evaluationArchive to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated evaluationArchive,
     * or with status 400 (Bad Request) if the evaluationArchive is not valid,
     * or with status 500 (Internal Server Error) if the evaluationArchive couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/evaluation-archives")
    @Timed
    public ResponseEntity<EvaluationArchive> updateEvaluationArchive(@RequestBody EvaluationArchive evaluationArchive) throws URISyntaxException {
        log.debug("REST request to update EvaluationArchive : {}", evaluationArchive);
        if (evaluationArchive.getId() == null) {
            return createEvaluationArchive(evaluationArchive);
        }
        EvaluationArchive result = evaluationArchiveService.save(evaluationArchive);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, evaluationArchive.getId().toString()))
            .body(result);
    }

    /**
     * GET  /evaluation-archives : get all the evaluationArchives.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of evaluationArchives in body
     */
    @GetMapping("/evaluation-archives")
    @Timed
    public ResponseEntity<List<EvaluationArchive>> getAllEvaluationArchives(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of EvaluationArchives");
        Page<EvaluationArchive> page = evaluationArchiveService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/evaluation-archives");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /evaluation-archives/:id : get the "id" evaluationArchive.
     *
     * @param id the id of the evaluationArchive to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the evaluationArchive, or with status 404 (Not Found)
     */
    @GetMapping("/evaluation-archives/{id}")
    @Timed
    public ResponseEntity<EvaluationArchive> getEvaluationArchive(@PathVariable Long id) {
        log.debug("REST request to get EvaluationArchive : {}", id);
        EvaluationArchive evaluationArchive = evaluationArchiveService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(evaluationArchive));
    }

    /**
     * DELETE  /evaluation-archives/:id : delete the "id" evaluationArchive.
     *
     * @param id the id of the evaluationArchive to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/evaluation-archives/{id}")
    @Timed
    public ResponseEntity<Void> deleteEvaluationArchive(@PathVariable Long id) {
        log.debug("REST request to delete EvaluationArchive : {}", id);
        evaluationArchiveService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
