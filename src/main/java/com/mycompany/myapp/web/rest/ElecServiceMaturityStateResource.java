package com.mycompany.myapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.ElecServiceMaturityState;
import com.mycompany.myapp.service.ElecServiceMaturityStateService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing ElecServiceMaturityState.
 */
@RestController
@RequestMapping("/api")
public class ElecServiceMaturityStateResource {

    private final Logger log = LoggerFactory.getLogger(ElecServiceMaturityStateResource.class);

    private static final String ENTITY_NAME = "elecServiceMaturityState";

    private final ElecServiceMaturityStateService elecServiceMaturityStateService;

    public ElecServiceMaturityStateResource(ElecServiceMaturityStateService elecServiceMaturityStateService) {
        this.elecServiceMaturityStateService = elecServiceMaturityStateService;
    }

    /**
     * POST  /elec-service-maturity-states : Create a new elecServiceMaturityState.
     *
     * @param elecServiceMaturityState the elecServiceMaturityState to create
     * @return the ResponseEntity with status 201 (Created) and with body the new elecServiceMaturityState, or with status 400 (Bad Request) if the elecServiceMaturityState has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/elec-service-maturity-states")
    @Timed
    public ResponseEntity<ElecServiceMaturityState> createElecServiceMaturityState(@RequestBody ElecServiceMaturityState elecServiceMaturityState) throws URISyntaxException {
        log.debug("REST request to save ElecServiceMaturityState : {}", elecServiceMaturityState);
        if (elecServiceMaturityState.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new elecServiceMaturityState cannot already have an ID")).body(null);
        }
        ElecServiceMaturityState result = elecServiceMaturityStateService.save(elecServiceMaturityState);
        return ResponseEntity.created(new URI("/api/elec-service-maturity-states/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /elec-service-maturity-states : Updates an existing elecServiceMaturityState.
     *
     * @param elecServiceMaturityState the elecServiceMaturityState to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated elecServiceMaturityState,
     * or with status 400 (Bad Request) if the elecServiceMaturityState is not valid,
     * or with status 500 (Internal Server Error) if the elecServiceMaturityState couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/elec-service-maturity-states")
    @Timed
    public ResponseEntity<ElecServiceMaturityState> updateElecServiceMaturityState(@RequestBody ElecServiceMaturityState elecServiceMaturityState) throws URISyntaxException {
        log.debug("REST request to update ElecServiceMaturityState : {}", elecServiceMaturityState);
        if (elecServiceMaturityState.getId() == null) {
            return createElecServiceMaturityState(elecServiceMaturityState);
        }
        ElecServiceMaturityState result = elecServiceMaturityStateService.save(elecServiceMaturityState);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, elecServiceMaturityState.getId().toString()))
            .body(result);
    }

    /**
     * GET  /elec-service-maturity-states : get all the elecServiceMaturityStates.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of elecServiceMaturityStates in body
     */
    @GetMapping("/elec-service-maturity-states")
    @Timed
    public ResponseEntity<List<ElecServiceMaturityState>> getAllElecServiceMaturityStates(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ElecServiceMaturityStates");
        Page<ElecServiceMaturityState> page = elecServiceMaturityStateService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/elec-service-maturity-states");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /elec-service-maturity-states/:id : get the "id" elecServiceMaturityState.
     *
     * @param id the id of the elecServiceMaturityState to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the elecServiceMaturityState, or with status 404 (Not Found)
     */
    @GetMapping("/elec-service-maturity-states/{id}")
    @Timed
    public ResponseEntity<ElecServiceMaturityState> getElecServiceMaturityState(@PathVariable Long id) {
        log.debug("REST request to get ElecServiceMaturityState : {}", id);
        ElecServiceMaturityState elecServiceMaturityState = elecServiceMaturityStateService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(elecServiceMaturityState));
    }

    /**
     * DELETE  /elec-service-maturity-states/:id : delete the "id" elecServiceMaturityState.
     *
     * @param id the id of the elecServiceMaturityState to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/elec-service-maturity-states/{id}")
    @Timed
    public ResponseEntity<Void> deleteElecServiceMaturityState(@PathVariable Long id) {
        log.debug("REST request to delete ElecServiceMaturityState : {}", id);
        elecServiceMaturityStateService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
