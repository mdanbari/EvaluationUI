package com.mycompany.myapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.QuestionAnswer;
import com.mycompany.myapp.service.QuestionAnswerService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing QuestionAnswer.
 */
@RestController
@RequestMapping("/api")
public class QuestionAnswerResource {

    private final Logger log = LoggerFactory.getLogger(QuestionAnswerResource.class);

    private static final String ENTITY_NAME = "questionAnswer";

    private final QuestionAnswerService questionAnswerService;

    public QuestionAnswerResource(QuestionAnswerService questionAnswerService) {
        this.questionAnswerService = questionAnswerService;
    }

    /**
     * POST  /question-answers : Create a new questionAnswer.
     *
     * @param questionAnswer the questionAnswer to create
     * @return the ResponseEntity with status 201 (Created) and with body the new questionAnswer, or with status 400 (Bad Request) if the questionAnswer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/question-answers")
    @Timed
    public ResponseEntity<QuestionAnswer> createQuestionAnswer(@RequestBody QuestionAnswer questionAnswer) throws URISyntaxException {
        log.debug("REST request to save QuestionAnswer : {}", questionAnswer);
        if (questionAnswer.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new questionAnswer cannot already have an ID")).body(null);
        }
        QuestionAnswer result = questionAnswerService.save(questionAnswer);
        return ResponseEntity.created(new URI("/api/question-answers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /question-answers : Updates an existing questionAnswer.
     *
     * @param questionAnswer the questionAnswer to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated questionAnswer,
     * or with status 400 (Bad Request) if the questionAnswer is not valid,
     * or with status 500 (Internal Server Error) if the questionAnswer couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/question-answers")
    @Timed
    public ResponseEntity<QuestionAnswer> updateQuestionAnswer(@RequestBody QuestionAnswer questionAnswer) throws URISyntaxException {
        log.debug("REST request to update QuestionAnswer : {}", questionAnswer);
        if (questionAnswer.getId() == null) {
            return createQuestionAnswer(questionAnswer);
        }
        QuestionAnswer result = questionAnswerService.save(questionAnswer);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, questionAnswer.getId().toString()))
            .body(result);
    }

    /**
     * GET  /question-answers : get all the questionAnswers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of questionAnswers in body
     */
    @GetMapping("/question-answers")
    @Timed
    public ResponseEntity<List<QuestionAnswer>> getAllQuestionAnswers(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of QuestionAnswers");
        Page<QuestionAnswer> page = questionAnswerService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/question-answers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /question-answers/:id : get the "id" questionAnswer.
     *
     * @param id the id of the questionAnswer to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the questionAnswer, or with status 404 (Not Found)
     */
    @GetMapping("/question-answers/{id}")
    @Timed
    public ResponseEntity<QuestionAnswer> getQuestionAnswer(@PathVariable Long id) {
        log.debug("REST request to get QuestionAnswer : {}", id);
        QuestionAnswer questionAnswer = questionAnswerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(questionAnswer));
    }

    /**
     * DELETE  /question-answers/:id : delete the "id" questionAnswer.
     *
     * @param id the id of the questionAnswer to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/question-answers/{id}")
    @Timed
    public ResponseEntity<Void> deleteQuestionAnswer(@PathVariable Long id) {
        log.debug("REST request to delete QuestionAnswer : {}", id);
        questionAnswerService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
