package com.mycompany.myapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.MaturityStage;
import com.mycompany.myapp.service.MaturityStageService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing MaturityStage.
 */
@RestController
@RequestMapping("/api")
public class MaturityStageResource {

    private final Logger log = LoggerFactory.getLogger(MaturityStageResource.class);

    private static final String ENTITY_NAME = "maturityStage";

    private final MaturityStageService maturityStageService;

    public MaturityStageResource(MaturityStageService maturityStageService) {
        this.maturityStageService = maturityStageService;
    }

    /**
     * POST  /maturity-stages : Create a new maturityStage.
     *
     * @param maturityStage the maturityStage to create
     * @return the ResponseEntity with status 201 (Created) and with body the new maturityStage, or with status 400 (Bad Request) if the maturityStage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/maturity-stages")
    @Timed
    public ResponseEntity<MaturityStage> createMaturityStage(@Valid @RequestBody MaturityStage maturityStage) throws URISyntaxException {
        log.debug("REST request to save MaturityStage : {}", maturityStage);
        if (maturityStage.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new maturityStage cannot already have an ID")).body(null);
        }
        MaturityStage result = maturityStageService.save(maturityStage);
        return ResponseEntity.created(new URI("/api/maturity-stages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /maturity-stages : Updates an existing maturityStage.
     *
     * @param maturityStage the maturityStage to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated maturityStage,
     * or with status 400 (Bad Request) if the maturityStage is not valid,
     * or with status 500 (Internal Server Error) if the maturityStage couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/maturity-stages")
    @Timed
    public ResponseEntity<MaturityStage> updateMaturityStage(@Valid @RequestBody MaturityStage maturityStage) throws URISyntaxException {
        log.debug("REST request to update MaturityStage : {}", maturityStage);
        if (maturityStage.getId() == null) {
            return createMaturityStage(maturityStage);
        }
        MaturityStage result = maturityStageService.save(maturityStage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, maturityStage.getId().toString()))
            .body(result);
    }

    /**
     * GET  /maturity-stages : get all the maturityStages.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of maturityStages in body
     */
    @GetMapping("/maturity-stages")
    @Timed
    public ResponseEntity<List<MaturityStage>> getAllMaturityStages(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of MaturityStages");
        Page<MaturityStage> page = maturityStageService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/maturity-stages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /maturity-stages/:id : get the "id" maturityStage.
     *
     * @param id the id of the maturityStage to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the maturityStage, or with status 404 (Not Found)
     */
    @GetMapping("/maturity-stages/{id}")
    @Timed
    public ResponseEntity<MaturityStage> getMaturityStage(@PathVariable Long id) {
        log.debug("REST request to get MaturityStage : {}", id);
        MaturityStage maturityStage = maturityStageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(maturityStage));
    }

    /**
     * DELETE  /maturity-stages/:id : delete the "id" maturityStage.
     *
     * @param id the id of the maturityStage to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/maturity-stages/{id}")
    @Timed
    public ResponseEntity<Void> deleteMaturityStage(@PathVariable Long id) {
        log.debug("REST request to delete MaturityStage : {}", id);
        maturityStageService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
