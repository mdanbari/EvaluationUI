package com.mycompany.myapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.Evaluation;
import com.mycompany.myapp.service.EvaluationService;
import com.mycompany.myapp.service.dto.EvaluationDTO;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Evaluation.
 */
@RestController
@RequestMapping("/api")
public class EvaluationResource {

    private final Logger log = LoggerFactory.getLogger(EvaluationResource.class);

    private static final String ENTITY_NAME = "evaluation";

    private final EvaluationService evaluationService;

    public EvaluationResource(EvaluationService evaluationService) {
        this.evaluationService = evaluationService;
    }

    /**
     * POST  /evaluations : Create a new evaluation.
     *
     * @param evaluation the evaluation to create
     * @return the ResponseEntity with status 201 (Created) and with body the new evaluation, or with status 400 (Bad Request) if the evaluation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/evaluations")
    @Timed
    public ResponseEntity<Evaluation> createEvaluation(@RequestBody Evaluation evaluation) throws URISyntaxException {
        log.debug("REST request to save Evaluation : {}", evaluation);
        if (evaluation.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new evaluation cannot already have an ID")).body(null);
        }
        Evaluation result = evaluationService.save(evaluation);
        return ResponseEntity.created(new URI("/api/evaluations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /evaluations : Updates an existing evaluation.
     *
     * @param evaluation the evaluation to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated evaluation,
     * or with status 400 (Bad Request) if the evaluation is not valid,
     * or with status 500 (Internal Server Error) if the evaluation couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/evaluations")
    @Timed
    public ResponseEntity<Evaluation> updateEvaluation(@RequestBody Evaluation evaluation) throws URISyntaxException {
        log.debug("REST request to update Evaluation : {}", evaluation);
        if (evaluation.getId() == null) {
            return createEvaluation(evaluation);
        }
        Evaluation result = evaluationService.save(evaluation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, evaluation.getId().toString()))
            .body(result);
    }

    /**
     * GET  /evaluations : get all the evaluations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of evaluations in body
     */
    @GetMapping("/evaluations")
    @Timed
    public ResponseEntity<List<Evaluation>> getAllEvaluations(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Evaluations");
        Page<Evaluation> page = evaluationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/evaluations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /evaluations/:id : get the "id" evaluation.
     *
     * @param id the id of the evaluation to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the evaluation, or with status 404 (Not Found)
     */
    @GetMapping("/evaluations/{id}")
    @Timed
    public ResponseEntity<Evaluation> getEvaluation(@PathVariable Long id) {
        log.debug("REST request to get Evaluation : {}", id);
        Evaluation evaluation = evaluationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(evaluation));
    }

    /**
     * DELETE  /evaluations/:id : delete the "id" evaluation.
     *
     * @param id the id of the evaluation to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/evaluations/{id}")
    @Timed
    public ResponseEntity<Void> deleteEvaluation(@PathVariable Long id) {
        log.debug("REST request to delete Evaluation : {}", id);
        evaluationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
	@GetMapping("/myevaluations")
	@Timed
	public ResponseEntity<List<Evaluation>> getAllMyEvaluations(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Current User Evaluations");
		Page<Evaluation> page = evaluationService.findAllMyEvaluation(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/myevaluations");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
	
	@GetMapping("/myevaluations/{id}")
	@Timed
	public ResponseEntity<EvaluationDTO> getMyEvaluation(@PathVariable Long id) {
		log.debug("REST request to get a detail of Current User Evaluation : {} " , id);
		EvaluationDTO evaluation = evaluationService.findMyEvaluation(id);
	
		return new ResponseEntity<>(evaluation, HttpStatus.OK);
	}

	@GetMapping("/getRandomEvaluation")
	@Timed
	public ResponseEntity<EvaluationDTO> getRandomEvaluation() {
		log.debug("REST request to get Random Evaluation : {}");
		EvaluationDTO evaluationDTO = evaluationService.getRandomEvaluation();
		return new ResponseEntity<>(evaluationDTO, HttpStatus.OK);
	}

	@GetMapping("/getCurrentEvaluation")
	@Timed
	public ResponseEntity<EvaluationDTO> getCurrentEvaluation() {
		log.debug("REST request to get Random Evaluation : {}");
		EvaluationDTO evaluationDTO = evaluationService.getCurrentEvaluation();
		return new ResponseEntity<>(evaluationDTO, HttpStatus.OK);
	}

	@GetMapping("/getCurrentGeneralEvaluation")
	@Timed
	public ResponseEntity<EvaluationDTO> getCurrentGeneralEvaluation() {
		log.debug("REST request to get Random Evaluation : {}");
		EvaluationDTO evaluationDTO = evaluationService.getCurrentGeneralEvaluation();
		return new ResponseEntity<>(evaluationDTO, HttpStatus.OK);
	}
	
	
	@GetMapping("/complete-evaluations")
	@Timed
	public ResponseEntity<List<Evaluation>> getAllCompleteEvaluations(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Complete Evaluations");
		Page<Evaluation> page = evaluationService.findAllCompleteEvaluation(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/complete-evaluations");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/my-expert-evaluations")
	@Timed
	public ResponseEntity<List<Evaluation>> getAllMyCompleteEvaluations(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Complete Evaluations");
		Page<Evaluation> page = evaluationService.findAllMyCompleteEvaluation(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/complete-evaluations");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/my-expert-evaluations/{id}")
	@Timed
	public ResponseEntity<EvaluationDTO> getAllMyCompleteEvaluations(@PathVariable Long id) {
		log.debug("REST request to get Evaluation for expert");
		EvaluationDTO evaluationDTO = evaluationService.findMyEvaluation(id);
		return new ResponseEntity<>(evaluationDTO, HttpStatus.OK);
	}	
	
	@GetMapping("/confirmed-evaluations")
	@Timed
	public ResponseEntity<List<Evaluation>> getAllConfirmeEvaluations(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Complete Evaluations");
		Page<Evaluation> page = evaluationService.findAllConfirmedEvaluation(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/complete-evaluations");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}


	@GetMapping("/getEvaluationGeneral/{id}")
	@Timed
	public ResponseEntity<EvaluationDTO> getEvaluationGeneral(@PathVariable Long id) {
		log.debug("REST request to get Evaluation for general");
		EvaluationDTO evaluationDTO = evaluationService.getEvaluationGeneral(id);
		return new ResponseEntity<>(evaluationDTO, HttpStatus.OK);
	}
	
	@GetMapping("/complete-evaluations/{id}")
	@Timed
	public ResponseEntity<EvaluationDTO> getCompleteEvaluation(@PathVariable Long id) {
		log.debug("REST request to get a detail of Current User Evaluation : {} ", id);
		EvaluationDTO evaluation = evaluationService.findMyEvaluation(id);

		return new ResponseEntity<>(evaluation, HttpStatus.OK);
	}

	@GetMapping("/answeringEvaluation/{id}")
	@Timed
	public ResponseEntity<Void> answeringEvaluation(@PathVariable Long id){
		log.debug("REST request to get a answeringEvaluation: {} ", id);
		evaluationService.answeringEvaluation(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	

	@PostMapping("/checkingEvaluation")
	@Timed
	public ResponseEntity<Void> checkingEvaluation(@RequestBody EvaluationDTO evaluationDTO){
		log.debug("REST request to post a checkingEvaluation: {} ", evaluationDTO);
		evaluationService.checkingEvaluation(evaluationDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping("/temporarySaveEvaluation")
	@Timed
	public ResponseEntity<Void> temporarySaveEvaluation(@RequestBody EvaluationDTO evaluationDTO){
		log.debug("REST request to post a temporarySaveEvaluation: {} ", evaluationDTO);
		evaluationService.temporarySaveEvaluation(evaluationDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	
	
	
}

