package com.mycompany.myapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.Department;
import com.mycompany.myapp.service.DepartmentService;
import com.mycompany.myapp.service.dto.ChildrenDepartment;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Department.
 */
@RestController
@RequestMapping("/api")
public class DepartmentResource {

	private final Logger log = LoggerFactory.getLogger(DepartmentResource.class);

	private static final String ENTITY_NAME = "department";

	private final DepartmentService departmentService;

	public DepartmentResource(DepartmentService departmentService) {
		this.departmentService = departmentService;
	}

	/**
	 * POST /departments : Create a new department.
	 *
	 * @param department
	 *            the department to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new department, or with status 400 (Bad Request) if the
	 *         department has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/departments")
	@Timed
	public ResponseEntity<Department> createDepartment(@Valid @RequestBody Department department)
			throws URISyntaxException {
		log.debug("REST request to save Department : {}", department);
		if (department.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new department cannot already have an ID")).body(null);
		}
		Department result = departmentService.save(department);
		return ResponseEntity.created(new URI("/api/departments/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /departments : Updates an existing department.
	 *
	 * @param department
	 *            the department to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         department, or with status 400 (Bad Request) if the department is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         department couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/departments")
	@Timed
	public ResponseEntity<Department> updateDepartment(@Valid @RequestBody Department department)
			throws URISyntaxException {
		log.debug("REST request to update Department : {}", department);
		if (department.getId() == null) {
			return createDepartment(department);
		}
		Department result = departmentService.save(department);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, department.getId().toString())).body(result);
	}

	/**
	 * GET /departments : get all the departments.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         departments in body
	 */
	@GetMapping("/departments")
	@Timed
	public ResponseEntity<List<Department>> getAllDepartments(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Departments");
		Page<Department> page = departmentService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/departments");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /departments/:id : get the "id" department.
	 *
	 * @param id
	 *            the id of the department to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         department, or with status 404 (Not Found)
	 */
	@GetMapping("/departments/{id}")
	@Timed
	public ResponseEntity<Department> getDepartment(@PathVariable Long id) {
		log.debug("REST request to get Department : {}", id);
		Department department = departmentService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(department));
	}

	/**
	 * DELETE /departments/:id : delete the "id" department.
	 *
	 * @param id
	 *            the id of the department to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/departments/{id}")
	@Timed
	public ResponseEntity<Void> deleteDepartment(@PathVariable Long id) {
		log.debug("REST request to delete Department : {}", id);
		departmentService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * @return
	 */
	@GetMapping("/departmentsparentisnull")
	@Timed
	public ResponseEntity<List<ChildrenDepartment>> getAllDepartmentsParentIsNull() {
		log.debug("REST request to get Departments parent is Null");
		return new ResponseEntity<>(departmentService.findAllByParentIsNull(), HttpStatus.OK);
	}

	/**
	 * @param id
	 * @return
	 */
	@GetMapping("/findallchildrenofdepartment/{id}")
	@Timed
	public ResponseEntity<List<ChildrenDepartment>> findAllChildrenOfDepartment(@PathVariable Long id) {
		log.debug("REST request to children of Department : {}", id);
		return new ResponseEntity<>(departmentService.findAllChildrenOfDepartment(id), HttpStatus.OK);
	}

	/**
	 * @param id
	 * @return
	 */
	@GetMapping("/findCountAllChildrenOfDepartment/{id}")
	@Timed
	public ResponseEntity<Long> findCountAllChildrenOfDepartment(@PathVariable Long id) {
		log.debug("REST request to count children of Department : {}", id);
		return new ResponseEntity<>(departmentService.findCountAllChildrenOfDepartment(id), HttpStatus.OK);
	}
	
	
	@GetMapping("/department/search/{search}/{local}")
	@Timed
	public ResponseEntity<List<Department>> search(@PathVariable String search,@PathVariable String local, @ApiParam Pageable pageable) {
		log.debug("REST request to search a page of Departments");
		Page<Department> page = departmentService.search(search ,local, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "api/department/search/");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

}
