package com.mycompany.myapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.UnMaturityModel;
import com.mycompany.myapp.service.UnMaturityModelService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing UnMaturityModel.
 */
@RestController
@RequestMapping("/api")
public class UnMaturityModelResource {

    private final Logger log = LoggerFactory.getLogger(UnMaturityModelResource.class);

    private static final String ENTITY_NAME = "unMaturityModel";

    private final UnMaturityModelService unMaturityModelService;

    public UnMaturityModelResource(UnMaturityModelService unMaturityModelService) {
        this.unMaturityModelService = unMaturityModelService;
    }

    /**
     * POST  /un-maturity-models : Create a new unMaturityModel.
     *
     * @param unMaturityModel the unMaturityModel to create
     * @return the ResponseEntity with status 201 (Created) and with body the new unMaturityModel, or with status 400 (Bad Request) if the unMaturityModel has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/un-maturity-models")
    @Timed
    public ResponseEntity<UnMaturityModel> createUnMaturityModel(@Valid @RequestBody UnMaturityModel unMaturityModel) throws URISyntaxException {
        log.debug("REST request to save UnMaturityModel : {}", unMaturityModel);
        if (unMaturityModel.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new unMaturityModel cannot already have an ID")).body(null);
        }
        UnMaturityModel result = unMaturityModelService.save(unMaturityModel);
        return ResponseEntity.created(new URI("/api/un-maturity-models/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /un-maturity-models : Updates an existing unMaturityModel.
     *
     * @param unMaturityModel the unMaturityModel to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated unMaturityModel,
     * or with status 400 (Bad Request) if the unMaturityModel is not valid,
     * or with status 500 (Internal Server Error) if the unMaturityModel couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/un-maturity-models")
    @Timed
    public ResponseEntity<UnMaturityModel> updateUnMaturityModel(@Valid @RequestBody UnMaturityModel unMaturityModel) throws URISyntaxException {
        log.debug("REST request to update UnMaturityModel : {}", unMaturityModel);
        if (unMaturityModel.getId() == null) {
            return createUnMaturityModel(unMaturityModel);
        }
        UnMaturityModel result = unMaturityModelService.save(unMaturityModel);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, unMaturityModel.getId().toString()))
            .body(result);
    }

    /**
     * GET  /un-maturity-models : get all the unMaturityModels.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of unMaturityModels in body
     */
    @GetMapping("/un-maturity-models")
    @Timed
    public ResponseEntity<List<UnMaturityModel>> getAllUnMaturityModels(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of UnMaturityModels");
        Page<UnMaturityModel> page = unMaturityModelService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/un-maturity-models");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /un-maturity-models/:id : get the "id" unMaturityModel.
     *
     * @param id the id of the unMaturityModel to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the unMaturityModel, or with status 404 (Not Found)
     */
    @GetMapping("/un-maturity-models/{id}")
    @Timed
    public ResponseEntity<UnMaturityModel> getUnMaturityModel(@PathVariable Long id) {
        log.debug("REST request to get UnMaturityModel : {}", id);
        UnMaturityModel unMaturityModel = unMaturityModelService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(unMaturityModel));
    }

    /**
     * DELETE  /un-maturity-models/:id : delete the "id" unMaturityModel.
     *
     * @param id the id of the unMaturityModel to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/un-maturity-models/{id}")
    @Timed
    public ResponseEntity<Void> deleteUnMaturityModel(@PathVariable Long id) {
        log.debug("REST request to delete UnMaturityModel : {}", id);
        unMaturityModelService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
